<?php

class KaidController extends Controller {
    public $layout='//layouts/admin';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('dovtalab','listgroup','ruihat',
                    'group','upddovtalab','updgroup','Addgroup',
                    'ihtisos','icreate','updateIhtisos','nozir'),
                'roles'=>array('4'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionDovtalab(){

        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('login/index'));

        $model=new User;
        if(isset($_POST['User'])){
            $num=rand(10000,99999);
            $login='st'.$num;
            $password=md5($login);
                $model->attributes=$_POST['User'];
            $model->login=$login;
            $model->password=$password;
            $model->avatar="/dist/img/user6-128x128.jpg";
            $model->datecreate=new CDbExpression('NOW()');
            $model->lastupdate=new CDbExpression('NOW()');
            $model->accessId=1;
            $model->status=1;
            if($model->validate()){

                if($model->save())
                    Yii::app()->user->setFlash('success', 'Довталаб бо муваффақият дохил карда шуд!');
                    $this->refresh();
                }

        }


        $this->render('dovtalab',array('model'=>$model));
        
    }

    public function actionRuihat(){

        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('login/index'));

        $model=new User;
        $this->render('ruihat',array('model'=>$model));
        
    }

    public function actionGroup($id=null){

        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('login/index'));
        if(!is_null($id)) $model=$this->loadGroup($id);

        $model=new Group;
        if(isset($_POST['Group'])){
            $model->attributes=$_POST['Group'];
            $model->datecreate=new CDbExpression('NOW()');
            $model->lastupdate=new CDbExpression('NOW()');
            $model->status=1;
            if($model->validate() && $model->save()){
                Yii::app()->user->setFlash('success', 'Гурӯҳ бо муваффақият дохил карда шуд!');
                $this->refresh();
            }
        }
        $this->render('group',array('model'=>$model));
        
    }

    public function actionListgroup(){

        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('login/index'));

        $this->render('listgroup');
        
    }

    public function actionUpddovtalab($id){
    $model=$this->loadUser($id);

        if(isset($_POST['User'])){
            $model->attributes=$_POST['User'];
            if($model->validate()){

                if($model->update()){
                    Yii::app()->user->setFlash('success', 'Довталаб бо муваффақият азнав карда шуд!');
                    $this->redirect(array('kaid/dovtalab'));
                }

            }


        }else{
            $this->render('dovtalab',array('model'=>$model));
        }

    }

    public function actionUpdgroup($id){

        $model=$this->loadGroup($id);

        if(isset($_POST['Group'])){
            $model->attributes=$_POST['Group'];
            $model->lastupdate=new CDbExpression('NOW()');

            if($model->validate() && $model->update()){
                Yii::app()->user->setFlash('success', 'Гурӯҳ бо муваффақият азнав карда шуд!');
                $this->redirect(array('kaid/group'));
            }
        }else{
            $this->render('group',array('model'=>$model));
        }

    }

    public function actionAddgroup(){
        if(isset($_POST['uId']) || isset($_POST['gId'])){
           $uId=intval($_POST['uId']);
           $gId=intval($_POST['gId']);
            $gropuin= new Groupin;
            $gropuin->groupId=$gId;
            $gropuin->userId=$uId;
            $gropuin->datecreate=new CDbExpression('NOW()');
            $gropuin->status=1;
            if($gropuin->save())
                echo 1;
        }
    }

    public function actionIhtisos(){

        $model=Ihtisos::model()->findAll();

        $this->render('ihtisos',array('model'=>$model));
    }

    public function actionIcreate()
    {
        $model=new Ihtisos;

        // Uncomment the following line if AJAX validation is needed
       // $this->performAjaxValidation($model,'test-form');

        if(isset($_POST['Ihtisos']))
        {
            $model->attributes=$_POST['Ihtisos'];
            $model->datecreate=new CDbExpression('NOW()');
            $model->status=1;
            if($model->validate()){

                if($model->save())
                    Yii::app()->user->setFlash('success', 'Довталаб бо муваффақият дохил карда шуд!');
                $this->redirect(array('kaid/ihtisos'));
            }
        }

        $this->render('ihtisos/create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdateIhtisos($id)
    {
        $model=$this->loadModel($id);


        //$this->performAjaxValidation($model,'test-form');

        if(isset($_POST['Ihtisos']))
        {
            $model->attributes=$_POST['Ihtisos'];
            if($model->validate() && $model->save())
                $this->redirect(array('kaid/ihtisos'));
        }

        $this->render('ihtisos/update',array(
            'model'=>$model,
        ));
    }

    public function loadUser($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function loadGroup($id)
    {
        $model=Group::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model,$form)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===$form)
        {
            echo CActiveForm::validate($model);
        }
    }
    public function loadModel($id)
    {
        $model=Ihtisos::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}

?>
