<?php

class SiteController extends Controller
{
    public function filters()
    {
        return array(
            array(
                'COutputCache',
                'duration'=>3600*24*365,
            ),
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login','index'),
				'roles'=>array('guest'),
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('send','compose','read','inbox','chat','checklogin','ajaxRequest','online','inboxu','post','pass','gotomessage','profile'),
				'roles'=>array('2','3','4','5','6','7','8'),
			),

			array('allow', // allow authenticated user
				'actions'=>array('user','current','online','pass','profile','discurr','group','dashed','transcript','dislast',
                    'error','change','showcontent','chat','logout','checklogin','ajaxRequest'),
				'roles'=>array('1'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionNot(){

    }
    public function actionShowcontent(){
        $chat=Chat::model()->findAll(array('select'=>'reciverId,senderId,id','condition'=>'reciverId='.Yii::app()->user->id,'group'=>'reciverId'));
        $this->renderPartial('chat/_showContent',array('chat'=>$chat));
    }

    public function actionChat($id=null){
        $chat=Chat::model()->findAll(array('select'=>'reciverId,senderId,id','condition'=>'reciverId='.Yii::app()->user->id,'group'=>'reciverId,senderId'));

        if(isset($_GET['id'])){
            $id=(int)$_GET['id'];
            $chatMessageId=Chat::model()->findByPk($id);
            $checkLoginExists=User::model()->findByPk($chatMessageId->senderId);
            if(!empty($chatMessageId))
                $checkMessageExists=Chat::model()->findAll(array('condition'=>'reciverId='.$chatMessageId->senderId.'
                                                                and senderId='.Yii::app()->user->id.'
                                                                or reciverId='.Yii::app()->user->id.' and senderId='.$chatMessageId->senderId,'order'=>'sendTime ASC'));

            if (!empty($checkLoginExists)) {
                    if(!empty($checkMessageExists))
                        $this->render('chat/__chat', array('model' => $checkLoginExists,'chat'=>$chat,'messages'=>$checkMessageExists));
            }
        }else
        $this->render('chat',array('chat'=>$chat));
    }

    public function actionPost()
    {
        if (isset($_POST['text']) || isset($_POST['recId'])) {
            $reciverId = $_POST['recId'];
            $text = mysql_real_escape_string($_POST['text']);
            $model = new Chat;
            $model->reciverId = $reciverId;
            $model->senderId = Yii::app()->user->id;
            $model->text = $text;
            $model->viewReciver = 0;
            $model->statusSender = 1;
            $model->statusReciver = 1;
            $model->sendTime = new CDbExpression('NOW()');
            $model->status = 1;
            if ($model->insert()) {
                echo 1;
            }
        }
    }

    public function actionAjaxRequest()
    {
        if (isset($_GET['checkId'])) {
            $checkId=$_GET['checkId'];
            if(!empty($checkId))
            $checkMessageExists = Chat::model()->findAll(array('condition' => 'reciverId=' . $checkId . '
                                                                and senderId=' . Yii::app()->user->id . '
                                                                or reciverId=' . Yii::app()->user->id . ' and senderId=' .$checkId,'order'=>'sendTime ASC'));


            if (!empty($checkMessageExists))
                $this->renderPartial('chat/_ajaxRequest', array( 'messages' => $checkMessageExists));

        }

    }

    public function actionGotomessage($id){
        if(isset($_GET['id'])){
            $id=(int)$_GET['id'];
            $chatMessageId=Chat::model()->findByPk($id);
            $checkLoginExists=User::model()->findByPk($chatMessageId->senderId);
            if(!empty($chatMessageId))
                $checkMessageExists=Chat::model()->findAll(array('condition'=>'reciverId='.$chatMessageId->senderId.'
                                                                and senderId='.Yii::app()->user->id.'
                                                                or reciverId='.Yii::app()->user->id.' and senderId='.$chatMessageId->senderId,'order'=>'sendTime ASC'));

            if (!empty($checkLoginExists)) {
                if ($checkLoginExists->id == Yii::app()->user->id)
                    return Users::Alert('warning','Шумо наметавонед бо худ чат созед!');
                else
                    if(!empty($checkMessageExists))
                        $this->renderPartial('chat/_chat', array('model' => $checkLoginExists,'messages'=>$checkMessageExists));
                    else
                        $this->renderPartial('chat/_chat', array('model' => $checkLoginExists));
            } else {
                return Users::Alert('danger','Ин хел чат мавҷуд нест!');
            }
            Yii::app()->end();
        }
    }

    public function actionChecklogin($login)
    {
        if ($login!="") {
            $login = mysql_real_escape_string($login);
            $checkLoginExists = User::model()->find(
                array('condition' => 'login="' . $login . '"')
            );
            if(!empty($checkLoginExists))
            $checkMessageExists=Chat::model()->findAll(array('condition'=>'reciverId='.$checkLoginExists->id.'
                                                                and senderId='.Yii::app()->user->id.'
                                                                or reciverId='.Yii::app()->user->id.' and senderId='.$checkLoginExists->id,'order'=>'sendTime ASC'));

            if (!empty($checkLoginExists)) {
                if ($checkLoginExists->id == Yii::app()->user->id)
                    return Users::Alert('warning','Шумо наметавонед бо худ чат созед!');
                else
                    if(!empty($checkMessageExists))
                        $this->renderPartial('chat/_chat', array('model' => $checkLoginExists,'messages'=>$checkMessageExists));
                    else
                        $this->renderPartial('chat/_chat', array('model' => $checkLoginExists));
            } else {
                return Users::Alert('danger','Ин хел истифодабаранда мавҷуд нест!');
            }

            Yii::app()->end();
        }

    }

        public function actionChange(){
            if(isset($_POST['id']) && isset($_POST['id'])!=""){
                if(is_numeric($_POST['id'])){
                    $id=(int)$_POST['id'];
                    $loadMailbox=Mailbox::model()->findAllByPk($id);
                    if($loadMailbox->notyReciver==0){
                        Mailbox::model()->updateByPk($id,array('notyReciver'=>1));
                    }
                    else {Mailbox::model()->updateByPk($id,array('notyReciver'=>0));}
                }
            }
        }

    public function actionSend(){
        $this->render('mailbox/send');
    }
    public function actionCompose($id=null){

        $model=new Mailbox;

        //Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model);

        if(isset($_POST['Mailbox']))
        {
            $userId=Users::returnUserId(mysql_real_escape_string($_POST['Mailbox']['reciverId']));
            $model->attributes=$_POST['Mailbox'];
            $model->filename=CUploadedFile::getInstance($model,'upload');
            $model->senderId=Yii::app()->user->id;
            if(empty($userId)){
                Yii::app()->user->setFlash('warning', 'Чунин истифодабаранда дар система мавҷуд нест!');
               $this->refresh();
            }else
            $model->reciverId=$userId;

            $model->statusSender=2;
            $model->statusReciver=2;
            $model->notyReciver=0;
            $model->viewReciver=0;
            $model->id2=rand(1000000,9999999);
            $model->datecreate=new CDbExpression('NOW()');
            $model->status=1;
            if($model->validate() && $model->save()){
                //$path=;
               // $model->filename->saveAs(Yii::app()->baseUrl.'/protected/data/'.$model->upload->getName());
                Yii::app()->user->setFlash('success', 'Мактуб ба истифодабаранда равона карда шуд!');
                $this->redirect(array('site/send'));
            }

        }

        $this->render('mailbox/compose',array('model'=>$model));
    }
    public function actionInbox(){
        $this->render('mailbox/inbox');
    }
    public function actionInboxu(){
        $this->renderPartial('mailbox/inboxu');
    }


	public function actionRead($id){

		$model=$this->loadMailbox($id);
        if($model->reciverId==Yii::app()->user->id){
            $update=Mailbox::model()->updateByPk($model->id,array('viewReciver'=>1));
        }
        $this->render('mailbox/read',array('model'=>$model));
	}
	
    public function actionIndex()
    {

        $this->redirect(array('login/index'));
    }

    public function actionLogin()
    {

        $this->redirect(array('login/index'));
    }

    public function actionPass()
    {
        if (isset($_POST['psw']) || isset($_POST['psw1']) || isset($_POST['psw2'])) {

            if (isset($_POST['psw']) != "" || isset($_POST['psw1']) != "" || isset($_POST['psw2']) != "") {

                $psw1 = mysql_real_escape_string($_POST['psw1']);
                $psw2 = mysql_real_escape_string($_POST['psw2']);
                //if(strcmp($psw1,$psw2)==0){
                $password = md5(mysql_real_escape_string($_POST['psw']));
                $newpassw = md5(mysql_real_escape_string($_POST['psw1']));
                $idUser = Yii::app()->session['user']['id'];
                $search = User::model()->find(array('condition' => 'id=' . $idUser . ' and password="' . $password . '"'));
                if (count($search) > 0) {

                    $update = User::model()->updateByPk($idUser,
                        array('password' => $newpassw));
                    if ($update) {
                        echo 3;
                    }
                } else {
                    echo 2;
                }
            } else {

                echo 1;
            }

        }
    }

    public function actionOnline(){

        $online=Visits::model()->findAll(array('condition'=>'not id_user='.Yii::app()->user->id.' and status=1'));


        $this->render('online',array('online'=>$online));
    }

    public function actionUser()
    {
        if (Yii::app()->session['user']['accessId'] == 1) {
            $groupin = Groupin::model()->find(array('condition' => 'userId=' . Yii::app()->session['user']['id']));
            if (empty($groupin)) {
                $this->actionLogout();
            }
            $alerts = Alert::model()->findAll(array('condition' => 'groupinId=' . $groupin->idGroupin . ' and status=1'));
            //if(empty($alerts))
            $this->render('index', array('alerts' => $alerts, 'groupin' => $groupin));
        } else {
            $this->render('index');
        }
    }

    public function actionProfile()
    {
        $user=User::model()->findbyPk(Yii::app()->user->id);

        $email=new EmailForm;

        $model=new PasswordForm;

        if(isset($_POST['PasswordForm']))
        {
            $model->attributes=$_POST['PasswordForm'];
            if($model->validate())
            {
                Yii::app()->user->setFlash('password', 'Парол бо муваффакият иваз карда шуд.');
                User::model()->updateByPk(Yii::app()->user->id,array('password'=>md5($model->new_password),'lastupdate'=>new CDbExpression('NOW()')));
                $this->refresh();
            }
            // do something
        }

        $this->ajaxValidate($email,'email-form');

        if(isset($_POST['EmailForm']))
        {
            $email->attributes=$_POST['EmailForm'];
            if($email->validate())
            {
                Yii::app()->user->setFlash('email', 'Почтаи электронӣ сабт карда шуд.');
                User::model()->updateByPk(Yii::app()->user->id,array('email'=>strtolower($email->email),'lastupdate'=>new CDbExpression('NOW()')));
                $this->refresh();
            }
            // do something
        }


        $this->render('profile',array('model'=>$model,'user'=>$user,'email'=>$email));
    }

    public function actionInfo()
    {
        $this->render('info');
    }

    public function actionDiscurr()
    {
        $this->render('student/absents/absents');
    }

    public function actionCurrent()
    {
        $this->render('student/bal/current');
    }

    public function actionGroup($id)
    {
        $decode = intval($id);
        $shedual = $this->loadShedual(Users::getDecode($decode));
        $subject = $this->loadSubject($shedual->subjectId);

        $this->render('student/bal/group', array('shedual' => $shedual, 'subject' => $subject));
    }

    public function loadScores($id)
    {
        $model = Scores::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function actionDashed()
    {

        $this->render('student/bal/all');
    }

    public function actionTranscript()
    {
        $this->render('student/bal/transcript');
    }

    public function actionDislast()
    {
        $this->render('student/absents/all');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->layout = 'column1';
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('login/index'));

        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */

    public function loadShedual($id)
    {
        $model = Shedual::model()->findByPk($id, array('condition' => 'status=1'));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadSubject($id)
    {
        $model = Subject::model()->findByPk($id, array('condition' => 'status=1'));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


    public function loadUser($id)
    {
        $model = User::model()->findByAttributes(array('login' => $id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function loadMailbox($id)
    {
        $model = Mailbox::model()->findByAttributes(array('id2'=>$id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionLogout()
    {
        Visits::model()->updateAll(array('status'=>0),'id_user='.Yii::app()->user->id);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createUrl('login/index'));
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='mailbox-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function ajaxValidate($model,$form)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===$form)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}