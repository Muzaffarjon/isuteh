<?php

class BugiController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('students','karzdoron','mablag'),
				'roles'=>array('9'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionStudents()
	{
		$this->render('students');
	}
    public function actionKarzdoron()
	{
		$this->render('karzdoron');
	}
    public function actionMablag()
	{
		$this->render('mablag');
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tab-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
