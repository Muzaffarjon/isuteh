<?php

class LoginController extends Controller
{
    public $layout = '//layouts/column1';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function actionIndex()
    {
        // if (!Yii::app()->user->isGuest)
        //                $this->redirect(Yii::app()->createUrl('site/user'));
        $model = new LoginForm;

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid

            $model->scenario = 'captchaRequired';

            if ($model->validate()) {
                $model->login();
                $count = Visits::model()->find('id_user=?', array(Yii::app()->user->id));
                if (empty($count)) {
                    $visit = new Visits;
                    $visit->id_user = Yii::app()->user->id;
                    $visit->sessionId = session_id();
                    $visit->browser = $_SERVER['HTTP_USER_AGENT'];
                    $visit->ip = $_SERVER['REMOTE_ADDR'];
                    $visit->visit_time = new CDbExpression('NOW()');
                    $visit->last_action = new CDbExpression('NOW()');
                    $visit->count_auth = 1;
                    $visit->status = 1;
                    $visit->save();
                } else {
                    $userVisit = Visits::model()->find('id_user=:uid', array('uid' => Yii::app()->user->id));
                    Visits::model()->updateByPk($userVisit->id, array('last_action' => new CDbExpression('NOW()'), 'datecreate' => new CDbExpression('NOW()'), 'sessionId' => session_id(), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'], 'status' => 1));
                }
                if ($model->checkStatus() == 0)
                    $this->redirect(Yii::app()->createUrl('login/lock'));
                else
                    $this->redirect(Yii::app()->createUrl('site/user'));
            }

        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionLock()
    {
        $this->render('lock');

    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createUrl('login/index'));
    }

    public function actionForgetPassword()
    {

        $model = new RestorePassword();
        $activate="asdaSdcvw5158SAd2822SAd";
        if (isset($_POST['RestorePassword'])) {
            $model->attributes = $_POST['RestorePassword'];
            if ($model->validate()) {
                $user = Users::searchByEmail($model->username);

                if (!empty($user)) {
                    $code=md5($user->email.time().rand(100000, 999999));

                    /* Для деактивации предидущих кодов */

                    Users::setInActiveCodes($user->id);

                    /* Высилает новых код */

                    Users::saveResetMail($user->id,$code);

                    $send = Users::sendMail($user->email, $user->fname . ' ' . $user->name . ' ' . $user->lname,$code);
                    if ($send==1) {
                        Yii::app()->user->setFlash('email', 'Ба почтаи <span style="font-size: 16px;"><i><b>' . $model->username . '</b></i></span> бо муваффакият мактуб равона карда шуд. Агар дар папкаи Входящий мактубро надида бошед, пас папкаи Спамро бинед');
                        $this->refresh();
                    }else{
                        Yii::app()->user->setFlash('email', 'Хатогӣ!');
                    }
                }

            }
        }
        $this->render('forget', array('model' => $model));
    }

    public function actionRestore($code){
        $model=new RestoreForm;
       $user=Users::checkCode($code);
        if(!empty($user)){
            if(isset($_POST['RestoreForm'])){
                $model->attributes=$_POST['RestoreForm'];
                if($model->validate()){
                    $update=Users::updateUserPassword($user->id_user,$model->new_password);
                    if($update){
                    Users::setInActiveCodes($user->id_user);
                    Yii::app()->user->setFlash('restore','Парол бо муваффақият иваз карда шуд!');
                    $this->redirect(array('login/index'));
                    }
                }
            }


            $this->render('restore',array('model'=>$model));

        }else{
            $this->render('restore_failed');
        }
    }

    public function actionRepair(){
        $this->render('repair');
    }
}