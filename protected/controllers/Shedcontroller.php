<?php

class ShedController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('fann','fannho','jadval','jadvaliguzawta','insert','cafedra','sector','insertfannho','insertfann','insertsector','insertcafedra'),
                'roles'=>array('7'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionFann()
    {

        $fann = Subject::model()->findAll();

        $this->render('fann', array('subject' => $fann));

    }

    public function actionFannho()
    {

        $fann = Subjects::model()->findAll();

        $this->render('fannho', array('subject' => $fann));

    }

    public function actionMuallimon()
    {

        $muallim = User::model()->findAll(array(
            'condition' => 'accessId=2'
        ));

        $this->render('muallim', array('muallim' => $muallim));

    }

    public function actionJadval()
    {

        $shedual = Shedual::model()->findAll(array('condition' => 'status=1', 'order' => 'cikl ASC'));

        $this->render('jadval/jadval', array('shedual' => $shedual));

    }

    public function actionJadvaliguzawta()
    {

        $shedual = Shedual::model()->findAll(array('condition' => 'status=0'));

        $this->render('jadval/guzawta', array('shedual' => $shedual));

    }

    public function actionMuallimaho(){
        $this->render('muallimaho');
    }


/* ACTION CRUD */
    public function actionInsert()
    {
        $model = new Shedual;

        $this->performAjaxValidation($model);

        if (isset($_POST['Shedual'])) {
            $model->attributes = $_POST['Shedual'];
            $model->datecreate=new CDbExpression('NOW()');
            $model->lastupdate=new CDbExpression('NOW()');
            $model->status=1;
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', 'Гурӯҳ бо муваффақият дохил карда шуд!');
                    $this->redirect(array('shed/jadval'));
                }
            }
        }

        $this->render('create/create', array('model' => $model));
    }

    public function actionUpdate($id = null)
    {
        $shedual = Shedual::model()->findByPk($id);

        $this->renderPartial('create/_update', array('model' => $shedual));
    }

    public function actionIns()
    {
        if (isset($_POST['Shedual'])) {
            if (isset($_POST['Shedual']['groupId']) != "" ||
                isset($_POST['Shedual']['subjectId']) != "" ||
                isset($_POST['Shedual']['cikl']) != "" ||
                isset($_POST['Shedual']['semestr']) != "" ||
                isset($_POST['Shedual']['dateStart']) != "" ||
                isset($_POST['Shedual']['dateEnd']) != "" ||
                isset($_POST['Shedual']['dateRest']) != ""
            ) {
                $groupId = intval($_POST['Shedual']['groupId']);
                $subjectId = intval($_POST['Shedual']['subjectId']);
                $cikl = intval($_POST['Shedual']['cikl']);
                $semestr = intval($_POST['Shedual']['semestr']);
                $dateStart = $_POST['Shedual']['dateStart'];
                $dateEnd = $_POST['Shedual']['dateEnd'];
                $dateRest = $_POST['Shedual']['dateRest'];

                $ins = Users::setShedual($groupId, $subjectId, $semestr, $cikl, $dateStart, $dateEnd, $dateRest);

                echo $ins;
            } else {
                echo 0;
            }
        }
    }

    public function actionUpd()
    {
        if (isset($_POST['Shedual'])) {
            $groupId = intval($_POST['Shedual']['groupId']);
            $subjectId = intval($_POST['Shedual']['subjectId']);
            $cikl = intval($_POST['Shedual']['cikl']);
            $semestr = intval($_POST['Shedual']['semestr']);
            $dateStart = $_POST['Shedual']['dateStart'];
            $dateEnd = $_POST['Shedual']['dateEnd'];
            $dateRest = $_POST['Shedual']['dateRest'];
            $lastupdate = new CDbExpression('NOW()');
            $id = intval($_POST['id']);

            $upd = Shedual::model()->updateByPk($id, array(
                'groupId' => $groupId,
                'subjectId' => $subjectId,
                'cikl' => $cikl,
                'semestr' => $semestr,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'dateRest' => $dateRest,
                'lastupdate' => $lastupdate,
            ));
            if ($upd) {
                echo 1;
            }
        }
    }

    public function actionUpde($id)
    {
        $model = $this->loadModel($id);

        $this->renderPartial('fann/_upd', array(
            'model' => $model,
        ));
    }

    public function actionUpdet($id)
    {
        $model = $this->loadSubjects($id);

        if (isset($_POST['Subjects'])) {
            $Id = intval($id);
            $subject = $_POST['Subjects']['subject'];
            $credit = $_POST['Subjects']['credit'];
            $upd = Subjects::model()->updateByPk($Id, array(
                'subject' => $subject,
                'credit' => $credit,
                'datecreate' => new CDbExpression('NOW()')
            ));
            if ($upd) {
                echo 1;
            }
        } else


            $this->renderPartial('fann/_updf', array(
                'model' => $model,
            ));
    }


    public function actionDelete()
    {
        if (isset($_POST['uid'])) {
            $id = intval($_POST['uid']);
            if ($this->loadModel($id)->delete())
                echo 1;
            else 0;
        }

    }


    public function actionInsertfann()
    {

        $model = new Subject;
        if (isset($_POST['Subject'])) {
            $model->attributes = $_POST['Subject'];
            $model->datecreate = new CDbExpression('NOW()');
            $model->status = 1;

            if($model->validate()){

                if($model->save())
                    Yii::app()->user->setFlash('success', 'Фанни нав бо муваффақият сохта шуд!');
                $this->refresh();
            }
        }

        $this->render('fann/_form', array(
            'model' => $model,
        ));
    }

    public function actionInsertfannho()
    {

        $model = new Subjects;
        if (isset($_POST['Subjects'])) {
            $model->attributes = $_POST['Subjects'];
            $c = $this->loadCafedra(intval($_POST['Subjects']['id_cafedra']));
            $n = $this->loadNamud(intval($_POST['Subjects']['id_namud']));
            $rand = rand(10000, 99999);
            $model->scode = $c->sname . $n->snamud . $rand;
            $model->datecreate = new CDbExpression('NOW()');
            $model->status = 1;
            if ($model->save())
                echo 1;
        } else {
            $this->render('fann/_form1', array(
                'model' => $model,
            ));
        }


    }

    public function actionDel()
    {
        if (isset($_POST['uid'])) {
            $id = intval($_POST['uid']);
            if ($this->loadSubjects($id)->delete())
                echo 1;
            else 0;
        }

    }


    public function actionCafedra()
    {
        $model = new Cafedra;
        $this->render('cafedra', array('model' => $model));
    }

    public function actionInsertcafedra()
    {
        $model = new Cafedra;
        if (isset($_POST['Cafedra'])) {
            $name = trim(mysql_real_escape_string($_POST['Cafedra']['name']));
            $sname = trim(mysql_real_escape_string($_POST['Cafedra']['sname']));
            $model->name = $name;
            $model->sname = $sname;
            $model->datecreate = new CDbExpression('NOW()');
            $model->lastupdate = new CDbExpression('NOW()');
            $model->status = 1;
            if ($model->insert()) {
                echo 1;
            } else
                echo 2;
        } else
            $this->renderPartial('create/_cafedra', array('model' => $model));
    }


    public function actionUpdcaf($id)
    {
        $model = $this->loadCafedra($id);
        if (isset($_POST['Cafedra'])) {
            $model->attributes = $_POST['Cafedra'];
            $model->datecreate = new CDbExpression('NOW()');
            $model->lastupdate = new CDbExpression('NOW()');
            if ($model->update()) {
                echo 1;
            } else
                echo 2;
        } else
            $this->renderPartial('create/_updcaf', array('model' => $model));
    }


    public function actionDelcaf()
    {
        if (isset($_POST['uid'])) {
            $id = intval($_POST['uid']);
            if ($this->loadCafedra($id)->delete())
                echo 1;
            else 0;
        }
    }

    public function actionSector()
    {
        $this->render('sector');
    }

    public function actionInsertsector()
    {
        $model = new NamudiFan();

        if (isset($_POST['NamudiFan'])) {

            $model->namud = trim(mysql_real_escape_string($_POST['NamudiFan']['namud']));
            $model->snamud = trim(mysql_real_escape_string($_POST['NamudiFan']['snamud']));
            $model->datecreate = new CDbExpression('NOW()');
            $model->lastupdate = new CDbExpression('NOW()');
            $model->status = 1;

            if ($model->insert()) {
                echo 1;
            } else
                echo 2;

        } else
            $this->renderPartial('create/_sector', array('model' => $model));
    }

    public function actionUpdsect($id)
    {
        $model = $this->loadNamud($id);

        if (isset($_POST['NamudiFan'])) {

            $model->attributes = $_POST['NamudiFan'];
            $model->datecreate = new CDbExpression('NOW()');
            $model->lastupdate = new CDbExpression('NOW()');

            if ($model->update()) {
                echo 1;
            } else
                echo 2;

        } else


            $this->renderPartial('create/_updsec', array('model' => $model));
    }

    public function actionDelsec()
    {
        if (isset($_POST['uid'])) {
            $id = intval($_POST['uid']);
            if ($this->loadNamud($id)->delete())
                echo 1;
            else 0;
        }
    }

    public function actionUpdating($id)
    {
        $model = $this->loadModel($id);
        if (isset($_POST['Subject'])) {
            $model->attributes = $_POST['Subject'];
            if ($model->update()) {
                echo 1;
            } else
                echo 2;
        }
    }




/*  LOAD MODELS  */

    public function loadModel($id)
    {
        $model = Subject::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadCafedra($id)
    {
        $model = Cafedra::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadNamud($id)
    {
        $model = NamudiFan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadSubjects($id)
    {
        $model = Subjects::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'shedual-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

?>
