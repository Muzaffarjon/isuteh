<?php
class AdminController extends Controller {
    public $layout='//layouts/admin';
	
	public function filters()
	{
		return array(
            array(
                'COutputCache',
                'duration'=>3600*24*365,
            ),
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Panel',
                     'mcreate','mupdate',
                    'students','shedual',
                    'UserStudentstatus','Userstatus',
                    'Userallstatus','Groupstatus',
                    'Groupallstatus','Shedualstatus',
                    'Shedualallstatus','ucreate','uupdate','status','muallimaho','cteacher','settings'),
                'roles'=>array('8'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCteacher(){

        $model=new Teachers;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'teachers-form');

        if(isset($_POST['Teachers']))
        {
            $model->datecreate=new CDbExpression('NOW()');
            $model->lastupdate=new CDbExpression('NOW()');
            $model->status=1;
            $model->attributes=$_POST['Teachers'];
            if($model->validate() && $model->save())
                $this->redirect(array('admin/muallimaho'));
        }

        $this->render('cteacher',array('model'=>$model));
    }

    public function actionMuallimaho(){

        $model=Teachers::model()->findAll();
        $this->render('muallimaho',array('model'=>$model));

    }


// Panel Menu
    public function actionPanel(){

        $model=UserAccess::model()->findAll();

        $this->render('panel',array('model'=>$model));
    }

    public function actionMcreate()
    {
        $model=new UserMenu;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'user-menu-form');

        if(isset($_POST['UserMenu'])) {
            $model->attributes = $_POST['UserMenu'];
            $model->datecreate = new CDbExpression('NOW()');
            $model->lastupdate = new CDbExpression('NOW()');
            $model->status = 1;
            if ($model->validate()) {
                if ($model->save())
                    $this->redirect(array('admin/panel'));

            }
        }

        $this->render('menu/create',array(
            'model'=>$model,
        ));
    }


    public function actionMupdate($id){
        $model=$this->loadMenu($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserMenu']))
        {
            $model->attributes=$_POST['UserMenu'];
            $model->lastupdate=new CDbExpression('NOW()');
            if($model->save())
                $this->redirect(array('admin/panel'));
        }

        $this->render('menu/update',array(
            'model'=>$model,
        ));
    }

    public function actionMdelete(){
        if(isset($_POST['id'])){
            $id=(int)$_POST['id'];
            $del=$this->loadMenu($id)->delete();
            if($del){
                echo 1;
            }
        }
    }

    public function actionStatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        $status = UserMenu::model()->findByPk($id);
        if ($status->status == 1) {
            $upd = UserMenu::model()->updateByPk($id, array('status' => 0));
            echo 1;
        } else {
            $upd = UserMenu::model()->updateByPk($id, array('status' => 1));
            echo 1;
        }
    }

// End Panel Menu

// Users
    public function actionUcreate(){
        $model=new User;

        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model,'user-form');


        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $model->password=md5($_POST['User']['password']);
            $model->datecreate=new CDbExpression('NOW()');
            $model->lastupdate=new CDbExpression('NOW()');
            $model->status=1;
            if($model->validate() && $model->save()){
                Yii::app()->user->setFlash('user', 'Истифодабарандаи нав сохта шуд!');
                $this->redirect(array('admin/students'));
            }

        }

        $this->render('user/create',array(
            'model'=>$model,
        ));
    }
    public function actionUupdate($id){
        $model=$this->loadUser($id);

        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model,'user-form');
        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $model->password=md5($_POST['User']['password']);
            $model->lastupdate=new CDbExpression('NOW()');
            if($model->validate() && $model->save()){
                Yii::app()->user->setFlash('user', 'Истифодабаранда азнав карда шуд!');
                $this->redirect(array('admin/students'));
            }

        }

        $this->render('user/update',array(
            'model'=>$model,
        ));
    }

    public function actionStudents()
    {
        $student = User::model()->findAll(array('condition'=>'accessId=1'));

        $this->render('students',array('student'=>$student));
    }

    public function actionUserStudentstatus()
    {
        if (isset($_POST['id']))
        $id = (int)$_POST['id'];
        if ($id != 0) {
            $upd = User::model()->updateAll(array('status' => 1),'accessId=1');
            if ($upd)
                echo 1;
        } else {
            $upd = User::model()->updateAll(array('status' => 0),'accessId=1');
            if ($upd)
                echo 1;
        }
    }

    public function actionUserstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        $status = User::model()->findByPk($id);
        if ($status->status == 1) {
            $upd = User::model()->updateByPk($id, array('status' => 0));
            echo 1;
        } else {
            $upd = User::model()->updateByPk($id, array('status' => 1));
            echo 1;
        }
    }

    public function actionUserallstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        if($id!=0){
            $upd=User::model()->updateAll(array('status'=>1),'accessId>1');
            if($upd)
                echo 1;
        }else{
            $upd=User::model()->updateAll(array('status'=>0),'accessId>1 and not id='.Yii::app()->session['user']['id']);
            if($upd)
                echo 1;
        }
    }

    public function actionGroupstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        $status = Group::model()->findByPk($id);
        if ($status->status == 1) {
            $upd = Group::model()->updateByPk($id, array('status' => 0));
            echo 1;
        } else {
            $upd = Group::model()->updateByPk($id, array('status' => 1));
            echo 1;
        }
    }

    public function actionGroupallstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        if($id!=0){
            $upd=Group::model()->updateAll(array('status'=>1));
            if($upd)
                echo 1;
        }else{
            $upd=Group::model()->updateAll(array('status'=>0));
            if($upd)
                echo 1;
        }
    }
// End Users


    public function actionShedual()
    {
        $this->render('shedual');
    }

    public function actionShedualstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        $status = Shedual::model()->findByPk($id);
        if ($status->status == 1) {
            $upd = Shedual::model()->updateByPk($id, array('status' => 0));
            echo 1;
        } else {
            $upd = Shedual::model()->updateByPk($id, array('status' => 1));
            echo 1;
        }
    }

    public function actionShedualallstatus()
    {
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        if($id!=0){
            $upd=Shedual::model()->updateAll(array('status'=>0));
            if($upd)
                echo 1;
        }
    }

    public function actionSettings(){
        if(isset($_POST['check'])){
            $check=Users::settings();
        }
    }


    //Load Models


    public function loadMenu($id)
    {
        $model=UserMenu::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadUser($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model,$form)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===$form)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
} 