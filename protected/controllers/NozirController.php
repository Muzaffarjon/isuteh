<?php



class NozirController extends Controller {

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
    {
        return array(
           array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('students','fann','ruihat',
                    'group','absent','absents','info','karzho','mespost','mesupd','nozir','imtihon','imt','ijozat'),
                'roles'=>array('3'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionImtihon(){

        $this->render('imtihon');

    }

    public function actionImt($id) {

        $shedual = $this->loadShed($id);

        $group = Group::model()->findByPk($id);
        $this->render('imtihon/imti', array('shedual' => $shedual, 'group' => $group));
    }


    public function actionStudents() {
        $this->render('students');
    }

    public function actionFann($id) {

        $shedual = $this->loadShed($id);

        $group = Group::model()->findByPk($id);
        $this->render('students/fann', array('shedual' => $shedual, 'group' => $group));
    }

    public function actionIjozat($id) {

        $decode = intval($id);
        //$shedual = $this->loadShedual(Users::getDecode($decode));
        $shedual = $this->loadShedual($decode);
        $subject = $this->loadSubject($shedual->subjectId);
        $this->render('imtihon/ijozat', array('shedual' => $shedual, 'subject' => $subject));
    }

    public function actionGroup($id) {

        $decode = intval($id);
        //$shedual = $this->loadShedual(Users::getDecode($decode));
        $shedual = $this->loadShedual($decode);
        $subject = $this->loadSubject($shedual->subjectId);
        $this->render('students/group', array('shedual' => $shedual, 'subject' => $subject));
    }

    public function actionRuihat(){
//
            $this->render('ruihat');
        }
        
    public function actionAbsent($id){
            $shedual=$this->loadShed($id);
            
            $group=  Group::model()->findByPk($id);
            $this->render('absent/absent', array('shedual'=>$shedual,'group'=>$group));
        }
        
        public function actionAbsents($id){
            $shedual=$this->loadShedual($id);
            $this->render('absent/absents', array('shedual'=>$shedual,'id'=>$id));
        }
        
        public function actionInfo(){

            $this->render('info');
        }

        public function actionKarzho(){
            
            if (Yii::app()->user->isGuest)
                        $this->redirect(Yii::app()->createUrl('login/index'));
            
            $this->render('karzho');
        }
        
        public function actionMespost(){
            
            if(isset($_POST['sId']) || isset($_POST['gId'])){
                //echo $_POST['sId'].$_POST['gId'];
                if(intval($_POST['sId']) || intval($_POST['gId'])){
                    $gId=$_POST['gId'];
                    $sId=$_POST['sId'];
                    $alert=new Alert;
                    $alert->groupinId=$gId;
                    $alert->shedualId=$sId;
                    $alert->datecreate=new CDbExpression('NOW()');
                    $alert->lastupdate=new CDbExpression('NOW()');
                    $alert->status=1;
                    if($alert->save())
                        echo 1;
                }
            }
        }
        
        public function actionMesupd(){
            
            if(isset($_POST['sId']) || isset($_POST['gId'])){
                //echo $_POST['sId'].$_POST['gId'];
                if(intval($_POST['sId']) || intval($_POST['gId'])){
                    $gId=$_POST['gId'];
                    $sId=$_POST['sId'];
                   $idUpdate=$this->loadAlert($sId,$gId);
                    $update=Alert::model()->updateByPk($idUpdate->id,array(
                        'lastupdate'=>new CDbExpression('NOW()'),
                        'status'=>0
                    ));
                    if($update){
                        echo 1;
                    }
                    
                }
            }
        }

        public function actionNozir(){
                    
           // if(isset($_POST['ok'])){
                    
               //if(isset($_POST['check'])){
                 $checkVal=intval($_POST['check']);
                 $uId=intval($_POST['userId']);
                 $sId=intval($_POST['shedualId']);
                 $type=intval($_POST['type']);
            $sum=0;
                if(isset($_POST['check1'])){
                    Users::insertCheckAbsents($uId, $sId, $type, 1,Absent);
                    $sum=$sum+1;
                }
                if(isset($_POST['check2'])){
                    Users::insertCheckAbsents($uId, $sId, $type, 2,Absent);
                    $sum=$sum+1;
                 }
                 if(isset($_POST['check3'])){
                     Users::insertCheckAbsents($uId, $sId, $type, 3,Absent);
                     $sum=$sum+1;
                 }

            echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="label label-danger">'.$sum.'</span>
                </a>';

            //}
        }
        
        
        /*       There are Loading Models          */
        
        public function loadShed($id)
	{
		$model=  Shedual::model()->findAll(array('condition'=>'groupId='.$id.' and status=1'));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        public function loadAlert($sId,$gId)
	{
		$model=  Alert::model()->find(array('condition'=>'groupinId='.$gId.' and shedualId='.$sId));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
         public function loadShedual($id)
	{
		//$model=  Shedual::model()->findByPk($id,array('condition'=>'status=1'));
		$model=  Shedual::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadSubject($id)
	{
		$model=Subject::model()->findByPk($id,array('condition'=>'status=1'));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}

?>
