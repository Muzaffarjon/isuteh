<?php

class TeacherController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
           array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('study','darsho','karz','ball','setball','setbal2','dellball','goib','teach'),
                'roles'=>array('2'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionStudy()
    {

      $this->render('study');
    }

    public function actionGoib($id)
    {

        $shedual = $this->loadShedual($id);
        $this->render('goib/absents', array('shedual' => $shedual));
    }

    public function actionDarsho()
    {


        $this->render('goib/ruihat');
    }

    public function actionBall($id)
    {
        $decode = intval($id);
        $shedual = $this->loadShedual(Users::getDecode($decode));
        $subject = $this->loadSubject($shedual->subjectId);

        $this->render('ball', array('shedual' => $shedual, 'subject' => $subject));
    }

    public function actionSetball()
    {


        if (isset($_POST['shedId']) || isset($_POST['userId']) || isset($_POST['type']) || isset($_POST['ball'])) {


            $shedualId = ($_POST['shedId']);
            $userId = ($_POST['userId']);
            $type = ($_POST['type']);
            $ball = intval($_POST['ball']);

            $group = Groupin::model()->findByPk($userId);
            $user = Users::getUser($group->userId);
            $count = Scores::model()->findByAttributes(
                array(
                    'shedualId' => $shedualId,
                    'groupinId' => $userId,
                    'typeId' => $type,

                ));

            if (count($count) == 0) {
                $newScores = new Scores();
                $newScores->shedualId = $shedualId;
                $newScores->groupinId = $userId;
                $newScores->typeId = $type;

                if ($ball > 10) {
                    $newball = $newScores->bal = 10;
                } elseif ($ball < 0) {
                    $newball = $newScores->bal = 0;
                } else {
                    $newball = $newScores->bal = $ball;
                }

                $newScores->datecreate = new CDbExpression('NOW()');
                $newScores->lastupdate = new CDbExpression('NOW()');
                $newScores->status = 1;
                if ($newScores->save()) {

                    echo '<img src=' . Yii::app()->request->baseUrl . $user->avatar . ' alt="avatar" width="40" style="margin-right:10px;border-radius: 25px;">';

                    echo $user->fname . ' ' . mb_substr($user->name,0,1,"UTF-8") . ', Балл:  ' . $newball;
                } else {
                    echo 'Error not insert';
                }

            } elseif (count($count) == 1) {

                $upd = Scores::model()->updateByPk($count->idScores,
                    array(
                        'shedualId' => $shedualId,
                        'groupinId' => $userId,
                        'bal' => $ball,
                        'typeId' => $type,
                        'lastupdate' => new CDbExpression('NOW()')
                    )
                );
                echo '<img src=' . Yii::app()->request->baseUrl . $user->avatar . ' alt="avatar" width="40" style="margin-right:10px;border-radius: 25px;">';
                echo $user->fname . ' ' . mb_substr($user->name,0,1,"UTF-8") . ',&nbsp;&nbsp;Азнавшуд:  ' . $_POST['ball'];


            }
        }
    }

    public function actionDellball(){
        if (isset($_POST['sId']) || isset($_POST['uId']) || isset($_POST['t']) && Yii::app()->request->isAjaxRequest) {
            $shedId = ($_POST['sId']);
            $userId = ($_POST['uId']);
            $t = ($_POST['t']);

            $count = Scores::model()->findByAttributes(
                array(
                    'shedualId' => $shedId,
                    'groupinId' => $userId,
                    'typeId' => $t,

                ));
            if (count($count) == 0) {
                echo 0;
            } else {
                $this->loadScores($count->idScores)->delete();
                echo 1;
            }
        }
    }
    public function actionTeach()
    {


        $uId = intval($_POST['userId']);
        $sId = intval($_POST['shedualId']);
        $type = intval($_POST['type']);
        $sum=0;
        if (isset($_POST['check1'])) {
            Users::insertCheckAbsents($uId, $sId, $type, 1,"Tabsent");
            $sum=$sum+1;
        }
        if (isset($_POST['check2'])) {
            Users::insertCheckAbsents($uId, $sId, $type, 2,"Tabsent");
            $sum=$sum+1;
        }
        if (isset($_POST['check3'])) {
            Users::insertCheckAbsents($uId, $sId, $type, 3,"Tabsent");
            $sum=$sum+1;
        }


        //}


        echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="label label-danger">'.$sum.'</span>
                </a>';
    }

    public static function actionSetbal2()
    {
        if (isset($_POST['tId']) || isset($_POST['sid'])) {
            $sid = (int)$_POST['sid'];
            $tid = (int)$_POST['tId'];
            $check = Checktable::model()->find(array('condition' => 'shedualId=' . $sid . ' and typeId=' . $tid));
            if (count($check) == 0) {
                if($tid==8){
                $insert=Users::InsertCheck($sid, $tid);
                    Users::InsertCheck($sid, 9);
                }elseif($tid==17){
                    $insert=Users::InsertCheck($sid, $tid);
                    Users::InsertCheck($sid, 18);
                }else{
                    $insert=Users::InsertCheck($sid, $tid);
                }
                    if ($insert == 1) {
                    echo 1;
                } else echo 0;
            } else echo 2;
        }
    }

        /* LOAD MODELS  */

    public function loadShedual($id)
    {
        $model = Shedual::model()->findByPk($id, array('condition' => 'status=1'));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadSubject($id)
    {
        $model = Subject::model()->findByPk($id, array('condition' => 'status=1'));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadScores($id)
    {
        $model = Scores::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
} 