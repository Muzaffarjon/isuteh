<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	//public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        
        function init(){
            //parent::init();

            $activity=Settings::model()->find();
            if($activity->status==0 && Yii::app()->user->id!=27){
                $this->layout="column2";
                $this->render('//login/repair');
                Yii::app()->end();
            }

          /*if (!Yii::app()->user->checkAccess('guest')) {
                $visit = Visits::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'sessionId' => session_id(), 'ip' => $_SERVER['REMOTE_ADDR']));
                Visits::model()->updateAll(array('status' => 0), "last_action < NOW() - INTERVAL '20' MINUTE");

                if ($visit) {
                    $visits = Visits::model()->findAll(array('condition' => "not id_user=" . Yii::app()->user->id . " and last_action < NOW() - INTERVAL '20' MINUTE"));
                    if (!empty($visits))
                        Visits::model()->updateByPk($visit->id, array('last_action' => new CDbExpression('NOW()')));
                } else {
                    $this->redirect(array('site/logout'));
                }

                $cs = Yii::app()->clientScript;
                $cs->registerCoreScript('jquery.js');
            }*/


        }


 }