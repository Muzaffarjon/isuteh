
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subject-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));


    $teacher = Teachers::model()->findAll();
    $teach = array();

    foreach ($teacher as $t){
        //$find=Subject::model()->find('teacherId=:tid',array('tid'=>$t->id));
        //    if(empty($find))
        $teach[$t->id] = $t->user->fname . ' ' . $t->user->name;
    }
    ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
            <?php echo $form->labelEx($model, 'subjectId'); ?>
            <?php echo $form->dropDownList($model, 'subjectId', CHtml::listData(Subjects::model()->findAll(), 'id', 'subject'), array('class' => 'form-control', 'style' => 'margin-bottom:5px', 'required' => "reqired")); ?>
            <?php echo $form->error($model, 'subjectId'); ?>
            <span id="fann" class="text-red hide">Фанн набояд холи бошад</span>
    </div>
    <div class="form-group">
            <?php echo $form->labelEx($model, 'userId'); ?>
            <?php echo $form->dropDownList($model, 'teacherId', $teach, array('class' => 'form-control', 'style' => 'margin-bottom:5px')); ?>
            <?php echo $form->error($model, 'teacherId'); ?>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo CHtml::submitButton('Сабт кардан', array('class' => 'btn btn-success')); ?>

        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->