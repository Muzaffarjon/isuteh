<script type="text/javascript">
    $('#ins').click(function () {
        var text=$.trim($('#checkSubject').val());
        var credit=$.trim($('#checkCredit').val());
        var form_data = $(this).closest('form').serialize();
        checkField('#checkSubject');
        checkField('#checkCredit');
        cred=checkCredit(credit);
        if(credit != "" && text != ""){
            if(credit<2 || credit>6){
               $('#checkCredit').parent().addClass('has-error');
            }else{

                $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl("shed/updet/".$_GET['id']);?>",
            data: form_data,
            cache: false,
            error: function () {
                //alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
            },
            beforeSend: function () {
                $('input[type=text]').attr('disabled', 'disabled');
                $('#ins').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data == 1) {
                    $.fancybox.close();
                    $('#table_reload').load('<?php Yii::app()->createUrl('shed/fann'); ?> #table_reload');
                }
            }
        });

            }
        }
    });

    function checkField(name){
        f = $(name);
        fVal = $.trim(f.val());
        if(fVal == ''){
            f.parent().addClass('has-error');
            f.attr('placeholder','Ин марзаро дуруст пур кунед!');
            f.parent().removeClass('has-success');
        }else{
            f.parent().removeClass('has-error');
           // f.parent().addClass('has-success');
        }
    }

    function checkCredit(credit){
        if(credit<2 || credit>6){

            return 0;
        }else{
            return 1;
        }
    }
</script>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subjects',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('style' => 'margin:0', 'onSubmit' => 'return false')
    ));

    $teacher = NamudiFan::model()->findAll();
    $teach = array();

    foreach ($teacher as $t)
        $teach[$t->id] = $t->namud;

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">

        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model, 'subject'); ?></label>

        <div class="col-sm-10">
            <?php echo $form->textfield($model, 'subject', array('class' => 'form-control', 'id'=>'checkSubject', 'style' => 'margin-bottom:5px',  'required' => 'required')); ?>
            <?php echo $form->error($model, 'subject'); ?>
        </div>
    </div>
    <div class="form-group">

        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model, 'credit'); ?></label>

        <div class="col-sm-10">
            <?php echo $form->textfield($model, 'credit', array('class' => 'form-control', 'id'=>'checkCredit', 'style' => 'margin-bottom:5px', 'required' => 'required')); ?>
            <?php echo $form->error($model, 'credit'); ?>
        </div>
    </div>
<!--
    <div class="form-group">

        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model, 'id_cafedra'); ?></label>

        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'id_cafedra', CHtml::listData(Cafedra::model()->findAll(), 'id', 'name'), array('class' => 'form-control', 'style' => 'margin-bottom:5px', 'required' => "reqired")); ?>
            <?php echo $form->error($model, 'id_cafedra'); ?>
            <span id="fann" class="text-red hide">Фанн набояд холи бошад</span>
        </div>
    </div>
    <div class="form-group">

        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model, 'id_namud'); ?></label>

        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'id_namud', $teach, array('class' => 'form-control', 'style' => 'margin-bottom:5px', 'required' => "reqired")); ?>
            <?php echo $form->error($model, 'id_namud'); ?>
        </div>
    </div>
-->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <!--  <input type="hidden" value="<?php echo $_GET['id']; ?>" name="sId">-->
            <?php echo CHtml::submitButton('Сабт кардан', array('class' => 'btn btn-success', "id" => "ins")); ?>

        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->