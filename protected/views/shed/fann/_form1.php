
    <div class="form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'subjects-form',
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        ));

        $teacher = NamudiFan::model()->findAll();
        $teach = array();

        foreach ($teacher as $t)
            $teach[$t->id] = $t->namud;
        ?>
        <?php echo $form->errorSummary($model); ?>

        <div class="form-group">
                <?php echo $form->labelEx($model, 'subject'); ?>

                <?php echo $form->textField($model, 'subject', array('class' => 'form-control', 'id' => 'sSubject')); ?>
                <?php echo $form->error($model, 'subject'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'credit'); ?>
            <?php echo $form->numberField($model, 'credit', array('class' => 'form-control', 'id' => 'sCredit')); ?>
            <?php echo $form->error($model, 'credit'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'id_cafedra'); ?>
            <?php echo $form->dropDownList($model, 'id_cafedra', CHtml::listData(Cafedra::model()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'id_cafedra'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'id_namud'); ?>
            <?php echo $form->dropDownList($model, 'id_namud', $teach, array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'id_namud'); ?>
        </div>


        <div class="form-group">
            <?php echo CHtml::submitButton('Сабт кардан', array('class' => 'btn btn-success')); ?>

        </div>

        <?php $this->endWidget(); ?>

    </div>
