
<script type="text/javascript">
    
    $('#ins').click(function(){
        var form_data = $(this).closest('form').serialize();
              var text=document.forms['subject']['Subject[subjectId]'].value;
              var uId=document.forms['subject']['Subject[teacherId]'].value;
              var sid=checkField(text);
              var tid=checkField(uId);
              if(sid!="" && tid!=""){
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl("shed/updating"); ?>",
                data: form_data,
                cache: false,
                error: function(){
                    alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
                },
                beforeSend: function(){
                    $('input[type=text]').attr('disabled', 'disabled');
                    $('select').attr('disabled', 'disabled');
                    $('#ins').attr('disabled', 'disabled');
                },
                success: function(data){
                    if(data==1){
                        $.fancybox.close();
                        $('#table_reload').load('<?php Yii::app()->createUrl('shed/fann'); ?> #table_reload');
                    }else{
                        $.fancybox.close();
                        alert('Ягон марза иваз нашуд');
                    }    
                }
            }); 
        }
    });

 function checkIt(value){
      var intValue = parseInt(value);
        if (intValue == Number.NaN) {
            return false;
        }else{
            return intValue;
        }

 }

    function checkField(name){
        f = $(name);
        fVal = $.trim(f.val());
        if(fVal == ''){
            f.parent().addClass('has-error');
            f.parent().removeClass('has-success');
        }else{
            f.parent().removeClass('has-error');
            f.parent().addClass('has-success');
        }
    }

    
    </script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'subject',
	'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
         ),
        'htmlOptions'=>array('style'=>'margin:0','onSubmit'=>'return false')
)); ?>

	<p class="note">Ҳамаи марзаҳо бояд пурра бошад.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="form-group">
                          
                        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model,'subject'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropDownList($model,'subjectId',CHtml::listData(Subjects::model()->findAll(),'id', 'subject'), array('class'=>'form-control', 'style'=>'margin-bottom:5px', 'required')); ?>
		          <?php echo $form->error($model,'subjectId'); ?>
                        </div>
                      </div>
        <div class="form-group">
                          
                        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model,'userId'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropDownList($model,'teacherId',CHtml::listData(Teachers::model()->findAll('status=1'), 'id', 'user.name'), array('class'=>'form-control', 'style'=>'margin-bottom:5px', 'required')); ?>
		          <?php echo $form->error($model,'teacherId'); ?>
                        </div>
                      </div>


	<div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">

                          		<?php echo CHtml::submitButton('Сабт кардан', array('class'=>'btn btn-success', "id"=>"ins")); ?>

                        </div>
                      </div>

<?php $this->endWidget(); ?>

</div><!-- form -->