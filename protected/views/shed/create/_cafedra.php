<script>


    function sendGaid(){
        name=$('#Cafedra_name').val();
        sname=$('#Cafedra_sname').val();
        checkField('#Cafedra_name');
        checkField('#Cafedra_sname');

        if(name!="" && sname!=""){
                form_var=$('#cafedra-form').serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl("shed/insertcafedra");?>",
                data: form_var,
                cache: false,
                error: function(){
                    //alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
                },
                beforeSend: function(){

                },
                success: function(data){
                    if(data==1){
                        $('#mess').show();
                        $('#contC').hide();

                        function set(){
                            $.fancybox.close();
                            $('#table_reload').load('<?php Yii::app()->createUrl('shed/cafedra'); ?> #table_reload');
                        }
                        setTimeout(set,3000);

                    }else{
                        $('#mess2').show();
                        $('#contC').hide();
                    }

                }
            });

        }
    }

    function checkField(name){
        f = $(name);
        fVal = $.trim(f.val());
        if(fVal == ''){
            f.parent().addClass('has-error');
            f.parent().removeClass('has-success');
        }else{
            f.parent().removeClass('has-error');
            f.parent().addClass('has-success');
        }
    }
    </script>
<div id="mess" style="display: none" class="alert alert-success alert-dismissable">

    <h4><i class="icon fa fa-info"></i> Диққат!</h4>
    Маълумот бо муваффақият сабт шуд.
</div>
<div id="mess2" style="display: none" class="alert alert-danger alert-dismissable">

    <h4><i class="icon fa fa-info"></i> Хатогӣ!</h4>
    Маълумот сабт карда нашуд.
</div>
<div id="contC">
<?php

$form=$this->beginWidget('CActiveForm', array(
    'id'=>'cafedra-form',
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
        'errorCssClass'=>'has-error',
        'successCssClass'=>'has-success',
        'afterValidate' => 'js:sendGaid'
    ),
    'htmlOptions'=>array('style'=>'margin:0','onSubmit'=>'return false;')
));

?>
	<?php echo $form->errorSummary($model); ?>

                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'name'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->textField($model,'name', array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'name'); ?>
                            <span id="mes1" class="text-red hide">Марза холи набошад</span>
                        </div>
                      </div>
                        <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'sname'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->textField($model,'sname', array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'sname'); ?>
                            <span id="mes1" class="text-red hide">Марза холи набошад</span>
                        </div>
                      </div>


                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <?php
                            echo CHtml::submitButton('Сабт кардан',array('class'=>'btn btn-success', 'onClick'=>'sendGaid();'));
                            /*echo CHtml::ajaxSubmitButton('Сабт кардан',Yii::app()->createUrl('shed/insertcafedra'),
                                array(
                                    'type'=>'POST',
                                    'data'=> 'js:$("#cafedra-form").serialize()',
                                    'success'=>'js:function(data){ alert(data); }'
                                ),array('class'=>'btn btn-success',));
                            */
                            ?>

                        </div>
                      </div>



<?php $this->endWidget(); ?>

    </div>