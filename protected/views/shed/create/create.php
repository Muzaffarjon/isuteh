<?php
/* @var $this ShedualController */
/* @var $model Shedual */

$this->breadcrumbs=array(
	'Ҷадвали дарсҳои ҳозира'=>array('shed/jadval'),
	'Сохтани ҷадвали нав',
);
?>

<section class="content">
<?php echo $this->renderPartial('create/_form', array('model'=>$model)); ?>
</section>