<?php
/* @var $this ShedualController */
/* @var $model Shedual */

$this->breadcrumbs=array(
	'Ҷадвали дарсҳои ҳозира'=>array('shed/jadval'),
	'Азнав кардани ҷадвали #'.$model->idShedual
);
?>

<section class="content">
<?php echo $this->renderPartial('create/_form', array('model'=>$model)); ?>
</section>