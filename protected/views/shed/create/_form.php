<?php
/* @var $this ShedualController */
/* @var $model Shedual */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shedual-form',
	'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
));

$models=Group::model()->findAll();
$data=array();
foreach($models as $m)
    $data[$m->idGroup]=$m->course.'_'.$m->ihtisos->code.$m->class;

$mods=Subject::model()->findAll();
$sub=array();

foreach($mods as $subject)
        $sub[$subject->idSubject]=$subject->subject->subject.'-'.$subject->teacher->user->fname.' '.$subject->teacher->user->name.' ('.$subject->subject->credit.') кредит'

?>
	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Гурӯҳ'); ?>
        <?php echo $form->dropDownList($model,'groupId',$data, array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
		<?php echo $form->error($model,'groupId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Фанн'); ?>
        <?php echo $form->dropDownList($model,'subjectId',$sub, array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
		<?php echo $form->error($model,'subjectId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Сикл'); ?>
        <?php echo $form->dropdownlist($model,'cikl',array(1=>1,2,3,4,5,6,7,8), array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
		<?php echo $form->error($model,'cikl'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Семестр'); ?>
        <?php echo $form->dropdownlist($model,'semestr',array(1=>1,2,3,4,5,6), array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
		<?php echo $form->error($model,'semestr'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Рӯзи саршавии дарс'); ?>
		<?php echo $form->textField($model,'dateStart', array('class'=>'form-control',"data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Рӯзи итмоми дарс'); ?>
		<?php echo $form->textField($model,'dateEnd', array('class'=>'form-control',"data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
		<?php echo $form->error($model,'dateEnd'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Рӯзҳои истироҳат'); ?>
		<?php echo $form->textArea($model,'dateRest',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'dateRest'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Сохтан',array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->