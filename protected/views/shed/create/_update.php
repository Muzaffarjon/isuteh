<script type="text/javascript">
    $('#upd').click(function(){
        var form_data = $(this).closest('form').serialize();
       $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl("shed/upd");?>",
            data: form_data,
            cache: false,
            error: function(){
               //alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
            },
            beforeSend: function(){
                $('#upd').attr('disabled', 'disabled');
            },
            success: function(data){ 
               if(data==1){
               $.fancybox.close();
               history.go(0);
               }
            }
         }); 
    });
    
    </script>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shedual-form',
	'enableAjaxValidation'=>false,
    'clientOptions'=>array(
            'validateOnSubmit'=>false,
         ),
        'htmlOptions'=>array('style'=>'margin:0','onSubmit'=>'return false')
));

$models=Group::model()->findAll();
$data=array();
foreach($models as $m)
    $data[$m->idGroup]=$m->course.'_'.$m->ihtisos->code.$m->class;

$mods=Subject::model()->findAll();
$sub=array();

foreach($mods as $subject)
        $sub[$subject->idSubject]=$subject->subject->subject.'-'.$subject->teacher->user->fname.' '.$subject->teacher->user->name.' ('.$subject->subject->credit.') кредит'


?>
	<?php echo $form->errorSummary($model); ?>

                      <div class="form-group">
                          
                        <label class="col-sm-2 control-label"><?php echo $form->labelEx($model,'groupId'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropDownList($model,'groupId',$data, array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
		          <?php echo $form->error($model,'groupId'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'subjectId'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropDownList($model,'subjectId',$sub, array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'subjectId'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'cikl'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropdownlist($model,'cikl',array(1=>1,2,3,4,5,6,7,8), array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'cikl'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputExperience" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'semestr'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->dropdownlist($model,'semestr',array(1=>1,2,3,4,5,6), array('class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'semestr'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'dateStart'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->textField($model,'dateStart', array('class'=>'form-control', 'style'=>'margin-bottom:5px', 'id'=>'datestart', "data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
                          <?php echo $form->error($model,'dateStart'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'dateEnd'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->textField($model,'dateEnd', array('class'=>'form-control', 'style'=>'margin-bottom:5px', 'id'=>'dateend', "data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
                          <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                          <?php echo $form->error($model,'dateEnd'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'dateRest'); ?></label>
                        <div class="col-sm-10">
                          <?php echo $form->textArea($model,'dateRest',array('rows'=>6, 'cols'=>50, 'class'=>'form-control', 'style'=>'margin-bottom:5px')); ?>
                          <?php echo $form->error($model,'dateRest'); ?>
                        </div>
                      </div>
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          		<?php echo CHtml::submitButton('Азнавкардан', array('class'=>'btn btn-success', "id"=>"upd")); ?>

                        </div>
                      </div>


		
		

		
		

		

		
		

		
		

		
		

		
		

<?php $this->endWidget(); ?>