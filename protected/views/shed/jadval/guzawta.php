<?php
$this->breadcrumbs=array(
    'Руйхати дарсҳои гузашта'
);
?>
<script type="text/javascript">
$(document).ready(function(){
 $('.spoiler_links').click(function(){
  $(this).parent().children('div.spoiler_body').toggle('normal');
  return false;
 });
});
</script>
<section class="content">
    <div style="overflow-y: auto; height: 500px;display: block">        
        
            
    <?php
    $i=1;
    $group=  Group::model()->findAll(array('condition'=>'status=1'));

    foreach ($group as $gr){

    echo '<div class="panel box box-danger">';

    echo ' <div class="box-header with-border">';
    echo '<h4 class="box-title">';
    echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$gr->idGroup.'">
                            '.$gr->course.'_'.$gr->ihtisos->code.$gr->class.'</a>';
    echo '</h4>';
    echo ' </div>';


    echo '<div id="collapse'.$gr->idGroup.'" class="panel-collapse collapse">';
    echo '<div class="box-body">';
    echo '<table class="table table-hover table-bordered">';
    ?>

        <table id="table_reload" class="table table-bordered table-striped table-hover dataTable">
            <thead>
            <th>№</th>
            <th>Гурӯҳ</th>
            <th>Фанн</th>
            <th>Муаллим</th>
            <th>Семестр</th>
            <th>Сикл</th>
            <th>Кредит</th>
            <th>Вақти саршавӣ</th>
            <th>Вақти итмом</th>
            <!--                <th>Хориҷ</th>-->
            <th>Пред просмотр</th>
            </thead>


            <tbody>

            <?php
            $shedual=Shedual::model()->findAll(array(
                'condition'=>'groupId='.$gr->idGroup.' and status=0',
                'order'=>'semestr, cikl'
            ));
            foreach ($shedual as $shed){
                echo '<tr>';
                echo '<td>'.$i++.'</td>';
                echo '<td>'.$shed->group->course.'_'.$shed->group->ihtisos->code.$shed->group->class.'</td>';
                echo '<td>'.$shed->subject->subject->subject.'</td>';
                echo '<td>'.$shed->subject->teacher->user->fname.' '.$shed->subject->teacher->user->name.'</td>';
                echo '<td>'.$shed->semestr.'</td>';
                echo '<td>'.$shed->cikl.'</td>';
                echo '<td>'.$shed->subject->subject->credit.'</td>';
                echo '<td>'.$shed->dateStart.'</td>';
                echo '<td>'.$shed->dateEnd.'</td>';
               // echo '<td><a class="update"  href="'.Yii::app()->createUrl('shed/update/'.$shed->idShedual).'"><i class="fa fa-edit"></i></a></td>';
                if($shed->status==1){
                    echo '<td align="center"><span class="label label-success">ON</span></td>';
                }
                //echo '<td align="center"><button onclick="myFunction('.$shed->idShedual.')" class="btn-xs btn-danger"><i class="fa fa-close"></i></button></td>';
                //echo '<td align="center"><a href="'.Yii::app()->createUrl('nozir/group/'.Users::getIncode($shed->idShedual)).'"><i class="fa fa-binoculars"></i></a></td>';
                echo '<td align="center"><a href="'.Yii::app()->createUrl('nozir/group/'.$shed->idShedual).'"><i class="fa fa-binoculars"></i></a></td>';

                echo '<a href="Javascript:void()"></a></td>';
                echo '</tr>';
            }
            ?>


            <?

            echo '</tr>
            </tbody>
            </table>
            </div>
            </div>
            </div>
            ';
            }

    
    ?>
               
    </div>  
        </div>

</section>