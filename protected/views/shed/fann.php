<?php
$this->breadcrumbs=array(
    'Фанн'
);
?>
<script>
function myFunction(id) {
    var x;
    if (confirm("Шумо рози ҳастед барои хориҷ!") == true) {
          
        $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl("shed/delete"); ?>",
                data: {uid:id},
                cache: false,
                error: function(){
                    alert("Хатогӣ!") ;
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    if(data==1){
                        $('#table_reload').load('<?php Yii::app()->createUrl('shed/fann'); ?> #table_reload');
                    }else{
                        alert('Ягон марза иваз нашуд');
                    }    
                }
            });
    } 
}    
    </script>
    

<section class="content">
    <a class="btn btn-app" href="<?php echo Yii::app()->createUrl('shed/insertfann'); ?>">
                    <i class="fa fa-edit"></i> Ворид кардан
                  </a>
<div style="overflow-y: auto; height: 500px;display: block">        
    <table id="table_reload" class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <th>№</th>
            <th>Фанн</th>
            <th>Омӯзгор</th>
            <th>Ҳолат</th>
            <th>Обн</th>
            <th>Хориҷ</th>
        </thead>
        <tbody>
    <?php
    $i=1;
    foreach($subject as $subj){
        echo '<tr>';
        echo '<td>'.$i++.'</td>';
        echo '<td>'.$subj->subject->subject.'</td>';
        echo '<td>'.$subj->teacher->user->fname.' '.$subj->teacher->user->name.'</td>';
        if($subj->status==1){
                    echo '<td align="center"><span class="label label-success">ON</span></td>';
        }else{
            echo '<td align="center"><span class="label label-danger">OFF</span></td>';
        }
        echo '<td><a class="update" href="'.Yii::app()->createUrl('shed/upde/'.$subj->idSubject).'"><i class="fa fa-edit"></i></a></td>';
        echo '<td align="center"><button onclick="myFunction('.$subj->idSubject.')" class="btn-xs btn-danger"><i class="fa fa-close"></i></button></td>';
        echo '</tr>';
    }
    ?>
            </tbody>
        </table>
    </div>
</section>