<?php

$this->breadcrumbs=array(
    'Муаллимон'
);

?>
<section class="content">
    <table class="table table-bordered table-hover table-striped table-condensed">
    <thead>
        <th>Ф.И.О</th>
        <th>Соли таввалуд</th>
        <th>Дараҷа</th>
        <th>Суроға</th>
        <th>Телефон</th>
    </thead>
        <?php
               $teachers=  Teachers::model()->findAll();
               
               foreach($teachers as $teacher){
                   $user=  User::model()->find(array('condition'=>'id='.$teacher->userId));
                   echo '<tr>';
                   echo '<td>'.$user->fname.' '.$user->name.'</td>';
                   echo '<td>'.$user->birthday.'</td>';
                   if($teacher->daraja=='easy'){
                        echo '<td><span class="label label-warning"> Оддӣ </span></td>';
                   }elseif($teacher->daraja=='medium'){
                       echo '<td><span class="label label-info"> Миёна </span></td>';
                   }elseif($teacher->daraja=='high'){
                       echo '<td><span class="label label-success"> Баланд </span></td>';
                   }
                  
                   
                   echo '<td>'.$user->address.'</td>';
                   echo '<td>'.$user->number_phone.'</td>';
                   echo '</tr>';
               }
    ?>
    </table>
</section>