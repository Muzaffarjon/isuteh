<?php
$this->breadcrumbs=array(
    'Руйхати Кафедра'
);
?>

<script>
    function myFunction(id) {
        var x;
        if (confirm("Шумо рози ҳастед барои хориҷ!") == true) {

            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl("shed/delcaf"); ?>",
                data: {uid:id},
                cache: false,
                error: function(){
                    alert("Хатогӣ!") ;
                },
                beforeSend: function(){

                },
                success: function(data){
                    if(data==1){
                        $('#table_reload').load('<?php Yii::app()->createUrl('shed/cafedra'); ?> #table_reload');
                    }else{
                        alert('Ягон марза иваз нашуд');
                    }
                }
            });
        }
    }
</script>


<section class="content">
    <a class="update btn btn-app" href="<?php echo Yii::app()->createUrl('shed/insertcafedra'); ?>">
        <i class="fa fa-edit"></i> Дохил кардан
    </a>
    <div style="overflow-y: auto; height: 500px;display: block">
        <table id="table_reload" class="table table-bordered table-striped table-hover dataTable">
            <thead>
            <th>№</th>
            <th>Ном</th>
            <th>Ҳолат</th>
            <th>Обн</th>
            <th>Хориҷ</th>
            </thead>
            <tbody>
            <?php
            $i=1;
            $subject=Cafedra::model()->findAll();
            foreach($subject as $subj){
                echo '<tr>';
                echo '<td>'.$i++.'</td>';
                echo '<td>'.$subj->name.'</td>';
                if($subj->status==1){
                    echo '<td align="center"><span class="label label-success">ON</span></td>';
                }else{
                    echo '<td align="center"><span class="label label-danger">OFF</span></td>';
                }
                echo '<td><a class="update" href="'.Yii::app()->createUrl('shed/updcaf/'.$subj->id).'"><i class="fa fa-edit"></i></a></td>';
                echo '<td align="center"><button onclick="myFunction('.$subj->id.')" class="btn-xs btn-danger"><i class="fa fa-close"></i></button></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</section>