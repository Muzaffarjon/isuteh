<?php
$this->pageTitle=Yii::app()->name . ' - Саҳифаи дастрасӣ';
?>
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="#"><b>Системаи КТХ</b></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name" style="text-align: center"><?php echo Yii::app()->session['user']['fname'].' '.Yii::app()->session['user']['name']; ?></div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="<?php echo Yii::app()->request->baseUrl.Yii::app()->session['user']['avatar'] ?>" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials">
            <div class="input-group">
                <input type="password" class="form-control" placeholder="password">
                <div class="input-group-btn">
                    <button class="btn" onclick="false"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form><!-- /.lockscreen credentials -->

    </div><!-- /.lockscreen-item -->
    <div class="help-block text-center" style="font-weight: 600;color: #000;">
        Шумо ҳуқуқи ба система ворид шуданро надоред. Аз ҳисоби маблағи хониш ё ин ки қарздорӣ ҳуқукҳои шумо аз система тоза карда шудааст!
    </div>
    <div class="text-center">
        <a href="<?php echo Yii::app()->createUrl('login/logout') ?>">Баромад</a>
    </div>
    <div class="lockscreen-footer text-center">
        Copyright © <?php echo date('Y'); ?> <b><a href="http://rekstar.com" class="text-black">RekstaR</a></b><br>
        Ҳамаи ҳуқуқҳо махфузанд.
    </div>
</div>