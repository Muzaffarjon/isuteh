<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Барқарор кардани парол';

?>

    <div class="login-box">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

      <div class="login-logo">
      </div><!-- /.login-logo -->
      <div class="login-box-body">
          <?php if(Yii::app()->user->hasFlash('email')): ?>
          <p class="login-box-msg" style="text-align: center"><div class="alert alert-success alert-dismissable">
              <?php echo Yii::app()->user->getFlash('email'); ?>

          </p></div>
          <?php else: ?>
        <p class="login-box-msg" style="text-align: center"><div class="alert alert-success alert-dismissable">
              Почтаи худро ворид кунед
          </p></div>
          <div class="form-group has-feedback">
		<?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'E-mail')); ?>
		<?php echo $form->error($model,'username'); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
              <?php if(Yii::app()->session->get('_try_send', 0)>4){ ?>
                      <?php $this->widget('CCaptcha'); ?>
                      <?php echo $form->textField($model,'verifyCode',array('class'=>'form-control','placeholder'=>'Коди дар тасвир буда')); ?>
                  <?php echo $form->error($model,'verifyCode');
              }
              ?>
          </div>
          <div class="row">
              <div class="col-xs-8">
                  <a href="<?php echo Yii::app()->createUrl('login/index') ?>"><i class="fa fa-home text-success"></i> Ба аввал</a>
              </div>
            <div class="col-xs-4">
                <?php echo CHtml::submitButton('Фирист', array('class'=>'btn btn-success btn-block')); ?>
            </div><!-- /.col -->
          </div>
    <a href="<?php echo Yii::app()->createUrl('login/contact')?>" class="text-center">Муроҷиат ба IT</a>

        <div class="row">
            <div style="padding: 15px; text-align: justify; font: 16px bold ;">
                <i>&nbsp;&nbsp;&nbsp;Агар почтаи электронӣ ҳам аз хотир баромада бошад, пас ба шӯъбаи иттилоотии коллеҷ муроҷиат кунед</i>
            </div>
        </div>
            <?php endif; ?>

<?php $this->endWidget(); ?>


    </div><!-- /.login-box -->


