<?php
$this->pageTitle=Yii::app()->name . ' - Барқарор кардани парол';
?>

<div class="login-box">
        <div class="login-box-body">
        <p class="login-box-msg">Барои иваз кардани парол</p>
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'email-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>
            <div class="form-group has-feedback">
                <?php echo $form->passwordField($model,'new_password',array('class'=>'form-control','placeholder'=>'Пароли нав')); ?>
                <?php echo $form->error($model,'new_password'); ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->passwordField($model,'repeat_password',array('class'=>'form-control','placeholder'=>'Такроран пароли нав')); ?>
                <?php echo $form->error($model,'repeat_password'); ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <?php echo CHtml::submitButton('Азнав кардан', array('class'=>'btn btn-success btn-block')); ?>
                </div><!-- /.col -->
            </div>

            <?php $this->endWidget(); ?>

        <a href="<?php echo Yii::app()->createUrl('login/index')?>">Парол ба ёдам омад!</a><br>

    </div><!-- /.login-box-body -->
</div>




