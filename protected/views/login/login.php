<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Воридшавӣ';

?>

    <div class="login-box">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

      <div class="login-logo">

      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Марзаҳоро пур кунед!</p>
          <div class="form-group has-feedback">
		<?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'Логин')); ?>
		<?php echo $form->error($model,'username'); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
		<?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Парол')); ?>
		<?php echo $form->error($model,'password'); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <?php if(Yii::app()->session->get('_try_login', 0)>4){ ?>
                      <?php $this->widget('CCaptcha'); ?>
                      <?php echo $form->textField($model,'verifyCode',array('class'=>'form-control','placeholder'=>'Коди дар тасвир буда')); ?>
                  <?php echo $form->error($model,'verifyCode');
              }
              ?>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                  <label>
                <?php echo $form->checkBox($model,'rememberMe'); ?>
                      <?php echo $form->label($model,'Дар ёд доштан'); ?>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <?php echo CHtml::submitButton('Ворид', array('class'=>'btn btn-primary btn-block btn-flat')); ?>
            </div><!-- /.col -->
          </div>

<?php $this->endWidget(); ?>
        <div class="social-auth-links text-center">
          <p>- Хабарҳои мо дар -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Twitter</a>
          <a href="#" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Linkedin</a>
        </div><!-- /.social-auth-links -->

        <a href="<?php echo Yii::app()->createUrl('login/forgetpassword') ?>">Пароли аккаунтро фаромӯш кардаед?</a><br>
      </div><!-- /.login-box-body -->

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

    </div><!-- /.login-box -->



