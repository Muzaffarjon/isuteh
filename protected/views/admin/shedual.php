<?php

$this->pageTitle=Yii::app()->name .' - Руйхати дарсҳо';

$this->breadcrumbs=array(
    'Руйхати дарсҳо'
);



?>
<script>
    function update(id) {
        var loading=$('#loading');
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/shedualstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                loading.show();
                $('button').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                loading.hide();
                    $('button').removeAttr('disabled');
                $('#btn_' + id).load("<?php echo Yii::app()->createUrl('admin/shedual')?> #btn_" + id);
            }
        })
    }

    function updateAll(id) {
        if(confirm('Шумо мехоҳед ҷадвали дарсиро ба охир расонед?')==true){
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/shedualallstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('button#test').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('button#test').removeAttr('disabled');
                location.reload();
            }
        });
        }else{
            alert('No update data!');
        }
    }
</script>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box">
<div class="box-header">
    <h3 class="box-title">Data Table With Full Features</h3>
</div><!-- /.box-header -->
    <button style="margin: 5px" onclick="updateAll(1)" id="test" class="btn btn-success">Қатъ кардан
    </button>
<div class="box-body">
<table id="example2" class="table table-bordered" width="100%">
    <i id="loading" class="fa fa-refresh fa-spin" style="display: none"></i>
<thead>
<tr>
    <th>#</th>
    <th>Фанн</th>
    <th>Муаллим</th>
    <th>Гурӯҳ</th>
    <th>Саршавӣ</th>
    <th>Итмом</th>
    <th>Ҳолат</th>
</tr>
</thead>
<tbody>
<?php
$shedual=Shedual::model()->findAll();
$i=1;
foreach($shedual as $shed){
    echo '<tr>';
    echo '<td>'.$i++.'</td>';
    echo '<td>'.$shed->subject->subject->subject.'</td>';
    echo '<td>'.$shed->subject->teacher->user->fname.' '.$shed->subject->teacher->user->name.' '.$shed->subject->teacher->user->lname.'</td>';
    echo '<td>'.$shed->group->course.'_'.$shed->group->ihtisos->code.''.$shed->group->class.'</td>';
    echo '<td>'.$shed->dateStart.'</td>';
    echo '<td>'.$shed->dateEnd.'</td>';
    if ($shed->status == 1)
       echo '<td id="btn_' . $shed->idShedual. '"><button onclick="update(' . $shed->idShedual . ')" class="btn btn-success">ON</button></td>';
    else
        echo '<td id="btn_' . $shed->idShedual . '"><button onclick="update(' . $shed->idShedual . ')" class="btn btn-danger">OFF</button></td>';

    echo '</tr>';
}
?>
</tbody>
<tfoot>
<tr>
    <th>#</th>
    <th>Фанн</th>
    <th>Муаллим</th>
    <th>Гурӯҳ</th>
    <th>Саршавӣ</th>
    <th>Итмом</th>
    <th>Ҳолат</th>
</tr>
</tfoot>
</table widwidth=>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>

<script>
    $(function () {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</section>