<?php
$this->pageTitle = Yii::app()->name . ' - Студенты';

$this->breadcrumbs = array(
    'Студенты'
);
?>

<script>
    $(function () {
        $("#myTable").DataTable();
        $("#uTable").DataTable();
        $('#gTable').DataTable();
    });
</script>
<section id="obt" class="content">
    <div class="nav-tabs-custom">

        <ul class="nav nav-tabs">
            <li class="active"><a href="#k_b" data-toggle="tab" aria-expanded="true">Донишҷӯён</a></li>
            <li class=""><a href="#k_i" data-toggle="tab" aria-expanded="true">Кормандон</a></li>
            <li class=""><a href="#k_g" data-toggle="tab" aria-expanded="true">Гурӯҳҳо</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="k_b">
            <?php echo $this->renderPartial('includes/_students',array('student'=>$student));?>
            </div>
            <!-- /.tab-pane -->

            <div class="tab-pane" id="k_i">
                <?php echo $this->renderPartial('includes/_workers');?>
            </div>

            <div class="tab-pane" id="k_g">
                <?php echo $this->renderPartial('includes/_group');?>
            </div>
            <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
</section>