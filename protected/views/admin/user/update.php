<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('students'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>
<section class="content">
<h1>Азнавкардани истифодабарандаи #<?php echo $model->id; ?></h1>
<?php echo $this->renderPartial('user/_form', array('model'=>$model)); ?>
</section>