<?php
/* @var $this TeachersController */
/* @var $model Teachers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'teachers-form',
    'errorMessageCssClass'=>'has-error',
	'enableAjaxValidation'=>true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
));

$teachers=User::model()->findAll(array('condition'=>'accessId=2'));
$data=array();
foreach ($teachers as $teach){
    $find=Teachers::model()->find('userId=:uid',array(':uid'=>$teach->id));
    if(empty($find)){
        $data[$teach->id]=$teach->fname.' '.$teach->name;
    }
}
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->dropDownList($model,'userId',$data,array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'daraja'); ?>
		<?php echo $form->textField($model,'daraja',array('size'=>6,'maxlength'=>6,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'daraja'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'staj'); ?>
		<?php echo $form->textField($model,'staj',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'staj'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'soli_kabul'); ?>
		<?php echo $form->textField($model,'soli_kabul',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'soli_kabul'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохтан' : 'Азнавкардан',array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->