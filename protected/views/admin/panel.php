<?php

$this->pageTitle=Yii::app()->name .' - Меню для пользователей';

$this->breadcrumbs=array(
    'Меню для пользователей'
);



?>
<script>
    function update(id){
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/status') ?>",
            cache: false,
            data:{id:id},
            error: function(){

            },
            beforeSend: function(){
                $('#loading').show();
                $('#btn_'+id).attr('disabled','disabled');
            },
            success: function(data){
                if(data=1)
                    $('#loading').hide();
                $('#btn_'+id).load("<?php echo Yii::app()->createUrl('admin/panel')?> #btn_"+id);
            }
        })
    }

    function mdelete(id){
        if(confirm('Шумо мехохед тоза кунед?')==true){
            $.ajax({
                type: 'POST',
                url: "<?php echo Yii::app()->createUrl('admin/mdelete') ?>",
                cache: false,
                data:{id:id},
                error: function(){

                },
                beforeSend: function(){
                    $('#loading').show();
                    $('#btn_'+id).attr('disabled','disabled');
                },
                success: function(data){
                    if(data=1)
                        $('#loading').hide();
                        $('#obn_'+id).load("<?php echo Yii::app()->createUrl('admin/panel')?> #obn_"+id);
                }
            })
        }

    }
</script>
<section class="content">
    <section class="content">
        <div class="nav-tabs-custom">

                    <a href="<?php echo Yii::app()->createUrl('admin/mcreate') ?>" class="btn btn-primary">Сохтани менюи нав</a>
            <i id="loading" class="fa fa-refresh fa-spin" style="display: none"></i>
            <div class="tab-content">
                <div class="tab-pane active" id="k_b">
                    <div class="box box-success">

                        <div class="box-body table-responsive">

                            <table id="myTable" class="table table-bordered table-hover dataTable" width="100%">

                                <thead>
                                <tr role="row">
                                    <th>Id</th>
                                    <th>P_Id</th>
                                    <th>Name</th>
                                    <th>Link</th>
                                    <th>Title</th>
                                    <th>Icon</th>
                                    <th>Order</th>
                                    <th>Access</th>
                                    <th>H_chev</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i=1;
                                $scores = UserMenu::model()->findAll();

                                foreach ($scores as $menu) {

                                        echo '<tr id="obn_'.$menu->idMenu.'">';
                                       // echo '<td>'.$i++.'</td>';
                                        echo '<td>'.$menu->idMenu.'</td>';
                                        echo '<td>'.$menu->parentId.'</td>';
                                        echo '<td>'.$menu->name.'</td>';
                                        echo '<td>'.$menu->href.'</td>';
                                        echo '<td>'.$menu->title.'</td>';
                                       echo '<td>'.$menu->icon.'</td>';
                                       echo '<td>'.$menu->orderId.'</td>';
                                      echo '<td>'.$menu->accessId.'</td>';
                                    if($menu->has_chevron=="yes"){
                                      echo '<td><span class="label label-success">'.$menu->has_chevron.'</span></td>';
                                    }else
                                        echo '<td><span class="label label-warning">'.$menu->has_chevron.'</span></td>';
                                    if($menu->status==1)
                                        echo '<td id="btn_'.$menu->idMenu.'"><button onclick="update('.$menu->idMenu.')" class="btn btn-success">ON</button></td>';
                                    else
                                        echo '<td id="btn_'.$menu->idMenu.'"><button onclick="update('.$menu->idMenu.')" class="btn btn-danger">OFF</button></td>';
                                    echo '<td><a href="'.Yii::app()->createUrl('admin/mupdate/'.$menu->idMenu).'"><i class="fa fa-edit"></i></a>&nbsp;<a class="text-red" href="#" class="btn btn-default" onclick="mdelete('.$menu->idMenu.')"><i class="fa fa-trash"></i></a></td>';
                                        echo '</tr>';
                                  }
                                ?>
                                    </tbody>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>P_Id</th>
                                    <th>Name</th>
                                    <th>Link</th>
                                    <th>Title</th>
                                    <th>Icon</th>
                                    <th>Order</th>
                                    <th>Access</th>
                                    <th>Has_chevron</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                                </tfoot>
                            </table>
                            <br>
                            <br>

                        </div><!-- /.box-body -->
                    </div>




                </div><!-- /.tab-pane -->



            </div><!-- /.tab-content -->
        </div><!-- /.nav-tabs-custom -->
    </section>
        <script>
            $(function () {
                $("#myTable").DataTable();

            });
        </script>
</section>
