<?php
/* @var $this UserMenuController */
/* @var $model UserMenu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-menu-form',
    'errorMessageCssClass'=>'has-error',
    'enableAjaxValidation'=>true,
    'clientOptions'=> array(
           ),
));

$user=UserAccess::model()->findAll();
$data=array();

foreach($user as $access){
    $data[$access->id]=$access->access;
}
?>

	<p class="note">Ҳама марзаҳо боянд пур шаванд.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'parentId'); ?>
		<?php echo $form->textField($model,'parentId', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'parentId',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'name',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'href'); ?>
		<?php echo $form->textField($model,'href',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'href',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'icon'); ?>
		<?php echo $form->textField($model,'icon',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'icon',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'orderId'); ?>
		<?php echo $form->textField($model,'orderId', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'orderId',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'accessId'); ?>
		<?php echo $form->dropDownList($model,'accessId',$data, array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'accessId',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'has_chevron'); ?>
		<?php echo $form->dropDownList($model,'has_chevron',array('yes'=>'yes','no'=>'no'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'has_chevron',array('class'=>'text-red')); ?>
	</div>
	<div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохтан' : 'Азнавкардан'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->