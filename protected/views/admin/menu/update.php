<?php
/* @var $this UserMenuController */
/* @var $model UserMenu */

$this->breadcrumbs=array(
	'User Menu'=>array('admin/panel'),
	'#'.$model->idMenu,
);

$this->menu=array(
	array('label'=>'List UserMenu', 'url'=>array('index')),
	array('label'=>'Create UserMenu', 'url'=>array('create')),
	array('label'=>'View UserMenu', 'url'=>array('view', 'id'=>$model->idMenu)),
	array('label'=>'Manage UserMenu', 'url'=>array('admin')),
);
?>

<h1>Update Menu #<?php echo $model->idMenu; ?></h1>
<section class="content">
<?php echo $this->renderPartial('menu/_form', array('model'=>$model)); ?>
</section>