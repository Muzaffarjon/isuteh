<script>
    $(document).ready(function(){
        var _st;
       $('a#activate').click(function(){
           $.ajax({
               type: 'POST',
               url: '<?php echo Yii::app()->createUrl('admin/settings') ?>',
               data:{check:1},
               success: function(data){
                       window.location.reload();
               }
           });
       });
    });
    </script>
<div class="row">
<div class="col-md-2">
<!-- Application buttons -->
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Доступ к сайту</h3>
    </div>
    <div class="box-body">
        <?php
        $activity=Users::getStatus();
        if($activity==0){
            echo '<a id="activate" class="btn btn-app text-green">
            <i class="fa fa-play"></i> Дастрасӣ
        </a>';
        }elseif($activity==1)
        echo '<a id="activate" class="btn btn-app text-red">
            <i class="fa fa-pause"></i> Қатъ кардан
        </a>';
        ?>



    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- Vertical grouping -->
</div><!-- /.col -->

    <div class="col-md-6">

        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title">Input masks</h3>
            </div>
            <div class="box-body">
                <!-- Date dd/mm/yyyy -->
                <!-- IP mask -->
                <div class="form-group">
                    <label>IP mask:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-laptop"></i>
                        </div>
                        <input type="text" class="form-control" data-inputmask="'alias': 'ip'" data-mask="" placeholder="255.255.255.255">
                    </div><!-- /.input group -->
                </div><!-- /.form group -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->



    </div>
</div>