<script>
    function updateUser(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/userstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('button').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('button').removeAttr('disabled');
                $('#btn_' + id).load("<?php echo Yii::app()->createUrl('admin/students')?> #btn_" + id);
            }
        })
    }

    function updateUall(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/userallstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('button#test1').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('button#test1').removeAttr('disabled');
                location.reload();
            }
        })
    }
</script>
<div class="box box-danger">

    <button style="margin: 5px" onclick="updateUall(0)" id="test1" class="btn btn-danger">Қатъ кардан
    </button>
    <button style="margin: 5px" onclick="updateUall(1)" id="test1" class="btn btn-success">Азнав кардан
    </button>
    <div class="box-body">
        <table id="uTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
            <thead>
            <tr role="row">
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Вазифа</th>
                <th>Логин</th>
                <th>Ҳолат</th>
                <th><i class="fa fa-edit text-blue"></i> Амалиёт</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            $user=User::model()->findAll(array('condition'=>'accessId not in (1) and not id='.Yii::app()->session['user']['id']));

            foreach ($user as $users) {
                echo '<tr>';
                echo '<td>' . $i++ . '</td>';
                echo '<td>' . $users->fname . ' ' . $users->name . ' ' . $users->lname . '</td>';
                echo '<td>'.$users->access->access.'</td>';
                echo '<td>' . $users->login . '</td>';
                if ($users->status == 1)
                    echo '<td id="btn_' . $users->id . '"><button onclick="updateUser(' . $users->id. ')" class="btn btn-success">ON</button></td>';
                else
                    echo '<td id="btn_' . $users->id. '"><button onclick="updateUser(' . $users->id. ')" class="btn btn-danger">OFF</button></td>';
                echo '<td><a href="' . Yii::app()->createUrl('admin/uupdate/' . $users->id) . '" class="btn btn-primary"><i class="fa fa-edit"></i> Азнавкардан</a></td>';
                echo '</tr>';
                //echo '<br>';
            }
            ?>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Гурӯҳ</th>
                <th>Логин</th>
                <th>Ҳолат</th>
                <th><i class="fa fa-edit text-blue"></i> Амалиёт</th>
            </tr>
            </tfoot>
        </table>
        <br>
        <br>
        <table>
            <tr>
                <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
            </tr>
        </table>
    </div>
    <!-- /.box-body -->
</div>