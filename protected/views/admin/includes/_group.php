<?php

$group=Group::model()->findAll();
$countSt1=Group::model()->findAll(array('condition'=>'status=1'));
$countSt0=Group::model()->findAll(array('condition'=>'status=0'));
?>
<script>
    function updateGroup(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/groupstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('.group').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('.group').removeAttr('disabled');
                $('#btn3_' + id).load("<?php echo Yii::app()->createUrl('admin/students')?> #btn3_" + id);
            }
        })
    }

    function updateGall(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/groupallstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('.group').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('.group').removeAttr('disabled');
                location.reload();
            }
        })
    }
</script>
<div class="box box-primary">

    <?php
    if(count($group)==count($countSt0)){
        echo '<button disabled="disabled" style="margin: 5px" id="test" class="btn btn-danger">Қатъ кардан
    </button>';
    }else echo '<button style="margin: 5px" onclick="updateGall(0)" id="test" class="btn btn-danger">Қатъ кардан
    </button>';

    if(count($group)==count($countSt1)){
    echo '<button disabled="disabled" style="margin: 5px" id="test" class="btn btn-success">Азнав кардан
    </button>';
    }else echo '<button style="margin: 5px" onclick="updateGall(1)" id="test" class="btn btn-success">Азнав кардан
    </button>';

    ?>


    <div class="box-body">
        <table id="gTable" class="table table-bordered table-hover dataTable" width="100%">
            <thead>
            <tr role="row">
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Миқдор</th>
                <th>Куратор</th>
                <th>Ҳолат</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;


            foreach ($group as $groups) {
                echo '<tr>';
                echo '<td>' . $i++ . '</td>';
                echo '<td>' . $groups->course.'_'.$groups->ihtisos->code.$groups->class.'</td>';
                echo '<td>'.Users::countGroupins($groups->idGroup).'</td>';
                echo '<td></td>';
                if ($groups->status == 1)
                    echo '<td id="btn3_' . $groups->idGroup . '"><button onclick="updateGroup(' . $groups->idGroup. ')" class="btn btn-success group">ON</button></td>';
                else
                    echo '<td id="btn3_' . $groups->idGroup. '"><button onclick="updateGroup(' . $groups->idGroup. ')" class="btn btn-danger group">OFF</button></td>';
                echo '</tr>';
                //echo '<br>';
            }
            ?>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Гурӯҳ</th>
                <th>Логин</th>
                <th>Ҳолат</th>
            </tr>
            </tfoot>
        </table>
        <br>
        <br>
        <table>
            <tr>
                <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
            </tr>
        </table>
    </div>
    <!-- /.box-body -->
</div>