<script>
    function update(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/userstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('button').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('button').removeAttr('disabled');
                $('#btn_' + id).load("<?php echo Yii::app()->createUrl('admin/students')?> #btn_" + id);
            }
        })
    }

    function updateAll(id) {
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl('admin/userstudentstatus') ?>",
            cache: false,
            data: {id: id},
            error: function () {

            },
            beforeSend: function () {
                $('button#test').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data = 1)
                    $('button#test').removeAttr('disabled');
                location.reload();
            }
        })
    }

</script>

<div class="box box-success">

    <button style="margin: 5px" onclick="updateAll(0)" id="test" class="btn btn-danger">Қатъ кардан
    </button>
    <button style="margin: 5px" onclick="updateAll(1)" id="test" class="btn btn-success">Азнав кардан
    </button>
    <div class="box-body">
        <table id="myTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
            <thead>
            <tr role="row">
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Гурӯҳ</th>
                <th>Логин</th>
                <th>Ҳолат</th>
                <th><i class="fa fa-edit text-blue"></i> Амалиёт</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;


            foreach ($student as $students) {
                echo '<tr>';
                echo '<td>' . $i++ . '</td>';
                echo '<td>' . $students->fname . ' ' . $students->name . ' ' . $students->lname . '</td>';
                echo '<td>' .Users::returnGroup($students->id). '</td>';
                echo '<td>' . $students->login . '</td>';
                if ($students->status == 1)
                    echo '<td id="btn_' . $students->id. '"><button onclick="update(' . $students->id . ')" class="btn btn-success">ON</button></td>';
                else
                    echo '<td id="btn_' . $students->id . '"><button onclick="update(' . $students->id . ')" class="btn btn-danger">OFF</button></td>';
                echo '<td><a href="' . Yii::app()->createUrl('admin/uupdate/' . $students->id) . '" class="btn btn-primary"><i class="fa fa-edit"></i> Азнавкардан</a></td>';
                echo '</tr>';
                //echo '<br>';
            }
            ?>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Н. Н. Д.</th>
                <th>Гурӯҳ</th>
                <th>Логин</th>
                <th>Ҳолат</th>
                <th><i class="fa fa-edit text-blue"></i> Амалиёт</th>
            </tr>
            </tfoot>
        </table>
        <br>
        <br>
        <table>
            <tr>
                <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
            </tr>
        </table>
    </div>
    <!-- /.box-body -->
</div>