<?php
$this->pageTitle=Yii::app()->name .' - Руйхати муаллимаҳо';

$this->breadcrumbs=array(
    'Руйхати муаллимаҳо'
);
?>
<section class="content">
<a class="btn btn-app" href="<?php echo Yii::app()->createUrl('admin/cteacher'); ?>">
    <i class="fa fa-edit"></i> Ворид кардан
</a>
<div style="overflow-y: auto; height: 500px;display: block">
    <table id="table_reload" class="table table-bordered table-striped table-hover table-responsive">
        <thead>
        <th>№</th>
        <th>Омӯзгор</th>
        <th>Дараҷа</th>
        <th>Стажи корӣ</th>
        <th>Рӯзи қабул</th>
        <th>Анҷоми кор</th>
        <th>Ҳолат</th>
        <th>Азнав</th>
        </thead>
        <tbody>
        <?php
        $i=1;
        foreach($model as $teacher){
            echo '<tr>';
            echo '<td>'.$i++.'</td>';
            echo '<td>'.$teacher->user->fname.' '.$teacher->user->name.'</td>';
            if($teacher->daraja=="easy")
            echo '<td>Нав</td>';
            elseif($teacher->daraja=="medium")
                echo '<td>Миёна</td>';
            else echo '<td>Баланд</td>';

            echo '<td align="center">'.$teacher->staj.'</td>';
            echo '<td align="center">'.$teacher->ruzi_kabul.'</td>';
            echo '<td align="center">'.$teacher->ruzi_raftan.'</td>';
            if($teacher->status==1){
                echo '<td align="center"><span class="label label-success">ON</span></td>';
            }else{
                echo '<td align="center"><span class="label label-danger">OFF</span></td>';
            }
            echo '<td><a class="update" href="'.Yii::app()->createUrl('admin/muallima/'.$teacher->id).'"><i class="fa fa-edit"></i></a></td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>
</section>