<?php
$this->pageTitle=Yii::app()->name .' - Ихтисос';

$this->breadcrumbs=array(
    'Ихтисос'
);
?>

<section class="content">
    <a class="btn btn-app" href="<?php echo Yii::app()->createUrl("kaid/icreate") ?>">
        <i class="fa fa-edit"></i> Ворид кардан
    </a>
    <table class="table table-bordered table-hover table stripped">
    <thead>
    <tr>
        <th>#</th>
        <th>Ихтисос</th>
        <th>Коди ихтисос</th>
        <th>Ҳолат</th>
        <th>Азнавкардан</th>
    </tr>
    </thead>

        <tbody>


    <?php
$i=1;
 foreach($model as $ihtisos){
     echo '<tr>';
     echo '<td>'.$i++.'</td>';
     echo '<td>'.$ihtisos->ihtisos.'</td>';
     echo '<td>'.$ihtisos->code.'</td>';
     if ($ihtisos->status == 1)
         echo '<td id="btn_' . $ihtisos->id. '"><button onclick="update(' . $ihtisos->id . ')" class="btn btn-success">ON</button></td>';
     else
         echo '<td id="btn_' . $ihtisos->id . '"><button onclick="update(' . $ihtisos->id . ')" class="btn btn-danger">OFF</button></td>';
     echo '<td><a class="btn btn-primary" href="'.Yii::app()->createUrl("kaid/updateihtisos/".$ihtisos->id).'"><i class="fa fa-pencil text-red"></i> Азнавкардан</td>';
 }
    echo '</tr>';
    ?>
        </tbody>
        <tfoot>
        <th>#</th>
        <th>Ихтисос</th>
        <th>Коди ихтисос</th>
        <th>Ҳолат</th>
        <th>Азнавкардан</th>
        </tfoot>
    </table>
</section>