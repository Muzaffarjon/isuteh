<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-from',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),

)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php //echo $form->labelEx($model,'fname'); ?>
		<?php echo $form->dropDownList($model,'ihtisosId',CHtml::listData(Ihtisos::model()->findAll(),'id','code'),array('class'=>'form-control','placeholder'=>'Ихтисос')); ?>
		<?php echo $form->error($model,'ihtisosId',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php //echo $form->labelEx($model,'lname'); ?>
		<?php echo $form->textField($model,'course',array('size'=>11,'maxlength'=>11,'class'=>'form-control','placeholder'=>'Курс')); ?>
		<?php echo $form->error($model,'course',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
		<?php //echo $form->labelEx($model,'birthday'); ?>
		<?php echo $form->textField($model,'class',array('class'=>'form-control','placeholder'=>'точики ё русӣ, мисол, та ё тб ё ра, рб')); ?>
		<?php echo $form->error($model,'class',array('class'=>'text-red')); ?>
	</div>
	<div class="box-footer clearfix">
		<?php echo CHtml::submitButton('Создать',array('class'=>'pull-right btn btn-default', 'id'=>"")); ?>
	</div>

<?php $this->endWidget(); ?>

