<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),

));

$login="st";
$rand=mt_rand(10000,99999);
$login=$login.$rand;
?>
	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Логин'); ?>
		<?php echo $form->textField($model,'login',array('size'=>50,'maxlength'=>50,'class'=>'form-control','value'=>$login,'disabled'=>'disabled')); ?>
		<?php echo $form->error($model,'login',array('class'=>'text-red')); ?>
	</div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'Насаб'); ?>
		<?php echo $form->textField($model,'fname',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Насаб')); ?>
		<?php echo $form->error($model,'fname',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Ном'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Ном')); ?>
		<?php echo $form->error($model,'name',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Номи падар'); ?>
		<?php echo $form->textField($model,'lname',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Номи падар')); ?>
		<?php echo $form->error($model,'lname',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Рӯзи таваллуд'); ?>
		<?php echo $form->textField($model,'birthday',array('class'=>'form-control','placeholder'=>'Мисол: 1996-10-13',"data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
		<?php echo $form->error($model,'birthday',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Ҷинс'); ?>
		<?php echo $form->dropDownList($model,'gender',array(0=>'Мард',1=>'Зан'),array('maxlength'=>1,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'gender',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Суроға'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100,'class'=>'form-control','placeholder'=>'Суроға')); ?>
		<?php echo $form->error($model,'address',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Рақами телефонӣ'); ?>
		<?php echo $form->textField($model,'number_phone',array('size'=>13,'maxlength'=>18,'class'=>'form-control','value'=>'+(992) ')); ?>
		<?php echo $form->error($model,'number_phone',array('class'=>'text-red')); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Ҷои зист'); ?>
		<?php echo $form->dropDownList($model,'cityId',CHtml::listData(City::model()->findAll('id'), 'id', 'city'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cityId',array('class'=>'text-red')); ?>
	</div>

	
	<div class="box-footer clearfix">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохтан' : 'Азнавкардан',array('class'=>$model->isNewRecord ? 'btn bg-purple' : 'btn btn-success', 'id'=>"sendEmail")); ?>
	</div>

<?php $this->endWidget(); ?>

