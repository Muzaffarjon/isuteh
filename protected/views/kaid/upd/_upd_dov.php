<section id="mess" class="col-lg-7" style="display: none">
        <div class="flash-success" style="font-size: 18px;text-align: justify">
            <div class="callout callout-success">
                <p>Маълумот азнав карда шуд!.</p>
            </div>

        </div>

</section>

<section id="contC" class="col-lg-7 connectedSortable ui-sortable">
    <div class="box box-info" style="position: relative;">
        <div class="box-header ui-sortable-handle" style="cursor: pointer;">
            <i class="fa fa-envelope"></i>
            <h3 class="box-title">Азнавкардани довталабон</h3>

        </div>
        <div class="box-body">
            <?php echo $this->renderPartial('upd/_update_dovtalab', array('model'=>$model)); ?>
        </div>
    </div>
</section>