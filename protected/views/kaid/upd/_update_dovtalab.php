<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    function sendUser(id){
        var form_data = $('#forma_'+id).serialize();

            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl("/kaid/upddovtalab/".$model->id);?>",
                data: form_data,
                cache: false,
                error: function(){
                    //alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
                },
                beforeSend: function(){

                },
                success: function(data){
                    if(data==1){
                        $('#mess').show();
                        $('#contC').hide();
                        function setTime(){
                            $.fancybox.close();
                            history.go(0);
                        }
                        setTimeout(setTime,2000);

                    }else{
                        alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
                    }

                }
            });
        return false;
    }
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'forma_'.$model->id,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
        'errorCssClass'=>'text-red',
        'successCssClass'=>'text-green',
        'afterValidate' => 'js:sendUser'
    ),
    'htmlOptions'=>array('onSubmit'=>'return false;')
)); ?>
<?php echo $form->errorSummary($model); ?>

<div class="form-group">
    <?php echo $form->textField($model,'login',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Насаб','disabled'=>'disabled')); ?>
    <?php echo $form->error($model,'login',array('class'=>'text-red')); ?>
</div>
<div class="form-group">
    <?php echo $form->textField($model,'fname',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Насаб')); ?>
    <?php echo $form->error($model,'fname',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Ном')); ?>
    <?php echo $form->error($model,'name',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->textField($model,'lname',array('size'=>50,'maxlength'=>50,'class'=>'form-control','placeholder'=>'Номи падар')); ?>
    <?php echo $form->error($model,'lname',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->textField($model,'birthday',array('class'=>'form-control','placeholder'=>'Мисол: 1996-10-13',"data-provide"=>"datepicker", 'data-date-format'=>"yyyy-mm-dd")); ?>
    <?php echo $form->error($model,'birthday',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->dropDownList($model,'gender',array(0=>'Мард',1=>'Зан'),array('maxlength'=>1,'class'=>'form-control')); ?>
    <?php echo $form->error($model,'gender',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100,'class'=>'form-control','placeholder'=>'Суроға')); ?>
    <?php echo $form->error($model,'address',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->textField($model,'number_phone',array('size'=>13,'maxlength'=>20,'class'=>'form-control','placeholder'=>'Намуна: +(992) 92 8344074')); ?>
    <?php echo $form->error($model,'number_phone',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php echo $form->dropDownList($model,'cityId',CHtml::listData(City::model()->findAll('id'), 'id', 'city'),array('class'=>'form-control')); ?>
    <?php echo $form->error($model,'cityId',array('class'=>'text-red')); ?>
</div>


<div class="box-footer clearfix">
    <?php echo CHtml::submitButton('Азнавкардан',array('class'=>'pull-right btn btn-default', 'id'=>"sendUpdate",'onClick'=>'sendUser('.$model->id.');')); ?>
</div>

<?php $this->endWidget(); ?>

