<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    function sendGroup(id){
        var form_data = $('#group_'+id).serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl("/kaid/updgroup/".$model->idGroup);?>",
            data: form_data,
            cache: false,
            error: function(){
                //alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
            },
            beforeSend: function(){

            },
            success: function(data){
                if(data==1){
                    $('#mess').show();
                    $('#contC').hide();
                    function setTime(){
                        $.fancybox.close();
                        history.go(0);
                    }
                    setTimeout(setTime,2000);

                }else{
                    alert("Ошибка!!! Повторите по позже, пожалуйста!") ;
                }

            }
        });
        return false;
    }
</script>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'group_'.$model->idGroup,
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
        'errorCssClass'=>'form-control has-error',
        'successCssClass'=>'form-control has-error',

    ),
    'htmlOptions'=>array('onsubmit'=>'return false;')
)); ?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<div class="form-group">
    <?php //echo $form->labelEx($model,'fname'); ?>
    <?php echo $form->dropDownList($model,'ihtisosId',CHtml::listData(Ihtisos::model()->findAll(),'id','code'),array('class'=>'form-control','placeholder'=>'Ихтисос')); ?>
    <?php echo $form->error($model,'ihtisosId',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php //echo $form->labelEx($model,'lname'); ?>
    <?php echo $form->textField($model,'course',array('class'=>'form-control','placeholder'=>'Курс')); ?>
    <?php echo $form->error($model,'course',array('class'=>'text-red')); ?>
</div>

<div class="form-group">
    <?php //echo $form->labelEx($model,'birthday'); ?>
    <?php echo $form->textField($model,'class',array('class'=>'form-control','placeholder'=>'точики ё русӣ, мисол, та ё тб ё ра, рб')); ?>
    <?php echo $form->error($model,'class',array('class'=>'text-red')); ?>
</div>
<div class="box-footer clearfix">
    <?php echo CHtml::submitButton('Азнавкардан',array('class'=>'pull-right btn btn-default', 'id'=>"sendEmail",'onClick'=>'sendGroup('.$model->idGroup.');')); ?>
</div>

<?php $this->endWidget(); ?>

