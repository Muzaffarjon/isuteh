<?php
$this->pageTitle=Yii::app()->name .' - Руйхати довталабон';

$this->breadcrumbs=array(
    'Руйхати довталабон'
);

$models=Group::model()->findAll(array('condition'=>'status=1'));
$data=array();
foreach($models as $m)
    $data[$m->idGroup]=$m->course.'_'.$m->ihtisos->code.$m->class;
?>
<script>
      $(function () {
        $("#myTable").DataTable();
      });
    function changeDrop(id){
        $('#but_'+id).removeAttr('disabled');
        $('#but_'+id).removeClass('btn-primary');
        $('#but_'+id).addClass('btn-success');
    }
    function sendGroup(id){
        data=$('#form_'+id).serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->createUrl("kaid/addgroup");?>",
            data: data,
            cache: false,
            error: function() {
               alert('nawud');
            },
            beforeSend: function() {
                $('input[button]').attr('disabled');
            },
            success: function(data) {
                if(data=1){
                    $('input[button]').attr('disabled');
                $('tr#obn_'+id).fadeOut();
                //history.go(0);
                }
            }
        });
    }
</script>
<section id="con" class="content table-responsive">
     <table id="myTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
                                              <thead>
                                                  <tr role="row">
                                                        <th>#</th>
                                                        <th>Н. Н. Д.</th>
                                                        <th>Соли таваллуд</th>
                                                        <th>Ҷинс</th>
                                                        <th>Телефон</th>
                                                        <th>Суроға</th>
                                                        <th>Амалиёт</th>
                                                   </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                  $i=1;
                                                  $users = User::model()->findAll(array('condition' =>'accessId=1'));

                                                  foreach ($users as $user) {
                                                      $check = Users::checkUserInGroupin($user->id);
                                                      if (empty($check)) {
                                                      echo '<tr id="obn_'.$user->id.'" disabled="disabled">';
                                                      echo '<td>'.$i++.'</td>';
                                                         echo '<td>'.$user->fname.' '.$user->name.' '.$user->lname.'</td>';
                                                         echo '<td>'.$user->birthday.'</td>';
                                                         
                                                         if($user->gender==1){
                                                             echo '<td> Зан </td>';
                                                         }else{
                                                             echo '<td> Мард </td>';
                                                         }
                                                          echo '<td>'.$user->number_phone.'</td>';
                                                          echo '<td>'.$user->address.'</td>';
                                                          echo '<td><form method="post" id="form_'.$user->id.'" onsubmit="return false;"><input type="hidden" value="'.$user->id.'" name="uId">'.CHtml::dropDownList('gId','text',$data,array('id'=>'drop_'.$user->id,'onClick'=>'changeDrop('.$user->id.')')).' &nbsp; <button class="btn btn-primary btn-xs" id="but_'.$user->id.'" onclick="sendGroup('.$user->id.')" disabled="disabled"><i class="fa fa-group"></i>&nbsp;Дохил кардан</button></form></td>';
                                                      echo '</tr>';     
                                                      } 
                                                        


                                                      //echo '<br>';
                                                  }
                                                  ?> 
                                              <tfoot>
                                                  <tr role="row">
                                                        <th>#</th>
                                                        <th>Н. Н. Д.</th>
                                                        <th>Соли таваллуд</th>
                                                        <th>Ҷинс</th>
                                                        <th>Телефон</th>
                                                        <th>Суроға</th>
                                                        <th>Амалиёт</th>
                                                   </tr>
                                              </tfoot>
                                          </table>
    
</section>