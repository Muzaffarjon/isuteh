<?php
$this->pageTitle=Yii::app()->name .' - Қайди довталабон';

$this->breadcrumbs=array(
    'Қайди довталабон'
);
?>
<script>
      $(function () {
        $("#myTable").DataTable();
      });
</script>
<section class="content">
    <div class="row">

        <div class="col-md-6">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
             <div class="box box-info" style="position: relative;">
                <div class="box-header ui-sortable-handle" style="cursor: pointer;">
                  <i class="fa fa-user-plus text-yellow"></i>
                  <h3 class="box-title">Қайди довталабон</h3>

                </div>
                 <div class="box-body">
                <?php echo $this->renderPartial('includes/_dovt', array('model'=>$model)); ?>
              </div>
              </div>
        </div>
        <section id="_upd" class="col-lg-6 connectedSortable ui-sortable">
            <div class="box box-success"  style="position: relative;">
                <div class="box-header ui-sortable-handle" style="cursor: pointer;">
                  <i class="fa fa-users text-blue"></i>
                  <h3 class="box-title">Қайди довталабон</h3>
                </div>
                <div class="box-body">
                                    <table id="myTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
                                              <thead>
                                                  <tr role="row">
                                                        <th>#</th>
                                                        <th>Н. Н. Д.</th>

                                                        <th>Амалиёт</th>
                                                   </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                  $i=1;
                                                  $users = User::model()->findAll(array('condition' =>'accessId=1 and status=1'));

                                                  foreach ($users as $user) {
                                                      //$check = Users::checkUserInGroupin($user->id);
                                                      //if (empty($check)) {
                                                      echo '<tr>';
                                                      echo '<td>'.$i++.'</td>';
                                                         echo '<td>'.$user->fname.' '.$user->name.' '.$user->lname.'</td>';
                                                         echo '<td><a href="'.Yii::app()->createUrl("kaid/upddovtalab/".$user->id).'" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Азнавкардан</a></td>';
                                                      echo '</tr>';     
                                                      //} 



                                                      //echo '<br>';
                                                  }
                                                  ?> 
                                              <tfoot>
                                                  <tr role="row">
                                                        <th>#</th>
                                                        <th>Н. Н. Д.</th>
                                                        <th>Амалиёт</th>
                                                   </tr>
                                              </tfoot>
                                    </table>
                    </div>
                </div>



        </section>
    </div>


</section>
