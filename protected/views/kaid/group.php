<?php
$this->pageTitle=Yii::app()->name .' - Ташкили гурӯҳ';

$this->breadcrumbs=array(
    'Ташкили гурӯҳ'
);
?>
<script>
      $(function () {
        $("#myTable").DataTable();
      });
</script>

<section class="content">
    <div class="row">

 <section class="col-lg-7 connectedSortable ui-sortable">
     <?php if (Yii::app()->user->hasFlash('success')): ?>
         <div class="alert alert-success alert-dismissable">
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             <?php echo Yii::app()->user->getFlash('success'); ?>
         </div>
     <?php endif; ?>
            <div class="box box-info" style="position: relative;">
                 <div class="box-header ui-sortable-handle" style="cursor: pointer;">
                     <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Қайди гурӯҳ</h3>
                 
                </div>
                 <div class="box-body">
                <?php echo $this->renderPartial('includes/_group', array('model'=>$model)); ?>
              </div>
              </div>
        </section>
        <section class="col-lg-5 connectedSortable ui-sortable">
            <div class="box box-success" style="position: relative;">
                <div class="box-header ui-sortable-handle" style="cursor: pointer;">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Руйхати гурӯҳ</h3>
                </div>
                <div class="box-body">
                    </div>
                <table id="myTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
                    <thead>
                    <tr role="row">
                        <th>#</th>
                        <th>Н. Н. Д.</th>

                        <th>Амалиёт</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    $groups = Group::model()->findAll(array('condition' =>'status=1'));

                    foreach ($groups as $group) {
                        //$check = Users::checkUserInGroupin($user->id);
                        //if (empty($check)) {
                        echo '<tr>';
                        echo '<td>'.$i++.'</td>';
                        echo '<td>'.$group->course.'_'.$group->ihtisos->code.$group->class.'</td>';
                        echo '<td><a href="'.Yii::app()->createUrl('kaid/updgroup/'.$group->idGroup).'" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Азнавкардан</a></td>';
                        echo '</tr>';
                        //}



                        //echo '<br>';
                    }
                    ?>
                    <tfoot>
                    <tr role="row">
                        <th>#</th>
                        <th>Н. Н. Д.</th>
                        <th>Амалиёт</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </section>

    </div>
   
</section>