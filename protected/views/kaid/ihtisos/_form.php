<?php
/* @var $this IhtisosController */
/* @var $model Ihtisos */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ihtisos-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),

)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ihtisos'); ?>
		<?php echo $form->textField($model,'ihtisos',array('size'=>50,'maxlength'=>250,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'ihtisos'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>50, 'minlength'=>5,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохтан' : 'Сабт кардан',array('class'=>'pull-left btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

