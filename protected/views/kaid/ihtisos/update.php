<?php
/* @var $this IhtisosController */
/* @var $model Ihtisos */

$this->breadcrumbs=array(
	'Ихтисосҳо'=>array('kaid/ihtisos'),
	'Азнавкардани ихтисоси '.$model->id,
);
?>

<h1>Азнавкардани ихтисосӣ <?php echo $model->id; ?></h1>
<section class="content">
<?php echo $this->renderPartial('ihtisos/_form', array('model'=>$model)); ?>
</section>