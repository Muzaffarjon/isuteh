<?php
/* @var $this IhtisosController */
/* @var $model Ihtisos */

$this->breadcrumbs=array(
	'Ихтисосҳо'=>array('kaid/ihtisos'),
	'Сохтан',
);
?>

<h1>Сохтани Ихтисос</h1>
<section class="content">
<?php echo $this->renderPartial('ihtisos/_form', array('model'=>$model)); ?>
</section>