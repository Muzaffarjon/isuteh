<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 23.03.16
 * Time: 15:43
 */
?>
<script>
    $(document).ready(function(){

        $("#ajaxRequest").scrollTop($("#ajaxRequest")[0].scrollHeight);

        var checkId=<?php echo $model->id; ?>;

        function setTimer() {
        $.ajax({
                cache: false,
                url: '<?php echo Yii::app()->createUrl('site/ajaxRequest') ?>',
                async: false,
                data: {checkId: checkId},
                success: function (data) {
                    $('#ajaxRequest').html(data);
                    $("#ajaxRequest").scrollTop($("#ajaxRequest")[0].scrollHeight);
                }
            });
        }



            if (timer !== null) return;
            timer = setInterval(function () {
                setTimer();
            }, interval);

        $("#stop").click(function() {
            clearInterval(timer);
            timer = null
        });

        //setInterval(setTimer,5000);
        $('#setMessage').click(function(){
           text=$('#textMessage').val();
            recId=<?php echo $model->id;?>;
            if(text!=""){
                $.ajax({
                   type: 'POST',
                    url: '<?php echo Yii::app()->createUrl('site/post') ?>',
                    data: {text: text,recId:recId},
                    success: function(){
                        $('#textMessage').val('');

                      }
                });
            }else{
                $('#errorMessage').addClass('has-error');
            }
        });
    });
/*    function load_page() {
        $("body").load('/echo/html/',{html:"лоҷлаова",delay:1},function(data){console.log('hit')});
    }
    $(function(){
        setInterval("load_page()",1000);
    });
*/
</script>
<div class="box box-danger direct-chat direct-chat-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $model->fname.' '.$model->name ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <!-- Conversations are loaded here -->
        <div id="ajaxRequest" class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <?php
            if(!empty($messages)){
            foreach($messages as $mess){
                if($mess->senderId==Yii::app()->user->id){
                Users::getMessage($mess->sender->fname.' '.$mess->sender->name,$mess->sendTime,$mess->sender->avatar,$mess->text);
                }elseif($mess->reciverId==Yii::app()->user->id){
                    Users::getMessage($mess->sender->fname.' '.$mess->sender->name,$mess->sendTime,$mess->sender->avatar,$mess->text,'right');
                }
            }
            }
            ?>
            <!-- Message to the right -->

        </div><!--/.direct-chat-messages-->
    </div><!-- /.box-body -->
    <div class="box-footer">
            <div id="errorMessage" class="input-group">
                <input type="text" name="message" id="textMessage" placeholder="Матни шумо ..." class="form-control">
                      <span class="input-group-btn">
                        <button type="button" id="setMessage" class="btn btn-danger btn-flat">Фирсондан</button>
                      </span>
            </div>
    </div><!-- /.box-footer-->
</div><!--/.direct-chat -->