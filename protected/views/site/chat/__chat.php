<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 23.03.16
 * Time: 13:07
 */
?>
<script>


$(document).ready(function(){
    $('#getChat').click(function(){
       text=$('#getLogin').val();
       if(text!=""){
           $('#load').show();
           $.ajax({
              type:'POST',
              url:'<?php echo Yii::app()->createUrl('site/checklogin') ?>',
               data: {login: text},
               cache: false,
               success: function(data){
                   $('#chat').html(data);
                    $('#getLogin').val('');
                   $('#load').hide();
               }
           });
       }else{
           $('#error').addClass('has-error');
       }
   });
});

function gotoMessage(id){
    if(id!=""){
        $('#load').show();
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl('site/gotomessage') ?>',
            data: {check: id},
            success:function(data){
                $('#load').hide();
                $('#chat').html(data);
            }
        });
    }
}

</script>

<section class="content">
    <div class="row">
        <div class="col-md-5">
            <!-- DIRECT CHAT DANGER -->
            <div class="box box-danger direct-chat direct-chat-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Руйхати истифодабаранда</h3>
                    <div class="box-tools pull-right">
                        <span data-toggle="tooltip" title="3 New Messages" class="badge bg-red"><?php
                            echo Users::getCountNotReadAllMessage();
                            ?></span>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        <ul class="contacts-list">
                           <?php
                            date_default_timezone_set('Asia/Dushanbe');
                           foreach ($chat as $message) {
                               $text=Users::getChatLastMessage($message->senderId);
                               $countNotReadMessage=Users::getCountNotReadMessage($message->senderId);

                               if($countNotReadMessage==0){
                                   $countNotReadMessage="";
                               }

                               $sendTime=new DateTime($txt->sendTime);

                              // echo '<li onclick="gotoMessage('.$message->id.')">';
                               echo '<li>';
                               echo '<a href="'.Yii::app()->createUrl('site/chat',array('id'=>$message->id)).'">';
                               //foreach($text as $txt){
                                   if($message->reciverId==Yii::app()->user->id){
                                   echo '<img class="contacts-list-img" src="'.Yii::app()->request->baseUrl.$message->sender->avatar.'">';
                                   echo '<div class="contacts-list-info">';
                                   echo '<span class="contacts-list-name text-black">';
                                       echo '<span style="font-size: 12px;">'.$message->sender->fname.' '.$message->sender->name.'</span>';
                                   }else{
                                       echo '<img class="contacts-list-img" src="'.Yii::app()->request->baseUrl.$message->reciver->avatar.'">';
                                       echo '<div class="contacts-list-info">';
                                       echo '<span class="contacts-list-name text-black">';
                                       echo '<span style="font-size: 12px;">'.$message->reciver->fname.' '.$message->reciver->name.'</span>';
                                   }
                               //}
                               echo '<small class="contacts-list-date pull-right">'.$sendTime->format('H:i d-M').'</small>';
                               echo '</span>';
                               foreach($text as $txt){
                               if(strlen($txt->text)>40){
                                   $txts=substr($txt->text,0,40-1);
                                   echo '<span class="contacts-list-msg">'.$txts.'...</span>';
                               }else
                               echo '<span class="contacts-list-msg">'.$txt->text.'</span>';
                               }
                               echo '<small class="contacts-list-msg pull-right"><span title="'.$countNotReadMessage.' New Messages"  class="badge bg-red">'.$countNotReadMessage.'</span>';
                               echo '</small>';
                               echo '</div>';
                               echo '</a>';
                               echo '</li>';
                           }

                           ?>

                        </ul><!-- /.contatcts-list -->
                    </div><!--/.direct-chat-messages-->

                    <!-- Contacts are loaded here -->

                </div><!-- /.box-body -->
                <div class="box-footer">
                        <div id="error" class="input-group">
                            <input type="text" id="getLogin" name="message" placeholder="Логин" class="form-control">
                      <span class="input-group-btn">
                        <button type="button" id="getChat" class="btn btn-danger btn-flat">Чат</button>
                      </span>
                        </div>
                </div>
                <div class="overlay" id="load" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div><!-- /.box-footer--><!-- /.box-footer-->
            </div><!--/.direct-chat -->
        </div>
<div id="chat" class="col-md-4">
    <?php
    echo $this->renderPartial('chat/_chat',array('messages'=>$messages,'model'=>$model));
    ?>

</div>
    </div>
</section>