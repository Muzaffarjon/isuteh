<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
$this->layout = 'main';
$this->breadcrumbs = array(
    'Acocӣ'
);


?>

<section class="content">
    <?php
    if (Yii::app()->user->checkAccess('1') && Yii::app()->user->id != 27) {
        ?>
        <div class="box-body">

            <!-- Small boxes (Stat box) -->
            <div class="callout alert-success alert-dismissable">
                <h4><i class="icon fa fa-warning"></i> &nbsp; Диққат!</h4>

                <p>Пеш аз рафтан ё тарк кардани система сараввал аз система бароед то ин ки дигар одамон ба аккаунти
                    шумо дастраси пайдо накунанд то ки пароли аккаунти шуморо иваз накунанд.</p>
            </div>

        </div>
        <div class="box-body">
            <?php
            if (!empty($alerts)) {
                $this->renderPartial('/students/activity/activity', array('alerts' => $alerts));
            }
            ?>

        </div>
    <?php
    }
    if (Yii::app()->user->checkAccess('8')) {
        //echo '1';
        $this->renderPartial('//admin/panel/index');
    }
    ?>
</section><!-- /.content -->
