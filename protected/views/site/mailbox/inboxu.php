<?php
$this->pageTitle=Yii::app()->name .' - Хабарҳои воридотӣ';
$this->breadcrumbs=array(
    'Хабарҳои воридотӣ'
);
?>
<script>
    function redirect(id){
        window.location.replace("<?php echo Yii::app()->createUrl('site/read',array('id'=>'')); ?>"+id);
    }

    function change(id){
        $.ajax({
            method: 'POST',
            url: '<?php echo Yii::app()->createUrl('site/change') ?>',
            data: {id:id}

        });
    }
    $(document).ready(function(){

        $('#update').click(function(){
            $('#load').show();

            $.ajax({
               url: '<?php echo Yii::app()->createUrl('site/inboxu') ?>',
                cache: false,
                success: function(html){
                    $('#load').hide();
                    $("#obnovaContent").html(html);
                }
            });

        });

    });

</script>

    <div  class="table-responsive mailbox-messages">


        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Хабарҳои воридотӣ</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="mailbox-controls">
                    <!-- Check all button -->
                    <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                        <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div><!-- /.btn-group -->
                    <button class="btn btn-default btn-sm" id="update"><i class="fa fa-refresh"></i></button>
                </div>
                <table id="example1" class="table table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Ирсолкунанда</th>
                        <th>Мавзӯъ</th>
                        <th width="150px" align="right">Вақти ирсол</th>
                    </tr>
                    </thead>
                    <tbody id="listMails">
                    <?php
                    $mail=Mailbox::model()->findAllByAttributes(array('reciverId'=>Yii::app()->user->id,'statusReciver'=>2),array('order'=>'datecreate DESC'));
                    if(!empty($mail)){
                        foreach($mail as $mailbox){
                            echo '<tr>';
                            echo '<td><input type="checkbox"></td>';
                            if($mailbox->notyReciver==0)
                            echo '<td class="mailbox-star"><a onclick="change('.$mailbox->id.')" href="#"><i class="fa fa-star-o text-yellow"></i></a></td>';
                            else
                                echo '<td class="mailbox-star"><a onclick="change('.$mailbox->id.')" href="#"><i class="fa fa-star text-yellow"></i></a></td>';
                            if($mailbox->viewReciver==0){
                            echo '<td onclick="redirect('.$mailbox->id2.')" ><img src="'.Yii::app()->request->baseUrl.$mailbox->sender->avatar.'" class="img-circle" width="30" height="30" alt="photo_reciver">&nbsp;<b>'.$mailbox->sender->fname.' '.$mailbox->sender->name.'</b></td>';
                            echo '<td><b>'.$mailbox->head.'</b></td>';
                            echo '<td><b>'.$mailbox->datecreate.'</b></td>';
                            }else{
                                echo '<td onclick="redirect('.$mailbox->id2.')" ><img src="'.Yii::app()->request->baseUrl.$mailbox->sender->avatar.'" class="img-circle" width="30" height="30" alt="photo_reciver">&nbsp;'.$mailbox->sender->fname.' '.$mailbox->sender->name.'</td>';
                                echo '<td>'.$mailbox->head.'</td>';
                                echo '<td>'.$mailbox->datecreate.'</td>';
                            }
                            echo '</tr>';
                        }
                    }else //echo '<td colspan="4">Шумо ягон мактуб ирсол накардаед</td>'
                    ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Ирсолкунанда</th>
                        <th>Мавзӯъ</th>
                        <th>Вақти ирсол</th>
                    </tr>
                    </tfoot>
                </table>
                <div class="mailbox-controls">
                    <!-- Check all button -->
                    <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                        <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div><!-- /.btn-group -->
                    <button class="btn btn-default btn-sm" id="update"><i class="fa fa-refresh"></i></button>
                </div>
            </div><!-- /.box-body -->
            <div class="overlay" id="load" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div><!-- /.box -->

    </div>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
        $(function () {
            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });

            //Handle starring for glyphicon and font awesome
            $(".mailbox-star").click(function (e) {
                e.preventDefault();
                //detect type
                var $this = $(this).find("a > i");
                var glyph = $this.hasClass("glyphicon");
                var fa = $this.hasClass("fa");

                //Switch states
                if (glyph) {
                    $this.toggleClass("glyphicon-star");
                    $this.toggleClass("glyphicon-star-empty");
                }

                if (fa) {
                    $this.toggleClass("fa-star");
                    $this.toggleClass("fa-star-o");
                }
            });
        });
    </script>
