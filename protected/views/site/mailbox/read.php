<?php
$this->pageTitle=Yii::app()->name .' - Мактуби '.$model->sender->fname.' '.$model->sender->name;
$this->breadcrumbs=array(
    'Мактубҳо'
);

$datetime=new DateTime($model->datecreate);
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="mailbox-read-info">
                <h3>Ирсолкунанда: <?php echo $model->sender->fname.' '.$model->sender->name ?></h3>
                <h5>Ба: <?php echo $model->reciver->fname.' '.$model->reciver->fname ?>  <span class="mailbox-read-time pull-right"><?php echo $datetime->format('H:m:s d-m-Y') ?></span></h5>
                <h5>Мавзӯъ: <?php echo $model->head; ?> </h5>
            </div><!-- /.mailbox-read-info -->
            <div class="mailbox-controls with-border text-center">
                    <!-- Check all button -->
                    <div class="btn-group">
                        <?php if($model->reciverId==Yii::app()->user->id){
                            $next=$model->id;
                            $last=$model->id;

                            ?>
                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></a>
                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-share"></i></a>
                        <?php } ?>
                    </div><!-- /.btn-group -->

            </div><!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
                Матн:<br>
                <?php
                echo $model->text;
                ?>
            </div><!-- /.mailbox-read-message -->
        </div><!-- /.box-body -->
        <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
                <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Sep2014-report.pdf</a>
                        <span class="mailbox-attachment-size">
                          1,245 KB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                    </div>
                </li>
                <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>
                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> App Description.docx</a>
                        <span class="mailbox-attachment-size">
                          1,245 KB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                    </div>
                </li>
            </ul>
        </div><!-- /.box-footer -->
        <div class="box-footer">
            <div class="pull-right">
                <?php
                if($model->senderId==Yii::app()->user->id){
                    echo '<button class="btn btn-default"><i class="fa fa-edit"></i> Ивазкардан</button>';
                }

                ?>
                <button class="btn btn-default"><i class="fa fa-trash-o"></i> Хориҷ кардан</button>
            </div>

        </div><!-- /.box-footer -->
    </div>
</section>