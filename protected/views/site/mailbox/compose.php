<?php
$this->pageTitle=Yii::app()->name . ' - Навиштани мактуб';
$this->breadcrumbs=array(
    'Навиштани мактуб',
);
?>
<section class="content">
    <?php if (Yii::app()->user->hasFlash('warning')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo Yii::app()->user->getFlash('warning'); ?>
        </div>
    <?php endif; ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Навиштани мактуби нав</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
<?php echo $this->renderPartial('mailbox/_create',array('model'=>$model))?>
        </div><!-- /.box-body -->
        <!-- /.box-footer -->
    </div><!-- /. box -->
    <script>
        $(function () {
            //Add text editor
            $("#compose-textarea").wysihtml5();
        });
    </script>
</section>