<?php
$this->pageTitle=Yii::app()->name .' - Мактубҳои фиристодашуда';
$this->breadcrumbs=array(
    'Мактубҳои фиристодашуда'
);
?>
<script>
    function redirect(id){
        window.location.replace("<?php echo Yii::app()->createUrl('site/read',array('id'=>'')); ?>"+id);
    }
</script>
<section class="content">
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <div class="table-responsive mailbox-messages">
<div class="box">
<div class="box-header">
    <h3 class="box-title">Мактубҳои фиристодашуда</h3>
</div><!-- /.box-header -->
<div class="box-body">
    <div class="mailbox-controls">
        <!-- Check all button -->
        <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
        <div class="btn-group">
            <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
            <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
            <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
        </div><!-- /.btn-group -->

    </div>
<table id="example1" class="table table-bordered table-hover" width="100%">
<thead>
<tr>
    <th></th>
    <th>Қабулкунанда</th>
    <th>Мавзӯъ</th>
    <th width="150px" align="right">Вақти ирсол</th>
</tr>
</thead>
<tbody>
<?php
$mail=Mailbox::model()->findAllByAttributes(array('senderId'=>Yii::app()->user->id,'statusSender'=>2),array('order'=>'datecreate DESC'));
if(!empty($mail)){
    foreach($mail as $mailbox){
    echo '<tr>';
        echo '<td><input type="checkbox"></td>';
        echo '<td onclick="redirect('.$mailbox->id2.')"><img src="'.Yii::app()->request->baseUrl.$mailbox->reciver->avatar.'" class="img-circle" style="border: 1px solid #909090;" width="35" height="35" alt="photo_reciver">&nbsp;&nbsp;'.$mailbox->reciver->fname.' '.$mailbox->reciver->name.'</td>';
        echo '<td><b>'.$mailbox->head.'</b></td>';
        echo '<td>'.$mailbox->datecreate.'</td>';
    echo '</tr>';
    }
}else //echo '<td colspan="3">Шумо ягон мактуб ирсол накардаед</td>'
?>

</tbody>
<tfoot>
<tr>
    <th></th>
    <th>Қабулкунанда</th>
    <th>Мавзӯъ</th>
    <th>Вақти ирсол</th>
</tr>
</tfoot>
</table>

</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
        <script>
            $(function () {
                $("#example1").DataTable();
            });
            $(function () {
                //Enable iCheck plugin for checkboxes
                //iCheck for checkbox and radio inputs
                $('.mailbox-messages input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue'
                });

                //Enable check and uncheck all functionality
                $(".checkbox-toggle").click(function () {
                    var clicks = $(this).data('clicks');
                    if (clicks) {
                        //Uncheck all checkboxes
                        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                    } else {
                        //Check all checkboxes
                        $(".mailbox-messages input[type='checkbox']").iCheck("check");
                        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                    }
                    $(this).data("clicks", !clicks);
                });

                //Handle starring for glyphicon and font awesome
                $(".mailbox-star").click(function (e) {
                    e.preventDefault();
                    //detect type
                    var $this = $(this).find("a > i");
                    var glyph = $this.hasClass("glyphicon");
                    var fa = $this.hasClass("fa");

                    //Switch states
                    if (glyph) {
                        $this.toggleClass("glyphicon-star");
                        $this.toggleClass("glyphicon-star-empty");
                    }

                    if (fa) {
                        $this.toggleClass("fa-star");
                        $this.toggleClass("fa-star-o");
                    }
                });
            });
        </script>
</section>