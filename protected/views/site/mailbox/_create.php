
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mailbox-form',
	'enableAjaxValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));


?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'reciverId'); ?>

		<?php
        if(isset($_GET['id'])){
            $id=(int)$_GET['id'];
            $findUser=User::model()->findByPk($id);
            echo $form->textField($model,'reciverId',array('class'=>'form-control','value'=>$findUser->login, 'id'=>"key"));

        }else{
            echo $form->textField($model,'reciverId',array('class'=>'form-control'));
        }

        ?>
		<?php echo $form->error($model,'reciverId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'head'); ?>
		<?php echo $form->textField($model,'head',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'head'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>10, 'id'=>'compose-textarea','cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'upload'); ?>
		<?php echo $form->fileField($model,'upload'); ?>
		<?php echo $form->error($model,'upload'); ?>
	</div>
<div class="box-footer">
    <div class="pull-right">
        <div class="row buttons">
            <?php echo CHtml::submitButton('Фиристодан',array('class'=>"btn btn-primary")); ?>
        </div>


    </div>
    <?php echo CHtml::resetButton('Тоза кардан',array('class'=>"btn btn-default")); ?>
</div>
<?php $this->endWidget(); ?>

