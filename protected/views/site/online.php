<?php
$this->pageTitle=Yii::app()->name . ' - Истифодабаранда онлайн';
$this->breadcrumbs=array(
    'Истифодабаранда онлайн',
);
?>
<section class="content">
    <div class="col-md-4">
        <!-- Info Boxes Style 2 -->
        <?php
        $text="Мутолиа";


        foreach($online as $onl){

            echo '<div class="info-box bg-white">';
            echo '<span class="info-box-icon bg-green"><img class="img-circle" style="margin-bottom: 10px;" src="'.Yii::app()->request->baseUrl.$onl->idUser->avatar.'" width="90" height="90"></span>';

                echo '<div class="info-box-content">';

                    echo '<span class="info-box-text">'.$onl->idUser->access->access.'</span>';
                    echo ' <span class="info-box-number">'.$onl->idUser->fname.' '.$onl->idUser->name.'</span>';

                        echo '<div class="progress">';
                        echo '<div class="progress-bar bg-blue" style="width: 20%"></div>';
                        echo '</div>';
                        echo '<span class="progress-description pull-right">';
            echo '<div class="btn-group">';
            echo '<a href="'.Yii::app()->createUrl('site/checklogin',array('login'=>$onl->idUser->login)).'" class="update btn btn-primary btn-xs" href="">Мутолиа</a>';
           /* echo CHtml::ajaxLink($text,$url,$ajaxOptions=array (
                    'type'=>'POST',
                    'data'=>'login:'.$onl->idUser->login.', test: 12',
                ),
                array ('class'=>'update btn btn-primary btn-xs')
            );
           */
            if(Yii::app()->user->checkAccess('2'))
                   echo '<a href="'.Yii::app()->createUrl('site/compose',array('id'=>$onl->idUser->id)).'" class="btn btn-success btn-xs" href="">Мактуб</a>';
                   echo '<a class="btn btn-info btn-xs" href="">Ҷӯрагӣ</a>';
            echo '</div>';
                 echo '</span>';

                echo ' </div>';

            echo '</div>';
        }
        ?>

    </div>

</section>