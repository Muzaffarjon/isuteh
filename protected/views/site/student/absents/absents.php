<?php
$groupin = Groupin::model()->findByAttributes(array('userId' => Yii::app()->session['user']['id']));
$group = Group::model()->findByPk($groupin->groupId);
$shedual = Shedual::model()->findAllByAttributes(
    array('groupId' => $group->idGroup, 'status' => 1)
);


$this->pageTitle = Yii::app()->name . ' - Нимсолаи Ҳозира';

$this->breadcrumbs = array(
    'Давомот' => array('site/discurr'),
    'Нимсолаи Ҳозира'
);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<section class="content">

    <p>Руйхати семетри ҳозира</p>

    <div style="overflow-x: auto;">
        <?php
        if (count($shedual) != null) {
            echo '<table class="table table-bordered table-striped dataTable" width="2700">';
            echo '<thead style="background-color: #ABAAAA;text-align:center">';
            echo '<th>№</th>';
            echo '<th style="text-align: center;">Фан</th>';
            echo '<th style="text-align: center;">Муаллим(а)</th>';
            echo '<th style="text-align: center;">Кредит</th>';
            echo '<th style="text-align: center;">Рӯзи 1</th>';
            echo '<th style="text-align: center;">Рӯзи 2</th>';
            echo '<th style="text-align: center;">Рӯзи 3</th>';
            echo '<th style="text-align: center;">Рӯзи 4</th>';
            echo '<th style="text-align: center;">Рӯзи 5</th>';
            echo '<th style="text-align: center;">Рӯзи 6</th>';
            echo '<th style="text-align: center;">Рӯзи 7</th>';
            echo '<th colspan=2 style="text-align: center;">Рӯзи 8 С. марҳилавӣ</th>';
            echo '<th style="text-align: center;">Рӯзи 9</th>';
            echo '<th style="text-align: center;">Рӯзи 10</th>';
            echo '<th style="text-align: center;">Рӯзи 11</th>';
            echo '<th style="text-align: center;">Рӯзи 12</th>';
            echo '<th style="text-align: center;">Рӯзи 13</th>';
            echo '<th style="text-align: center;">Рӯзи 14</th>';
            echo '<th style="text-align: center;">Рӯзи 15</th>';
            echo '<th colspan=2 style="text-align: center;">Рӯзи 16 С. марҳилавӣ</th>';
            echo '<th style="text-align: center;">Умумӣ</th>';
//    echo '<th>С. Устод</th>';
//    echo '<th>С. Ниҳои</th>';
//    echo '<th>Б. умумӣ</th>';
//    echo '<th>Баҳо</th>';
            echo '</thead>';
            echo '<tbody>';

            $i = 1;
            $ruzho = Type::model()->findAll(
                array(
                    'condition' => 'id NOT IN(19,20,21,22,23,24,25)'
                ));
            Yii::app()->session['count']=0;
            foreach ($shedual as $shed) {

                $subject = Subject::model()->findByPk($shed->subjectId);
                $teacher = Teachers::model()->findByPk($subject->teacherId);
                $user = User::model()->findByPk($teacher->userId);
                echo '<tr>';
                echo '<td>' . $i++ . '</td>';
                echo '<td><a href="Javascript:void()">' . $subject->subject->subject . '</a></td>';
                echo '<td width="200"><small><i class="fa  fa-user"></i>&nbsp;&nbsp;' . $user->fname . ' ' . $user->name . ' ' . $user->lname . '</small></td>';
                echo '<td>' . $subject->subject->credit . '</td>';


                foreach ($ruzho as $ruz) {
//        $scores=  Scores::model()->findByAttributes(
//        array('shedualId'=>$shed->subjectId, 'groupinId'=>$groupin->idGroupin)
//                );
                    $scores = Users::getAllAbsents($shed->idShedual, $groupin->idGroupin, $ruz->id);
                    $sum_score = Users::getAllSumAbsents($shed->idShedual, $groupin->idGroupin, $ruz->id);
                    $absents = Users::getAbsents($shed->idShedual, $groupin->idGroupin, $ruz->id);

                    if ($ruz->id == 9) {
                        if (count($scores) != NULL) {
                            ?>
                            <td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="label label-danger"><?php echo "ғ"; ?></span>
                                </a>

                            </td>

                        <?php

                        } else {
                            echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" >';
                            echo '-';
                            echo '</td>';
                        }
                    } elseif ($ruz->id == 18) {
                        if (count($scores) != NULL) {
                            ?>
                            <td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="label label-danger"><?php echo "ғ"; ?></span>
                                </a>

                            </td>

                        <?php

                        } else {
                            echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" >';
                            echo '-';
                            echo '</td>';
                        }
                    } elseif ($ruz->id == 19) {
                        if (count($scores) > 0) {
                            echo '<td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">' . $scores->bal . '</td>';
                            Users::SumOfPoints($shed->idShedual, $groupin->idGroupin);
                        } else echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" > - </td>';
                    } else {
                        if (count($scores) != NULL) {
                            ?>
                            <td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center"
                                class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="label label-danger"><?php echo $sum_score; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="footer">
                                        <div class="btn-group">
                                            <?php

                                            foreach ($absents as $absent) {
                                                if ($absent->checkId == 1) {
                                                    echo '<button type="button" style="padding: 3px 6px" class="btn btn-danger"><i class="fa fa-clock-o">1</i></button>';
                                                } elseif ($absent->checkId == 2) {
                                                    echo '<button type="button" style="padding: 3px 6px" class="btn btn-danger"><i class="fa fa-clock-o">2</i></button>';
                                                } elseif ($absent->checkId == 3) {
                                                    echo '<button type="button" style="padding: 3px 6px" class="btn btn-danger"><i class="fa fa-clock-o">3</i></button>';
                                                }

                                            }
                                            ?>
                                        </div>
                                    </li>
                                </ul>
                            </td>

                        <?php
                        } else echo '<td style="vertical-align: inherit;" align="center" > - </td>';
                    }
                }
                echo '<td style="vertical-align: inherit;" align="center">
            <a href="#">
                  <span class="label label-primary" style="padding: 10px;font-size: 14px;">' . Users::getAllSumShedualAbsents($shed->idShedual, $groupin->idGroupin) . '</span>
                </a> </td>';
                echo '</tr>';
                //Yii::app()->session['count']=0;
                $user_absent = Users::getSumGroupinAbsents($shed->idShedual, $groupin->idGroupin);
            }
            echo '</tbody>';
            echo '</table>';

        }
        ?>
    </div>
    <br>
    <br>
    Ҳамагӣ: <a class="label label-danger" style="padding: 10px 15px;font-size: 17px;"><?php
        echo $user_absent;
        ?></a>
</section>
