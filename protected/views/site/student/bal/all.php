<?php
$group = Groupin::model()->find(array('condition' => 'userId=' . $_SESSION['user']['id']));
$semestr = Shedual::model()->findAll(array('condition' => 'groupId=' . $group->groupId . ' and status=0',
    'group' => 'semestr',
    'order' => 'semestr ASC'));
$ruzho = Type::model()->findAll();
$this->pageTitle = Yii::app()->name . ' - Нимсолаи Гузашта';

$this->breadcrumbs = array(
    'Пешрафт'=>array('site/current'),
    'Семестрҳои гузашта'
);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<section class="content">


    <div class="box box-default">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Семестрҳои гузашта</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-group" id="accordion">
                    <?php foreach ($semestr as $sem) {

                        ?>
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse_<?php echo $sem->semestr; ?>" aria-expanded="false"
                                       class="collapsed">
                                        <?php
                                        echo 'Нимсолаи ' . $sem->semestr;
                                        ?>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapse_<?php echo $sem->semestr; ?>" class="panel-collapse collapse"
                                 aria-expanded="false" style="height: 0px;">
                                <div class="box-body">
                                    <div style="overflow-x: auto;">
                                        <?php
                                        $shedual = Shedual::model()->findAll(array('condition' => 'semestr=' . $sem->semestr . ' and groupId=' . $group->groupId . ' and status=0'));
                                        echo '<table class="table table-bordered table-striped dataTable" width="2700">';
                                        echo '<thead style="background-color: #ABAAAA;">';
                                        echo '<th>№</th>';
                                        echo '<th>Фан</th>';
                                        echo '<th>Муаллим(а)</th>';
                                        echo '<th>Кредит</th>';
                                        echo '<th>Рӯзи 1</th>';
                                        echo '<th>Рӯзи 2</th>';
                                        echo '<th>Рӯзи 3</th>';
                                        echo '<th>Рӯзи 4</th>';
                                        echo '<th>Рӯзи 5</th>';
                                        echo '<th>Рӯзи 6</th>';
                                        echo '<th>Рӯзи 7</th>';
                                        echo '<th colspan=2>Рӯзи 8 С. марҳилавӣ</th>';
                                        echo '<th>Рӯзи 9</th>';
                                        echo '<th>Рӯзи 10</th>';
                                        echo '<th>Рӯзи 11</th>';
                                        echo '<th>Рӯзи 12</th>';
                                        echo '<th>Рӯзи 13</th>';
                                        echo '<th>Рӯзи 14</th>';
                                        echo '<th>Рӯзи 15</th>';
                                        echo '<th colspan=2>Рӯзи 16 С. марҳилавӣ</th>';
                                        echo '<th>С. Устод</th>';
                                        echo '<th>Кори семестрӣ</th>';
                                        echo '<th>С. Ниҳои Кӯшиши якум</th>';
                                        echo '<th>С. Ниҳои Кӯшиши дуюм</th>';
                                        echo '<th>Триместр</th>';
                                        echo '<th>Б. умумӣ</th>';
                                        echo '<th>Баҳо</th>';
                                        echo '</thead>';
                                        ?>
                                        <tbody>
                                        <?php
                                        $shedual = Shedual::model()->findAll(array('condition' => 'semestr=' . $sem->semestr . ' and groupId=' . $group->groupId . ' and status=0'));
                                        $i=1;
                                        foreach ($shedual as $shed) {

                                            $subject = Subject::model()->findByPk($shed->subjectId);
                                            $teacher = Teachers::model()->findByPk($subject->teacherId);
                                            $user = User::model()->findByPk($teacher->userId);
                                            echo '<tr>';
                                            echo '<td>' . $i++ . '</td>';
                                            echo '<td><a href="javascript::void()">' . $subject->subject->subject . '</a></td>';
                                            echo '<td width="200"><small><i class="fa  fa-user"></i>&nbsp;&nbsp;' . $user->fname . ' ' . $user->name . ' ' . $user->lname . '</small></td>';
                                            echo '<td>' . $subject->subject->credit . '</td>';


                                            foreach ($ruzho as $ruz) {
//        $scores=  Scores::model()->findByAttributes(
//        array('shedualId'=>$shed->subjectId, 'groupinId'=>$groupin->idGroupin)
//                );

                                                $scores = Users::getAllScores($shed->idShedual, $group->idGroupin, $ruz->id);

                                                if ($ruz->id == 9) {
                                                    if (count($scores) > 0) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">' . $scores->bal . '</td>';
                                                    } else echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" > - </td>';
                                                } elseif ($ruz->id == 18) {
                                                    if (count($scores) > 0) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">' . $scores->bal . '</td>';
                                                    } else echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" > - </td>';
                                                } elseif ($ruz->id == 19) {
                                                    if (count($scores) > 0) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;" bgcolor="#C7C7C7" align="center">' . $scores->bal . '</td>';
                                                    } else echo '<td style="vertical-align: inherit;" bgcolor="#C7C7C7" align="center" > - </td>';
                                                } elseif ($ruz->id == 24) {
                                                    $gscores = Users::getBallsStudents($shed->idShedual, $group->idGroupin);
                                                    if ($gscores != NULL) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;background:rgba(243, 156, 18, 0.46);" align="center" > ' . $gscores->bal . ' </td>';
                                                    } else echo '<td style="vertical-align: inherit;background:rgba(243, 156, 18, 0.46);" align="center" > - </td>';
                                                } elseif ($ruz->id == 25) {
                                                    $gscores = Users::getBallsStudents($shed->idShedual, $group->idGroupin);
                                                    if ($gscores != NULL) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;background: rgba(243, 18, 18, 0.29);font-weight: 800;" align="center" > ' . $gscores->znak . ' </td>';
                                                    } else echo '<td style="vertical-align: inherit;font-size: 18px;background: rgba(243, 18, 18, 0.29);font-weight: 800;" align="center" > - </td>';
                                                } else {
                                                    if (count($scores) > 0) {
                                                        echo '<td style="vertical-align: inherit;font-size: 18px;" align="center">' . $scores->bal . '</td>';
                                                    } else echo '<td style="vertical-align: inherit;" align="center" > - </td>';
                                                }
                                            }


                                        }
                                        echo '</tr>';
                                        echo '</table>';
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


</section>


