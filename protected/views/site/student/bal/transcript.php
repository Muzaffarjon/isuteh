<?php
$group=  Groupin::model()->find(array('condition'=>'userId='.$_SESSION['user']['id']));
$semestr = Shedual::model()->findAll(array('condition' => 'groupId=' . $group->groupId.' and status=0',
        'group' => 'semestr',
        'order' => 'semestr ASC'));
$this->pageTitle=Yii::app()->name .' - Транскрипт';

$this->breadcrumbs=array(
    'Транскрипт'
);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<section class="content">
    <table class="table table-hover table-bordered" align="center">
        <tbody>
            <tr>
                <td>ФИО Донишҷӯ:</td>
                <td align="right"><? echo Yii::app()->session['user']['fname'].' '.Yii::app()->session['user']['name'].' '.Yii::app()->session['user']['lname'] ?></td>
            </tr>

            <tr>
                <td>Бахши:</td>
                <td align="right"><? echo $group->group->course; ?></td>
            </tr>
            <tr>
                <td >Гурӯҳи:</td>
                <td align="right"><? echo $group->group->ihtisos->code; ?></td>
            </tr>
            <tr>
                <td>Ихтисосӣ:</td>
                <td align="right"><? echo $group->group->ihtisos->ihtisos; ?></td>
            </tr>
            <tr>
                <td>ID донишҷӯ:</td>
                <td align="right"><? echo $group->idGroupin; ?></td>
            </tr>

        </tbody>
    </table>
    <?php foreach($semestr as $sem){ ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse_<?php echo $sem->semestr; ?>" aria-expanded="false"
                                   class="collapsed">
                                    <?php
                                    echo 'Нимсолаи ' . $sem->semestr;
                                    ?>
                                </a>
                            </h4>
                        </div>
                        <?php 

                            $shedual=  Shedual::model()->findAll(array('condition'=>'semestr='.$sem->semestr.' and groupId='.$group->groupId. ' and status=0'));
                            ?>
                          <div class="box-body">
                              <table class="table table-bordered table-striped dataTable" width="100%">
                                      <thead>
                                      <th>№</th>
                                      <th>Сикл</th>  
                                      <th>Номи фанн</th>  
                                      <th>Номи Муаллимон</th>  
                                      <th>Кредит</th>  
                                      <th>Фоиши иштирок</th>  
                                      <th>Б. Умумӣ</th>  
                                      <th>Алфавит</th>  
                                      </thead>
                                      <tbody>   
                                          <?php
                                          $i = 1;
                                          foreach ($shedual as $subj) {
                                              $ball=Users::getBallsOfStudents($subj->idShedual,$group->idGroupin);
                                              ?>

                                              <?php
                                              echo '<tr>';
                                              echo '<td>' . $i++ . '</td>';
                                              echo '<td align="center"><a style="font-size: 12px;" href="javascript::void()" class="label bg-red">' . $subj->cikl . '</a></td>';
                                              echo '<td>' . $subj->subject->subject->subject . '</td>';
                                              echo '<td>' . $subj->subject->teacher->user->fname .' '. $subj->subject->teacher->user->name .'</td>';
                                              echo '<td>' . $subj->subject->subject->credit. '</td>';
                                              echo '<td align="center"><span class="label label-danger">'.Users::getPercentOfAbsent($subj->idShedual,$group->idGroupin).'</span></td>';
                                              if($ball->bal!=""){
                                                  echo '<td align="center"><span class="label label-warning">'.$ball->bal.'</span></td>';
                                              }else echo '<td align="center"><span class="label label-warning"> - </span></td>';
                                              if($ball->znak!=""){
                                                  echo '<td align="center"><a class="btn btn-bitbucket" style="font-size:16px;">'.$ball->znak.'</a></td>';
                                              }else
                                                  echo '<td align="center"><a class="btn btn-bitbucket" style="font-size:16px;"> - </a></td>';
                                              echo '</tr>';
                                              ?>
                                          
                            
                         
                        <?php
                         }
                          
                        ?>
                                          </tbody>
                                      </table>
                        </div>
                    </div>
                    <?php } ?>
</section>
