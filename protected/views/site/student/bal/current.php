<?php
$groupin=Groupin::model()->findByAttributes( array('userId'=>Yii::app()->session['user']['id']));
$group=Group::model()->findByPk($groupin->groupId);
$shedual=  Shedual::model()->findAllByAttributes(
        array('groupId'=>$group->idGroup, 'status'=>1)
        );


$this->pageTitle=Yii::app()->name .' - Нимсолаи Ҳозира';

$this->breadcrumbs=array(
    'Пешрафт'=>array('site/current'),
    'Нимсолаи Ҳозира'
);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<section class="content">
    
    <p>Руйхати семетри ҳозира</p>
    <div style="overflow-x: auto;">
    <?php
    if(count($shedual)!=null){
    echo '<table class="table table-bordered table-striped dataTable" width="2700">';
    echo '<thead style="background-color: #ABAAAA;">';
    echo '<th>№</th>';
        echo '<th>Кредит</th>';
        echo '<th>Сикл</th>';
    echo '<th>Фан</th>';
    echo '<th>Муаллим(а)</th>';
    echo '<th>Рӯзи 1</th>';
    echo '<th>Рӯзи 2</th>';
    echo '<th>Рӯзи 3</th>';
    echo '<th>Рӯзи 4</th>';
    echo '<th>Рӯзи 5</th>';
    echo '<th>Рӯзи 6</th>';
    echo '<th>Рӯзи 7</th>';   
   echo '<th colspan=2>Рӯзи 8 С. марҳилавӣ</th>';  
    echo '<th>Рӯзи 9</th>';   
    echo '<th>Рӯзи 10</th>';   
    echo '<th>Рӯзи 11</th>';   
    echo '<th>Рӯзи 12</th>';
    echo '<th>Рӯзи 13</th>';
    echo '<th>Рӯзи 14</th>';
    echo '<th>Рӯзи 15</th>';
    echo '<th colspan=2>Рӯзи 16 С. марҳилавӣ</th>';
    echo '<th>С. Устод</th>';
    echo '<th>Кори семестрӣ</th>';
    echo '<th>С. Ниҳои Кӯшиши якум</th>';
    echo '<th>С. Ниҳои Кӯшиши дуюм</th>';
    echo '<th>Триместр</th>';
    echo '<th>Б. умумӣ</th>';
    echo '<th>Баҳо</th>';
    echo '</thead>';
    echo '<tbody>';
    
    $i=1;
    $ruzho=  Type::model()->findAll();
    foreach($shedual as $shed){
        
        $subject=Subject::model()->findByPk($shed->subjectId);
        $teacher=Teachers::model()->findByPk($subject->teacherId);
        $user=User::model()->findByPk($teacher->userId);
        echo '<tr>';
        echo '<td>'.$i++.'</td>';
        echo '<td align="center" style="background: rgba(90, 3, 3, 0.18); vertical-align:middle">'.$subject->subject->credit.'</td>';
        echo '<td align="center" style="background: rgba(255, 235, 59, 0.49); vertical-align:middle">'.$shed->cikl.'</td>';
        echo '<td> <a href="'.Yii::app()->createUrl('site/group',array('id'=>Users::getIncode($shed->idShedual))).'"><i class="fa  fa-book text-blue"></i> '.$subject->subject->subject.'</a></td>';
        echo '<td width="200" ><small><i class="fa  fa-user text-red"></i>&nbsp;&nbsp;'.$user->fname.' '.$user->name.' '.$user->lname.'</small></td>';


       
        foreach ($ruzho as $ruz) {
//        $scores=  Scores::model()->findByAttributes(
//        array('shedualId'=>$shed->subjectId, 'groupinId'=>$groupin->idGroupin)
//                );
        
        $scores=  Users::getAllScores($shed->idShedual,$groupin->idGroupin,$ruz->id);

            if($ruz->id==9){
                if(count($scores)>0){
                    echo '<td style="vertical-align: inherit;font-size: 18px;background: rgba(33, 107, 243, 0.63);" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: rgba(33, 107, 243, 0.63);" align="center" > - </td>';
            }elseif($ruz->id==18){
                if(count($scores)>0){
                    echo '<td style="vertical-align: inherit;font-size: 18px;background: rgba(33, 107, 243, 0.63);" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: rgba(33, 107, 243, 0.63);" align="center" > - </td>';
            }elseif($ruz->id==19){
                if(count($scores)>0){

                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #4CAF50;" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: #4CAF50;" align="center" > - </td>';
            }elseif($ruz->id==20){
                if(count($scores)>0){

                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #009688;" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: #009688;" align="center" > - </td>';
            }elseif($ruz->id==21){
                if(count($scores)>0){

                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: rgba(244, 67, 54, 0.55);" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: rgba(244, 67, 54, 0.55);" align="center" > - </td>';
            }elseif($ruz->id==22){
                if(count($scores)>0){

                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #03A9F4;" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: #03A9F4;" align="center" > - </td>';
            }elseif($ruz->id==23){
                if(count($scores)>0){

                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #FFEB3B;" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;background: #FFEB3B;" align="center" > - </td>';
            }elseif($ruz->id==24){
                $gscores=Users::getBallsStudents($shed->idShedual,$groupin->idGroupin);
                if($gscores!=NULL){
                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #61A85C;" align="center">' . $gscores->bal2. '</td>';
                }else echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #61A85C;" align="center"> - </td>';
            }elseif($ruz->id==25){
                $gscores=Users::getBallsStudents($shed->idShedual,$groupin->idGroupin);
                if($gscores!=NULL){
                    echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #5C93A8;" align="center">' . $gscores->znak. '</td>';
                }else echo '<td style="vertical-align: inherit;font-size: 18px;font-weight:bold;background: #5C93A8;" align="center"> - </td>';

            }else{
                if(count($scores)>0){
                    echo '<td style="vertical-align: inherit;font-size: 18px;" align="center">' . $scores->bal. '</td>';
                }else echo '<td style="vertical-align: inherit;" align="center" > - </td>';
            }
        }
        
        echo '</tr>';

    }
        echo '</tbody>';
        echo '</table>';

    }
    ?>
        </div>
</section>