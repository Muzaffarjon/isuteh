<div class="callout callout-danger">
    <h4><i class="icon fa fa-info"></i>&nbsp; Донишҷӯи мӯҳтарам!</h4>
    Шумо аз чунин фаннҳо ғоибҳо доред. Ба назди нозир рафта онҳоро отработка кунед!
    <table class="table table-bordered" width="100%" style="text-align: center">
        <thead>
        <tr >
            <th style="text-align: center" >
                №
            </th>
            <th style="text-align: center">
                Фанн
            </th>
            <th style="text-align: center">
                Муаллим
            </th>
            <th style="text-align: center">
                Семестр
            </th>
            <th style="text-align: center">
                Сикл
            </th>
            <th style="text-align: center">
                Кредит
            </th>
            <th style="text-align: center">
                М. ғоиб
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i=1;
        foreach($alerts as $alert){
            $count=Users::countAbsents($groupin->idGroupin, $alert->shedualId);
            $shedual=Shedual::model()->findByPk($alert->shedualId);
            echo '<tr>';
            echo '<td>'.$i++.'</td>';
            echo '<td>'.$shedual->subject->subject->subject.'</td>';
            echo '<td>'.$shedual->subject->teacher->user->fname.' '.$shedual->subject->teacher->user->name.'</td>';
            echo '<td>'.$shedual->semestr.'</td>';
            echo '<td>'.$shedual->cikl.'</td>';
            echo '<td>'.$shedual->subject->subject->credit.'</td>';
            echo '<td>'.$count.'</td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>