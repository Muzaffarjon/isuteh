<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - 404 Хатогӣ';
$this->breadcrumbs=array(
	'Error',
);
?>
<section class="content">
          <div class="error-page">
            <h2 class="headline text-blue"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-blue"></i> Уппс<b>!</b> Саҳифа ёфт нашуд.</h3>
              <p>
                Бубахшед ин хел дархост дар система маҷуд нест.</br>
                Агар ин саҳифа боз якчанд бор во хӯрад шумо метавонед ба мо <a href="<?php echo Yii::app()->createUrl('site/bug') ?>">хабар ирсол кунед </a> ё ин ки ба <a href="<?php echo Yii::app()->createUrl('login/index') ?>">Саҳифаи аввал</a> баргардед.
               .
              </p>
              <div class="error">
                <?php echo CHtml::encode($message); ?>
                </div>
              <form class="search-form">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Ҷустуҷӯ">
                  <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i></button>
                  </div>
                </div><!-- /.input-group -->
              </form>
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section>