<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'email-form',
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions'=>array(
        'class'=>'form-horizontal',
    ),
)); ?>

    <div class="form-group has-feedback">
        <label for="inputName" class="col-sm-2 control-label">E-mail</label>
        <div class="col-sm-10">
            <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'misol@mail.ru')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo CHtml::submitButton('Фиристодан', array('class'=>'btn btn-danger')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>