<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'password-form',
    'htmlOptions'=>array(
        'class'=>'form-horizontal',
    ),
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <div id="psw" class="form-group has-feedback">
        <label for="inputName" class="col-sm-2 control-label">Пароли ҳозира</label>
        <div class="col-sm-10">
            <?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    </div>
    <div id="newpsw" class="form-group has-feedback">
        <label for="inputEmail" class="col-sm-2 control-label">Пароли нав</label>
        <div class="col-sm-10">
            <?php echo $form->passwordField($model,'new_password',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'new_password'); ?>
        </div>
    </div>
    <div id="newpsw2" class="form-group has-feedback">
        <label for="inputName" class="col-sm-2 control-label">Такрори пароли нав</label>
        <div class="col-sm-10">
            <?php echo $form->passwordField($model,'repeat_password',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'repeat_password'); ?>
        </div>
    </div>



    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo CHtml::submitButton('Иваз кардан', array('class'=>'btn btn-danger')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>