<?php

?>

<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="<?php echo Yii::app()->request->baseUrl . $user->avatar ?>" alt="User profile picture">

                    <h3 class="profile-username text-center"><?php echo $user->fname . ' ' . $user->name ?></h3>

                    <p class="text-muted text-center"><?php echo $user->access->access ?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Рейтинг</b>
                        </li>
                        <li class="list-group-item">

                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

                    <p class="text-muted">
                        B.S. in Computer Science from the University of Tennessee at Knoxville
                    </p>

                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                    <p class="text-muted">Malibu, California</p>

                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                    <p>
                        <span class="label label-danger">UI Design</span>
                        <span class="label label-success">Coding</span>
                        <span class="label label-info">Javascript</span>
                        <span class="label label-warning">PHP</span>
                        <span class="label label-primary">Node.js</span>
                    </p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Ивази парол</a></li>
                    <li class=""><a href="#email" data-toggle="tab" aria-expanded="true">Почтаи электронӣ</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="settings">

                        <!-- FLASH FOR EMAIL -->

                        <?php if (Yii::app()->user->hasFlash('email')): ?>
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Муваффақият!</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->

                                <div class="box-body">
                                    <?php echo Yii::app()->user->getFlash('email'); ?>
                                </div>
                            </div>
                        <?php endif; ?>


                            <!-- FLASH FOR PASSWORD -->

                        <?php if (Yii::app()->user->hasFlash('password')): ?>
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Муваффақият!</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->

                                <div class="box-body">
                                    <?php echo Yii::app()->user->getFlash('password'); ?>
                                </div>
                            </div>

                        <?php else: ?>
                            <?php echo $this->renderPartial('include/password', array('model' => $model)); ?>
                        <?php endif; ?>
                    </div>

                    <div class="tab-pane" id="email">
                        <?php
                            if ($user->email == "") {
                                echo $this->renderPartial('include/email', array('model' => $email));
                            } else {
                                ?>
                                <form class="form-horizontal" id="password-form" action="/1isu/site/profile/test1"
                                      method="post">
                                    <div id="psw" class="form-group has-feedback">
                                        <div class="col-sm-10">
                                            <div id="psw" class="form-group has-feedback">
                                                <label for="inputName" class="col-sm-2 control-label">E-mail</label>

                                                <div class="col-sm-10">
                                                    <input class="form-control" name="PasswordForm[password]"
                                                           id="PasswordForm_password" type="text" maxlength="24" value="<?php echo $user->email ?>" disabled>
                                                </div>
                                            </div>
                                </form>
                            <?php
                            }
                            ?>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>