
<?php

//$groupClass=  Groupin::model()->findAllByAttributes(
//        array('groupId'=>$shedual->groupId, 'status'=>1), array('order'=>' DESC')
//        );
//$groupClass=Groupin::model()->findAllBySql('SELECT g.userId,u.fname,u.name,u.lname FROM groupin AS g, `user` AS u WHERE groupId='.$shedual->groupId.' AND g.`userId`=u.`id` ORDER BY u.`fname`');

$groupClass=Groupin::model()->with('user')->findAll(array('condition'=>'groupId='.$shedual->groupId,'order'=>'fname ASC'));

$this->pageTitle=Yii::app()->name .' - Интихоби фанн';

$this->breadcrumbs=array(
        'Дарсҳо'=>array('teacher/study'),
    'Интихоби фанн'
);

function getWeek($day, $month, $year){
    
    $week=date('N',mktime(0, 0, 0, $month, $day-1, $year));
    if($week==1){
        $wee="Душанбе";
    }elseif($week==2){
        $wee="Сешанбе";
    }elseif($week==3){
        $wee="Чоршанбе";
    }elseif($week==4){
        $wee="Панҷшанбе";
    }elseif($week==5){
        $wee="Ҷумъа";
    }elseif($week==6){
        $wee="Шанбе";
    }elseif($week==7){
        $wee="Якшанбе";
    }
    return $wee;
}

?>
<style>
    .disabled{
        background: rgba(239, 23, 32, 0.43);
    }
    #content {
        width: 100%;
        overflow-x: scroll;
    }
    #container {
        height: 100px;
        width: 200px;
    }
    #topscrl  {
        height: 20px;
        width: 100%;
        overflow-x: scroll;
        display: none;
    }
    #topfake {
        height: 1px;
        width: 1200px;
    }
</style>
<section class="content">
<script type="text/javascript">
    function sendBall(id){
//var forma=$('#theForm_'+id);
        $('#bal_'+id).change(function(){
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl("teacher/setball");?>",
                data: {shedId: this.form.shedId.value, type: this.form.type.value, userId: this.form.userId.value, ball: this.form.bal.value},
                error: function(){
                },
                beforeSend: function(){
                    $('select').attr('disabled');
                    $('#bal_'+id).unbind('focus');
                },
                success: function(data){
                    $('select').removeAttr('disabled');
                    $('#show').html(data);
                    $('#get').removeClass('classHidden');
                    $('#get').addClass('classShow');
                }
            });
        });
    }

    function dellBall(id){
        $('#delete_'+id).click(function(){

            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl("teacher/dellball");?>",
                data: {sId: this.form.shedId.value, t: this.form.type.value, uId: this.form.userId.value},
                cache: false,
                error: function(){
                },
                beforeSend: function(){
                    $('input[type=button] .delete').attr('disabled');
                    $('select').attr('disabled');
                },
                success: function(data){

                    if(data==0){
                        $('#war').removeClass('classHidden');
                        $('#war').addClass('classShow');
                        function setTimeWar(){
                            $('#war').removeClass('classShow');
                            $('#war').addClass('classHidden');
                        }
                        setTimeout(setTimeWar, 5000);

                    }
                    if(data==1){
                        $('#del').removeClass('classHidden');
                        $('#del').addClass('classShow');
                        function setTimeDel(){
                            $('#del').removeClass('classShow');
                            $('#del').addClass('classHidden');
                        }
                        setTimeout(setTimeDel, 5000);
                    }
                    $('select').removeAttr('disabled');
                    $('#bal_'+id).val('');
                }
            });
        });
    }

    $(document).ready(function(){

        $('#refresh').click(function(){
            window.location.reload();
        });

        $('#refresh').dblclick()

        $('#obnova').click(function(){

            var checkElements=document.getElementsByName("checked");
            for(var i=0; i<checkElements.length; i++)
            {
                checkElement = checkElements[i];
                if (checkElement.checked==1)
                {
                    if(confirm('Шумо розӣ ҳастед рӯзи дарсиро сахт кардан?')==true){
                        var id=checkElement.value;

                        $.ajax({
                            type: "POST",
                            url: "<?php echo Yii::app()->createUrl("teacher/setbal2");?>",
                            data: {tId: id, sid: <?php echo $shedual->idShedual; ?>},
                            cache: false,
                            error: function(){
                            },
                            beforeSend: function(){

                            },
                            success: function(data){
                                if(data=1){
                                    $('#showCheck').removeClass('classHidden');
                                    $('#showCheck').addClass('classShow');
                                    function showCheck(){
                                        $('#showCheck').removeClass('classShow');
                                        $('#showCheck').addClass('classHidden');
                                    }
                                    setTimeout(showCheck, 3000);
                                }


                            }
                        });
                        if(id==8){
                            $('td#ruz_8 select').attr('disabled','disabled').css('background:red');
                            $('td#ruz_9 select').attr('disabled','disabled').css('background:red');
                        }else if(id==17){
                            $('td#ruz_17 select').attr('disabled','disabled').css('background:red');
                            $('td#ruz_18 select').attr('disabled','disabled').css('background:red');
                        }else{
                            $('td#ruz_'+id+' select').attr('disabled','disabled').css('background:red');
                        }
                        setInterval(function(){
                            window.location.reload();
                        },4000)
                        $('#agree_'+id).attr('disabled','disabled')

                    }

                }
            }



//$('#table_reload').load('<?php Yii::app()->createUrl('site/ball'.$_GET['id']); ?> #table_reload');

        });
        $('#showCheck').click(function(){
            $('#showCheck').removeClass('classShow');
            $('#showCheck').addClass('classHidden');

        });

        $('#hide').click(function(){
            $('#get').removeClass('classShow');
            $('#get').addClass('classHidden');
        });
        function setTime(){
            $('#get').removeClass('classShow');
            $('#get').addClass('classHidden');
        }
        $('select').change(function(){
            setTimeout(setTime, 5000);
        });

        $('#dele').click(function(){
            $('.delete').removeClass('classHidden');
            $('.delete').addClass('classShow');
            function setTime(){
                $('.delete').removeClass('classShow');
                $('.delete').addClass('classHidden');
            }
            setTimeout(setTime, 25000);
        });

    });


</script>
    <h4><?php echo 'Семестр: '.$shedual->semestr.', Сикл: '.$shedual->cikl.', Фанн: '.$subject->subject->subject.', Кредит: '.$subject->subject->credit?></h4>

<ul id="get" class="alert noty_cont noty_layout_bottomRight classHidden">
    <li>
        <div class="noty_bar noty_theme_default noty_layout_topLeft noty_success" id="noty_success_1443432502281" style="cursor: pointer; display: block;">
            <div class="noty_message">
                <span id="show" class="hidden-xs"></span>
                <button type="button" class="close" id="hide">х</button>
            </div>
            
        </div>
    </li>
</ul>

    <ul id="showCheck" class="alert noty_cont noty_layout_topRight classHidden">
        <li>
            <div class="noty_bar noty_theme_default noty_layout_topRight noty_success" id="noty_error_1443518185616" style="cursor: pointer; display: block;">
                <div class="noty_message">
                    <span class="noty_text">Рӯзи интихоб шуда сахт шуд!</span>
                </div>
            </div>
        </li>
    </ul>

<ul id="war" class="alert noty_cont noty_layout_topRight classHidden">
    <li>
        <div class="noty_bar noty_theme_default noty_layout_topRight noty_error" id="noty_error_1443518185616" style="cursor: pointer; display: block;">
            <div class="noty_message">
                <span class="noty_text">Хатогӣ шуд.</span>
            </div>
        </div>
    </li>
</ul>    
    
<ul id="del" class="alert noty_cont noty_layout_topRight classHidden">
    <li>
        <div class="noty_bar noty_theme_default noty_layout_topLeft noty_success" id="noty_success_1443432502281" style="cursor: pointer; display: block;">
            <div class="noty_message">
                <span id="show" class="hidden-xs">Хориҷ карда шуд!</span>
            </div>
            
        </div>
    </li>
</ul>
  <?php
  // There dateStart begin 
  $expStart=explode('-',$shedual->dateStart);
   $day=$expStart[2];
    if($expStart[1]<10){
        $str=  substr($expStart[1], 1,1);
        $month=$str;
    }else{
        $month=$expStart[1];
    }
   $year=$expStart[0];
   
// There dateRest Begin 
   
   $expRest=explode(',',$shedual->dateRest);
    $list=array();
    for($i=0;$i<=count($expRest);$i++){
        $list[$i]=isset($expRest[$i]) ? $expRest[$i] : '' ;
    }
      
    ?>
<script type="text/javascript">
    window.onload = function() {
        document.getElementById("topfake").style.width = document.getElementById("content").scrollWidth+"px";
        document.getElementById("topscrl").style.display = "block";
        document.getElementById("topscrl").onscroll = topsclr;
        document.getElementById("content").onscroll = bottomsclr;
    }
    function topsclr() {
        document.getElementById("content").scrollLeft = document.getElementById("topscrl").scrollLeft;
    }
    function bottomsclr() {
        document.getElementById("topscrl").scrollLeft = document.getElementById("content").scrollLeft;
    }
</script>
    <br>
<div id="topscrl">
    <div id="topfake"></div>
</div>
<div id="content">
    <?php //
    if($subject->subject->credit==6){
    echo '<table class="table table-bordered table-striped dataTable tableDiv table-responsive" id="table_reload" width="2100">';
    }elseif($subject->subject->credit==4){
         echo '<table class="table table-bordered table-striped dataTable tableDiv table-responsive" id="table_reload" width="2100">';
    }else{
         echo '<table class="table table-bordered table-striped dataTable tableDiv table-responsive" id="table_reload" width="900">';
    }
    echo '<thead style="background-color: #ABAAAA;">';
    echo '<th>№</th>';
    echo '<th width="244px">Ному Насаб</th>';
    $r=1;
    if($subject->subject->credit==6){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id Between 1 and 20'
    ));
    
    for($i = 1;$i<=16+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==8){
            echo '<td colspan=2 style="text-align:center;" align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }elseif($r==16){
                echo '<td colspan=2 style="text-align:center;" align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }else{
                echo '<td style="text-align:center;" >Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
            }
        }
        
        }
        echo '<td style="text-align:center;">С. Устод</td>';
        echo '<td style="text-align:center;">Кори семестрӣ</td>';
    }elseif($subject->subject->credit==4){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id NOT IN(1,2,10,11,21,22,23,24,25)'
    ));
    for($i = 1;$i<=12+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==6){
            echo '<td colspan=2 style="text-align:center;" align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }elseif($r==12){
                echo '<td style="text-align:center;" colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }else{
                echo '<td style="text-align:center;" >Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
            }
        } 
        }
       echo '<td valign="middle" align="center">С. Устод</td>';
       echo '<td align="center" valign="middle">Кори семестрӣ</td>';
    }elseif($subject->subject->credit==2){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id=19'
    ));
      echo '<td style="text-align:center;">С. Устод</td>';
    }
    
    //echo '<td>C. РЈСЃС‚РѕРґ</td>';
    echo '</thead>';
    echo '<tbody>';
//    echo '<tr>';
//    echo '<td></td>';
//    echo '<td></td>';
//    foreach ($ruzho as $ruzi){
//    echo '<td align="center">';
//    echo CHtml::checkBox('check',false,array('id'=>'check_'.$ruzi->id));
//    echo '</td>';
//    }
//    echo '</tr>';
    
   echo $this->renderPartial('ruzho/_ruz16_2', array('groupClass'=>$groupClass,'ruzho'=>$ruzho,'shedual'=>$shedual,'subject'=>$subject));
    echo '</tbody>';
    echo '<tfoot style="background-color: #ABAAAA;">';
            ?>
        <tr>
            <th>№</th>
            <th style="text-align: right;">Розигӣ ва тасдиқӣ баҳо.</th>
            <?php

            foreach($ruzho as $ruz){
                if($ruz->id==9 || $ruz->id==18){}
                else{
                $check=Checktable::model()->find(array('condition'=>'shedualId='.$shedual->idShedual.' and typeId='.$ruz->id));
                if($ruz->id==8 || $ruz->id==17){
                echo '<th colspan=2 id="check"><center>';
                }else{
                    echo '<th  id="check"><center>';
                }
                    if(count($check)==0)
                echo '<input type="radio" class="minimal" id="agree_' . $ruz->id . '" name="checked" value="' . $ruz->id . '">';
                else
                    echo '<input type="radio" disabled class="minimal" value="' . $ruz->id . '">';
                echo '</center>
                      </th>';
                }

            }

            ?>
        </tr>

       <?php
    echo '</tfoot>';
        echo '</table>';


    ?>

    </div>
<table>
    <tr>
        <!-- <td><button class="btn btn-block btn-danger" id="dele"> <i class="fa fa-edit"></i>&nbsp;Хориҷ кардан</button> </td>-->
        <td><button style="margin-right: 15px;" class="btn btn-block btn-primary" id="refresh"><i class="fa fa-repeat"></i> &nbsp; Азнав кардан</button> </td>
        <td><button style="margin-left: 15px;" class="btn btn-block btn-success" id="obnova"><i class="fa fa-unlock-alt"></i> &nbsp; Тасдиқ кардан</button> </td>
    </tr>
</table>
</section>
