<?php

$Teacher=Teachers::model()->findByAttributes( array('userId'=>Yii::app()->session['user']['id'], 'status'=>1));
if(isset($Teacher))
$Subject=Subject::model()->findAll( array('condition'=>'teacherId='.$Teacher->id.' and status=1'));
//$groupClass=  Group::model()->findAllByAttributes(
//        array('groupId'=>$groupin->groupId, 'status'=>1)
//        );

$this->pageTitle=Yii::app()->name .' - Интихоби гурӯҳ';

$this->breadcrumbs=array(
       'Интихоби гурӯҳ'
);
//echo $Teacher->id;
echo '<section class="content">';
echo '<table class="table table-hover table-bordered table-striped dataTable" width="100%">';
echo '<thead>';
echo '<th>#</th>';
echo '<th>Фанн</th>';
echo '<th>Гурӯҳ</th>';
echo '<th>Сикл</th>';
echo '<th>Сем</th>';
echo '<th>Кредит</th>';
echo '<th>S</th>';
echo '</thead>';

$num=1;


if(isset($Teacher))
foreach ($Subject as $subject){
    $shedual=Users::getAllShedual($subject->idSubject);
    
    foreach ($shedual as $shed){
       echo '<tr>';
       echo '<td>'.$num++.'</td>';
        $group=Users::getGroups($shed->groupId);
        echo '<td>'.$subject->subject->subject.'</td>';
        echo '<td><a href="'.Yii::app()->createUrl('teacher/ball',array('id'=>Users::getIncode($shed->idShedual))).'">'.$group->course.'_'.$group->ihtisos->code.' '.$group->class.'</a></td>';
        echo '<td>'.$shed->cikl.'</td>';
        echo '<td>'.$shed->semestr.'</td>';
        echo '<td>'.$subject->subject->credit.'</td>';
       if($shed->status==1){
            echo '<td><span class="label label-success"><i class="fa fa-check-square"></i></span></td>';
        }
        
      echo '</tr>';

    }

}
echo '</table>';

echo '</section>';
?>
