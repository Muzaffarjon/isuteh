    <script type="text/javascript">
        function sendAbsent(id){
            
        $('#abs_'+id).click(function(event ){
            event.preventDefault();
            var form_data = $(this).closest('form').serialize();
//            var formValidation={
//            'sId'              : this.form.shedId.value,
//            'uId'              : this.form.userId.value,
//            'tId'              : this.form.type.value,
//            'check'            : services
//            };
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl("nozir/nozir"); ?>",
                data: form_data,
                cache: false,
                error: function(){
                },
                beforeSend: function(){
                    $('#abs_'+id).attr('disabled', 'disabled');
                },
                success: function(data){
                    $('#rel_'+id).css('background','#648675');
                    $('#rel_'+id).html(data);

                }
            });
        });   
    };


    </script>
<?php

Yii::app()->clientScript->registerScript('toogle', "
$('#table_toggle').click(function(){
	$('#table_show').toggle();
	return false;
});");
$this->pageTitle=Yii::app()->name .' - Гузоштани ғоиб';

$this->breadcrumbs=array(
       'Ғоибҳо'=>array('nozir/ruihat'),
        $shedual->group->ihtisos->code=>array('nozir/absent/'.$shedual->group->idGroup),
        $shedual->subject->subject->subject
);

$groupClass=   Groupin::model()->with('user')->findAll(
    array('condition'=>'groupId='.$shedual->groupId, 'order'=>'fname ASC')
);

$expStart=explode('-',$shedual->dateStart);
   $day=$expStart[2];
    if($expStart[1]<10){
        $str=  substr($expStart[1], 1,1);
        $month=$str;
    }else{
        $month=$expStart[1];
    }
   $year=$expStart[0];
   
// There dateRest Begin 
   
   $expRest=explode(',',$shedual->dateRest);
    $list=array();
    for($i=0;$i<=count($expRest);$i++){
        $list[$i]=$expRest[$i];
    }

    function getWeek($day, $month, $year){
    
    $week=date('N',mktime(0, 0, 0, $month, $day-1, $year));
    if($week==1){
        $wee="Душанбе";
    }elseif($week==2){
        $wee="Сешанбе";
    }elseif($week==3){
        $wee="Чоршанбе";
    }elseif($week==4){
        $wee="Панҷшанбе";
    }elseif($week==5){
        $wee="Ҷумъа";
    }elseif($week==6){
        $wee="Шанбе";
    }elseif($week==7){
        $wee="Якшанбе";
    }
    return $wee;
}
    
?>
<section class="content">

<div style="overflow-x: auto; width:1050px;">
    <?php
    if($shedual->subject->subject->credit==6){
        echo '<table id="table_reload2" class="table table-bordered table-striped table-hover dataTable" style="margin-bottom: 60px;" role="grid" width="2300">';
    }elseif($shedual->subject->subject->credit==4){
        echo '<table id="table_reload2" class="table table-bordered table-striped table-hover dataTable" style="margin-bottom: 60px;" role="grid" width="2300">';
    }else{
        echo '<table id="table_reload2" class="table table-bordered table-striped table-hover dataTable" style="margin-bottom: 60px;" role="grid" width="1300">';
    }
    ?>

<thead style="background-color: #ABAAAA;">
  <th width="20">№:</th>
  <th width="244" >Ному насаб</th>
  
  
  <?php
 //echo $shedual->dateStart; 
  $r=1;
    if($shedual->subject->subject->credit==6){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id Between 1 and 18'
    ));
    
    for($i = 1;$i<=16+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==8){
            echo '<th colspan=2 style="text-align:center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</th>';
            }elseif($r==16){
                echo '<th colspan=2 style="text-align:center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</th>';
            }else{
                echo '<th style="text-align:center">Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</th>';
            }
        } 
        }
        echo '<th>Миқдори умумӣ</th>';
    }elseif($shedual->subject->subject->credit==4){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id NOT IN(1,2,10,11,19,20,21,22,23,24,25)'
    ));
    for($i = 1;$i<=12+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==6){
            echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }elseif($r==12){
                echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }else{
                echo '<td>Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
            }
        } 
        }
     echo '<td>Миқдори умумӣ</td>';
    }elseif($shedual->subject->subject->credit==2){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id Between 1 and 6'
    ));
        for ($i = 1; $i <= 6 + count($expRest); $i++) {
            $data = date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
            $week = getWeek($day, $month, $year);
            if (in_array($data, $list)) {
                echo '';
            } else {
                echo '<td>Рӯзи ' . $r++ . ',<br>' . $data . '<br>' . $week . '</td>';
            }
        }
        echo '<td>Миқдори ғоиб</td>';
    }
  
  ?>
</thead>
    <tbody>
       <?php
       echo $this->renderPartial('absent/_absent', array('groupClass'=>$groupClass,'ruzho'=>$ruzho,'shedual'=>$shedual));
       ?> 
    </tbody>
</table>
<?php

$expStart=explode('-',$shedual->dateStart);
   $day=$expStart[2];
    if($expStart[1]<10){
        $str=  substr($expStart[1], 1,1);
        $month=$str;
    }else{
        $month=$expStart[1];
    }
   $year=$expStart[0];
   
// There dateRest Begin 
   
   $expRest=explode(',',$shedual->dateRest);
    $list=array();
    for($i=0;$i<=count($expRest);$i++){
        $list[$i]=$expRest[$i];
    }

?>    
    </div>
    <br>
    <br>
    <br>
<b>Муаллим(а):</b> <button class="label label-info" id="table_toggle" style="font-size: 16px;"><?php echo $shedual->subject->teacher->user->fname.' '.$shedual->subject->teacher->user->name; ?></button>
<br>
<br>
    <div id="table_show" style="display:none; overflow-x: auto;">
        <?php
        if($shedual->subject->subject->credit==6){
            echo '<table id="table_reload" class="table table-bordered table-striped table-hover dataTable" role="grid" width="2300">';
        }elseif($shedual->subject->subject->credit==4){
            echo '<table id="table_reload" class="table table-bordered table-striped table-hover dataTable" role="grid" width="2300">';
        }else{
            echo '<table id="table_reload" class="table table-bordered table-striped table-hover dataTable" role="grid" width="1300">';
        }
        ?>
        <thead style="background-color: #ABAAAA;">
  <th>№:</th>
  <th>Ному насаб</th>
  
  
  <?php
 //echo $shedual->dateStart; 
  $r=1;
    if($shedual->subject->subject->credit==6){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id Between 1 and 18'
    ));
    
    for($i = 1;$i<=16+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==8){
            echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }elseif($r==16){
                echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }else{
                echo '<td>Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
            }
        } 
        }
    }elseif($shedual->subject->subject->credit==4){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id NOT IN(1,2,10,11,19,20,21,22,23,24,25)'
    ));
    for($i = 1;$i<=12+count($expRest);$i++){
       $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
      $week=  getWeek($day, $month, $year);
      if(in_array($data,$list)){
            echo '';
        }else{
            if($r==6){
            echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }elseif($r==12){
                echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
            }else{
                echo '<td>Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
            }
        } 
        }
    }elseif($shedual->subject->subject->credit==2){
        $ruzho=  Type::model()->findAll(array(
        'condition'=>'id Between 1 and 6'
    ));

        for($i = 1;$i<=6+count($expRest);$i++){
            $data=date("Y-m-d", mktime(0, 0, 0, $month, $day++, $year));
            $week=  getWeek($day, $month, $year);
            if(in_array($data,$list)){
                echo '';
            }else{
                if($r==6){
                    echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
                }elseif($r==12){
                    echo '<td colspan=2 align="center">Рӯзи '.$r++.', C. Марҳилавӣ <br>'.$data.'<br>'.$week.'</td>';
                }else{
                    echo '<td>Рӯзи '.$r++.',<br>'.$data.'<br>'.$week.'</td>';
                }
            }
        }
    }
  
  ?>
</thead>
        <tbody>
           <?php
       echo $this->renderPartial('//teacher/goib/_absent', array('groupClass'=>$groupClass,'ruzho'=>$ruzho,'shedual'=>$shedual));
       ?>  
        </tbody>
    </table>
      </div>
    </section>