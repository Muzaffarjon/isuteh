<?php

    $semestr = Shedual::model()->findAll(array('condition' => 'groupId=' . $group->idGroup,
        'group' => 'semestr',
        'order' => 'semestr ASC'));
$this->pageTitle=Yii::app()->name .' - Интихоби фан';

$this->breadcrumbs=array(
       "Гурӯҳҳо"=>array('nozir/students'),
        'Интихоби фанн'
);

?>
<section class="content">
    <h2><?php echo $group->course.'_'.$group->ihtisos->code.$group->class ?></h2>
   
    <div class="col-md-6">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Интихоби Нимсолаҳо</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <?php foreach($semestr as $sem){ ?>
                    <div class="panel box box-primary">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $sem->semestr ?>" aria-expanded="false" class="">
                            <?php 
                            echo 'Нимсолаи '.$sem->semestr;
                            $subjects=  Shedual::model()->findAll(array('condition'=>'semestr='.$sem->semestr.' and groupId='.$group->idGroup));
                            ?>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse_<?php echo $sem->semestr ?>" class="panel-collapse collapse" aria-expanded="true">
                          <div class="box-body">
                              <table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                      <thead>
                                      <th>№</th>  
                                      <th>Номи фанн</th>  
                                      <th>Муаллим</th>
                                      <th>Сикл</th>
                                      </thead>
                                      <tbody>   
                                          <?php
                                          $i = 1;
                                          foreach ($subjects as $subj) {
                                              ?>

                                              <?php
                                              echo '<tr>';
                                              echo '<td>' . $i++ . '</td>';
                                              echo '<td><a href="' . Yii::app()->createUrl('nozir/group/' . $subj->idShedual) . '">' . $subj->subject->subject->subject . '</a></td>';
                                              echo '<td>'. $subj->subject->teacher->user->fname." ".$subj->subject->teacher->user->name.'</td>';
                                              echo '<td align="center"><a style="font-size: 12px;" href="javascript::void()" class="label bg-red">' . $subj->cikl . '</a></td>';
                                              echo '</tr>';
                                              ?>
                                          
                            
                         
                        <?php
                         }
                          
                        ?>
                                          </tbody>
                                      </table>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
     
    <?php 

    ?>

</section>