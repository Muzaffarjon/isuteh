<?php
$this->pageTitle=Yii::app()->name .' - Интихоби гурӯҳ';

$this->breadcrumbs=array(
       'Интихоби гурӯҳ'
);

?>
<section class="content">
    <div style="overflow-y: auto; height: 350px;width: 250px">
    <?php
    echo '<table class="table table-bordered table-striped table-hover">';
    echo '<thead>';
    echo '<th>Гурӯҳҳо</th>';
    echo '<th>М/дон</th>';
    echo '</thead>';
    echo '<tbody>';
    
    $group=  Group::model()->findAll(array('condition'=>'status=1'));
    foreach ($group as $shed){
        echo '<tr>';
        echo '<td><a href="'.Yii::app()->createUrl('nozir/fann/'.$shed->idGroup).'">'.$shed->course.'_'.$shed->ihtisos->code.$shed->class.'</a></td>';
        echo '<td><a href="javascript::void()">'.Users::countGroupins($shed->idGroup).'</a></td>'; 
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
 ?>
        </div>
</section>
