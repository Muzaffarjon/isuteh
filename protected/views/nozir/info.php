<?php
$user=User::model()->findbyAttributes(array('login'=>$_GET['name'], 'accessId'=>1));
//$groupin=Groupin::model()->findbyAttributes(array('userId'=>$user->id));
$groupin=Groupin::model()->findbyAttributes(array('userId'=>58));
?>
<section class="content">
        <?php
    $semestr = Shedual::model()->findAll(array('condition' => 'groupId=' . $groupin->groupId,
        'group' => 'semestr',
        'order' => 'semestr DESC'));



$this->breadcrumbs=array(
    'Донишҷӯ'=>array('nozir/includes'),
    $user->fname." ".$user->name
);
    ?>

    <?php
//    foreach ($semestr as $sem) {
//        echo '<div class="tab-pane" id="sem_' . $sem->semestr . '">
//                                                tes' . $sem->idShedual . '
//                                        </div>';
//    }
    ?>
<script>
    $(document).ready(function(){
    $('#tab1').addClass('active');
    $('#tab_1').addClass('active');
    });
    
</script>


          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?php echo Yii::app()->request->baseUrl.$user->avatar; ?>" alt="User profile picture">
                  <h3 class="profile-username text-center"><?php
                  
                  echo $user->fname." ".$user->name;
                  
                  ?></h3>
                  <p class="text-muted text-center"><?php
                  
                  echo $user->access->access;
                  
                  ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Гурӯҳ</b> <a class="pull-right"><?php echo $groupin->group->course.'_'.$groupin->group->ihtisos->code.$groupin->group->class; ?></a>
                    </li>

                  </ul>

                  <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">Malibu, California</p>

                  <hr>

                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                  </p>

                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class=""><a href="#activity" data-toggle="tab" aria-expanded="false">Транскрипт</a></li>
                  <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Карздори</a></li>
                  <li class=""><a href="#absent" data-toggle="tab" aria-expanded="false">Миқдори ғоиб</a></li>
                </ul>
                <div class="tab-content">
                  <!-- TAB Activity -->
                    <div class="tab-pane active" id="activity">
                           <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    <?php
                    
                    foreach ($semestr as $sem) {
                        echo '<li class="" id="#tab' . $sem->semestr . '"><a href="#tab_' . $sem->semestr . '"  data-toggle="tab" aria-expanded="true"><span class="label label-success">Ним. ' . $sem->semestr . '</span></a></li>';
                    }
                    
                    ?>
           
                  
                  <li class="pull-left header"><i class="fa fa-th"></i>Руйхати фаннҳо</li>
                </ul>
                <div class="tab-content">
                  <?php
                  $i=1;
                  foreach ($semestr as $sem) {
                        
                 echo   '<div class="tab-pane" id="tab_'.$sem->semestr.'">';
                 echo '<a href="javascript::void()" class="btn btn-success btn-block">Нимсолаи '.$sem->semestr.'</a>';
                 echo '</br>';
                    $subjects=  Shedual::model()->findAll(array('condition'=>'semestr='.$sem->semestr.' and groupId='.$groupin->groupId));
                    
                    echo '<table class="table table-hover table-bordered" width="100%">';
                    echo '<thead>';
                    echo '<th>№</th>';
                    echo '<th>Фанн</th>';
                    echo '<th>Муаллим</th>';
                    echo '<th>Фоизи иштирок</th>';
                    echo '<th>Кредит</th>';
                    echo '<th>Баҳои миёна</th>';
                    echo '<th>Алфавит</th>';
                    echo '</thead>';
                    
                    echo '<tbody>';
                    foreach($subjects as $subj){
                        $ball=Users::getBallsOfStudents($subj->idShedual,$groupin->idGroupin);
                        echo '<tr>';
                        echo '<td>'.$subj->cikl.'</td>';
                        echo '<td>'.$subj->subject->subject->subject.'</td>';
                        echo '<td>'.$subj->subject->teacher->user->fname.' '.$subj->subject->teacher->user->name.'</td>';
                        echo '<td align="center"><span class="label label-danger">'.Users::getPercentOfAbsent($subj->idShedual,$groupin->idGroupin).'</span></td>';
                        echo '<td align="center">'.$subj->subject->subject->credit.'</td>';
                        if(!empty($ball->bal)){
                        echo '<td align="center">'.$ball->bal.'</td>';
                        }else echo '<td align="center"> - </td>';
                        if(!empty($ball->znak)){
                            echo '<td align="center">'.$ball->znak.'</td>';
                        }else
                        echo '<td align="center"> - </td>';
                        echo '</tr>';
                      
                    }
                    echo '</tbody>';
                    echo '</table>';
                 
                 echo '</div>';
                   
                    }
                    
                    ?>  
                   
                  
                </div> 
              </div>
                    </div><!-- /.tab-pane -->
                  <!-- TAB Karzdori -->
                 <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    Карздорӣ!
                  </div>
                    <div class="tab-pane" id="absent">
                    <!-- The timeline -->
                    Миқдори ғоиб!
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section>

