<?php
$this->pageTitle=Yii::app()->name .' - Қарздориҳо';

$this->breadcrumbs=array(
    'Қарздориҳо'
);
?>

<script>

function postMessage(shedId,groupId) {
    if(shedId!=null && groupId!=null){
        if(parseInt(shedId) && parseInt(groupId)){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('nozir/mespost') ?>',
                data:{sId: shedId, gId: groupId},
                afterPost: function (){
                    $('#btn_'+shedId+groupId).attr('disabled', 'disabled');
                },
                success: function(data){
                    if(data==1){
                        $('#td_'+shedId+groupId).load('<?php echo Yii::app()->createUrl('nozir/karzho') ?> #td_'+shedId+groupId);
                    }
                }
            });
        }
    }
}

function update(shedId,groupId){
if(shedId!=null && groupId!=null){
        if(parseInt(shedId) && parseInt(groupId)){
            if (confirm("Дар ҳақикат отработка карда шуд!") == true) {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('nozir/mesupd') ?>',
                data:{sId: shedId, gId: groupId},
                afterPost: function (){
                    $('#otn_'+shedId+groupId).attr('disabled', 'disabled');
                },
                success: function(data){
                    if(data==1){
                        $('#td_'+shedId+groupId).load('<?php echo Yii::app()->createUrl('nozir/karzho') ?> #td_'+shedId+groupId);
                    }
                }
            });
            }
            
        }
    }
}
    
$(document).ready(function() 
    { 
       //$("#myTable").tablesorter(); 
       $("#table2").tablesorter(); 
    } 
); 
</script>
    <script>
      $(function () {
        $("#myTable").DataTable();
        $('#table2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<section class="content">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#k_b" data-toggle="tab" aria-expanded="true">Қарздори бо баҳо</a></li>
                    <li class=""><a href="#k_g" data-toggle="tab" aria-expanded="true">Қарздори бо ғоиб</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="k_b">
                      <div class="box box-success">
                        
                          <div class="box-body">
                              <table id="myTable" class="table table-bordered table-hover dataTable" width="100%" role="grid">
                                              <thead>
                                                  <tr role="row">
                                                        <th>#</th>
                                                        <th>Н. Н. Д.</th>
                                                        <th>Гурӯҳ</th>
                                                        <th>Фанн</th>
                                                        <th>Ҳолат</th>
                                                   </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                  $i=1;
                                                  $scores = Scores::model()->findAll(array('group' => 'groupinId, shedualId'));

                                                  foreach ($scores as $score) {
                                                      $check = Users::checkGroupin($score->groupinId, $score->shedualId);
                                                      if ($check == 1) {
                                                      echo '<tr>';
                                                      echo '<td>'.$i++.'</td>';
                                                          echo '<td>'.$score->groupin->user->fname.' '.$score->groupin->user->name.'</td>';
                                                           echo '<td>'.$score->groupin->group->course.'_'.$score->groupin->group->ihtisos->code.$score->groupin->group->class.'</td>';
                                                          echo '<td>'.$score->shedual->subject->subject->subject.'</td>';
                                                          echo '<td>1</td>';
                                                      echo '</tr>';     
                                                      } 
                                                        


                                                      //echo '<br>';
                                                  }
                                                  ?> 
                                              <tfoot>
                                                  <tr>
                                                      <th><a href="javascript::void()">#</a></th>
                                                        <th><a href="javascript::void()">Н. Н. Д</a></th>
                                                        <th><a href="javascript::void()">Гурӯҳ</a></th>
                                                        <th><a href="javascript::void()">Фанн</a></th>
                                                        <th><a href="javascript::void()">Ҳолат</a></th>
                                                  </tr>
                                              </tfoot>
                                          </table>
                              <br>
                              <br>
                              <table>
                                  <tr>
                                      <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                                      <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                                      <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
                                  </tr>
                              </table>
                          </div><!-- /.box-body -->
                      </div>
                     
                      
                      
                      
                  </div><!-- /.tab-pane -->
                  
                  <div class="tab-pane" id="k_g">
                      <table>
                          <tr>
                              <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                              <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                              <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
                          </tr>
                      </table>
                      <br>
                      <div class="box box-success">
                        
                          <div class="box-body">
                              <table id="table2" class="table table-bordered table-hover dataTable" width="100%" role="grid">
                                              <thead>
                                                        <th><a href="javascript::void()">#</a></th>
                                                        <th><a href="javascript::void()">Н. Н. Д</a></th>
                                                        <th><a href="javascript::void()">Гурӯҳ</a></th>
                                                        <th><a href="javascript::void()">Фанн</a></th>
                                                        <th><a href="javascript::void()">Соат</a></th>
                                                        <th><a href="javascript::void()">Ҳолат</a></th>
                                              </thead>
                                              <tbody>
                                                 <?php
                      $absents=Absent::model()->findAll(array('group'=>'groupinId,shedualId'));
                      $i=1;
                      foreach($absents as $absent){
                          echo '<tr>';
                          
                          $count=Users::countAbsents($absent->groupinId, $absent->shedualId);
                          $status=Users::statusAlerts($absent->groupinId, $absent->shedualId);
                          if($count>=12){
                              echo '<td>'.$i++.'</td>';
                          echo '<td>'.$absent->groupin->user->fname.' '.$absent->groupin->user->name.'</td>';    
                          echo '<td>'.$absent->groupin->group->course.'_'.$absent->groupin->group->ihtisos->code.$absent->groupin->group->class.'</td>';
                          echo '<td>'.$absent->shedual->subject->subject->subject.'</td>';    
                          echo '<td>'.$count.'</td>';
                          $exist=Users::countAlerts($absent->groupinId,$absent->shedualId);
                          if($exist==1){
                              if($status==1){
                              echo '<td id="td_'.$absent->shedualId.$absent->groupinId.'" ><button id="otn_'.$absent->shedualId.$absent->groupinId.'" onClick="update('.$absent->shedualId.','.$absent->groupinId.')" class="btn btn-info">Отработка</button></td>';    
                              }else{
                                  echo '<td id="td_'.$absent->shedualId.$absent->groupinId.'" ><button class="btn btn-success" disabled>Отработка шуд</button></td>'; 
                              }
                              
                          }else{
                           echo '<td id="td_'.$absent->shedualId.$absent->groupinId.'" ><button id="btn_'.$absent->shedualId.$absent->groupinId.'" onClick="postMessage('.$absent->shedualId.','.$absent->groupinId.')" class="btn btn-warning">Огоҳ кардан</button></td>';    
                          }
                          }
                          
                          echo '<tr>';

                          
                      }
                   
                   
                   
                   ?>   </tbody>
                                              <tfoot>
                                                  <tr>
                                                      <th><a href="javascript::void()">#</a></th>
                                                        <th><a href="javascript::void()">Н. Н. Д</a></th>
                                                        <th><a href="javascript::void()">Гурӯҳ</a></th>
                                                        <th><a href="javascript::void()">Фанн</a></th>
                                                        <th><a href="javascript::void()">Соат</a></th>
                                                        <th><a href="javascript::void()">Ҳолат</a></th>
                                                  </tr>
                                              </tfoot>
                     </table>
                              <br>
                              <br>
                              <table>
                                  <tr>
                                      <td><span class="label label-warning">&nbsp;</span> - Огоҳ кардан&nbsp;&nbsp;</td>
                                      <td><span class="label label-info">&nbsp;</span> - Отработка&nbsp;&nbsp;</td>
                                      <td><span class="label label-success">&nbsp;</span> - Отработка шуд</td>
                                  </tr>
                              </table>
                          </div><!-- /.box-body -->
                      </div>
                      
                      
                  </div><!-- /.tab-pane -->
                 
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
</section>
