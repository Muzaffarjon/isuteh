<?php 
$user=User::model()->findByPk(Yii::app()->session['user']['id']); 
$groupin=Groupin::model()->findByAttributes( array('userId'=>Yii::app()->session['user']['id']));
if(!empty($groupin)){
$group=Group::model()->findByPk($groupin->groupId);
$ihtisos=  Ihtisos::model()->findByPk($group->ihtisosId);
}
?>  
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo Yii::app()->request->baseUrl.$user->avatar;?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $user->fname.' '.$user->name?> &nbsp; 
              
              
              <p><?php
              if($user->accessId==1){

                  echo Users::returnGroup(Yii::app()->session['user']['id']);
                  
              }else{
                 echo $user->access->access;
              }
                    ?>
                  &nbsp;
              <a href="#"><i class="fa fa-circle text-success"></i> online</a></p>
              </p>
            </div>
          </div>
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
              
            <li class="header">Менюи Асосӣ</li>
             <li><a href="<?php echo Yii::app()->createUrl('site/user');?>"><i class="fa fa-home text-red"></i> <span>Асосӣ</span></a></li>
            <?php
            $menu=  UserMenu::model()->findAll(
                        array(
                            'condition'=>'status=1 and accessId='.$user->accessId,
                            'order'=>'orderId'
                        )
                    );
            
                        
                    
                    foreach ($menu as $men ){
                        $smenu=  UserMenu::model()->findAll(
                        array(
                            'condition'=>'status=1 and parentId='.$men->idMenu,
                            'order'=>'orderId'
                        )
                        );
                        
                     if($men->has_chevron=='yes'){
                     echo   ' <li class="treeview">';
                     echo   '<a href="'.Yii::app()->createUrl($men->href).'" title='.$men->title.'><i class="'.$men->icon.'"></i> <span>'.$men->name.'</span><i class="fa fa-angle-left pull-right"></i></a>';
                     
                     echo '<ul class="treeview-menu">';
                    foreach ($smenu as $smen){
                     
                     echo '<li><a href="'.Yii::app()->createUrl($smen->href).'" title="'.$smen->title.'"><i class="'.$smen->icon.'"></i>'.$smen->name.'</a></li>';
                     
                            }
                            echo '</ul>';
                echo   '</li>';
                    
                     }else{
                         if($men->parentId==0){
                     echo   ' <li>';
                     echo   '<a href="'.Yii::app()->createUrl($men->href).'" title='.$men->title.'><i class="'.$men->icon.'"></i> <span>'.$men->name.'</span></a>';
                                    
                     echo   '</li>';
                         }
                     }
                    }
            $mailbox=Users::getCountReciveViewMail(Yii::app()->user->id);
            if(Yii::app()->user->checkAccess('2')){
            echo '<li class="treeview">';
             echo '<a href="#">
                <i class="fa fa-envelope text-red"></i> <span>Мактубҳо</span>';
            if(!empty($mailbox))
           echo '<small class="label pull-right bg-yellow">'.$mailbox.'</small>';

            echo '</a>
              <ul class="treeview-menu">
                <li><a href="'.Yii::app()->createUrl('site/compose').'"><i class="fa fa-envelope text-blue"></i>Навиштани мактуб</a></li>
                <li class="active"><a href="'.Yii::app()->createUrl('site/inbox').'"><i class="fa fa-arrow-down text-green"></i>Хабарҳои воридотӣ <span class="label label-success pull-right">'.Users::getCountReciveMail(Yii::app()->user->id).'</span></a></li>
                <li><a href="'.Yii::app()->createUrl('site/send').'"><i class="fa fa-arrow-up text-green"></i>Фиристодашуда <span class="label label-primary pull-right">'.Users::getCountSendMail(Yii::app()->user->id).'</span></a></a></li>
              </ul>
            </li>';
            }
            echo   ' <li>';
            echo   '<a href="'.Yii::app()->createUrl('site/online').'" title="Одамон онлайн"><i class="fa fa-user text-success"></i><span>Онлайн '.Users::getCountOnlineUsers().' нафар</span></a>';

            echo   '</li>';

                ?>
                
                   
            
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
