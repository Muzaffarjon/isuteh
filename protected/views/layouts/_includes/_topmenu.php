<?php 
$user=Users::getUser(Yii::app()->session['user']['id']); 
$groupin=Groupin::model()->findByAttributes( array('userId'=>Yii::app()->session['user']['id']));
if(!empty($groupin->groupId)){
 $group=Group::model()->findByPk($groupin->groupId);
 $ihtisos=  Ihtisos::model()->findByPk($group->ihtisosId);
}
?> 
<header class="main-header">
        <!-- Logo -->
        <a href="<?php echo Yii::app()->createUrl('site/user')?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>СИ</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>C. И. Коллеҷ</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <div id="toogle">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
              </div>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- Tasks: style can be found in dropdown.less -->
                <li  class="dropdown messages-menu">
                    <a id="getMessage" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>

                        <?php
                            $count=Users::getCountNotReadAllMessage();
                            if($count!=0){
                                echo  '<span class="label label-success">'.$count.'</span>';
                            }

                            ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Шумо <?php echo Users::getCountNotReadAllMessage(); ?> хабари нахонда доред!</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">
                                        <div class="pull-left">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li><!-- end message -->
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">
                                        <div class="pull-left">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            AdminLTE Design Team
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">
                                        <div class="pull-left">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Developers
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">
                                        <div class="pull-left">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Sales Department
                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">
                                        <div class="pull-left">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Reviewers
                                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="<?php echo Yii::app()->createUrl('site/chat'); ?>">See All Messages</a></li>
                    </ul>
                </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo Yii::app()->request->baseUrl.$user->avatar;?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $user->fname.' '.$user->name;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo Yii::app()->request->baseUrl.$user->avatar;?>" class="img-circle" alt="User Image">
                    <p>
                      <?php 
                      //echo Yii::app()->session['user']['fname'].' '.Yii::app()->session['user']['name']
                      echo $user->fname.' '.$user->name;?> - <?php echo $user->access->access?><br>
                      <small><?php
                      if($user->accessId==1){
                      echo 'Ихтисос: &nbsp;'.$ihtisos->ihtisos.'<br>'; 
                      }
                              ?></small> 
                      <small><?php
                        $ex=explode('-', $user->datecreate);
                        echo 'Аъзо аз соли '.$ex[0];
                      ?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
               
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                        <a href="<?php echo Yii::app()->createUrl('site/profile/'.$user->login) ?>" class="btn btn-default btn-flat">Профайл</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo Yii::app()->createUrl('site/logout')?>" class="btn btn-default btn-flat">Баромад</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             <!-- <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>