      <footer class="main-footer">
        <div class="pull-right hidden-xs">

          <b>Version</b> <?php 
          $version=  Version::model()->find(
                  array(
                      'condition'=>'status=1',
                      'order'=>'id DESC',
                      'limit'=>1
                  )
                  );          
                  echo $version->version;
          ?>
        </div>
        <strong>&copy; <?php echo date('Y')?> <a href="http://rekstar.com">RekstaR</a>.</strong>
      </footer>
