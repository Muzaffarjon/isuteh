
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->pageTitle;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/style.css">
      <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icon.ico">
    <!-- noty -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/noty/jquery.noty.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/noty/noty_theme_default.css">
    <!-- noty -->
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/dist/css/skins/_all-skins.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

      <?php

      Yii::app()->clientScript->registerCoreScript('jquery');
      Yii::app()->clientScript->registerCoreScript('iCheck');
      ?>

    <!-- jQuery UI 1.11.4 -->
<!--    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/bootstrap/js/bootstrap.js"></script>
    <!-- Morris.js charts -->
<!--    <script src="<?php echo Yii::app()->request->baseUrl;?>/plugins/datatables/dataTables.bootstrap.min.js"></script>-->
<!--    <script src="<?php echo Yii::app()->request->baseUrl;?>/plugins/datatables/jquery.dataTables.min.js"></script>-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
    <!-- Sparkline -->
    <!-- daterangepicker -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>-->
    <!-- datepicker -->
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <!-- AdminLTE App -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/dist/js/app.min.js"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- AdminLTE for demo purposes -->


	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/source/jquery.fancybox.css" media="screen" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script>
    var timer = null,
        interval = 2000,
        value = 0;
</script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini skin-purple-light fixed">
    <div class="wrapper">

     <?php
  
     
     
    $this->renderPartial('//layouts/_includes/_topmenu');
    ?>
      <!-- Left side column. contains the logo and sidebar -->
     <?php
    //if(Yii::app()->session['user']['accessId']==2)
    $this->renderPartial('//layouts/_includes/_leftmenu');
    // else
     //ech</div>';
    ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
               'links'       => $this->breadcrumbs,
               'homeLink'=>true,
               'tagName'=>'div',
               //'separator'=>' >> ',
               'homeLink'=>CHtml::link('<i class="fa fa-home"></i>',array('site/user')),
               'activeLinkTemplate'=>' <a href="{url}">{label}</a>',
               'inactiveLinkTemplate'=>'{label}',
               'htmlOptions'=>array ('class'=>'content-header')
  )); ?><!-- breadcrumbs -->
      <?php endif?>
      <?php echo $content?>

      </div><!-- /.content-wrapper -->

       <?php
    $this->renderPartial('//layouts/_includes/_footer');
    
    $this->widget('application.extensions.fancybox.EFancyBox', array(
            'target'=>'.newshed',
            'config'=>array(
               'type' => 'ajax',
               'hideOnOverlayClick' => false,
              // 'title' => Yii::t('main','Ворид кардан'),
               'transitionIn' => 'elastic',
               'transitionOut' => 'elastic',
               'speedIn' => 200,
               'speedOut' => 200,
               'autoDimensions' => false,
               'autoSize' => false,
               'width' => 700,
               'height' => 'auto',
               'closeBtn'=> true,
               //'onClosed' => 'js:function(){parent.location = parent.location;}',
               'titlePosition' => 'over',	 
            ),
         ));

    
    $this->widget('application.extensions.fancybox.EFancyBox', array(
            'target'=>'.update',
            'config'=>array(
               'type' => 'ajax',
               'hideOnOverlayClick' => false,
              //'title' => Yii::t('main','Ворид кардан'),
               'transitionIn' => 'elastic',
               'transitionOut' => 'elastic',
               'speedIn' => 0,
               'speedOut' => 0,
               'autoDimensions' => false,
               'autoSize' => false,
               'width' => 350,
               'height' => 400,
               'closeBtn'=> true,
               'afterClose' => 'js:function(){clearInterval(timer);timer = null;}',
               'titlePosition' => 'over',
            ),
         ));

   ?>

      <!-- Control Sidebar -->

      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  </body>
</html>

