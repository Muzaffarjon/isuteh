<?php
class ULoginForm extends CFormModel
{
        public $login;
        public $password;
        public $rememberMe;

        public function rules()
        {
                return array(
                        array('login, password', 'required'),
                        array('login, password', 'purgeXSS'),
                        array('login', 'email'),
                        array('rememberMe', 'boolean'),
                        array('password', 'auth'),
                );
        }

        public function attributeLabels()
        {
                return array(
                        'login'      => Yii::t("UserAdminModule.LoginForm","E-mail"),
                        'password'   => Yii::t("UserAdminModule.LoginForm","Пароль"),
                        'rememberMe' => Yii::t("UserAdminModule.LoginForm","Запомнить"),
                );
        }

        public function purgeXSS($attr)
        {
                $this->$attr = htmlspecialchars($this->$attr, ENT_QUOTES);
                return true;
        }

        /**
         * auth 
         */
        public function auth(){
           $visit = Visit::checkUserVisit($this->login,$this->password,$_SERVER['REMOTE_ADDR']); 
           //если убрать 1==1 то один пользователь не может зайти с разных местах
           if(!$visit || 1==1){
               $user = User::model()->active()->findByAttributes(array(
                  'login'    => $this->login,
                  'password' => User::getHashedPassword($this->password),
               ));

               if (! $user) 
               {
                     $this->addError('password', Yii::t("UserAdminModule.LoginForm","Логин или пароль не правельны"));
                     return false;
               }
               else
               {
                     $name = new CUserIdentity($user->id, null);
                     $duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
                     Yii::app()->user->login($name, $duration);

                     return true;
               }
           }else{
              $this->addError('login', Yii::t("UserAdminModule.LoginForm",""));
              $this->addError('password', Yii::t("UserAdminModule.LoginForm","Данный пользователь уже онлайн в системе!"));
              return false;
           }
        }
}
