<?php

class UserRole extends CActiveRecord{
        /**
         * Used in view by checkBoxList() 
         * 
         * @var array
         */
   public $taskIds = array();

   public function getSearchFields(){
      return array(
         'name' => array(
            'type'  => 'string',
            'value' => Yii::t('main', 'Название')
         ),
         'code' => array(
            'type'  => 'string',
            'value' => Yii::t('main', 'Код')
         ),
         'home_page' => array(
            'type'  => 'string',
            'value' => Yii::t('main', 'Главная страница')
         ),
         'description' => array(
            'type'  => 'string',
            'value' => Yii::t('main', 'Описание')
         ),
      );
   }
   
   public static function getAllRoles($status, $condition = ''){
      $sql   = sprintf("SELECT * FROM user_role WHERE status = %d", $status);
      
      if($condition)
         $sql .= $condition;
      
      $sql .= " ORDER BY id DESC";
         
      return Yii::app()->db->createCommand($sql)->queryAll();
   }
   
   public static function roleIdExists($idRole){
      $sql   = "SELECT count(id) as mik FROM user_role WHERE id=:id";
      $query = Yii::app()->db->createCommand($sql)->bindParam(':id', $idRole, PDO::PARAM_INT)->queryRow();
      return (sizeof($query)==1 && $query['mik']==1)?1:0;
   }
   
   public static function restoreRole($idRole){
      if(self::roleIdExists($idRole)){
         return Yii::app()->db->createCommand("UPDATE user_role SET status = 1 WHERE id=:id")
               ->bindParam(":id", $idRole, PDO::PARAM_INT)
               ->execute();
      }
   }
   
   public static function archiveRole($idRole){
      if(self::roleIdExists($idRole)){
         return Yii::app()->db->createCommand("UPDATE user_role SET status = 0 WHERE id=:id")
               ->bindParam(":id", $idRole, PDO::PARAM_INT)
               ->execute();
      }
   }
   
   public static function deleteRole($idRole, $delete = false){
      if(self::roleIdExists($idRole)){
         if(!$delete){
            return Yii::app()->db->createCommand("UPDATE user_role SET status = -1 WHERE id=:id")
               ->bindParam(":id", $idRole, PDO::PARAM_INT)
               ->execute();
         }else{
            return Yii::app()->db->createCommand("DELETE FROM user_role WHERE id=:id")
               ->bindParam(":id", $idRole, PDO::PARAM_INT)
               ->execute();
         }
      }
   }

   public static function model($className=__CLASS__){
      return parent::model($className);
   }

   public function tableName(){
      return 'user_role';
   }

   public function rules()   {
      return array(
         array('name, code', 'required'),
         array('code', 'unique'),
         array('code', 'checkCode'),
         array('name', 'length', 'max'=>50),
         array('code', 'length', 'max'=>100),
         array('description, home_page', 'length', 'max'=>255),

         array('id, name, description, code', 'safe', 'on'=>'search'),
      );
   }

   /**
   * Code should not be "isSuperAdmin" or "tasks"
   */
   public function checkCode()
   {
      $code = trim(strtolower($this->code));

      if ( ($code == 'issuperadmin') OR ($code == 'tasks'))
            $this->addError('code', Yii::t("UserAdminModule.admin","Choose another code"));
   }

   public function relations()
   {
      return array(
            'users' => array(self::MANY_MANY, 'User', 'user_has_user_role(user_role_code, user_id)'),
            'tasks' => array(self::MANY_MANY, 'UserTask', 'user_role_has_user_task(user_role_id, user_task_id)'),
      );
   }

   public function attributeLabels()
   {
         return array(
               'id'          => 'ID',
               'name'        => Yii::t("UserAdminModule.label",'Название'),
               'code'        => Yii::t("UserAdminModule.label",'Код'),
               'home_page'   => Yii::t("UserAdminModule.label",'Главная страница'),
               'description' => Yii::t("UserAdminModule.label",'Описание'),
         );
   }

   public function search()
   {
      $criteria=new CDbCriteria;

      $criteria->compare('id',$this->id);
      $criteria->compare('name',$this->name,true);
      $criteria->compare('code',$this->code,true);
      $criteria->compare('description',$this->description,true);

      return new CActiveDataProvider($this, array(
         'criteria'=>$criteria,
         'sort'=>array(
                  'defaultOrder'=>'id DESC',
         ),
         'pagination'=>array(
                  'pageSize'=>Yii::app()->user->getState('pageSize',20), 
         ),
      ));
   }

   /**
   * afterDelete 
   * 
   * Reset cache
   */
   protected function afterDelete()
   {
      UserCache::model()->updateAll(array(
            'status' => 0,
      ));
   }
}
