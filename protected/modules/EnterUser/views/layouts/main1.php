<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/module/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/module/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/module/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/module/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/module/form.css" />
   <?php Yii::app()->bootstrap->register(); ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>   
   <?php $this->widget('bootstrap.widgets.TbNavbar', array(
      'type'=>'null', // null or 'inverse'
      'brand'=>Yii::t('main', Yii::app()->name),
      'brandUrl'=>array('/'),
      'collapse'=>false, // requires bootstrap-responsive.css
      'fixed' => 'top',
      'items'=>array(
         array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
               array('label'=>Yii::t('main', 'Пользователи'), 'url'=>array('/UserAdmin/user/admin'), 'visible'=>User::checkTask('userAdmin')),
               array('label'=>Yii::t('main', 'Роли'), 'url'=>array('/UserAdmin/userRole/admin'), 'visible'=>User::checkTask('userRoleAdmin')),
               array('label'=>Yii::t('main', 'Задачи'), 'url'=>array('/UserAdmin/userTask/admin'), 'visible'=>User::checkTask('isSuperAdmin')),
               array('label'=>Yii::t('main', 'Выход'), 'url'=>array('/UserAdmin/auth/logout')),
            ),
         ),
      ),
      'htmlOptions' => array(
        'id' => 'mainMenu'
      )
   )); ?>
   
   <div class="container" id="page" style="margin-top:60px;">
      <?php echo $content; ?>

      <div id="footer">
         CopyRight &copy; <?php echo date('Y'); ?> by Avena Group.<br/>
         All Rights Reserved.<br/>
      </div><!-- footer -->
   </div><!-- page -->
</body>
</html>
