<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="language" content="en" />
      <!-- blueprint CSS framework -->
      <!--[if lt IE 8]>
         <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
      <![endif]-->
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
      <?php 
         $cs = Yii::app()->clientScript;
         $cs->registerCoreScript('jquery');
         $cs->registerScriptFile(Yii::app()->request->baseUrl."/js/jquery.cookie.js"); 
         $cs->registerScriptFile(Yii::app()->request->baseUrl."/js/prefixfree-1.0.7.js"); 
      ?>
      <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <style>
         html{
            overflow-y: scroll;
         }
         
      </style>
</head>

<body> 
      <div class="contentS">
         <div class="footer">
            <div class="logo">
               <h1><?php echo Yii::app()->name; ?></h1>
            </div>
         </div>
         <div id="mainmenu">
            <?php $this->widget('zii.widgets.CMenu',array(
               'items'=>array(
                  array('label'=>Yii::t('main', 'Главная'), 'url'=>array('/site/index')),
                  array('label'=>Yii::t('main', 'Страны'), 'url'=>array('/country/index'), 'visible'=>!Yii::app()->user->isGuest),
                  array('label'=>Yii::t('main', 'Компании'), 'url'=>array('/company/index'), 'visible'=>!Yii::app()->user->isGuest),
                  //array('label'=>Yii::t('main', 'Пользователи'), 'url'=>array('/user/index'), 'visible'=>!Yii::app()->user->isGuest),
                  array('label'=>Yii::t('main', 'Заголовки'), 'url'=>array('/label/index'), 'visible'=>!Yii::app()->user->isGuest),
                  array('label'=>Yii::t('main', 'Управление'), 'url'=>array('/UserAdmin/user/admin'), 'visible'=>User::checkTask('userAdmin')),
                  array('label'=>Yii::t('main', 'О нас'), 'url'=>array('/site/page', 'view'=>'about')),
                  array('label'=>Yii::t('main', 'Контакты'), 'url'=>array('/site/contact')),                  
                  array('label'=>Yii::t('main', 'Вход'), 'url'=>array('/UserAdmin/auth/login'), 'visible'=>Yii::app()->user->isGuest),
                  array('label'=>Yii::t('main', 'Выход'), 'url'=>array('/UserAdmin/auth/logout'), 'visible'=>!Yii::app()->user->isGuest)
               ),
            )); ?>
         </div>
         
         <div class="contentDiv">
            <?php if(isset($this->breadcrumbs)):?>
               <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                  //'separator'=>' &rarr; ',
                  'separator'=>'',
                  'links'=>$this->breadcrumbs,
                  'htmlOptions' => array(
                     'class' => 'myBreadcrumbs'
                  )
               )); ?><!-- breadcrumbs -->
            <?php endif?>
            <div id="mainmenu" style="margin: 20px 0">
               <?php $this->widget('zii.widgets.CMenu',array(
                  'items'=>array(
                     array('label'=>Yii::t('main', 'Пользователи'), 'url'=>array('/UserAdmin/user/admin'), 'visible'=>User::checkTask('userAdmin')),
                     array('label'=>Yii::t('main', 'Роли'), 'url'=>array('/UserAdmin/userRole/admin'), 'visible'=>User::checkTask('userRoleAdmin')),
                     array('label'=>Yii::t('main', 'Задачи'), 'url'=>array('/UserAdmin/userTask/admin'), 'visible'=>User::checkTask('isSuperAdmin')),
                  ),
               )); ?>
            </div>
            <?php echo $content; ?>
         </div>
      </div>
      <div id="footer">
         CopyRight &copy; <?php echo date('Y'); ?> by Avena Group.<br/>
         All Rights Reserved.<br/>
      </div><!-- footer -->
   <?php /*$this->widget('bootstrap.widgets.TbNavbar', array(
      'type'=>'null', // null or 'inverse'
      'brand'=>Yii::t('main', Yii::app()->name),
      'brandUrl'=>array('/'),
      'collapse'=>false, // requires bootstrap-responsive.css
      'fixed' => 'top',
      'items'=>array(
         array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
               array('label'=>Yii::t('main', 'Пользователи'), 'url'=>array('/UserAdmin/user/admin'), 'visible'=>User::checkTask('userAdmin')),
               array('label'=>Yii::t('main', 'Роли'), 'url'=>array('/UserAdmin/userRole/admin'), 'visible'=>User::checkTask('userRoleAdmin')),
               array('label'=>Yii::t('main', 'Задачи'), 'url'=>array('/UserAdmin/userTask/admin'), 'visible'=>User::checkTask('isSuperAdmin')),
               array('label'=>Yii::t('main', 'Выход'), 'url'=>array('/UserAdmin/auth/logout')),
            ),
         ),
      ),
      'htmlOptions' => array(
        'id' => 'mainMenu'
      )
   )); */?>
</body>
</html>
