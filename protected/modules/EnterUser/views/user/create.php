<?php $this->breadcrumbs = array(
        Yii::t("UserAdminModule.breadcrumbs","Manage users") => array('admin'),
        Yii::t("UserAdminModule.breadcrumbs","Create")
); ?>

<h1 style="display:inline-block;">
        <?php echo Yii::t("UserAdminModule.admin", "Creating"); ?>
</h1>
<div class='clearfix'>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
