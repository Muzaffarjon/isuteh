<style>
   .centered{
      text-align: center;
      font-weight: bold;
      font-size: 18px;
   }
   .table-bordered{
      width: 100%
   }
   .table-bordered td{
      border: 1px solid #ccc;
      padding: 20px;
   }
   .form input{
      vertical-align: middle;
      display: inline;
   }
   div.form label label{
      display: inline-block
   }
</style>
<?php 
   $this->breadcrumbs = array(
      Yii::t("UserAdminModule.breadcrumbs","Пользователи") => array('/user/index'),
      Yii::t("UserAdminModule.breadcrumbs","Детали")
   ); 
?>

<h1><?php echo Yii::t("UserAdminModule.admin","Детали"); ?></h1>

<div class="form" style="margin-top: 20px;" >
   <table class='table table-condensed table-bordered' style="border: 1px solid #ccc; border-radius: 10px;box-shadow: 0 0 10px #ccc;">
      <tr>
         <th class='grayHeader' width='50%'><h2 class='centered'><?php echo Yii::t("UserAdminModule.admin","Роли"); ?></h2></th>
         <?php if(User::checkRole('isSuperAdmin')): ?>
            <th class='grayHeader' width='50%'><h2 class='centered'><?php echo Yii::t("UserAdminModule.admin","Дополнительные задачи"); ?></h2></th>
         <?php endif; ?>
      </tr>
      <tr>
         <td style='padding: 10px;'>
            <?php if(Yii::app()->user->getFlash('roleSaved')): ?>
               <h4 class='alert alert-success centered hide-on-click' style="padding:10px;border-radius: 10px;">
                  <?php echo Yii::t("UserAdminModule.admin","Сохранено!"); ?>
               </h4>
            <?php endif ?>
            <?php $roleForm = $this->beginWidget('CActiveForm'); ?>
            <?php 
               echo $roleForm->checkBoxList(
                  $model, 'roleIds', 
                  CHtml::listData(UserRole::model()->findAll(), 'code', 'name'),
                  array(
                     'template'=>"<label class='checkbox'>{input} {label}</label>",
                     'separator'=>'',
                  )
               ); 
            ?>
            <br>
            <div style="text-align: center">
               <?php 
                  echo CHtml::htmlButton(
                     Yii::t("UserAdminModule.admin","Сохранить"), 
                     array(
                        'class'=>'button success',
                        'type'=>'submit',
                     )
                  ); 
               ?>
            </div>
            <?php $this->endWidget(); ?>
         </td>
         <?php if(User::checkRole('isSuperAdmin')): ?>
            <td style='padding: 10px;'>
               <?php if(Yii::app()->user->getFlash('taskSaved')): ?>
                  <h4 class='alert alert-success centered hide-on-click' style="padding:10px;border-radius: 10px;">
                     <?php echo Yii::t("UserAdminModule.admin","Сохранено!"); ?>
                  </h4>
               <?php endif ?>
               <?php $taskForm = $this->beginWidget('CActiveForm'); ?>

               <?php 
                  echo $taskForm->checkBoxList(
                     $model, 'taskIds', 
                     CHtml::listData(UserTask::model()->findAll("code != 'freeAccess'"), 'code', 'name'),
                     array(
                        'template'=>"<label class='checkbox'>{input} {label}</label>",
                        'separator'=>'',
                     )
                  ); 
               ?>
               <br>
               <div style="text-align: center">
                  <?php 
                     echo CHtml::htmlButton(
                        Yii::t("UserAdminModule.admin","Сохранить"), 
                        array(
                           'class'=>'button success',
                           'type'=>'submit',
                        )
                     ); 
                  ?>
               </div>
               <?php $this->endWidget(); ?>
            </td>
         <?php endif; ?>
      </tr>
   </table>
</div>