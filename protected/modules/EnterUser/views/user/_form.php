<style>
   
</style>
<div class='cont form'>

<?php $form = $this->beginWidget('CActiveForm', array(
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>false,
            'errorCssClass'=>'error-state error',
            'successCssClass'=>'success-state success',
        ),
)); ?>

        <?php if($model->isNewRecord OR ($model->id != Yii::app()->user->id)): ?>
                <div class='control-group'>
                        <?php echo $form->labelEx($model, 'active', array('class'=>'control-label')); ?>
                        <div class='input-control select'>
                                <?php echo $form->dropDownList(
                                        $model,'active',
                                        array('1'=>'On', '0'=>'Off'),
                                        array('class'=>'input-small')
                                ); ?>
                                
                        </div>
                        <?php echo $form->error($model, 'active',array('inputContainer' => 'div')); ?>
                </div>
        <?php endif ?>
        
        <?php if(User::checkRole('isSuperAdmin') AND (Yii::app()->user->id != $model->id)): ?>
                <div class='control-group'>
                        <?php echo $form->labelEx($model, 'is_superadmin', array('class'=>'control-label')); ?>
                        <div class='input-control select'>
                                <?php echo $form->dropDownList(
                                        $model,'is_superadmin',
                                        User::getIsSuperAdminList(false),
                                        array('empty'=>'', 'class'=>'input-small')
                                ); ?>
                                
                        </div>
                        <?php echo $form->error($model, 'is_superadmin',array('inputContainer' => 'div')); ?>
                </div>
        <?php endif ?>

        <div class='control-group'>
                <?php echo $form->labelEx($model, 'login', array('class'=>'control-label')); ?>
                <div class='input-control text'>
                        <?php echo $form->textField($model, 'login', array('class'=>'input-xxlarge', 'autocomplete'=>'off')); ?>
                </div>
                <?php echo $form->error($model, 'login',array('inputContainer' => 'div')); ?>
        </div>

        <div class='control-group'>
                <?php echo $form->labelEx($model, 'password', array('class'=>'control-label')); ?>
                <div class='input-control text'>
                        <?php echo $form->passwordField($model,'password',array('class'=>'input-xxlarge')); ?>
                </div>
                <?php echo $form->error($model, 'password',array('inputContainer' => 'div')); ?>
        </div>

        <div class='control-group'>
                <?php echo $form->labelEx($model, 'repeat_password', array('class'=>'control-label')); ?>
                <div class='input-control text'>
                        <?php echo $form->passwordField($model, 'repeat_password', array('class'=>'input-xxlarge')); ?>
                </div>
                <?php echo $form->error($model, 'repeat_password',array('inputContainer' => 'div')); ?>
        </div>

        <br>
        <?php echo CHtml::htmlButton(
                $model->isNewRecord ? 
                '<i class="icon-plus-sign icon-white"></i> '.Yii::t("UserAdminModule.admin","Create") : 
                '<i class="icon-ok icon-white"></i> '.Yii::t("UserAdminModule.admin","Save"), 
                array(
                        'class'=>'button success',
                        'type'=>'submit',
                )
        ); ?>


<?php $this->endWidget(); ?>

</div>
