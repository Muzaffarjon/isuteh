<?php $this->breadcrumbs = array(Yii::t("UserAdminModule.breadcrumbs","Управление пользователями")); ?>

<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>
<h1 style="display:inline-block;"><?php echo Yii::t('UserAdminModule.admin','Управление пользователями'); ?></h1>

<?php echo CHtml::link(
        '<i class="icon-plus-2 on-left"></i> '.Yii::t('UserAdminModule.admin','Добавить запись'), 
        array('create'),
        array('class'=>'button primary large aAddNew')
); ?>


<?php $form=$this->beginWidget("CActiveForm"); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
      'ajaxUpdate'=>false,
	'filter'=>$model,
      'pager' => array(
         'firstPageLabel'=>'<i class="icon-first-2"></i>',
         'prevPageLabel'=>'<i class="icon-previous"></i>',
         'nextPageLabel'=>'<i class="icon-next"></i>',
         'lastPageLabel'=>'<i class="icon-last-2"></i>',
         'maxButtonCount'=>'5',
         'header'=>'',
         'internalPageCssClass'=>'pageM',
         'selectedPageCssClass'=>'active',
      ),
      'template'=>'{pager}{items}{summary}',
      'summaryText' => 'Всего строк: {start} - {end} из {count}',
	'columns'=>array(
         array(
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows'=>2,
         ),
         array(
            'header'=>'№',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions'=>array(
               'width'=>'25',
               'class'=>'centered'
            )
         ),
         array(
            'name'=>'login',
            'value'=>'CHtml::link($data->login, array("view", "id"=>$data->id))',
            'type'=>'raw',
         ),
         array(
            'name'=>'findByRole',
            'filter'=>CHtml::listData(UserRole::model()->findAll(), 'code', 'name'),
            'value'=>'User::getRoles($data->roles)',
         ),
         array(
            'name'=>'is_superadmin',
            'filter'=>User::getIsSuperAdminList(false),
            'value'=>'User::getIsSuperAdminValue($data->is_superadmin)',
            'type'=>'raw',
            'visible'=>User::checkRole('isSuperAdmin'),
            'htmlOptions'=>array(
               'width'=>'55',
               'style'=>'text-align:center',
            )
         ),
         array(
            'name'=>'active',
            'filter'=>array(1=>'On', 0=>'Off'),
            'value'=>'UHelper::attributeToggler($data, "active")',
            'type'=>'raw',
            'htmlOptions'=>array(
               'width'=>'55',
               'style'=>'text-align:center',
            )
         ),
         
         array(
            'class'=>'CButtonColumn',
            'buttons'=>array(
               'delete'=>array(
                  'visible'=>'($data->id != Yii::app()->user->id)',
               ),
            ),
            'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100,200=>200),array(
               'onchange'=>"$.fn.yiiGridView.update('user-grid',{ data:{pageSize: $(this).val() }})",
               'style'=>'width:50px'
            )),
         ),
	),
      //'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>


<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('user-grid');
}
</script>

<?php echo CHtml::ajaxSubmitButton("",array(), array(),array("style"=>"visibility:hidden;")); ?>
<div style="border:0px solid;text-align: right;margin: 0">
   <?php echo CHtml::ajaxSubmitButton(
      Yii::t("UserAdminModule.admin", "Delete selected"),
      array("deleteSelected"), 
      array("success"=>"reloadGrid"),
      array(
         "class"=>"button danger", 
         "confirm"=>Yii::t("UserAdminModule.admin", "Delete selected elements ?"),
      )
   ); ?>
</div>
<?php $this->endWidget(); ?>
