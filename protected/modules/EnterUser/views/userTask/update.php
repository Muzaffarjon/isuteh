<?php $this->breadcrumbs = array(
        Yii::t("main","Пользователи") => array('/user/index'),
        Yii::t("UserAdminModule.breadcrumbs","Задачи пользователей") => array('admin'),
        Yii::t("UserAdminModule.breadcrumbs","Редактирование")
); ?>

<h1 style="display:inline-block;">
        <?php echo Yii::t("UserAdminModule.admin", "Редактирование"); ?>
</h1>
<div class='clearfix'>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
