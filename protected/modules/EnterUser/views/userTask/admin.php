<?php 
   $this->breadcrumbs = array(
      Yii::t("main","Пользователи") => array('/user/index'),
      Yii::t("UserAdminModule.breadcrumbs","Задачи пользователей")
   ); 
?>

<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>

<?php  
   $this->renderPartial('//layouts/titleIndex', array(
      'title'       => Yii::t('UserAdminModule.admin','Задачи пользователей'),
      'url'         => array('create'),
   ));      
?>
<?php if(Yii::app()->user->hasFlash('userTaskSuccess')): ?>
   <div class="flash-success" style="text-align:center;">
      <?php echo Yii::app()->user->getFlash('userTaskSuccess'); ?>
      <a href="javascript:close_flash_success();" class="icon-cancel-2 cancel-flash"></a>
   </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('userTaskError')): ?>
   <div class="flash-error" style="text-align:center;">
      <?php echo Yii::app()->user->getFlash('userTaskError'); ?>
      <a href="javascript:close_flash_error();" class="icon-cancel-2 cancel-flash"></a>
   </div>
<?php endif; ?>  
<?php $form=$this->beginWidget("CActiveForm"); ?>
<?php  
   $this->renderPartial('//layouts/topGrid', array(
      'search'       => $search,
      'postSearch'   => $postSearch,
      'updateButton' => $updateButton,
      'otherButtons' => array(
         CHtml::submitButton('Детали', array('name'=>'detail', 'style'=>'margin:0','class'=>'button default')),
      )
   )); 
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-task-grid',
	'dataProvider'=>$dataProvider,
      'pager' => array(
         'firstPageLabel'=>'<i class="icon-first-2"></i>',
         'prevPageLabel'=>'<i class="icon-previous"></i>',
         'nextPageLabel'=>'<i class="icon-next"></i>',
         'lastPageLabel'=>'<i class="icon-last-2"></i>',
         'maxButtonCount'=>'5',
         'header'=>'',
         'internalPageCssClass'=>'pageM',
         'selectedPageCssClass'=>'active',
      ),
      'template'=>'{pager}{items}{summary}',
      'summaryText' => 'Всего строк: {start} - {end} из {count}',
	'columns'=>array(
         array(
            'class'=>'CCheckBoxColumn',
            'name' => 'id',
            'id' => 'taskCheck',
            'selectableRows' => 2,
         ),
         array(
            'header' => 'Название',
            'name'   => 'name',
            //'value'  => 'CHtml::link($data->name, array("view", "id"=>$data->id))',
            'value'  => 'CHtml::link($data["name"],array("view","id"=>$data["id"]),array("style"=>"text-decoration:underline"))',
            'type'   => 'raw',
         ),
         array(
            'header' => 'Код',
            'name'   => 'code',
            'type'   => 'raw',
            'htmlOptions' => array(
               'style'   => 'text-align:right',
            )
         ),
         array(
            'header' => 'Описание',
            'name'   => 'description',
            'type'   => 'raw',
            'htmlOptions' => array(
               'style'   => 'text-align:left',
            )
         ),       
	),
        //'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>
<div style="text-align: center">
<?php 
   $this->renderPartial('//layouts/links');
?>
</div>
<?php $this->endWidget(); ?>
