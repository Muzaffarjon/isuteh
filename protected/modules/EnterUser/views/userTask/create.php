<?php $this->breadcrumbs = array(
        Yii::t("main","Пользователи") => array('/user/index'),
        Yii::t("UserAdminModule.breadcrumbs","Задачи пользователей") => array('admin'),
        Yii::t("UserAdminModule.breadcrumbs","Добавление")
); ?>

<h1 style="text-align: center">
        <?php echo Yii::t("UserAdminModule.admin", "Добавление"); ?>
</h1>
<div class='clearfix'>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
