<style>
   .centered{
      text-align: center;
      font-weight: bold;
      font-size: 18px;
   }
   .table-bordered{
      width: 100%
   }
   .table-bordered td{
      border: 1px solid #ccc;
      padding: 10px;
   }
</style>
<?php $this->breadcrumbs = array(
        Yii::t("main","Пользователи") => array('/user/index'),
        Yii::t("UserAdminModule.breadcrumbs","Задачи пользователей") => array('admin'),
        Yii::t("UserAdminModule.breadcrumbs","Детали")
); ?>

<h1 style="display:inline-block;">
        <?php echo Yii::t("UserAdminModule.admin","Детали"); ?>
</h1>
<div class="default_table">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$taskDetails,
	'attributes'=>array(
		'name',
		'code',
                array(
                        'name'=>'description',
                        'value'=>nl2br($taskDetails->description),
                        'type'=>'raw',
                ),
	),
        //'htmlOptions'=>array('class'=>'table table-condensed table-striped table-hover table-bordered table-side'),
        'htmlOptions'=>array('class'=>'items'),
)); ?>
</div>
<style>
   .form input{
      vertical-align: middle;
   }
</style>
<div class="cont" style="border:1px dotted #ccc; padding: 10px;box-shadow: 0 0 10px #ccc" class="form">
   <?php if(Yii::app()->user->getFlash('success')): ?>
      <h4 class='alert alert-success centered hide-on-click' style="padding:10px;border-radius: 10px;">
         <?php echo Yii::t("UserAdminModule.admin","Изменения сохранены!"); ?>
      </h4>
   <?php endif ?>

   <?php echo CHtml::beginForm(); ?>
      <div style='text-align:left'>
         <?php 
            echo CHtml::htmlButton(
               '<i class="icon-ok icon-white"></i> '.Yii::t("UserAdminModule.admin","Сохранить"), 
               array(
                  'class'=>'button success',
                  'type'=>'submit',
               )
            ); 
         ?>
         &nbsp;
         &nbsp;
         <?php echo CHtml::link(
            Yii::t("UserAdminModule.admin","Обновить лист контроллеров"), 
            array('refreshOperations', 'id'=>$taskDetails->id),
            array('class'=>'button success','style'=>'float:right')
         ); ?>
      </div>
      <br>
      <table class='table table-bordered table-condensed' style="border:1px solid #ccc;box-shadow: 0 0 10px #ccc;">
         <tr>
            <th class='grayHeader' width='50%'><h2 class='centered'><?php echo Yii::t("UserAdminModule.admin","Общие контроллеры"); ?></h2></th>
            <th class='grayHeader' width='50%'><h2 class='centered'><?php echo Yii::t("UserAdminModule.admin","Модули"); ?></h2></th>
         </tr>
         <tr>
            <td class='controllers-box'>
               <?php $display = array(); ?>
               <?php foreach($generalControllers as $generalController): ?>
                  <?php 
                     $controllerName = UserTask::getModuleAndControllerName($generalController->route);
                     if(in_array($generalController->id, $operationIds)){
                        $display[$controllerName] = true;
                     }
                  ?>
               <?php endforeach; ?>
               
               <?php foreach($generalControllers as $generalController): ?>
                  <?php
                     $controllerName = UserTask::getModuleAndControllerName($generalController->route);
                     if(!isset($prevName) OR ($prevName != $controllerName)){
                        $prevName = $controllerName;
                        echo "<div class='controllerNameContainer' style='font-weight:bold'>".
                             "   <span class='commonControllerName' param='{$controllerName}'><i class='icon-plus'></i> {$controllerName}</span></div>";
                     } 
                  ?>

                  <label class="checkbox  commonControllerHide commonController<?php echo $controllerName; ?>" style="font-weight:normal;<?php echo array_key_exists($controllerName, $display) ? '' : 'display:none' ?>">
                     <?php 
                        echo CHtml::checkBox(
                           'Operation['.$generalController->id.']',
                           in_array($generalController->id, $operationIds),
                           array(
                              'value'=>$generalController->id,
                              'class'=>'commonControllerCheckBox'.$controllerName,
                              'style'=>'margin-left:20px;',
                           )
                        ); 
                     ?>
                     <?php echo UserTask::pretifyRoute($generalController->route); ?>
                  </label>
               <?php endforeach ?>
            </td>
            <td>
               <div class='controllers-box'>
                  <?php $displayModule = array();?>
                  <?php foreach($moduleControlers as $moduleControler): ?>
                     <?php 
                        $tmp = UserTask::getModuleAndControllerName($moduleControler->route);
                        $controllerName = $tmp['controller'];
                        if(in_array($moduleControler->id, $operationIds)){
                           $displayModule[$controllerName] = true;
                        }
                     ?>
                  <?php endforeach; ?>
                  <?php foreach($moduleControlers as $moduleControler): ?>
                     <?php
                        $tmp = UserTask::getModuleAndControllerName($moduleControler->route);
                        $moduleName = $tmp['module'];
                        $controllerName = $tmp['controller'];

                        if (! isset($prevModuleName) OR ($prevModuleName != $moduleName) ){
                           $prevModuleName = $moduleName;
                           echo "<h4>{$moduleName} <br/><span class='btn btn-small moduleHider' style='cursor:pointer' param='{$moduleName}'>".
                                 Yii::t("UserAdminModule.admin","Свернуть всё!").
                                 "</span> | <span class='btn btn-small btn-success moduleOpener' style='cursor:pointer' param='{$moduleName}'>".
                                 Yii::t("UserAdminModule.admin","Развернуть всё!").
                                 "</span></h4><hr/>";
                        }
                        if (! isset($prevControllerName) OR ($prevControllerName != $controllerName) )
                        {
                           $prevControllerName = $controllerName;
                           echo "<div class='controllerNameContainer' style='font-weight:bold'>".
                                    " <span class='moduleControllerName' param='{$moduleName}-{$controllerName}'><i class='icon-plus'> </i>{$controllerName}</span></div>";
                        } 
                        ?>

                        <label class='checkbox  module<?php echo $moduleName; ?> moduleController<?php echo $moduleName.'-'.$controllerName; ?>' style="font-weight: normal;<?php echo array_key_exists($controllerName, $displayModule) ? '' : 'display:none' ?>">
                           <?php
                              echo CHtml::checkBox(
                                 'Operation['.$moduleControler->id.']',
                                 in_array($moduleControler->id, $operationIds),
                                 array(
                                    'value' => $moduleControler->id,
                                    'class' => 'moduleControllerCheckBox'.$moduleName.$controllerName,
                                    'style' => 'margin-left:20px'
                                 )
                              ); 
                           ?>
                           <?php echo UserTask::pretifyRoute($moduleControler->route); ?>
                        </label>
                     <?php endforeach ?>
                  </div>
               </td>
            </tr>
         </table>

        <div style='text-align:left'>
            <?php 
               echo CHtml::htmlButton(
                  '<i class="icon-ok icon-white"></i> '.Yii::t("UserAdminModule.admin","Сохранить"), 
                  array(
                     'class'=>'button success',
                     'type'=>'submit',
                  )
               ); 
            ?>
            &nbsp;
            &nbsp;
            <?php echo CHtml::link(
               Yii::t("UserAdminModule.admin","Обновить лист контроллеров"), 
               array('refreshOperations', 'id'=>$taskDetails->id),
               array('class'=>'button success','style'=>'float:right')
            ); ?>
         </div>
<?php echo CHtml::endForm(); ?>
</div>