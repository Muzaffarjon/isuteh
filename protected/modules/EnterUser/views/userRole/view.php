<style>
   .centered{
      text-align: center;
      font-weight: bold;
      font-size: 18px;
   }
   .table-bordered{
      width: 100%
   }
   .table-bordered td{
      border: 1px solid #ccc;
      padding: 20px;
   }
   .form input{
      vertical-align: middle;
      display: inline;
   }
   div.form label label{
      display: inline-block
   }
</style>
<?php $this->breadcrumbs = array(
        Yii::t("main","Пользователи") => array('/user/index'),
        Yii::t("UserAdminModule.breadcrumbs","Роли пользователей") => array('admin'),
        Yii::t("UserAdminModule.breadcrumbs","Детали")
); ?>

<h1>
   <?php echo Yii::t("UserAdminModule.admin","Детали"); ?>
</h1>
<div class="default_table">
   <?php $this->widget('zii.widgets.CDetailView', array(
         'data'=>$model,
         'attributes'=>array(
            'name',
            'code',
            array(
               'name'=>'description',
               'value'=>nl2br($model->description),
               'type'=>'raw',
            ),
         ),
         'htmlOptions'=>array('class'=>'items'),
   )); ?>
</div>

<div class="form">
   <table class='table table-bordered table-condensed' style="border: 1px solid #ccc;box-shadow: 0 0 10px #ccc;">
      <tr>
         <th class='grayHeader'><h2 class='centered'><?php echo Yii::t("UserAdminModule.admin","Задачи"); ?></h2></th>
      </tr> 
      <tr>
         <td>
            <?php if(Yii::app()->user->getFlash('taskSaved')): ?>
               <h4 class='alert alert-success centered hide-on-click' style="padding:10px;border-radius: 10px;">
                  <?php echo Yii::t("UserAdminModule.admin","Изменения сохранены!"); ?>
               </h4>
            <?php endif ?>
            
            <?php $taskForm = $this->beginWidget('CActiveForm'); ?>

            <?php 
               echo $taskForm->checkBoxList(
                  $model, 'taskIds', 
                  CHtml::listData(UserTask::model()->findAll("code != 'freeAccess'"), 'id', 'name'),
                  array(
                     'template'=>"<label class='checkbox'>{input} {label}</label>",
                     'separator'=>'',
                  )
               ); 
            ?>

            <br>
            <div style="text-align: center">
               <?php 
                  echo CHtml::htmlButton(
                     '<i class="icon-ok icon-white"></i> '.Yii::t("UserAdminModule.admin","Сохранить"), 
                     array(
                        'class'=>'button success',
                        'type'=>'submit',
                     )
                  ); 
               ?>
            </div>
            <?php $this->endWidget(); ?>
         </td>
      </tr>
   </table>
</div>
