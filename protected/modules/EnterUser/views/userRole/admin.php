<?php 
   $this->breadcrumbs = array(
      Yii::t("main","Пользователи") => array('/user/index'),
      Yii::t("UserAdminModule.breadcrumbs","Роли пользователей")
   ); 
?>

<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>

<?php  
   $this->renderPartial('//layouts/titleIndex', array(
      'title'       => Yii::t('UserAdminModule.admin','Роли пользователей'),
      'url'         => array('create'),
   ));      
?>
<?php if(Yii::app()->user->hasFlash('userRoleSuccess')): ?>
   <div class="flash-success" style="text-align:center;">
      <?php echo Yii::app()->user->getFlash('userRoleSuccess'); ?>
      <a href="javascript:close_flash_success();" class="icon-cancel-2 cancel-flash"></a>
   </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('userRoleError')): ?>
   <div class="flash-error" style="text-align:center;">
      <?php echo Yii::app()->user->getFlash('userRoleError'); ?>
      <a href="javascript:close_flash_error();" class="icon-cancel-2 cancel-flash"></a>
   </div>
<?php endif; ?>  

<?php $form=$this->beginWidget("CActiveForm"); ?>
<?php  
   $this->renderPartial('//layouts/topGrid', array(
      'search'       => $search,
      'postSearch'   => $postSearch,
      'updateButton' => $updateButton,
      'otherButtons' => array(
         CHtml::submitButton('Детали', array('name'=>'tasks', 'style'=>'margin:0','class'=>'button default')),
      )
   )); 
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-role-grid',
	'dataProvider'=>$dataProvider,
      //'ajaxUpdate'=>false,
	//'filter'=>$model,
      'pager' => array(
         'firstPageLabel'=>'<i class="icon-first-2"></i>',
         'prevPageLabel'=>'<i class="icon-previous"></i>',
         'nextPageLabel'=>'<i class="icon-next"></i>',
         'lastPageLabel'=>'<i class="icon-last-2"></i>',
         'maxButtonCount'=>'5',
         'header'=>'',
         'internalPageCssClass'=>'pageM',
         'selectedPageCssClass'=>'active',
      ),
      'template'=>'{pager}{items}{summary}',
      'summaryText' => 'Всего строк: {start} - {end} из {count}',
	'columns'=>array(
         array(
            'class'=>'CCheckBoxColumn',
            'name' => 'id',
            'id' => 'roleCheck',
            'selectableRows' => 2,
         ),
         array(
            'header' => 'Название',
            'name'   => 'name',
            //'value'  => 'CHtml::link($data->name, array("view", "id"=>$data->id))',
            'value'  => 'CHtml::link($data["name"],array("view","id"=>$data["id"]),array("style"=>"text-decoration:underline"))',
            'type'   => 'raw',
         ),
         array(
            'header' => 'Код',
            'name'   => 'code',
            'type'   => 'raw',
            'htmlOptions' => array(
               'style'   => 'text-align:right',
            )
         ),
         array(
            'header' => 'Главная страница',
            'name'   => 'home_page',
            'type'   => 'raw',
            'htmlOptions' => array(
               'style'   => 'text-align:left',
            )
         ), 
         array(
            'header' => 'Описание',
            'name'   => 'description',
            'type'   => 'raw',
            'htmlOptions' => array(
               'style'   => 'text-align:left',
            )
         ),       
	),
       // 'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>
<div style="text-align: center">
<?php 
   $this->renderPartial('//layouts/links');
?>
</div>
<?php $this->endWidget(); ?>
