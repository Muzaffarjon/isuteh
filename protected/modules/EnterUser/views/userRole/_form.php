
<div class='cont form'>

<?php $form = $this->beginWidget('CActiveForm', array(
        'enableClientValidation'=>true,
        'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false,
           'errorCssClass'=>'error-state error',
            'successCssClass'=>'success-state success',
        ),
)); ?>
   <span class="note"><?php echo Yii::t('main', 'Поля, отмеченные <span class="required">*</span> обязательны для заполнения');?>.</span>
      
      <div class="row">
         <?php echo $form->labelEx($model,'name'); ?>
         <div class="input-control text" data-role="input-control" style="margin-bottom: 0px;">
            <?php echo $form->textField($model,'name',array()); ?>
         </div>
         <?php echo $form->error($model,'name',array('inputContainer' => 'div')); ?>
      </div>
   
      <div class="row">
         <?php echo $form->labelEx($model,'code'); ?>
         <div class="input-control text" data-role="input-control" style="margin-bottom: 0px;">
            <?php echo $form->textField($model,'code',array()); ?>
         </div>
         <?php echo $form->error($model,'code',array('inputContainer' => 'div')); ?>
      </div>

      <div class="row">
         <?php echo $form->labelEx($model,'home_page'); ?>
         <div class="input-control text" data-role="input-control" style="margin-bottom: 0px;">
            <?php echo $form->textField($model,'home_page',array()); ?>
         </div>
         <?php echo $form->error($model,'home_page',array('inputContainer' => 'div')); ?>
      </div>

      <div class="row">
         <?php echo $form->labelEx($model,'description'); ?>
         <div class="input-control textarea" data-role="input-control" style="margin-bottom: 0px;">
            <?php echo $form->textArea($model,'description',array('rows'=>5)); ?>
         </div>
         <?php echo $form->error($model,'description',array('inputContainer' => 'div')); ?>
      </div>
      <div style="text-align: center">
        <?php echo CHtml::htmlButton(
                $model->isNewRecord ? 
                '<i class="icon-plus-sign icon-white"></i> '.Yii::t("UserAdminModule.admin","Добавить") : 
                '<i class="icon-ok icon-white"></i> '.Yii::t("UserAdminModule.admin","Сохранить"), 
                array(
                        'class'=>'button success',
                        'type'=>'submit',
                )
        ); ?>
      </div>
<?php $this->endWidget(); ?>

</div>
