<?php
   $this->pageTitle = 'Авторизация | ' . Yii::app()->name;
?>
<style>
   .h1{
      text-align: center;
      padding: 60px 0px 20px;
      font-size: 33px;
      line-height: 40px;
      border-bottom: 3px solid #FECB1C;
      text-transform: uppercase;
      font-family: "sansus_webissimoregular";
      margin-bottom: 30px;
   }
   .row{
      text-align: left
   }
   .txt{
      box-shadow: 0 0 10px #ccc;
      border: 1px solid #999;
      padding: 0px 20px;
      height: 48px;
      border-radius: 10px;
      color: #000 !important;
      vertical-align: middle;
   }
</style>
<div style="width: 400px;text-align: center;margin: auto">
   <div class="h1">Авторизация</div>

   <div class="cont form" style="box-shadow: 0 0 10px #999;">
      <?php $form = $this->beginWidget('CActiveForm', array(
         'focus'=>array($model, 'login'),
         'id'=>'login-form',
         'enableClientValidation'=>true,
         'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'errorCssClass'=>'error-state error',
            'successCssClass'=>'success-state success',
         ),
      )); ?>
         <div class="row">
            <?php echo $form->labelEx($model, 'login'); ?>
            <div class="input-control text" data-role="input-control" style="margin-bottom: 0px;">
               <?php echo $form->textField($model, 'login',array('autocomplete'=>'off','class'=>'txt')); ?>
            </div>
            <?php echo $form->error($model, 'login'); ?>
         </div>

         <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?>
            <div class="input-control text" data-role="input-control" style="margin-bottom: 0px;">
               <?php echo $form->passwordField($model, 'password',array('class'=>'txt')); ?>
            </div>
            <?php echo $form->error($model, 'password'); ?>
         </div>

         <div class='row rememberMe'>
            <?php echo $form->checkBox($model,'rememberMe', array('style'=>'display:inline-block;margin:3px;margin-left:0;')); ?>
            <?php echo $form->label($model,'rememberMe', array('style'=>'display:inline-block;margin:3px;margin-left:1px;')); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
         </div>

         <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main', 'Войти'),array('class'=>'button success','style'=>'width:100%;padding:15px;border-radius:10px;box-shadow:0 0 10px #ccc')); ?>
         </div>

      <?php $this->endWidget(); ?>
   </div><!-- form -->
</div>
