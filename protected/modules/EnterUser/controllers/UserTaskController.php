<?php
class UserTaskController extends AdminDefaultController
{
        public $modelName = 'UserTask';
        public $noPages = array('index', 'redirect'=>array('admin'));
        public $createRedirect = array('admin');
        public $updateRedirect = array('admin');

        /**
         * actionView 
         * 
         * @param int $id 
         */

   public function actionAdmin(){
      
      if(isset($_POST['update']) || isset($_POST['restore']) || isset($_POST['archive']) || isset($_POST['delete']) || isset($_POST['detail'])){
         if(isset($_POST['taskCheck'])){
            $dataIds = array();
            $mess    = ''; 
            foreach($_POST['taskCheck'] as $idTask) { 
               if(UserTask::taskIdExists($idTask)){
                  $dataIds[] = $idTask;
                  if(isset($_POST['restore'])){
                     if(UserTask::restoreTask($idTask))
                        $mess = Yii::t('main', 'Задача(и) успешно восстановлен(ы)!');
                  }elseif(isset($_POST['archive'])){
                     if(UserTask::archiveTask($idTask))       
                        $mess = Yii::t('main', 'Задача(и) успешно перемещен(ы) в архив!');
                  }elseif(isset($_POST['delete'])){
                     $delete =  Yii::app()->session['statusPage'] == -1 ? true : false;
                     UserTask::deleteTask($idTask, $delete);                        
                     if($delete){
                        $mess = Yii::t('main', 'Задача(и) успешно удален(ы)!');
                     }else{
                        $mess = Yii::t('main', 'Задача(и) успешно перемещен(ы) в корзину!');
                     }
                  }
               } 
            }
      
            if(isset($_POST['update'])){
               foreach($dataIds as $idTask){
                  $this->redirect(array('update', 'id' => $idTask));
               }               
            }elseif(isset($_POST['detail'])){
               foreach($dataIds as $idTask){
                  $this->redirect(array('view', 'id' => $idTask));
               }               
            }
         }    
         if($mess != ''){
            Yii::app()->user->setFlash('userTaskSuccess',$mess);
         } 
         $this->refresh();
         
      }
      
      $search = new Search($this->modelName); 

      if($_GET['resetSearch'] == 'on'){
         $_SESSION['Search'][$this->id][$this->action->id] = array();
      }      
      
      if(isset($_POST['Search'])){
         $session = Yii::app()->session['Search'];
         $session[$this->id][$this->action->id] = $_POST['Search'];
         Yii::app()->session['Search'] = $session;
      }
            
      $postSearch = Yii::app()->session['Search'][$this->id][$this->action->id];
      
      $search->generateCondition($postSearch);

      $array = UserTask::getAllTasks(Yii::app()->session['statusPage'], $search->getCondition());
      $dataProvider = new CArrayDataProvider(
         $array, array(
            'sort' => array(
               'attributes'   => array('id', 'name','code','description'),
               'defaultOrder' => array('id'=>true),
            ),
            'pagination'  => array(
               'pageSize' => 30,
            ),
      ));
      
      $this->render('admin',array(
         'model'            => $model,
         'dataProvider'     => $dataProvider,
         'error'            => $error,
         'search'           => $search,
         'postSearch'       => $postSearch 
      ));

   }
        
        public function actionView($id)
        {
                if (isset($_POST['Operation']))
                {
                        // Reset cache
                        UserCache::model()->updateAll(array(
                                'status' => 0,
                        ));

                        UserTaskHasUserOperation::model()->deleteAll('user_task_id = '.$id);
                        foreach ($_POST['Operation'] as $user_operation_id) 
                        {
                                $model = new UserTaskHasUserOperation;
                                $model->user_task_id = $id;
                                $model->user_operation_id = $user_operation_id;
                                $model->save(false);
                        }

                        Yii::app()->user->setFlash('success', 'aga');
                        $this->redirect(array('view', 'id'=>$id));
                }

                $taskDetails = $this->loadModel($id);

                $generalControllers = UserOperation::model()->findAll('is_module = 0');
                $moduleControlers = UserOperation::model()->findAll('is_module = 1');

                // Get ids of operations realted to this task (for checkboxes)
                $uthuoS = UserTaskHasUserOperation::model()->findAll('user_task_id = '.$id);
                $operationIds = array();
                foreach ($uthuoS as $uthuo) 
                {
                        $operationIds[] = $uthuo->user_operation_id;
                }

                $this->render('view', compact('taskDetails', 'operationIds', 'generalControllers', 'moduleControlers'));
        }

        /**
         * actionRefreshOperations 
         * 
         * @param int $id - task id
         */
        public function actionRefreshOperations($id)
        {
                UserOperation::updateData();

                // Reset cache
                UserCache::model()->updateAll(array(
                        'status' => 0,
                ));
                
                $this->redirect(array('view', 'id'=>$id));
        }
}
