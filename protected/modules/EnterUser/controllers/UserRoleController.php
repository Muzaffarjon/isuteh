<?php


class UserRoleController extends AdminDefaultController{
   
   public $modelName = 'UserRole';
   public $noPages = array('index', 'redirect'=>array('admin'));
   public $createRedirect = array('admin');

   public function actionAdmin(){
      
      if(isset($_POST['update']) || isset($_POST['restore']) || isset($_POST['archive']) || isset($_POST['delete']) || isset($_POST['tasks'])){
         if(isset($_POST['roleCheck'])){
            $dataIds = array();
            $mess    = ''; 
            foreach($_POST['roleCheck'] as $idRole) { 
               if(UserRole::roleIdExists($idRole)){
                  $dataIds[] = $idRole;
                  if(isset($_POST['restore'])){
                     if(UserRole::restoreRole($idRole))
                        $mess = Yii::t('main', 'Роль(ы) успешно восстановлен(ы)!');
                  }elseif(isset($_POST['archive'])){
                     if(UserRole::archiveRole($idRole))       
                        $mess = Yii::t('main', 'Роль(ы) успешно перемещен(ы) в архив!');
                  }elseif(isset($_POST['delete'])){
                     $delete =  Yii::app()->session['statusPage'] == -1 ? true : false;
                     UserRole::deleteRole($idRole, $delete);                        
                     if($delete){
                        $mess = Yii::t('main', 'Роль(ы) успешно удален(ы)!');
                     }else{
                        $mess = Yii::t('main', 'Роль(ы) успешно перемещен(ы) в корзину!');
                     }
                  }
               } 
            }
      
            if(isset($_POST['update'])){
               foreach($dataIds as $idRole){
                  $this->redirect(array('update', 'id' => $idRole));
               }               
            }elseif(isset($_POST['tasks'])){
               foreach($dataIds as $idRole){
                  $this->redirect(array('view', 'id' => $idRole));
               }               
            }
         }    
         if($mess != ''){
            Yii::app()->user->setFlash('userRoleSuccess',$mess);
         } 
         $this->refresh();
         
      }
      
      $search = new Search($this->modelName); 

      if($_GET['resetSearch'] == 'on'){
         $_SESSION['Search'][$this->id][$this->action->id] = array();
      }      
      
      if(isset($_POST['Search'])){
         $session = Yii::app()->session['Search'];
         $session[$this->id][$this->action->id] = $_POST['Search'];
         Yii::app()->session['Search'] = $session;
      }
            
      $postSearch = Yii::app()->session['Search'][$this->id][$this->action->id];
      
      $search->generateCondition($postSearch);

      $array = UserRole::getAllRoles(Yii::app()->session['statusPage'], $search->getCondition());
      $dataProvider = new CArrayDataProvider(
         $array, array(
            'sort' => array(
               'attributes'   => array('id', 'name','code','home_page','description'),
               'defaultOrder' => array('id'=>true),
            ),
            'pagination'  => array(
               'pageSize' => 30,
            ),
      ));
      
      $this->render('admin',array(
         'model'            => $model,
         'dataProvider'     => $dataProvider,
         'error'            => $error,
         'search'           => $search,
         'postSearch'       => $postSearch 
      ));

   }
        public function actionUpdate($id)
        {
                $model = $this->loadModel($id);

                // To persist 'isGuest' code
                $modelCode = $model->code;

                if (isset($_POST['UserRole']))
                {
                        $model->attributes = $_POST['UserRole'];

                        // To persist 'isGuest' code
                        if ($modelCode == 'isGuest')
                                $model->code = $modelCode;

                        if ($model->save()) 
                        {
                                Yii::app()->clientScript->registerScript('goBack',"
                                        history.go(-2);
                                ");
                        }
                }

                $this->render('update', compact('model'));
        }

        /**
         * actionView 
         * 
         * @param int $id 
         */
        public function actionView($id)
        {
                $model = $this->loadModel($id);


                // Fill taskIds for checkBoxList
                foreach ($model->tasks as $task) 
                        $model->taskIds[] = $task->id;

                if (isset($_POST['UserRole']['taskIds']))
                {
                        UserRoleHasUserTask::model()->deleteAllByAttributes(array(
                                'user_role_id'=>$id,
                        ));

                        // Reset cache
                        UserCache::model()->updateAll(array(
                                'status' => 0,
                        ));

                        if (is_array($_POST['UserRole']['taskIds'])) 
                        {
                                foreach ($_POST['UserRole']['taskIds'] as $taskId) 
                                {
                                        $newTask = new UserRoleHasUserTask;
                                        $newTask->user_role_id = $id;
                                        $newTask->user_task_id = $taskId;
                                        $newTask->save(false);
                                }
                        }

                        Yii::app()->user->setFlash('taskSaved', 'aga');
                        $this->redirect(array('view', 'id'=>$id));
                }


                $this->render('view', compact('model'));
        }
}
