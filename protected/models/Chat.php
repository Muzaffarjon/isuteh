<?php

/**
 * This is the model class for table "chat".
 *
 * The followings are the available columns in table 'chat':
 * @property integer $id
 * @property integer $senderId
 * @property integer $reciverId
 * @property string $text
 * @property integer $viewReciver
 * @property integer $statusSender
 * @property integer $statusReciver
 * @property string $sendTime
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $sender
 * @property User $reciver
 */
class Chat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Chat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('senderId, reciverId, text, viewReciver, statusSender, statusReciver, sendTime, status', 'required'),
			array('senderId, reciverId, viewReciver, statusSender, statusReciver, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().

			// Please remove those attributes that should not be searched.
			array('id, senderId, reciverId, text, viewReciver, statusSender, statusReciver, sendTime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sender' => array(self::BELONGS_TO, 'User', 'senderId'),
			'reciver' => array(self::BELONGS_TO, 'User', 'reciverId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'senderId' => 'Sender',
			'reciverId' => 'Reciver',
			'text' => 'Text',
			'viewReciver' => 'View Reciver',
			'statusSender' => 'Status Sender',
			'statusReciver' => 'Status Reciver',
			'sendTime' => 'Send Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('senderId',$this->senderId);
		$criteria->compare('reciverId',$this->reciverId);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('viewReciver',$this->viewReciver);
		$criteria->compare('statusSender',$this->statusSender);
		$criteria->compare('statusReciver',$this->statusReciver);
		$criteria->compare('sendTime',$this->sendTime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}