<?php

/**
 * This is the model class for table "namudi_fan".
 *
 * The followings are the available columns in table 'namudi_fan':
 * @property integer $id
 * @property string $namud
 * @property string $snamud
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Subjects[] $subjects
 */
class NamudiFan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NamudiFan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'namudi_fan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('namud, snamud, datecreate, lastupdate, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('namud', 'length', 'max'=>150),
			array('snamud', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, namud, snamud, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subjects' => array(self::HAS_MANY, 'Subjects', 'id_namud'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'namud' => 'Namud',
			'snamud' => 'Snamud',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('namud',$this->namud,true);
		$criteria->compare('snamud',$this->snamud,true);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}