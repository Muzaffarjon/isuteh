<?php

/**
 * This is the model class for table "visits".
 *
 * The followings are the available columns in table 'visits':
 * @property integer $id
 * @property integer $id_user
 * @property string $sessionId
 * @property string $ip
 * @property string $browser
 * @property string $visit_time
 * @property string $last_action
 * @property integer $count_auth
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $idUser
 */
class Visits extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Visits the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visits';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, sessionId, ip, browser, visit_time, last_action, count_auth, status', 'required'),
			array('id_user, count_auth, status', 'numerical', 'integerOnly'=>true),
			array('sessionId, ip', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_user, sessionId, ip, browser, visit_time, last_action, count_auth, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'sessionId' => 'Session',
			'ip' => 'Ip',
			'browser' => 'Browser',
			'visit_time' => 'Visit Time',
			'last_action' => 'Last Action',
			'count_auth' => 'Count Auth',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('sessionId',$this->sessionId,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('browser',$this->browser,true);
		$criteria->compare('visit_time',$this->visit_time,true);
		$criteria->compare('last_action',$this->last_action,true);
		$criteria->compare('count_auth',$this->count_auth);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}