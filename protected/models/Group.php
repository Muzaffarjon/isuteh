<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $idGroup
 * @property integer $ihtisosId
 * @property integer $course
 * @property string $class
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Ihtisos $ihtisos
 * @property Groupin[] $groupins
 * @property Shedual[] $sheduals
 */
class Group extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ihtisosId, course, class, datecreate, lastupdate, status', 'required'),
			array('ihtisosId, course, status', 'numerical', 'integerOnly'=>true),
			array('class', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idGroup, ihtisosId, course, class, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ihtisos' => array(self::BELONGS_TO, 'Ihtisos', 'ihtisosId'),
			'groupins' => array(self::HAS_MANY, 'Groupin', 'groupId'),
			'sheduals' => array(self::HAS_MANY, 'Shedual', 'groupId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idGroup' => 'Id Group',
			'ihtisosId' => 'Ihtisos',
			'course' => 'Course',
			'class' => 'Class',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idGroup',$this->idGroup);
		$criteria->compare('ihtisosId',$this->ihtisosId);
		$criteria->compare('course',$this->course);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getFullName()
    {
        return $this->course.' '.$this->course;
    }
}