<?php

/**
 * This is the model class for table "subjects".
 *
 * The followings are the available columns in table 'subjects':
 * @property integer $id
 * @property string $subject
 * @property string $scode
 * @property integer $credit
 * @property integer $id_namud
 * @property integer $id_cafedra
 * @property string $datecreate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Subject[] $subjects
 * @property NamudiFan $idNamud
 * @property Cafedra $idCafedra
 */
class Subjects extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subjects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subjects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, scode, credit, id_namud, id_cafedra, datecreate, status', 'required'),
			array('credit, id_namud, id_cafedra, status', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>250),
			array('scode', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, subject, scode, credit, id_namud, id_cafedra, datecreate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subjects' => array(self::HAS_MANY, 'Subject', 'subjectId'),
			'idNamud' => array(self::BELONGS_TO, 'NamudiFan', 'id_namud'),
			'idCafedra' => array(self::BELONGS_TO, 'Cafedra', 'id_cafedra'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'Subject',
			'scode' => 'Scode',
			'credit' => 'Credit',
			'id_namud' => 'Id Namud',
			'id_cafedra' => 'Id Cafedra',
			'datecreate' => 'Datecreate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('scode',$this->scode,true);
		$criteria->compare('credit',$this->credit);
		$criteria->compare('id_namud',$this->id_namud);
		$criteria->compare('id_cafedra',$this->id_cafedra);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}