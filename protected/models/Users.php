<?php
class Users {

    public static function getUser($id){
            
            $find=User::model()->findByPk($id);
            
            return $find;
        }
        
        public static function getUsers($id){
            
            $find=User::model()->findByPk($id,array('condition'=>'status=1','order'=>'fname ASC'));
            
            return $find;
        }
        
        public static function getIncode($int){
            
            $in=intval($int);
            
            return ($in+100)*(2412/2);
//            $sec = Yii::app()->getSecurityManager();
//            $sec->cryptAlgorithm = 'des'; //РїРѕРїСЂРѕР±РѕРІР°Р» РІСЃРµ РІР°СЂРёР°РЅС‚С‹ :)
//            $param = $int;
//           return $url =$sec->encrypt($param);
            
        }
        
        public static function getDecode($int){
                        
            return ($int*2)/2412-100;
//            $sec = Yii::app()->getSecurityManager();
//            $sec->cryptAlgorithm = 'des'; //РїРѕРїСЂРѕР±РѕРІР°Р» РІСЃРµ РІР°СЂРёР°РЅС‚С‹ :)
//            $param = $int;
//         return $url =$sec->decrypt($int);
            
        }
        
        public static function getAllScores($idShedual,$idGroupin,$typeId){
                $scores=  Scores::model()->findBySql('Select * from scores where shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId.' and status=1');
                return $scores;
        }
        
        //NOZIR
        public static function getAllAbsents($idShedual,$idGroupin,$typeId){
                $absent= Absent::model()->findBySql('Select * from absents where shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId.' and status=1');
                return $absent;
        }
        
        public static function getAllSumAbsents($idShedual,$idGroupin,$typeId){
            $absent=Absent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId));
        
            return count($absent);
        }
        
        //NOZIR END
        //
        
        public static function getTabsent($idShedual,$idGroupin,$typeId){
                $absent= Tabsent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId.' and status=1'));
                return $absent;
        }
        
        public static function getSumTabsent($idShedual,$idGroupin,$typeId){
            $absent=Tabsent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId));
        
            return count($absent);
        }
        
        public static function getAllSumShedualAbsents($idShedual,$idGroupin){

          $absent=Absent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId NOT IN (9,18)'));

           return count($absent);
        }

        public static function getSumGroupinAbsents($shedualId,$idGroupin){

            $absent=Absent::model()->findAll(array('condition'=>'shedualId='.$shedualId.' and groupinId='.$idGroupin.' and typeId NOT IN (9,18)'));
            Yii::app()->session['count']+=count($absent);
            return Yii::app()->session['count'];
        }
        public static function getAbsents($idShedual,$idGroupin,$typeId){
            $absent=Absent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId));
        
            return $absent;
        }
        public static function getTAbsents($idShedual,$idGroupin,$typeId){
            $absent=Tabsent::model()->findAll(array('condition'=>'shedualId='.$idShedual.' and groupinId='.$idGroupin.' and typeId='.$typeId));
        
            return $absent;
        }
        
        public static function getAllShedual($subjectId){

            $scores=Shedual::model()->findAll(
                    array(
                        'condition'=>'subjectId='.$subjectId.' and status=1',
                        'order'=>'idShedual ASC'
                        )
                    );
            return $scores;
        }
        
        public static function getGroups($idGroup){
           return Group::model()->findbyPk($idGroup);
        }
        
        public static function setScores($shedId,$groupId,$ball,$type) {

        $sql = "INSERT INTO scores SET 
                `shedualId`=:shed,
                `groupinId`=:group,
                `bal`=:bal,
                 typeId=:typeId, 
                 datecreate=NOW(), 
                 lastupdate=NOW(), 
                 `status`=1";

        $command = Yii::app()->db->createCommand($sql);
        
        $command->bindParam(":shed", $shedId, PDO::PARAM_INT);
        $command->bindParam(":group", $groupId, PDO::PARAM_INT);
        $command->bindParam(":bal", $ball, PDO::PARAM_INT);
        $command->bindParam(":typeId", $type, PDO::PARAM_INT);


        $command->execute();
    }
    public static function SumOfPoints($shedualId, $groupinId, $credit){
        
        $scores=Scores::model()->findAll(array(
            'condition'=>'shedualId='.$shedualId.' and groupinId='.$groupinId.' and typeId BETWEEN 1 and 20'));
        //$s_count=count($scores);
        foreach ($scores as $score){
            $sum=$sum + $score->bal;
        }
        if($credit==6)
        $ball=substr($sum/20,0,4);
        elseif ($credit==4) 
        $ball=substr($sum/16,0,4);
        elseif ($credit==2) 
        $ball=substr($sum/2,0,3);
        
            $type=21;
        
        $check=  self::getAllScores($shedualId, $groupinId, $type);
        if(count($check)==0){
        self::setScores($shedualId, $groupinId, $ball, $type);
        }else{
          $update=  Scores::model()->updateByPk($check->idScores,array(
              'bal'=>$ball,
              'datecreate=NOW()'
          ));  
        }        
    }
    
    public static function IsLockDay($shedualId,$typeId){
       $query =Tablelock::model()->find(array('condition'=>'shedualId='.intval($shedualId).' and typeId='.intval($typeId). ' and status=1')  );
       return count($query);
    }
    
    public static function countScores($shedualId,$typeId){
        $query=  Scores::model()->findAll(array(
            'condition'=>'shedualId='.$shedualId.' and typeId='.$typeId
        ));
        
        return count($query);
    }
    public static function getSubject($id){
        return Subject::model()->findByPk($id);
    }
    
    public static function LockTable($shedualId,$typeId){
         $sql = "INSERT INTO tablelock SET 
                 shedualId=:shed,
                 typeId=:typeId, 
                 datecreate=NOW(), 
                 lastupdate=NOW(), 
                 `status`=1";

        $command = Yii::app()->db->createCommand($sql);
        
        $command->bindParam(":shed", $shedualId, PDO::PARAM_INT);
        $command->bindParam(":typeId", $typeId, PDO::PARAM_INT);


        $command->execute();
    }
    public static function countGroupins($groupId){
        $query=  Groupin::model()->findAll(array(
            'condition'=>'groupId='.$groupId.' and status=1'
        ));
        return count($query);
    }
    public static function Groupins($groupId){
        $query=  Groupin::model()->findAll(array(
            'condition'=>'groupId='.$groupId.' and status=1'
        ));
        return $query;
    }
    
    public static function setShedual($gId,$sId,$sem,$cikl,$dStart,$dEnd,$dRest){
       
        $sql = "INSERT INTO shedual SET 
                `groupId`=:gId,
                `subjectId`=:sId,
                `cikl`=:cikl,
                 semestr=:semestr, 
                 dateStart=:dStart, 
                 dateEnd=:dEnd, 
                 dateRest=:dRest, 
                 datecreate=NOW(), 
                 lastupdate=NOW(), 
                 `status`=1";

        $command = Yii::app()->db->createCommand($sql);
        
        $command->bindParam(":gId", $gId, PDO::PARAM_INT);
        $command->bindParam(":sId", $sId, PDO::PARAM_INT);
        $command->bindParam(":cikl", $cikl, PDO::PARAM_INT);
        $command->bindParam(":semestr", $sem, PDO::PARAM_INT);
        $command->bindParam(":dStart", $dStart, PDO::PARAM_STR);
        $command->bindParam(":dEnd", $dEnd, PDO::PARAM_STR);
        $command->bindParam(":dRest", $dRest, PDO::PARAM_STR);

        if($command->execute()){
            return 1;
        }else{
            return 0;
        }
        
        
    }
   
    public function getBallsOfStudents($shedualId,$groupinId){
      
        $scores=  Gscores::model()->find(array('condition'=>'shedualId='.$shedualId.' and groupinId='.$groupinId.' and status=1'));
        
       
            return $scores;
                
    }
    public function getBallsStudents($shedualId,$groupinId){
      
        $scores=  Gscores::model()->find(array('condition'=>'shedualId='.$shedualId.' and groupinId='.$groupinId.' and status=1'));
        
       
            return $scores;
                
    }
    
    public static function updbypk($id,$subject,$uId,$credit){
            $sql = "Update `subject` SET 
                `subjectId`=:subject,
                `teacherId`=:uId,
                `credit`=:credit where idSubject=".$id;
                 

        $command = Yii::app()->db->createCommand($sql);
        
        $command->bindParam(":subject", $subject, PDO::PARAM_INT);
        $command->bindParam(":uId", $uId, PDO::PARAM_INT);
        $command->bindParam(":credit", $credit, PDO::PARAM_INT);

        if($command->execute()){
            return 1;
        }else{
            return 0;
        }
        }
        
        public static function getPercentOfAbsent($shedualId,$groupinId){
            
            $shedual=Shedual::model()->findByPk($shedualId);
            $absents=Absent::model()->findAll(array('condition'=>'shedualId='.$shedualId.' and groupinId='.$groupinId));
            $count=count($absents);
            if($shedual->subject->subject->credit==6){

               return self::getPercent($count,48);

            }elseif($shedual->subject->subject->credit==4){
                return self::getPercent($count,36);
            }elseif($shedual->subject->subject->credit==2){
                return self::getPercent($count,16);
            }
            
        }

        public function getPercent($count, $hour){
            $a=$count*100;
            $b=$a/$hour;
            $c=100-$b;
            return round($c,0)."%";
        }

        public static function checkGroupin($groupinId, $shedualId){
            $credit=Scores::model()->find(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId));
            $score=Scores::model()->findAll(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId));
            
            if($credit->shedual->subject->subject->credit==6){
                if(count($score)>9){
                    $karz=Scores::model()->findAll(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId.' and typeId IN (9,18)'));
                    foreach ($karz as $k){
                        if($k->bal==11){
                            return 1;
                        }elseif($k->bal<3){
                            return 1;
                        }
                    }
                }
            }elseif($credit->shedual->subject->subject->credit==4){
                if(count($score)>8){
                    
                }
            }else{
                
            }
            return ;
        }
        public static function checkUserInGroupin($userId){
            
           return Groupin::model()->find(array('condition'=>'userId='.$userId.' and status=1'));
            
        }
        
        
        /*  COUNTS  */
        
        public static function countAbsents($groupinId, $shedualId){
            $count=Absent::model()->findAll(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId.' and typeId NOT IN  (9,18)'));
            return count($count);
        }
        public static function countAlerts($groupinId, $shedualId){
            
            $count=Alert::model()->find(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId));
            return count($count);
        }
        
        public static function statusAlerts($groupinId, $shedualId){
            
            $status=Alert::model()->find(array('condition'=>'groupinId='.$groupinId.' and shedualId='.$shedualId));
            return $status->status;
        }
        

    public static function InsertCheck($sId,$tId){

        $nm=new Checktable;

        $nm->shedualId=$sId;
        $nm->typeId=$tId;
        $nm->datecreate=new CDbExpression('NOW()');
        $nm->lastupdate=new CDbExpression('NOW()');
        $nm->status=1;
            if($nm->insert()){
                return 1;
            }else return 0;

    }

    public static function baho($ruzId, $uId, $idShedual, $groupIn, $color=null){

        $show="";

        if($color==null){
            $show .= '<td id="ruz_' . $ruzId . '" style="vertical-align: inherit;"  align="center"  >';
        } else $show .= '<td id="ruz_' . $ruzId . '" style="vertical-align: inherit;" bgcolor="' . $color . '" align="center" >';

        $show.='<form id="theForm_' . $ruzId . $uId . '" class="remove">';
        $show.=CHtml::hiddenField('type', $ruzId, array('id' => 'type'));
        $show.=CHtml::hiddenField('shedualId', $idShedual, array('id' => 'shedId'));
        $show.=CHtml::hiddenField('userId', $groupIn, array('id' => 'userId'));
        $show.=CHtml::dropDownList('bal', 'text', array(0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10), array('onchange' => 'sendBall(' . $ruzId . $groupIn . ')','id' => 'bal_' . $ruzId . $groupIn,'empty'=>' '));
        $show.=' <button type="button" class="delete classHidden" onfocus="dellBall(' . $ruzId . $groupIn . ')" id="delete_' . $ruzId . $groupIn . '">×</button>';
        $show.='</form>';
        $show.='</td>';

        return $show;
    }

    public static function insertCheckAbsents($uId,$sId,$type,$checkId,$table=null){
        $newAbsent = new $table;

        $newAbsent->groupinId = $uId;
        $newAbsent->shedualId = $sId;
        $newAbsent->typeId = $type;
        $newAbsent->datecreate = new CDbExpression('NOW()');
        $newAbsent->lastupdate = new CDbExpression('NOW()');
        $newAbsent->status = 1;
        $newAbsent->checkId = $checkId;
        $newAbsent->save();
    }

    public static function returnGroup($id){

        $groupin=Groupin::model()->findByAttributes( array('userId'=>$id));
        if(!empty($groupin)){
        $group=Group::model()->findByPk($groupin->groupId);
        $ihtisos=  Ihtisos::model()->findByPk($group->ihtisosId);
            return $group->course.' - '.$ihtisos->code.$group->class;
        }

    }

    public static function returnUserId($login){
        $model=User::model()->findByAttributes(array('login'=>$login));

         return $model->id;
    }

    public static function getCountSendMail($senderId){
        $model=Mailbox::model()->findAllByAttributes(array('senderId'=>$senderId,'statusSender'=>2));

        return count($model);
    }
    public static function getCountReciveViewMail($reciverId){
        $model=Mailbox::model()->findAllByAttributes(array('reciverId'=>$reciverId,'statusReciver'=>2,'viewReciver'=>0));

        return count($model);
    }

    public static function getCountReciveMail($reciverId){
        $model=Mailbox::model()->findAllByAttributes(array('reciverId'=>$reciverId,'statusReciver'=>2));

        return count($model);
    }

    public static function getNextMessage($reciverId,$id2){

    }

    public static function getTimer($datetime)
    {
        //date_default_timezone_set('Asia/Khujand');
        $startTimer = date_create($datetime);
        $endTimer = date_create();
        $diff = date_diff($startTimer, $endTimer);
        if ($diff->format('%Y') > 0) {
            return $diff->format('%Y сол');
        } elseif ($diff->format('%m') > 0) {
            return $diff->format('%m моҳ');
        } elseif ($diff->format('%d') > 0) {
            return $diff->format('%d рӯз');
        } elseif ($diff->format('%H') > 0) {
            if ($diff->format('%H') < 10) {
                $str = $diff->format('%H');
                $hour = substr($str, 1, 1);
                return $hour . ' соат';
            } else {
                return $diff->format('%H соат');
            }

        }elseif ($diff->format('%i') > 0) {
            return $diff->format('%i дақ');
        }  elseif ($diff->format('%s') > 0) {
            return $diff->format('%s сония');
        }


    }

    public static function Alert($type,$text){
        echo '<div class="alert alert-'.$type.' alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            '.$text.'
        </div>';
    }

    public static function getMessage($nickname,$datetime,$avatar,$text,$class=null){
        $dateTime=new DateTime($datetime);
        if(!is_null($class))
        echo '<div class="direct-chat-msg '.$class.'">';
        else echo '<div class="direct-chat-msg">';
        echo '<div class="direct-chat-info clearfix">';
        echo '<span class="direct-chat-name pull-left">'.$nickname.'</span>';
        echo '<span class="direct-chat-timestamp pull-right">'.$dateTime->format('H:m:s d-m-Y').'</span>';
        echo '</div>';
        echo '<img class="direct-chat-img" src="'.Yii::app()->request->baseUrl.$avatar.'" alt="message user image">';
        echo '<div class="direct-chat-text">';
        echo $text;
        echo '</div>';
        echo '</div>';
    }

    public static function getChatLastMessage($senderId){
       return Chat::model()->findAll(array('condition' => 'reciverId=' . $senderId . '
                                                                and senderId=' . Yii::app()->user->id . '
                                                                or reciverId=' . Yii::app()->user->id . ' and senderId=' .$senderId,'order'=>'sendTime DESC','limit'=>1));
    }

    public static function getCountNotReadMessage($senderId){
       $chat=Chat::model()->findAll(array('select'=>'text,sendTime','condition' =>'reciverId=' . Yii::app()->user->id . ' and senderId=' .$senderId.' and viewReciver=0'));
        return count($chat);
    }
    public static function getCountNotReadAllMessage(){
       $chat=Chat::model()->findAll(array('select'=>'text,sendTime','condition' =>'reciverId=' . Yii::app()->user->id . ' and viewReciver=0'));
        return count($chat);
    }

    public static function getSenderOrReciverProfile($avatar,$fname,$name){
        echo '<img class="contacts-list-img" src="'.Yii::app()->request->baseUrl.$avatar.'">';
        echo '<div class="contacts-list-info">';
        echo '<span class="contacts-list-name text-black">';
        echo $fname.' '.$name;
    }

    public static function updateRecivedMessage(){
        Chat::model()->updateAll(array('viewReciver'=>1),'reciverId='.Yii::app()->user->id);
    }

    public static function getCountOnlineUsers(){
        return count(Visits::model()->findAll(array('condition'=>'not id_user='.Yii::app()->user->id.' and status=1')));
    }


    public static function getTryStudent($shedualId,$groupinId,$typeId){

        $scores=Scores::model()->find('shedualId=:sId and groupinId=:gId and typeId=:tId',array('sId'=>$shedualId,'gId'=>$groupinId,'tId'=>$typeId));
        if(!empty($scores)){
            return $scores;
        }else return 0;
    }

    public function getAlertofStudents($alerts,$groupinId)
    {
        $show = "<div class='callout callout-danger'>";
        $show .= "<h4><i class='icon fa fa-info'></i>&nbsp; Донишҷӯи мӯҳтарам!</h4>";

        $show .= "Шумо аз чунин фаннҳо ғоибҳо доред. Ба назди нозир рафта онҳоро отработка кунед!";
        $show .= '<table class="table table-bordered" width="100%" style="text-align: center">';
        $show .= "<thead>";
        $show .= "<tr>";
        $show .= "<th style='text-align: center' >№</th>";
        $show .= "<th style='text-align: center'>Фанн</th>";
        $show .= "<th style='text-align: center'>Муаллим</th>";
        $show .= "<th style='text-align: center'>Семестр</th>";
        $show .= "<th style='text-align: center'>Сикл</th>";
        $show .= "<th style='text-align: center'>Кредит</th>";
        $show .= "<th style='text-align: center'>М. ғоиб</th>";
        $show .= "</tr>";
        $show .= "</thead>";
        $show .= "<tbody>";

        $i = 1;
        foreach ($alerts as $alert) {
            $count = Users::countAbsents($groupinId, $alert->shedualId);
            $shedual = Shedual::model()->findByPk($alert->shedualId);
            $show .'<tr>';
            $show .'<td>' . $i++ . '</td>';
            $show .'<td>' . $shedual->subject->subject->subject . '</td>';
            $show .'<td>' . $shedual->subject->teacher->user->fname . ' ' . $shedual->subject->teacher->user->name . '</td>';
            $show .'<td>' . $shedual->semestr . '</td>';
            $show .'<td>' . $shedual->cikl . '</td>';
            $show .'<td>' . $shedual->subject->subject->credit . '</td>';
            $show .'<td>' . $count . '</td>';
            $show .'</tr>';
        }

        $show .= "</tbody>";
        $show .= "</table>";
        $show .= "</div>";

    return $show;
    }

    public function actionCheckAttemptofStudents($groupinId,$shedId){
        $goiB=Absent::model()->findAll(array('condition'=>'shedualId='.$shedId.' and groupinId='.$groupinId));
        $goib=count($goiB);
        $check9=Scores::model()->find('shedualId=:sId and groupinId=:gId and typeId=9', array(':sId'=>$shedId,':gId'=>$groupinId));
        $check18=Scores::model()->find('shedualId=:sId and groupinId=:gId and typeId=18', array(':sId'=>$shedId,':gId'=>$groupinId));
        $check21=Scores::model()->find('shedualId=:sId and groupinId=:gId and typeId=21', array(':sId'=>$shedId,':gId'=>$groupinId));
        $check22=Scores::model()->find('shedualId=:sId and groupinId=:gId and typeId=22', array(':sId'=>$shedId,':gId'=>$groupinId));

        if($check9->bal===NULL or $check18->bal===NULL or $goib===NULL){
            return 0;
        }elseif($check9->bal>=3 || $check18->bal>=3 || $goib===null){
            return 2;
        }elseif($check9->bal>=3 || $check18->bal>=3 || $goib<9){
            if($check21->bal>3){
                return 3;
            }else{
                return 1;
            }
        }elseif($check9->bal>=3 || $check18->bal>=3 || $goib>9){
            return 1;
        }elseif($check9->bal<3 or $check18->bal<3 or $goib>9){
            return 1;
        }

    }

    public static function  sendMail($email,$name,$activate){

        $activation = 'Ассалому алейкум! <br>';
        $activation .= '<br>';
        $activation .= 'Шумо барои иваз кардани пароли худ ба шӯъбаи иттилоотӣ муроҷиат карда будед. Барои аз нав кардани пароли худ, бояд ҳаволаро пахш карда ба саҳифаи он равона шавед.' . '<br>';
        $activation .= "<br><a target='_blank' href='http://isuteh.su/restore?code=".$activate."'>Азнав кардани пароли аккаунт</a>";
        $activation .= "<br><br>Агар шумо чунин корро иҷро накарда бошед, пас мактубро тоза кунед.";
        $activation .= "<br><br>-------<br><br> Бо эҳтиром<br> Шӯъбаи Иттилоотии Коллеҷ";

        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->Host = 'isuteh.su';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@isuteh.su';
        $mail->Password = 'D0r1V7z0';
        $mail->SetFrom('noreply@isuteh.su', 'Коллеҷи Техникии шаҳри Хуҷанд');
        $mail->Subject = 'Иваз кардани парол';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($activation);
        $mail->AddAddress($email, $name);
        $send=$mail->Send();
        if($send){
            return 1;
        }
    }

    public static function searchByEmail($email){
        return User::model()->findByAttributes(array('email'=>$email));
    }

   public static function saveResetMail($idUser,$code){
       $mail=new ResetPassword();
       $mail->id_user=$idUser;
       $mail->code=$code;
       $mail->datecreate=new CDbExpression('NOW()');
       $mail->lastupdate=new CDbExpression('NOW()');
       $mail->status=0;
       if($mail->save()){
           return 1;
       }

   }

    public static function setInActiveCodes($idUser){
        $update=ResetPassword::model()->updateAll(array('status'=>1),'id_user='.$idUser);
        if($update){
            return 1;
        }
    }



    public static function checkCode($code){
        $code=ResetPassword::model()->findByAttributes(array('code'=>$code, 'status'=>0));
        if(!empty($code)){
            return $code;
        }
    }

    public static function updateUserPassword($idUser,$password){
        return User::model()->updateByPk($idUser,array('password'=>md5($password),'lastupdate'=>new CDbExpression('NOW()')));
    }

    public static function settings(){
        $status=Settings::model()->find();
        if($status->id==1 && $status->status==0){
            $status->status=1;
            $status->update();
            return 1;
        }else{
            $status->status=0;
            $status->update();
            return 1;
        }
    }

    public static function getStatus(){
        $status=Settings::model()->find();
        return $status->status;
    }
}
?>
