<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 05.08.16
 * Time: 14:36
 */

class PasswordForm extends CFormModel
{
    public $password;
    public $new_password;
    public $repeat_password;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('password, new_password, repeat_password', 'required','message'=>'Марза набояд холи бошад'),
            array('new_password, repeat_password','length', 'min'=>6),
            array('password, new_password, repeat_password','length', 'max'=>24),

            array('repeat_password', 'compare', 'compareAttribute'=>'new_password','message'=>'Парол такроран дуруст ворид карда шавад.'),
            array('password','customValidation'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'password'=>'Парол',
            'new_password'=>'Пароли нав',
            'repeat_password'=>'Такрори пароли нав',
        );
    }

    public function customValidation($attribute,$params){

    $user=User::model()->findByPk(Yii::app()->user->id);
        if(!$this->hasErrors())
        {
            if(md5($this->password)!=$user->password)
                $this->addError($attribute,'Парол хато ворид шуд.');
        }


    }

}
?>