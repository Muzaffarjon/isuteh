<?php

/**
 * This is the model class for table "absents".
 *
 * The followings are the available columns in table 'absents':
 * @property integer $id
 * @property integer $shedualId
 * @property integer $groupinId
 * @property integer $typeId
 * @property integer $checkId
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Shedual $shedual
 * @property Type $type
 * @property Groupin $groupin
 */
class Absent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Absent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'absents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shedualId, groupinId, typeId, checkId, datecreate, lastupdate, status', 'required'),
			array('shedualId, groupinId, typeId, checkId, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, shedualId, groupinId, typeId, checkId, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'shedual' => array(self::BELONGS_TO, 'Shedual', 'shedualId'),
			'type' => array(self::BELONGS_TO, 'Type', 'typeId'),
			'groupin' => array(self::BELONGS_TO, 'Groupin', 'groupinId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shedualId' => 'Shedual',
			'groupinId' => 'Groupin',
			'typeId' => 'Type',
			'checkId' => 'Check',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('shedualId',$this->shedualId);
		$criteria->compare('groupinId',$this->groupinId);
		$criteria->compare('typeId',$this->typeId);
		$criteria->compare('checkId',$this->checkId);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}