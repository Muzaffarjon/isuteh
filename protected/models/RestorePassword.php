<?php

class RestorePassword extends CFormModel
{
    public $username;

    public function rules()
    {
        return array(
            // username and password are required
            array('username', 'required','message'=>'Марза набояд холи бошад'),

            array('username', 'email'),

            array('username', 'check'),

        );
    }

    public function attributeLabels()
    {
        return array(
            'username'=>'Почтаи электронӣ',
        );
    }

    public function check(){
        $user=User::model()->findByAttributes(array('email'=>$this->username));
        if(empty($user)){
            $this->addError('username','Ин хел почта мавчуд нест!');
        }


    }
}