<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 11.08.16
 * Time: 10:35
 */

class RestoreForm extends CFormModel {

    public $new_password;
    public $repeat_password;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('new_password,repeat_password', 'required'),
            array('new_password,repeat_password', 'length','min'=>6),
            array('new_password,repeat_password', 'length','max'=>50),
            array('repeat_password', 'compare', 'compareAttribute'=>'new_password','message'=>'Парол такроран дуруст ворид карда шавад.'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'new_password'=>'Пароли нав',
            'repeat_password'=>'Такрори Парол',
        );
    }

    public function passwordValidate(){
        $user=User::model()->findByAttributes(array('email'=>$this->email));
        if(!$this->hasErrors())
        {
            if(!empty($user)){
                $this->addError('email','Чунин E-mail вуҷуд дорад. Дигар E-mail-ро интихоб кунед!');
            }
        }

    }

}
?>

} 