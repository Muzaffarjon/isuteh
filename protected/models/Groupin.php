<?php

/**
 * This is the model class for table "groupin".
 *
 * The followings are the available columns in table 'groupin':
 * @property integer $idGroupin
 * @property integer $userId
 * @property integer $groupId
 * @property string $datecreate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Group $group
 * @property Scores[] $scores
 */
class Groupin extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Groupin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'groupin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, groupId, datecreate, status', 'required'),
			array('userId, groupId, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idGroupin, userId, groupId, datecreate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'group' => array(self::BELONGS_TO, 'Group', 'groupId'),
			'scores' => array(self::HAS_MANY, 'Scores', 'groupinId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idGroupin' => 'Id Groupin',
			'userId' => 'User',
			'groupId' => 'Group',
			'datecreate' => 'Datecreate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idGroupin',$this->idGroupin);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('groupId',$this->groupId);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}