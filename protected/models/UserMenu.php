<?php

/**
 * This is the model class for table "user_menu".
 *
 * The followings are the available columns in table 'user_menu':
 * @property integer $idMenu
 * @property integer $parentId
 * @property string $name
 * @property string $href
 * @property string $title
 * @property string $icon
 * @property integer $orderId
 * @property integer $accessId
 * @property string $has_chevron
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property UserAccess $access
 */
class UserMenu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parentId, name, href, title, icon, orderId, accessId, has_chevron, datecreate, lastupdate, status', 'required'),
			array('parentId, orderId, accessId, status', 'numerical', 'integerOnly'=>true),
			array('name, href, title, icon', 'length', 'max'=>50),
			array('has_chevron', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idMenu, parentId, name, href, title, icon, orderId, accessId, has_chevron, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'access' => array(self::BELONGS_TO, 'UserAccess', 'accessId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idMenu' => 'Id Menu',
			'parentId' => 'Parent',
			'name' => 'Name',
			'href' => 'Href',
			'title' => 'Title',
			'icon' => 'Icon',
			'orderId' => 'Order',
			'accessId' => 'Access',
			'has_chevron' => 'Has Chevron',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idMenu',$this->idMenu);
		$criteria->compare('parentId',$this->parentId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('href',$this->href,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('orderId',$this->orderId);
		$criteria->compare('accessId',$this->accessId);
		$criteria->compare('has_chevron',$this->has_chevron,true);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}