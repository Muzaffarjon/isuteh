<?php

/**
 * This is the model class for table "shedual".
 *
 * The followings are the available columns in table 'shedual':
 * @property integer $idShedual
 * @property integer $groupId
 * @property integer $subjectId
 * @property integer $cikl
 * @property integer $semestr
 * @property string $dateStart
 * @property string $dateEnd
 * @property string $dateRest
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Absents[] $absents
 * @property Scores[] $scores
 * @property Group $group
 * @property Subject $subject
 * @property Tablelock[] $tablelocks
 * @property Tabsent[] $tabsents
 */
class Shedual extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Shedual the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shedual';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('groupId, subjectId, cikl, semestr, dateStart, dateEnd, dateRest, datecreate, lastupdate, status', 'required'),
			array('groupId, subjectId, cikl, semestr, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idShedual, groupId, subjectId, cikl, semestr, dateStart, dateEnd, dateRest, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'absents' => array(self::HAS_MANY, 'Absents', 'shedualId'),
			'scores' => array(self::HAS_MANY, 'Scores', 'shedualId'),
			'group' => array(self::BELONGS_TO, 'Group', 'groupId'),
			'subject' => array(self::BELONGS_TO, 'Subject', 'subjectId'),
			'tablelocks' => array(self::HAS_MANY, 'Tablelock', 'shedualId'),
			'tabsents' => array(self::HAS_MANY, 'Tabsent', 'shedualId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idShedual' => 'Id Shedual',
			'groupId' => 'Group',
			'subjectId' => 'Subject',
			'cikl' => 'Cikl',
			'semestr' => 'Semestr',
			'dateStart' => 'Date Start',
			'dateEnd' => 'Date End',
			'dateRest' => 'Date Rest',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idShedual',$this->idShedual);
		$criteria->compare('groupId',$this->groupId);
		$criteria->compare('subjectId',$this->subjectId);
		$criteria->compare('cikl',$this->cikl);
		$criteria->compare('semestr',$this->semestr);
		$criteria->compare('dateStart',$this->dateStart,true);
		$criteria->compare('dateEnd',$this->dateEnd,true);
		$criteria->compare('dateRest',$this->dateRest,true);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}