<?php

/**
 * This is the model class for table "gscores".
 *
 * The followings are the available columns in table 'gscores':
 * @property integer $id
 * @property integer $shedualId
 * @property integer $groupinId
 * @property double $bal
 * @property integer $bal2
 * @property string $znak
 * @property integer $typeId
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 */
class Gscores extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gscores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gscores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shedualId, groupinId, bal, bal2, znak, typeId, datecreate, lastupdate, status', 'required'),
			array('shedualId, groupinId, bal2, typeId, status', 'numerical', 'integerOnly'=>true),
			array('bal', 'numerical'),
			array('znak', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, shedualId, groupinId, bal, bal2, znak, typeId, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shedualId' => 'Shedual',
			'groupinId' => 'Groupin',
			'bal' => 'Bal',
			'bal2' => 'Bal2',
			'znak' => 'Znak',
			'typeId' => 'Type',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('shedualId',$this->shedualId);
		$criteria->compare('groupinId',$this->groupinId);
		$criteria->compare('bal',$this->bal);
		$criteria->compare('bal2',$this->bal2);
		$criteria->compare('znak',$this->znak,true);
		$criteria->compare('typeId',$this->typeId);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}