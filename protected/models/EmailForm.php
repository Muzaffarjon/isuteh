<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 05.08.16
 * Time: 14:36
 */

class EmailForm extends CFormModel
{
    public $email;
    public $password;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('email', 'required'),
            array('email', 'email'),
            array('email','emailValidate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'email'=>'E-mail',
            'password'=>'Парол',
        );
    }

    public function emailValidate(){
        $user=User::model()->findByAttributes(array('email'=>$this->email));
        if(!$this->hasErrors())
        {
            if(!empty($user)){
                $this->addError('email','Чунин E-mail вуҷуд дорад. Дигар E-mail-ро интихоб кунед!');
            }
        }

    }

}
?>