<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $avatar
 * @property string $fname
 * @property string $name
 * @property string $lname
 * @property string $birthday
 * @property integer $gender
 * @property string $address
 * @property string $number_phone
 * @property integer $cityId
 * @property integer $accessId
 * @property string $datecreate
 * @property string $lastupdate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Groupin[] $groupins
 * @property Teachers[] $teachers
 * @property UserAccess $access
 * @property City $city
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname','required','message'=>'Насаб набояд холи бошад.'),

            array('name','required','message'=>'Ном набояд холи бошад.'),

            array('lname','required','message'=>'Номи падар набояд холи бошад.'),

            array('login, password, avatar, birthday, gender, address, number_phone, cityId, accessId, datecreate, lastupdate, status', 'required'),

			array('gender, cityId, accessId, status', 'numerical', 'integerOnly'=>true),
			array('login, password, avatar, fname, name, lname', 'length', 'max'=>50),
            array('email','email'),
            array('email','length','max'=>255),
			array('address', 'length', 'max'=>100),
			array('number_phone', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login, password, avatar, fname, name, lname, birthday, gender, address, number_phone, cityId, accessId, datecreate, lastupdate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'groupins' => array(self::HAS_MANY, 'Groupin', 'userId'),
			'teachers' => array(self::HAS_MANY, 'Teachers', 'userId'),
			'access' => array(self::BELONGS_TO, 'UserAccess', 'accessId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'password' => 'Password',
			'avatar' => 'Avatar',
			'fname' => 'Fname',
			'name' => 'Name',
			'lname' => 'Lname',
			'birthday' => 'Birthday',
			'gender' => 'Gender',
			'address' => 'Address',
			'number_phone' => 'Number Phone',
			'cityId' => 'City',
			'accessId' => 'Access',
			'datecreate' => 'Datecreate',
			'lastupdate' => 'Lastupdate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('number_phone',$this->number_phone,true);
		$criteria->compare('cityId',$this->cityId);
		$criteria->compare('accessId',$this->accessId);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}