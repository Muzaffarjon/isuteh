<?php

/**
 * This is the model class for table "mailbox".
 *
 * The followings are the available columns in table 'mailbox':
 * @property integer $id
 * @property integer $senderId
 * @property integer $reciverId
 * @property string $head
 * @property string $text
 * @property string $upload
 * @property string $filename
 * @property integer $statusSender
 * @property integer $statusReciver
 * @property integer $viewReciver
 * @property integer $notyReciver
 * @property integer $id2
 * @property string $datecreate
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $sender
 * @property User $reciver
 */
class Mailbox extends CActiveRecord
{
	public $filename;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mailbox';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('senderId, reciverId, head, text, statusSender, statusReciver, viewReciver, notyReciver, id2, datecreate, status', 'required'),
			array('senderId, statusSender, statusReciver, viewReciver, notyReciver, id2, status', 'numerical', 'integerOnly'=>true),
			array('head, filename', 'length', 'max'=>50),
            array('upload', 'length', 'max'=>250),
            array('upload','file', 'allowEmpty' => true, 'types' => 'zip,doc,xls,ppt,jpg,gif,png,txt,docx,pptx,xlsx,pdf,csv,bmp', 'maxSize' => 1048576),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, senderId, reciverId, head, text, upload, filename, statusSender, statusReciver, viewReciver, notyReciver, id2, datecreate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sender' => array(self::BELONGS_TO, 'User', 'senderId'),
			'reciver' => array(self::BELONGS_TO, 'User', 'reciverId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'senderId' => 'Sender',
			'reciverId' => 'Қабулкунанда',
			'head' => 'Сарлавҳа',
			'text' => 'Матн',
			'upload' => 'Пайваст кардани файл',
			'filename' => 'Filename',
			'statusSender' => 'Status Sender',
			'statusReciver' => 'Status Reciver',
			'viewReciver' => 'View Reciver',
			'notyReciver' => 'Noty Reciver',
			'id2' => 'Id2',
			'datecreate' => 'Datecreate',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('senderId',$this->senderId);
		$criteria->compare('reciverId',$this->reciverId);
		$criteria->compare('head',$this->head,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('upload',$this->upload,true);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('statusSender',$this->statusSender);
		$criteria->compare('statusReciver',$this->statusReciver);
		$criteria->compare('viewReciver',$this->viewReciver);
		$criteria->compare('notyReciver',$this->notyReciver);
		$criteria->compare('id2',$this->id2);
		$criteria->compare('datecreate',$this->datecreate,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}