/*
SQLyog Ultimate v8.8 
MySQL - 5.1.73 : Database - isuteh
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`isuteh` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `isuteh`;

/*Table structure for table `absents` */

DROP TABLE IF EXISTS `absents`;

CREATE TABLE `absents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `groupinId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `checkId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_absents2` (`typeId`),
  KEY `FK_absents` (`shedualId`),
  KEY `FK_absents4` (`groupinId`),
  CONSTRAINT `FK_absents` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_absents2` FOREIGN KEY (`typeId`) REFERENCES `type` (`id`),
  CONSTRAINT `FK_absents4` FOREIGN KEY (`groupinId`) REFERENCES `groupin` (`idGroupin`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

/*Data for the table `absents` */

insert  into `absents`(`id`,`shedualId`,`groupinId`,`typeId`,`checkId`,`datecreate`,`lastupdate`,`status`) values (68,1,90,3,1,'2016-04-21 12:06:41','2016-04-21 12:06:41',1),(69,1,90,3,2,'2016-04-21 12:06:41','2016-04-21 12:06:41',1),(70,1,90,3,3,'2016-04-21 12:06:41','2016-04-21 12:06:41',1),(71,1,90,4,1,'2016-04-21 12:06:47','2016-04-21 12:06:47',1),(72,1,90,4,2,'2016-04-21 12:06:47','2016-04-21 12:06:47',1),(73,1,90,4,3,'2016-04-21 12:06:47','2016-04-21 12:06:47',1);

/*Table structure for table `alert` */

DROP TABLE IF EXISTS `alert`;

CREATE TABLE `alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupinId` int(11) NOT NULL,
  `shedualId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_alert` (`groupinId`),
  KEY `FK_alert1` (`shedualId`),
  CONSTRAINT `FK_alert` FOREIGN KEY (`groupinId`) REFERENCES `groupin` (`idGroupin`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_alert1` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `alert` */

/*Table structure for table `cafedra` */

DROP TABLE IF EXISTS `cafedra`;

CREATE TABLE `cafedra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `sname` char(2) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `cafedra` */

insert  into `cafedra`(`id`,`name`,`sname`,`datecreate`,`lastupdate`,`status`) values (2,'Барномасози','Б','2016-02-01 16:38:39','2016-02-01 16:38:39',1),(4,'Математикаи Оли','МО','2016-02-01 14:46:35','2016-02-01 14:46:35',1),(5,'Барномасози','М1','2016-02-04 10:49:34','2016-02-04 10:49:34',1);

/*Table structure for table `chat` */

DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderId` int(11) NOT NULL,
  `reciverId` int(11) NOT NULL,
  `text` text NOT NULL,
  `viewReciver` tinyint(4) NOT NULL,
  `statusSender` tinyint(4) NOT NULL,
  `statusReciver` tinyint(4) NOT NULL,
  `sendTime` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_chat1` (`senderId`),
  KEY `FK_chat2` (`reciverId`),
  CONSTRAINT `FK_chat1` FOREIGN KEY (`senderId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_chat2` FOREIGN KEY (`reciverId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `chat` */

insert  into `chat`(`id`,`senderId`,`reciverId`,`text`,`viewReciver`,`statusSender`,`statusReciver`,`sendTime`,`status`) values (15,3,27,'фҷвфҷв',1,1,1,'2016-03-30 09:05:44',1),(16,27,3,'Assalom',1,1,1,'2016-03-31 13:38:19',1),(17,27,3,'коро чи хел?',1,1,1,'2016-04-01 11:10:50',1),(18,27,195,'dd',0,1,1,'2016-04-21 08:53:30',1),(19,27,21,'ddd',0,1,1,'2016-04-21 08:53:45',1),(20,19,193,'ggg',1,1,1,'2016-04-26 11:53:37',1),(21,193,19,'sadsad',1,1,1,'2016-04-26 11:55:34',1),(22,19,193,'dfdsfdsfsdf',1,1,1,'2016-04-26 11:56:14',1);

/*Table structure for table `checktable` */

DROP TABLE IF EXISTS `checktable`;

CREATE TABLE `checktable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `checktable` */

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `city` */

insert  into `city`(`id`,`city`) values (1,'Хуҷанд'),(2,'Ҷаббор Расулов'),(3,'Ашт'),(4,'Спитамен'),(5,'Ғончи'),(6,'Мастчоҳи Кӯҳи'),(7,'Мастчоҳ'),(8,'Истаравшан'),(9,'Б. Ғафуров'),(10,'Чкаловск');

/*Table structure for table `code` */

DROP TABLE IF EXISTS `code`;

CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `code` */

/*Table structure for table `group` */

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `idGroup` int(11) NOT NULL AUTO_INCREMENT,
  `ihtisosId` int(100) NOT NULL,
  `course` tinyint(4) NOT NULL,
  `class` varchar(10) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`idGroup`),
  KEY `FK_groupd` (`ihtisosId`),
  CONSTRAINT `FK_groupd` FOREIGN KEY (`ihtisosId`) REFERENCES `ihtisos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `group` */

insert  into `group`(`idGroup`,`ihtisosId`,`course`,`class`,`datecreate`,`lastupdate`,`status`) values (1,19,1,'р','2016-02-29 10:14:25','2016-02-29 10:35:18',1),(2,18,1,'т','2016-02-29 10:35:33','2016-02-29 01:52:43',0),(3,20,1,'т б','2016-03-28 14:48:17','2016-03-28 14:48:17',1),(4,20,1,'т а','2016-03-28 14:52:58','2016-03-28 14:52:58',1),(5,21,1,'т а','2016-04-01 13:48:22','2016-04-01 13:48:56',1),(6,21,1,'т б','2016-04-01 13:49:14','2016-04-01 13:49:14',1),(7,22,1,'т','2016-04-01 13:49:39','2016-04-01 13:49:39',1),(8,23,1,'т','2016-04-01 13:49:59','2016-04-01 13:49:59',1),(9,23,2,'т','2016-04-01 14:02:25','2016-04-01 14:02:25',1),(10,22,2,'т','2016-04-01 14:02:44','2016-04-01 14:02:44',1);

/*Table structure for table `groupin` */

DROP TABLE IF EXISTS `groupin`;

CREATE TABLE `groupin` (
  `idGroupin` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`idGroupin`),
  KEY `FK_groupin` (`userId`),
  KEY `FK_groupin13` (`groupId`),
  CONSTRAINT `FK_groupin` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_groupin13` FOREIGN KEY (`groupId`) REFERENCES `group` (`idGroup`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

/*Data for the table `groupin` */

insert  into `groupin`(`idGroupin`,`userId`,`groupId`,`datecreate`,`status`) values (1,4,3,'2016-03-28 14:48:42',1),(2,5,3,'2016-03-28 14:48:47',1),(3,6,3,'2016-03-28 14:48:51',1),(4,7,3,'2016-03-28 14:48:55',1),(5,8,3,'2016-03-28 14:49:00',1),(6,9,3,'2016-03-28 14:49:06',1),(7,10,3,'2016-03-28 14:49:11',1),(8,11,3,'2016-03-28 14:49:19',1),(9,124,3,'2016-03-28 14:49:39',1),(10,123,3,'2016-03-28 14:49:45',1),(11,122,3,'2016-03-28 14:49:50',1),(12,121,3,'2016-03-28 14:49:53',1),(13,120,3,'2016-03-28 14:49:57',1),(14,119,3,'2016-03-28 14:50:06',1),(15,118,3,'2016-03-28 14:50:11',1),(16,117,3,'2016-03-28 14:50:18',1),(17,116,3,'2016-03-28 14:50:38',1),(18,115,3,'2016-03-28 14:51:47',1),(19,114,3,'2016-03-28 14:51:52',1),(20,113,3,'2016-03-28 14:51:56',1),(21,112,3,'2016-03-28 14:52:07',1),(22,111,3,'2016-03-28 14:52:11',1),(23,110,3,'2016-03-28 14:52:16',1),(24,109,3,'2016-03-28 14:52:23',1),(25,108,3,'2016-03-28 14:52:27',1),(26,107,3,'2016-03-28 14:52:31',1),(27,106,4,'2016-03-28 14:53:17',1),(28,105,4,'2016-03-28 14:53:21',1),(29,104,4,'2016-03-28 14:53:27',1),(30,103,4,'2016-03-28 14:53:32',1),(31,102,4,'2016-03-28 14:53:36',1),(32,101,4,'2016-03-28 14:53:41',1),(33,100,4,'2016-03-28 14:53:46',1),(34,99,4,'2016-03-28 14:53:50',1),(35,98,4,'2016-03-28 14:53:55',1),(36,97,4,'2016-03-28 14:54:02',1),(37,96,4,'2016-03-28 14:54:06',1),(38,95,1,'2016-03-28 14:54:12',1),(39,94,4,'2016-03-28 14:54:21',1),(40,93,4,'2016-03-28 14:54:25',1),(41,92,4,'2016-03-28 14:54:30',1),(42,91,4,'2016-03-28 14:54:35',1),(43,90,4,'2016-03-28 14:54:40',1),(44,89,4,'2016-03-28 14:54:43',1),(45,88,4,'2016-03-28 14:54:55',1),(46,87,4,'2016-03-28 14:55:04',1),(47,86,4,'2016-03-28 14:55:09',1),(48,85,4,'2016-03-28 14:55:15',1),(49,84,4,'2016-03-28 14:55:20',1),(50,83,4,'2016-03-28 14:55:28',1),(51,82,4,'2016-03-28 14:55:34',1),(52,81,4,'2016-03-28 14:55:39',1),(53,80,4,'2016-03-28 14:55:44',1),(54,79,4,'2016-03-28 14:55:47',1),(55,78,4,'2016-03-28 14:55:51',1),(56,35,6,'2016-04-01 13:51:15',1),(57,36,6,'2016-04-01 13:51:36',1),(58,37,6,'2016-04-01 13:51:50',1),(59,38,6,'2016-04-01 13:52:02',1),(60,39,6,'2016-04-01 13:52:09',1),(61,40,6,'2016-04-01 13:52:17',1),(62,41,6,'2016-04-01 13:52:24',1),(63,42,6,'2016-04-01 13:52:30',1),(64,43,6,'2016-04-01 13:52:35',1),(65,44,6,'2016-04-01 13:52:39',1),(66,45,6,'2016-04-01 13:52:44',1),(67,46,6,'2016-04-01 13:52:48',1),(68,47,6,'2016-04-01 13:52:53',1),(69,48,6,'2016-04-01 13:53:02',1),(70,49,6,'2016-04-01 13:53:06',1),(71,50,6,'2016-04-01 13:53:11',1),(72,51,6,'2016-04-01 13:53:15',1),(73,52,6,'2016-04-01 13:53:20',1),(74,53,6,'2016-04-01 13:53:24',1),(75,54,6,'2016-04-01 13:53:29',1),(76,55,6,'2016-04-01 13:53:36',1),(77,56,6,'2016-04-01 13:53:40',1),(78,57,5,'2016-04-01 13:53:45',1),(79,58,5,'2016-04-01 13:53:57',1),(80,59,5,'2016-04-01 13:54:04',1),(81,60,5,'2016-04-01 13:54:08',1),(82,61,5,'2016-04-01 13:54:13',1),(83,62,5,'2016-04-01 13:54:17',1),(84,63,5,'2016-04-01 13:54:22',1),(85,64,5,'2016-04-01 13:54:27',1),(86,65,5,'2016-04-01 13:54:35',1),(87,66,5,'2016-04-01 13:54:41',1),(88,67,5,'2016-04-01 13:54:46',1),(89,68,5,'2016-04-01 13:54:56',1),(90,69,5,'2016-04-01 13:55:01',1),(91,70,5,'2016-04-01 13:55:06',1),(92,71,5,'2016-04-01 13:55:11',1),(93,72,5,'2016-04-01 13:55:15',1),(94,73,5,'2016-04-01 13:55:26',1),(95,74,5,'2016-04-01 13:55:33',1),(96,75,5,'2016-04-01 13:55:37',1),(97,76,5,'2016-04-01 13:55:42',1),(98,77,5,'2016-04-01 13:55:53',1),(99,125,3,'2016-04-01 13:56:08',1),(100,126,3,'2016-04-01 13:56:49',1),(101,127,3,'2016-04-01 13:56:56',1),(102,128,3,'2016-04-01 13:57:03',1),(103,129,3,'2016-04-01 13:57:08',1),(104,130,3,'2016-04-01 13:57:13',1),(105,131,8,'2016-04-01 13:57:19',1),(106,132,8,'2016-04-01 13:57:23',1),(107,133,8,'2016-04-01 13:57:28',1),(108,134,8,'2016-04-01 13:57:33',1),(109,135,8,'2016-04-01 13:57:43',1),(110,136,8,'2016-04-01 13:58:01',1),(111,137,8,'2016-04-01 13:58:08',1),(112,138,8,'2016-04-01 13:58:13',1),(113,139,8,'2016-04-01 13:58:17',1),(114,140,8,'2016-04-01 13:58:22',1),(115,141,8,'2016-04-01 13:58:26',1),(116,142,8,'2016-04-01 13:58:32',1),(117,143,8,'2016-04-01 13:58:37',1),(118,144,8,'2016-04-01 13:58:41',1),(119,145,8,'2016-04-01 13:58:50',1),(120,146,8,'2016-04-01 13:58:55',1),(121,147,8,'2016-04-01 13:59:00',1),(122,148,8,'2016-04-01 13:59:05',1),(123,149,8,'2016-04-01 13:59:10',1),(124,150,8,'2016-04-01 13:59:14',1),(125,151,8,'2016-04-01 13:59:21',1),(126,152,8,'2016-04-01 13:59:26',1),(127,153,7,'2016-04-01 13:59:44',1),(128,154,7,'2016-04-01 13:59:53',1),(129,155,7,'2016-04-01 14:00:02',1),(130,156,7,'2016-04-01 14:00:06',1),(131,157,7,'2016-04-01 14:00:10',1),(132,158,7,'2016-04-01 14:00:16',1),(133,159,7,'2016-04-01 14:00:22',1),(134,160,7,'2016-04-01 14:00:28',1),(135,161,7,'2016-04-01 14:00:33',1),(136,162,7,'2016-04-01 14:00:38',1),(137,163,7,'2016-04-01 14:00:44',1),(138,164,7,'2016-04-01 14:00:49',1),(139,165,7,'2016-04-01 14:00:59',1),(140,166,7,'2016-04-01 14:01:03',1),(141,167,7,'2016-04-01 14:01:07',1),(142,168,7,'2016-04-01 14:01:11',1),(143,169,7,'2016-04-01 14:01:16',1),(144,170,7,'2016-04-01 14:01:21',1),(145,171,7,'2016-04-01 14:01:25',1),(146,172,7,'2016-04-01 14:01:29',1),(147,173,7,'2016-04-01 14:01:34',1),(148,174,7,'2016-04-01 14:01:38',1),(149,175,7,'2016-04-01 14:01:47',1),(150,176,7,'2016-04-01 14:01:53',1),(151,177,7,'2016-04-01 14:01:58',1),(152,181,10,'2016-04-01 14:03:15',1),(153,182,10,'2016-04-01 14:03:21',1),(154,183,10,'2016-04-01 14:03:27',1),(155,184,10,'2016-04-01 14:03:32',1),(156,185,10,'2016-04-01 14:03:36',1),(157,186,10,'2016-04-01 14:03:40',1),(158,187,10,'2016-04-01 14:03:47',1),(159,188,10,'2016-04-01 14:03:55',1),(160,189,10,'2016-04-01 14:03:59',1),(161,190,10,'2016-04-01 14:04:04',1),(162,178,9,'2016-04-01 14:04:14',1),(163,179,9,'2016-04-01 14:04:21',1),(164,180,9,'2016-04-01 14:04:28',1),(165,191,10,'2016-04-01 14:04:36',1),(166,192,10,'2016-04-01 14:04:40',1);

/*Table structure for table `gscores` */

DROP TABLE IF EXISTS `gscores`;

CREATE TABLE `gscores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `groupinId` int(11) NOT NULL,
  `bal` float NOT NULL,
  `bal2` int(11) NOT NULL,
  `znak` char(2) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gscores` (`groupinId`),
  KEY `FK_gscores1` (`shedualId`),
  CONSTRAINT `FK_gscores` FOREIGN KEY (`groupinId`) REFERENCES `groupin` (`idGroupin`),
  CONSTRAINT `FK_gscores1` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

/*Data for the table `gscores` */

insert  into `gscores`(`id`,`shedualId`,`groupinId`,`bal`,`bal2`,`znak`,`datecreate`,`lastupdate`,`status`) values (42,1,90,0.9,9,'Fx','2016-04-21 14:37:16','2016-04-26 23:40:29',0),(43,1,82,0.9,9,'Fx','2016-04-21 14:41:22','2016-04-26 23:40:39',0),(44,1,98,0.5,5,'Fx','2016-04-22 08:12:51','2016-04-26 23:35:15',0),(45,8,163,0,0,'Fx','2016-04-26 12:00:01','2016-04-26 12:01:05',0),(46,8,162,0,0,'Fx','2016-04-26 12:00:06','2016-04-26 12:01:07',0),(47,8,164,0,0,'Fx','2016-04-26 12:00:09','2016-04-26 12:01:09',0),(48,1,80,6,6,'F','2016-04-26 23:35:19','2016-04-26 23:35:19',0),(49,1,91,8,8,'F','2016-04-26 23:35:22','2016-04-26 23:35:22',0),(50,1,79,9,9,'F','2016-04-26 23:35:26','2016-04-26 23:35:26',0),(51,1,93,9,9,'F','2016-04-26 23:35:32','2016-04-26 23:35:32',0),(52,1,92,9,9,'F','2016-04-26 23:35:37','2016-04-26 23:35:37',0),(53,1,87,6,6,'F','2016-04-26 23:35:40','2016-04-26 23:35:40',0),(54,1,83,10,10,'F','2016-04-26 23:35:43','2016-04-26 23:35:43',0),(55,1,88,7,7,'F','2016-04-26 23:35:48','2016-04-26 23:35:48',0),(56,1,85,8,8,'F','2016-04-26 23:35:52','2016-04-26 23:35:52',0),(57,1,95,6,6,'F','2016-04-26 23:35:56','2016-04-26 23:35:56',0),(58,1,94,7,7,'F','2016-04-26 23:35:59','2016-04-26 23:35:59',0),(59,1,96,8,8,'F','2016-04-26 23:36:02','2016-04-26 23:36:02',0),(60,1,97,9,9,'F','2016-04-26 23:36:07','2016-04-26 23:36:07',0),(61,1,84,5,5,'F','2016-04-26 23:36:11','2016-04-26 23:36:11',0),(62,1,78,8,8,'F','2016-04-26 23:36:16','2016-04-26 23:36:16',0),(63,1,81,6,6,'F','2016-04-26 23:36:20','2016-04-26 23:36:20',0),(64,1,86,5,5,'F','2016-04-26 23:36:26','2016-04-26 23:36:26',0),(65,1,89,5,5,'F','2016-04-26 23:36:30','2016-04-26 23:36:30',0);

/*Table structure for table `ihtisos` */

DROP TABLE IF EXISTS `ihtisos`;

CREATE TABLE `ihtisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ihtisos` varchar(250) NOT NULL,
  `code` varchar(50) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `ihtisos` */

insert  into `ihtisos`(`id`,`ihtisos`,`code`,`datecreate`,`status`) values (18,'Таҷҳизот ва технологияи истеҳсоли кафшергарӣ','2-36.01.06','2016-02-26 15:56:10',1),(19,'Иқтисодиёт ва ташкили хоҷагии меҳмонхонаҳо (техник-иқтисодчӣ)','2-27.01.31.26','2016-02-29 10:09:31',1),(20,'Ташкили хамлу накл ва идораи наклиёти автомобили ва шахри','2-44.01.01.','2016-03-28 14:47:44',1),(21,'Таъмини барномавии технологияи иттилоотӣ','2-40.01.01.','2016-03-29 15:26:45',1),(22,'Сохтмони саноатӣ ва шахрвандӣ','2-70.02.01.','2016-03-29 15:27:12',1),(23,'Технологияи истехсоли хурока','2-49.01.31.','2016-03-29 15:27:37',1);

/*Table structure for table `mailbox` */

DROP TABLE IF EXISTS `mailbox`;

CREATE TABLE `mailbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderId` int(11) NOT NULL,
  `reciverId` int(11) NOT NULL,
  `head` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `upload` varchar(250) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `statusSender` tinyint(4) NOT NULL,
  `statusReciver` tinyint(4) NOT NULL,
  `viewReciver` tinyint(4) NOT NULL,
  `notyReciver` tinyint(4) NOT NULL,
  `id2` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mailbox1` (`senderId`),
  KEY `FK_mailbox2` (`reciverId`),
  CONSTRAINT `FK_mailbox1` FOREIGN KEY (`senderId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_mailbox2` FOREIGN KEY (`reciverId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mailbox` */

/*Table structure for table `namudi_fan` */

DROP TABLE IF EXISTS `namudi_fan`;

CREATE TABLE `namudi_fan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namud` varchar(150) NOT NULL,
  `snamud` char(1) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `namudi_fan` */

insert  into `namudi_fan`(`id`,`namud`,`snamud`,`datecreate`,`lastupdate`,`status`) values (1,'Умумитахассусӣ','У','2016-04-08 08:40:35','2016-04-08 08:40:35',1),(2,'Тахассусӣ','Т','2016-04-08 08:41:15','2016-04-08 08:41:15',1);

/*Table structure for table `scores` */

DROP TABLE IF EXISTS `scores`;

CREATE TABLE `scores` (
  `idScores` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `groupinId` int(11) NOT NULL,
  `bal` double NOT NULL,
  `typeId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`idScores`),
  KEY `FK_scores0` (`typeId`),
  KEY `FK_scores` (`shedualId`),
  KEY `FK_scores25` (`groupinId`),
  CONSTRAINT `FK_scores` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_scores0` FOREIGN KEY (`typeId`) REFERENCES `type` (`id`),
  CONSTRAINT `FK_scores25` FOREIGN KEY (`groupinId`) REFERENCES `groupin` (`idGroupin`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `scores` */

insert  into `scores`(`idScores`,`shedualId`,`groupinId`,`bal`,`typeId`,`datecreate`,`lastupdate`,`status`) values (1,8,163,0,4,'2016-04-26 12:00:01','2016-04-26 12:00:01',1),(2,8,162,0,4,'2016-04-26 12:00:06','2016-04-26 12:00:06',1),(3,8,164,0,4,'2016-04-26 12:00:09','2016-04-26 12:00:09',1),(4,8,163,0,3,'2016-04-26 12:01:05','2016-04-26 12:01:05',1),(5,8,162,0,3,'2016-04-26 12:01:07','2016-04-26 12:01:08',1),(6,8,164,0,3,'2016-04-26 12:01:09','2016-04-26 12:01:09',1),(7,1,90,10,4,'2016-04-26 23:35:04','2016-04-26 23:35:04',1),(8,1,82,9,4,'2016-04-26 23:35:13','2016-04-26 23:35:13',1),(9,1,98,8,4,'2016-04-26 23:35:15','2016-04-26 23:35:15',1),(10,1,80,6,4,'2016-04-26 23:35:19','2016-04-26 23:35:19',1),(11,1,91,8,4,'2016-04-26 23:35:22','2016-04-26 23:35:22',1),(12,1,79,9,4,'2016-04-26 23:35:26','2016-04-26 23:35:26',1),(13,1,93,9,4,'2016-04-26 23:35:32','2016-04-26 23:35:32',1),(14,1,92,9,4,'2016-04-26 23:35:37','2016-04-26 23:35:37',1),(15,1,87,6,4,'2016-04-26 23:35:40','2016-04-26 23:35:40',1),(16,1,83,10,4,'2016-04-26 23:35:43','2016-04-26 23:35:43',1),(17,1,88,7,4,'2016-04-26 23:35:48','2016-04-26 23:35:48',1),(18,1,85,8,4,'2016-04-26 23:35:52','2016-04-26 23:35:52',1),(19,1,95,6,4,'2016-04-26 23:35:56','2016-04-26 23:35:56',1),(20,1,94,7,4,'2016-04-26 23:35:59','2016-04-26 23:35:59',1),(21,1,96,8,4,'2016-04-26 23:36:02','2016-04-26 23:36:02',1),(22,1,97,9,4,'2016-04-26 23:36:07','2016-04-26 23:36:07',1),(23,1,84,5,4,'2016-04-26 23:36:11','2016-04-26 23:36:11',1),(24,1,78,8,4,'2016-04-26 23:36:16','2016-04-26 23:36:16',1),(25,1,81,6,4,'2016-04-26 23:36:20','2016-04-26 23:36:20',1),(26,1,86,5,4,'2016-04-26 23:36:26','2016-04-26 23:36:26',1),(27,1,89,5,4,'2016-04-26 23:36:30','2016-04-26 23:36:30',1);

/*Table structure for table `shedual` */

DROP TABLE IF EXISTS `shedual`;

CREATE TABLE `shedual` (
  `idShedual` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `cikl` int(11) NOT NULL,
  `semestr` int(11) NOT NULL,
  `dateStart` date NOT NULL,
  `dateEnd` date NOT NULL,
  `dateRest` text NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`idShedual`),
  KEY `FK_shedual6` (`subjectId`),
  KEY `FK_shedual` (`groupId`),
  CONSTRAINT `FK_shedual` FOREIGN KEY (`groupId`) REFERENCES `group` (`idGroup`) ON DELETE CASCADE,
  CONSTRAINT `FK_shedual6` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`idSubject`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `shedual` */

insert  into `shedual`(`idShedual`,`groupId`,`subjectId`,`cikl`,`semestr`,`dateStart`,`dateEnd`,`dateRest`,`datecreate`,`lastupdate`,`status`) values (1,5,20,3,1,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-08 14:00:37','2016-04-08 14:00:37',1),(2,6,4,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 08:54:46','2016-04-13 08:54:46',1),(3,4,14,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:49:18','2016-04-13 09:49:18',1),(4,3,15,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:50:05','2016-04-13 09:50:05',1),(5,8,16,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:50:45','2016-04-13 09:50:45',1),(6,7,17,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:51:23','2016-04-13 09:51:23',1),(7,10,18,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:51:53','2016-04-13 09:51:53',1),(8,9,19,4,2,'2016-04-01','2016-04-19','2016-04-02,2016-04-03,2016-04-04,2016-04-09,2016-04-10,2016-04-16,2016-04-17','2016-04-13 09:52:24','2016-04-13 09:52:24',1),(9,5,21,5,2,'2016-04-20','2016-05-11','2016-04-23,2016-04-24,2016-04-30,2016-05-01,2016-05-07,2016-05-08','2016-04-21 11:42:31','2016-04-21 11:42:31',1);

/*Table structure for table `subject` */

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `idSubject` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` int(50) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`idSubject`),
  KEY `FK_subject22` (`teacherId`),
  KEY `FK_subject4` (`subjectId`),
  CONSTRAINT `FK_subject22` FOREIGN KEY (`teacherId`) REFERENCES `teachers` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_subject4` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `subject` */

insert  into `subject`(`idSubject`,`subjectId`,`teacherId`,`datecreate`,`status`) values (1,1,1,'2016-04-08 13:35:25',1),(4,5,5,'2016-04-08 13:39:41',1),(14,6,6,'2016-04-08 13:48:19',1),(15,7,7,'2016-04-08 13:49:30',1),(16,9,8,'2016-04-08 13:49:49',1),(17,10,9,'2016-04-08 13:50:08',1),(18,11,10,'2016-04-08 13:50:29',1),(19,12,11,'2016-04-08 13:50:48',1),(20,1,4,'2016-04-08 13:53:44',1),(21,13,1,'2016-04-21 11:40:05',1);

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `scode` varchar(50) NOT NULL,
  `credit` int(11) NOT NULL,
  `id_namud` int(11) NOT NULL,
  `id_cafedra` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_subjects` (`id_namud`),
  KEY `FK_subjects0` (`id_cafedra`),
  CONSTRAINT `FK_subjects` FOREIGN KEY (`id_namud`) REFERENCES `namudi_fan` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_subjects0` FOREIGN KEY (`id_cafedra`) REFERENCES `cafedra` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `subjects` */

insert  into `subjects`(`id`,`subject`,`scode`,`credit`,`id_namud`,`id_cafedra`,`datecreate`,`status`) values (1,'Асосҳои Web-дизайн','БТ71155',4,2,2,'2016-04-08 08:41:56',1),(5,'Забони Олмонӣ','БУ86544',4,1,2,'2016-04-08 08:51:01',1),(6,'Назария Автомобил-1','БУ40728',4,1,2,'2016-04-08 09:02:05',1),(7,'Математикаи олӣ-1','МОУ55090',4,1,4,'2016-04-08 13:22:57',1),(9,'Механикаи техники','БУ15182',4,1,2,'2016-04-08 13:33:07',1),(10,'Забони тоҷикӣ','БУ56727',4,1,2,'2016-04-08 13:33:31',1),(11,'Ас. пурзӯркунии конс. сохтмонӣ','БУ51119',4,1,2,'2016-04-08 13:34:36',1),(12,'Ҷараён ва аппаратҳои хӯрока-2','БУ21911',4,1,2,'2016-04-08 13:35:06',1),(13,'Информатика','БУ28934',6,1,2,'2016-04-21 11:39:48',1);

/*Table structure for table `tablelock` */

DROP TABLE IF EXISTS `tablelock`;

CREATE TABLE `tablelock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tablelock` (`shedualId`),
  CONSTRAINT `FK_tablelock` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tablelock` */

/*Table structure for table `tabsent` */

DROP TABLE IF EXISTS `tabsent`;

CREATE TABLE `tabsent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `groupinId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `checkId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tabsent23` (`shedualId`),
  CONSTRAINT `FK_tabsent23` FOREIGN KEY (`shedualId`) REFERENCES `shedual` (`idShedual`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `tabsent` */

insert  into `tabsent`(`id`,`shedualId`,`groupinId`,`typeId`,`checkId`,`datecreate`,`lastupdate`,`status`) values (1,3,44,3,1,'2016-04-14 11:47:30','2016-04-14 11:47:30',1),(2,3,44,3,2,'2016-04-14 11:47:30','2016-04-14 11:47:30',1),(3,3,44,3,3,'2016-04-14 11:47:30','2016-04-14 11:47:30',1),(4,3,27,3,1,'2016-04-14 11:47:40','2016-04-14 11:47:40',1),(5,3,27,3,2,'2016-04-14 11:47:40','2016-04-14 11:47:40',1),(6,3,27,3,3,'2016-04-14 11:47:40','2016-04-14 11:47:40',1),(7,1,90,3,1,'2016-04-20 12:43:59','2016-04-20 12:43:59',1),(8,1,90,3,2,'2016-04-20 12:43:59','2016-04-20 12:43:59',1),(9,1,90,3,3,'2016-04-20 12:43:59','2016-04-20 12:43:59',1),(10,1,82,3,1,'2016-04-22 11:51:22','2016-04-22 11:51:22',1),(11,1,82,3,2,'2016-04-22 11:51:22','2016-04-22 11:51:22',1),(12,1,82,3,3,'2016-04-22 11:51:22','2016-04-22 11:51:22',1),(13,1,90,4,1,'2016-04-22 11:52:13','2016-04-22 11:52:13',1),(14,1,90,4,2,'2016-04-22 11:52:13','2016-04-22 11:52:13',1),(15,1,90,4,3,'2016-04-22 11:52:13','2016-04-22 11:52:13',1),(16,1,82,4,1,'2016-04-22 11:52:22','2016-04-22 11:52:22',1),(17,1,82,4,2,'2016-04-22 11:52:22','2016-04-22 11:52:22',1),(18,1,82,4,3,'2016-04-22 11:52:22','2016-04-22 11:52:22',1);

/*Table structure for table `teachers` */

DROP TABLE IF EXISTS `teachers`;

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `daraja` enum('easy','medium','high') NOT NULL,
  `staj` tinyint(4) NOT NULL,
  `soli_kabul` date NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_teachers` (`userId`),
  CONSTRAINT `FK_teachers22` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `teachers` */

insert  into `teachers`(`id`,`userId`,`daraja`,`staj`,`soli_kabul`,`datecreate`,`lastupdate`,`status`) values (1,2,'',5,'0000-00-00','2016-04-08 10:23:00','2016-04-08 10:23:00',1),(2,26,'easy',1,'0000-00-00','2016-04-08 13:37:36','2016-04-08 13:37:36',1),(3,3,'easy',1,'0000-00-00','2016-04-08 13:37:45','2016-04-08 13:37:45',1),(4,193,'easy',1,'0000-00-00','2016-04-08 13:37:57','2016-04-08 13:37:57',1),(5,194,'easy',1,'0000-00-00','2016-04-08 13:38:06','2016-04-08 13:38:06',1),(6,195,'easy',1,'0000-00-00','2016-04-08 13:38:16','2016-04-08 13:38:16',1),(7,196,'easy',1,'0000-00-00','2016-04-08 13:38:25','2016-04-08 13:38:25',1),(8,197,'easy',1,'0000-00-00','2016-04-08 13:38:33','2016-04-08 13:38:33',1),(9,198,'easy',1,'0000-00-00','2016-04-08 13:38:42','2016-04-08 13:38:42',1),(10,199,'easy',1,'0000-00-00','2016-04-08 13:38:51','2016-04-08 13:38:51',1),(11,200,'easy',1,'0000-00-00','2016-04-08 13:39:01','2016-04-08 13:39:01',1);

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shedualId` int(11) NOT NULL,
  `groupinId` int(11) NOT NULL,
  `bal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `test` */

/*Table structure for table `type` */

DROP TABLE IF EXISTS `type`;

CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `type` */

insert  into `type`(`id`,`type`,`status`) values (1,'1',1),(2,'2',1),(3,'3',1),(4,'4',1),(5,'5',1),(6,'6',1),(7,'7',1),(8,'8',1),(9,'C. М. №1',1),(10,'9',1),(11,'10',1),(12,'11',1),(13,'12',1),(14,'13',1),(15,'14',1),(16,'15',1),(17,'16',1),(18,'C. М. №2',1),(19,'С. Устод',1),(20,'Кори семестри',1),(21,'С. Ниҳои Кӯшиши якум',1),(22,'С. Ниҳои Кӯшиши дуюм',1),(23,'Триместр',1),(24,'Балли умумӣ',1),(25,'Баҳо (Алфавит)',1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `address` varchar(100) NOT NULL,
  `number_phone` char(20) NOT NULL,
  `cityId` int(11) NOT NULL,
  `accessId` int(11) NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user1` (`cityId`),
  KEY `FK_user` (`accessId`),
  CONSTRAINT `FK_user` FOREIGN KEY (`accessId`) REFERENCES `user_access` (`id`),
  CONSTRAINT `FK_user1` FOREIGN KEY (`cityId`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`login`,`password`,`avatar`,`fname`,`name`,`lname`,`birthday`,`gender`,`address`,`number_phone`,`cityId`,`accessId`,`datecreate`,`lastupdate`,`status`) values (2,'test','202cb962ac59075b964b07152d234b70','/dist/img/get_foto.jpg','Расулов','Музаффарҷон','Илҳомҷонович','1993-10-16',1,'Washington 23','+(992)',1,2,'2015-09-15 08:53:34','2016-03-28 21:16:57',1),(3,'test1','098f6bcd4621d373cade4e832627b4f6','/dist/img/user7-128x128.jpg','Давлатова','Моҳирахон','Аъзамовна','1978-08-12',0,'Washington 23','0',1,2,'2015-09-15 11:43:13','2015-09-15 11:43:13',1),(4,'test4','86985e105f79b95d6bc918fb45ec7727','/dist/img/user6-128x128.jpg','Амиров ','Амирбек','.','1996-12-16',0,'н.Спитамен','+(992) 927520967',1,1,'2015-09-16 11:16:34','2015-09-16 11:16:34',1),(5,'test5','e3d704f3542b44a621ebed70dc0efe13','/dist/img/user6-128x128.jpg','Зарипов ','Мусо','.','1997-09-14',0,'н.Спитамен','+(992) 927774608',1,1,'2015-09-16 11:16:45','2015-09-16 11:16:45',1),(6,'test6','4cfad7076129962ee70c36839a1e3e15','/dist/img/user6-128x128.jpg','Имомов','Улмас','Шоназарович','1997-07-15',0,'ш.Чкаловск, к.Победа 8','+(992) 929045151',1,1,'2015-09-16 11:16:46','2015-09-16 11:16:46',1),(7,'test7','b04083e53e242626595e2b8ea327e525','/dist/img/user6-128x128.jpg','Нурматов','Бунёд','.','1995-11-10',0,'н.Ашт, ч.Понгоз','+(992) 927606549',1,1,'2015-09-16 11:16:46','2015-09-16 11:16:46',1),(8,'test8','5e40d09fa0529781afd1254a42913847','/dist/img/user6-128x128.jpg','Туракулов','Ахрор','.','1996-05-04',0,'ш.Конибодом, д.Ниёзбек','+(992) 929301333',1,1,'2015-09-16 11:16:46','2015-09-16 11:16:46',1),(9,'test9','739969b53246b2c727850dbb3490ede6','/dist/img/user6-128x128.jpg','Шамсиев','Хушбахт','.','1997-09-10',0,'н.Шахристон,д.Шахристон','+(992) 987064090',1,1,'2015-09-16 11:16:47','2015-09-16 11:16:47',1),(10,'test10','c1a8e059bfd1e911cf10b626340c9a54','/dist/img/user6-128x128.jpg','Хочаев','Хайрулло','.','1997-05-09',0,'ш.Хучанд,8-91-17','+(992) 9227701449',1,1,'2015-09-16 11:16:47','2015-09-16 11:16:47',1),(11,'test11','f696282aa4cd4f614aa995190cf442fe','/dist/img/user6-128x128.jpg','Сулаймонов','Дилшод','.','1997-04-20',0,'н.Гончи','+(992)',1,1,'2015-09-16 11:16:47','2015-09-16 11:16:47',1),(12,'test12','60474c9c10d7142b7508ce7a50acf414','/dist/img/user6-128x128.jpg','Ҷалилов','Фазлиддин','Testovich','1996-10-12',0,'Wanshington destrict','0',1,1,'2015-09-16 11:16:47','2015-09-16 11:16:47',1),(13,'test13','33fc3dbd51a8b38a38b1b85b6a76b42b','/dist/img/user6-128x128.jpg','Раҳмонбердиев','Акмалхӯҷа','Исомадиинович','1996-10-12',0,'Wanshington destrict','+(992)',1,1,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(14,'test14','b99c94f62fb2a61433c4e44e27406050','/dist/img/user6-128x128.jpg','Худойбердиев','Илёс','Testovich','1996-10-12',0,'Wanshington destrict','0',1,1,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(15,'test15','4b377d23309d4ed39c9da5791417aeff','/dist/img/user6-128x128.jpg','Солиев','Акрам','Testovich','1996-10-12',0,'Wanshington destrict','0',1,1,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(16,'test16','0c1ccf98666ed505310c0471529429db','/dist/img/user6-128x128.jpg','Амонов','Камолҷон','Testovich','1996-10-12',0,'Wanshington destrict','0',1,1,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(17,'test17','fcb1a7bbe091b4ee78748946cb762a84','/dist/img/user6-128x128.jpg','Кӯчқоров','Парвизҷон','Testovich','1996-10-12',0,'Wanshington destrict','0',1,1,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(18,'test18','df71df92c31111f810a7d89bd2c2e35d','/dist/img/user6-128x128.jpg','Афандихон','Афандихонов','Testovich','1996-10-12',0,'Wanshington destrict','0',1,5,'2015-09-16 11:16:48','2015-09-16 11:16:48',1),(19,'test19','202cb962ac59075b964b07152d234b70','/dist/img/user6-128x128.jpg','Назорат','Назорат','Testovich','1996-10-12',0,'Wanshington destrict','0',1,3,'2015-09-16 11:16:49','2015-09-16 11:16:49',1),(20,'rasulov','7f17f3604ab904cd9ae53b3f25093b0f','/dist/img/user6-128x128.jpg','Холматов','Дӯстмуҳаммад','A.','1996-10-12',0,'dasd','0',1,7,'2015-10-30 14:28:58','2015-10-30 14:28:58',1),(21,'kaid','5965c16902e4d95ceca4d2f2836829ab','/dist/img/user6-128x128.jpg','Қайди','Донишҷӯён',' ','1996-10-12',1,'123','992901822223',1,4,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(22,'st22623','d7312164a71d683688af5e60b5128360','/dist/img/user6-128x128.jpg','Расулов','Музаффарҷон','Илҳомҷонович','1993-10-16',0,'кӯч. Айни 23','+(992) 92 8344074',1,1,'2015-12-03 14:21:51','2015-12-03 14:21:51',1),(23,'st51885','1a820262d75d60c2823da319e1366f14','/dist/img/user6-128x128.jpg','Расулов','Музаффарҷон','','1996-10-26',0,'кӯч Эгамбердиев 23','+(992) 92 8344074',1,1,'2015-12-03 14:28:33','2015-12-03 14:28:33',1),(24,'st61190','966249e25bdbd1a3b119e60d3f182cf2','/dist/img/user6-128x128.jpg','Раҳимбердиев','Акмалхӯҷа','Исматуллоевич','1993-01-14',0,'кӯч. Айни 23','+(992)92-834-4074',9,1,'2015-12-07 11:41:34','2015-12-07 11:41:34',1),(25,'st88428','8cf911d003a273309daabdedd0f254a4','/dist/img/user6-128x128.jpg','12','12',' H','1996-12-24',0,'кӯч. Айни 23','+(992)',1,1,'2015-12-09 13:06:38','2015-12-09 13:06:38',1),(26,'vacansy','202cb962ac59075b964b07152d234b70','/images/user.jpg','Вакансия',' ',' ','0000-00-00',0,'1','0',1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(27,'va7tal','202cb962ac59075b964b07152d234b70','/images/user.jpg','Admin','','','0000-00-00',1,'1','+(992)',1,8,'0000-00-00 00:00:00','2016-02-29 22:25:35',1),(28,'st73130','6031a2fcd554d7ffef776977603156af','/dist/img/user6-128x128.jpg','Расулов','Музаффарҷон','Илҳомҷонович','2016-02-04',0,'test 223','+(992)928344074',7,1,'2016-02-23 14:36:21','2016-02-23 14:36:21',1),(29,'st38690','0b7a465779e96e876fe19e56d2f92da4','/dist/img/user6-128x128.jpg','Test','Акмалхӯҷа','Исомадиинович','2016-02-17',1,'test 223','+(992)928344074',1,1,'2016-02-23 14:37:43','2016-02-23 14:37:43',1),(30,'st69015','523fc23cd682506091c974f11a66b296','/dist/img/user6-128x128.jpg','Расулов','Акмалхӯҷа','Исматуллоевич','2016-02-12',1,'test 223','+(992)918344562',10,1,'2016-02-23 14:38:39','2016-02-23 14:38:39',1),(31,'st92518','df1c463e72b44739551153fca2180158','/dist/img/user6-128x128.jpg','Тестов','Тест','Тестович','2016-02-27',0,'asd','+(992)',8,1,'2016-02-29 10:47:35','2016-02-29 07:36:51',1),(32,'st85462','0b6778c1962fd744b27c750fdc1ed34f','/dist/img/user6-128x128.jpg','213','32','Илҳомҷонович','1993-01-14',0,'32 мкр 40 дом 5 квартира','+(992)',1,1,'2016-02-29 01:25:58','2016-02-29 01:25:58',1),(33,'st87906','a4c61279f34dd783c99262ceab0e4a43','/dist/img/user6-128x128.jpg','Раҳимбердиев','Акмалхӯҷа','Исомадиинович','1999-02-17',0,'test 223','+(992)',1,1,'2016-03-01 03:32:38','2016-03-01 03:32:38',1),(34,'st37479','88a9ffa91cbc2604c3a9f98330e3ddd7','/dist/img/user6-128x128.jpg','fds','fsd','sdf','2016-03-24',0,'ds','+(992)',1,1,'2016-03-01 05:34:43','2016-03-01 05:34:43',1),(35,'st47938','9bbe9e42cc489d163f34e7193ab3e020','/dist/img/user6-128x128.jpg','Абдумаликзода ','Амирчон','.','1997-09-01',0,'в.Сугд,н.Б.Гафуров','+(992) 938620077',9,1,'2016-03-04 04:02:58','2016-03-04 04:02:58',1),(36,'st64690','a38a475ed96a28a47b304f310700d09c','/dist/img/user6-128x128.jpg','Назирбоев','Фаррух','Исломчонович','1997-05-04',0,'в.Суғд,н.Ашт,ш.Гулистон,к.Ҷавонӣ ','+(992)',3,1,'2016-03-04 04:05:31','2016-03-04 04:05:31',1),(37,'st68769','04488948c0016ecf87351a1341a34479','/dist/img/user6-128x128.jpg','Абдукаримов ','Абдурасул','Абдуазизович','1997-01-23',0,'в.Суғд,н.Б.Ғафуров,ҷ.Ёва,к.Сартӯқай','+(992) 927600372',9,1,'2016-03-04 04:07:40','2016-03-04 04:07:40',1),(38,'st58787','31c85fa879f3c19d99f6e363514eae77','/dist/img/user6-128x128.jpg','Ахмедов','Зухурулло','Мухторович','1997-04-24',0,'в.Суғд,ш.Исфара,д.Навгилем ','+(992)',1,1,'2016-03-04 04:10:25','2016-03-04 04:10:25',1),(39,'st56273','e8c6dabb4c1cadfa66aef5dd2474f9ba','/dist/img/user6-128x128.jpg','Олимов ','Мухаммадчон','Анварович','1997-05-23',0,'ш.Хуҷанд,12 - 7 - 2 ','+(992)',1,1,'2016-03-04 04:11:52','2016-03-04 04:11:52',1),(40,'st31582','39154b5fe7d9610e71553ab63bc7f2ab','/dist/img/user6-128x128.jpg','Шарипов','Шарифчон','Солиевич','1997-08-14',0,'в.Суғд,ш.Хуҷанд,ҷ.Ёва,к.Ёва 59 ','+(992) 927566843',1,1,'2016-03-04 04:19:05','2016-03-04 04:19:05',1),(41,'st55510','76cf975d093ff08cecc476b7265a11bb','/dist/img/user6-128x128.jpg','Туйчиев','Шахзод','Зафарчонович','1997-05-24',0,'в.Суғд,н.Ашт,д.Бахмал ','+(992) 927126103',3,1,'2016-03-04 04:20:46','2016-03-04 04:20:46',1),(42,'st93133','e0dd6a718b681531504b45d034c5c2c7','/dist/img/user6-128x128.jpg','Абдуллоев','Абдуазиз','Абдуллатифович','1997-06-25',0,'в.Суғд,н.Б.Ғафуров,к.Ҷомӣ 28 а ','+(992) 927744613',9,1,'2016-03-04 04:25:29','2016-03-04 04:25:29',1),(43,'st89097','152750aba258f259a8b00d1ce643b8b1','/dist/img/user6-128x128.jpg','Тешаев ','Бахтиёр','Усмончонович','1997-08-28',0,'в.Суғд,ш.Хуҷанд,31-40-75 ','+(992) 927772471',1,1,'2016-03-04 04:28:00','2016-03-04 04:28:00',1),(44,'st42716','55c4af6b07a2f9163ff5a20d64b057b5','/dist/img/user6-128x128.jpg','Махмудов','Манучехр','Кобилчонович','1997-04-06',0,'в.Суғд,н.Б.Ғафуров,к.Мир 19 а ','+(992) 927330080',9,1,'2016-03-04 04:30:23','2016-03-04 04:30:23',1),(45,'st87558','30774c084e0af6693d59fd8bea1245aa','/dist/img/user6-128x128.jpg','Гаффоров','Фируз','Курбоналиевич','1997-08-08',0,'в.Суғд,н.Конибодом,д.Маҳрам ','+(992) 927356172',1,1,'2016-03-04 04:31:54','2016-03-04 04:31:54',1),(46,'st47941','83bfaeee10995297862b00c7adbe9a21','/dist/img/user6-128x128.jpg','Ёрмамадов','Алимухаммад','Наврузмамадович','1996-08-15',0,'ш.Хуҷанд,31-26-52 ','+(992)',1,1,'2016-03-04 04:33:29','2016-03-04 04:33:29',1),(47,'st48414','44d52c064387c11ffe0ef9f85f4d8bfb','/dist/img/user6-128x128.jpg','Саидхучаев','Маъруфхуча','Мухторхучаевич','1996-08-26',0,'ш.Хуҷанд,к.Ватан 1 \"а\" ','+(992) 929999015',1,1,'2016-03-04 04:34:55','2016-03-04 04:34:55',1),(48,'st63099','e3dd040842f49a1be7a481aad8398430','/dist/img/user6-128x128.jpg','Эргашев','Баходурбек','Бахтиёрович','1997-06-26',0,'в.Суғд,ш.Хуҷанд,13-12-10 ','+(992) 929171213',1,1,'2016-03-04 04:36:08','2016-03-04 04:36:08',1),(49,'st91268','129d4cd9468a6f7e7ef966ea318226a0','/dist/img/user6-128x128.jpg','Мирзозода','Бобохон','Мирзоохун','1998-02-01',0,'в.Суғд,н.Кӯҳистони Мастчоҳ ','+(992)',6,1,'2016-03-04 04:37:28','2016-03-04 04:37:28',1),(50,'st26882','032292c3af013c8b399e29cc21cd11f5','/dist/img/user6-128x128.jpg','Саъдиев','Ноибшох','Шорохматович','1997-05-22',0,'в.Суғд,ш.Истаравшан,д.Ругунд ','+(992)',8,1,'2016-03-04 04:38:42','2016-03-04 04:38:42',1),(51,'st99706','45dea0ae8a5bc8053e8b2a319536bbae','/dist/img/user6-128x128.jpg','Воситов ','Сайдулло','.','1996-08-08',0,'в.Сугд,ш.Хучанд,19-1-38 ','+(992) 927086818',1,1,'2016-03-04 04:40:31','2016-03-04 04:40:31',1),(52,'st63704','6dfc12af766f1c6440b79d14409e7b32','/dist/img/user6-128x128.jpg','Шодибеков','Оятулло','.','1995-05-03',0,'в.Сугд,ш.Исфара,ч.Чорку,к.Сомониён ','+(992) 927073727',1,1,'2016-03-04 04:42:06','2016-03-04 04:42:06',1),(53,'st27546','a5a50f5e2cc29838515ca7ac7fde70fd','/dist/img/user6-128x128.jpg','Усмонова','Рухшонахон','Усуфшеховна','1998-02-24',1,'н.Б.Ғафуров,к. Шариф Қурбонов 127 ','+(992)',9,1,'2016-03-04 04:44:44','2016-03-04 04:44:44',1),(54,'st71384','39b5c3d6f1f576b5d56c71768353a7ac','/dist/img/user6-128x128.jpg','Уролова ','Юлдузхон','Усуфшеховна','1997-04-24',1,'в.Суғд,н.Ашт,д.Ҷигда ','+(992) 927830980',3,1,'2016-03-04 04:47:06','2016-03-04 04:47:06',1),(55,'st15547','6b9c61ece53e0e7ef1e528bafd55ced3','/dist/img/user6-128x128.jpg','Суюнова','Дилнавоз','Уткировна','1997-09-12',0,'н.Ғончӣ,к.Ҳазорчашма ','+(992) 927108114',5,1,'2016-03-04 04:48:13','2016-03-04 04:48:13',1),(56,'st59648','a90995105bb1570281ceaf14d1e534ab','/dist/img/user6-128x128.jpg','Ходихонова','Марямхон','.','1997-07-19',1,'в.Сугд,н.Гончи,ч.Мачнун,д.Хазорчашма ','+(992) 927683373',5,1,'2016-03-04 04:49:33','2016-03-04 04:49:33',1),(57,'st62275','1f94ae27bad8a3451f336197b200a3e4','/dist/img/user6-128x128.jpg','Файзуллоева','Фарзона','Фатхуллохоновна','1997-09-07',1,'12-5 Б-16 ','+(992) 929542883',1,1,'2016-03-04 04:51:17','2016-03-04 04:51:17',1),(58,'st62323','e19939e5955364eadcd420b637aa4b09','/dist/img/user6-128x128.jpg','Ганиева','Мадина','Аличоновна','1997-09-16',1,'34-35-27 ','+(992)',1,1,'2016-03-04 04:52:17','2016-03-04 04:52:17',1),(59,'st12538','62c3722509b155411ca173e6832c5f96','/dist/img/user6-128x128.jpg','Ахмедов','Далерхон','Сайдуллохонович','1997-07-23',0,'31-43-24 ','+(992) 927053666',1,1,'2016-03-04 04:53:20','2016-03-04 04:53:20',1),(60,'st45680','e4930d8114645f18b35011a1da6d6371','/dist/img/user6-128x128.jpg','Хайдаров','Сухроб','Абдукаримович','1997-05-07',0,'31-43-17 ','+(992) 927755502',1,1,'2016-03-04 23:45:55','2016-03-04 23:45:55',1),(61,'st72441','fe6813a55c6276184f5e3d4b4a498c31','/dist/img/user6-128x128.jpg','Азимов','Чахонгир','Чумабоевич','1997-01-31',0,'Бофанда 7,Хонаи 60 ','+(992) 928581148',1,1,'2016-03-04 23:46:55','2016-03-04 23:46:55',1),(62,'st57332','027552a07173c3de0f0610f005197057','/dist/img/user6-128x128.jpg','Махкамов','Назирчон','Наимович','1996-08-28',0,'н.Б.Ғафуров, ҷ.Ҳ.Усмонов ','+(992) 927223169',9,1,'2016-03-04 23:53:21','2016-03-04 23:53:21',1),(63,'st22530','8dd0329dcdb59060da7ba7a1e17b9d63','/dist/img/user6-128x128.jpg','Файзимамадов','Саидчон','Собирович','1997-01-30',0,'н.Б.Ғафуров,ҷ.Ёва,к.А.Мадаминов 4 ','+(992) 926097871',9,1,'2016-03-04 23:55:25','2016-03-04 23:55:25',1),(64,'st23816','ab87fc3b114b0afd51311ce9d962f127','/dist/img/user6-128x128.jpg','Негматов','Юсуфхон','Дилшодхонович','1997-07-25',0,'ш.Хуҷанд,к.Саид Носир 12 ','+(992) 926210060',1,1,'2016-03-04 23:56:34','2016-03-04 23:56:34',1),(65,'st93342','387d39794c7df76a46fcc32aa24a96e1','/dist/img/user6-128x128.jpg','Хамробоев','Алишер','Каримчонович','1997-09-04',0,'ш.Хуҷанд,19 мкр,к .Беш-Каппа 16 ','+(992) 927131735',1,1,'2016-03-04 23:57:54','2016-03-04 23:57:54',1),(66,'st97499','a706667280265f00a3cc94b4324f2aa0','/dist/img/user6-128x128.jpg','Мадазимов','Фархат','Шухратович','1997-08-24',0,'ш.Хуҷанд,31-11-14 ','+(992) 927697744',1,1,'2016-03-04 23:59:03','2016-03-04 23:59:03',1),(67,'st75843','144accc916740acc284eedf99c4ae445','/dist/img/user6-128x128.jpg','Мирмухамедов','Фирдавс','Музаффарович','1997-10-09',0,'ш.Хуҷанд,к.Киев 16 ','+(992) 928959550',1,1,'2016-03-05 00:00:25','2016-03-05 00:00:25',1),(68,'st49765','064aafd04157b75b07061c90fbb51521','/dist/img/user6-128x128.jpg','Чабборова','Мехрангез','Сайфуддиновна','1997-07-05',0,'н.Мастчоҳ,д.Оббурдон,к.Исмоилов ','+(992) 926000837',1,1,'2016-03-05 00:01:41','2016-03-05 00:01:41',1),(69,'st58591','6ef7431ba0750ac71473415b66d33701','/dist/img/user6-128x128.jpg','Абдурашидов','Бахтиёр','Соличонович','1998-01-02',0,'ш.Хуҷанд,13-4-25 ','+(992) 927730788',1,1,'2016-03-05 00:03:59','2016-03-05 00:03:59',1),(70,'st45571','dbbbdc0a04c448eedb5159259fd77dea','/dist/img/user6-128x128.jpg','Бобоев','Исмоилчон','Исломович','1997-11-12',0,'в.Суғд, ш.Исфара,д.Новгилем,к.Қодиров 296 ','+(992) 918221510',1,1,'2016-03-05 00:05:28','2016-03-05 00:05:28',1),(71,'st58888','812a44fecdc320533703a60168e21436','/dist/img/user6-128x128.jpg','Кодиров','Ислом','Илхомович','1997-11-30',0,'в.Суғд,н.Б.Ғафуров,д.Сойча 42/1 ','+(992) 926007429',9,1,'2016-03-05 00:06:42','2016-03-05 00:06:42',1),(72,'st56971','55f0c1c7a61a5d66d10e64f4f7c52f82','/dist/img/user6-128x128.jpg','Ёкубов','Толибчон','Икромович','1997-09-08',0,'в.Суғд,ш.Хуҷанд,к.Камнин 22 \"а\" ','+(992) 928738035',1,1,'2016-03-05 00:09:16','2016-03-05 00:09:16',1),(73,'st20347','e13a032ef048161c7977e74fe04f7149','/dist/img/user6-128x128.jpg','Турахонов','Аличон','Самадчонович','1997-10-09',0,'в.Суғд,н.Ашт,д.Ашт ','+(992)',1,1,'2016-03-05 00:10:51','2016-03-05 00:10:51',1),(74,'st70076','05363b39c803276954c2ded91492d277','/dist/img/user6-128x128.jpg','Сангинов','Нозимчон','.','1997-04-12',0,'в.Сугд,н.Б.Гафуров,д.Галамайдон ','+(992) 927564204',9,1,'2016-03-05 00:12:08','2016-03-05 00:12:08',1),(75,'st61768','42cdfca9c5dd49ded1694865b6d75ab1','/dist/img/user6-128x128.jpg','Умматов','Амончон','.','1997-01-02',0,'в.Сугд,ш.Хучанд,12-9-24 ','+(992) 927566911',1,1,'2016-03-05 00:13:37','2016-03-05 00:13:37',1),(76,'st12398','8a438cb2ea9f11f6a672a324d2b9bd6e','/dist/img/user6-128x128.jpg','Фазилова','Ширин','Зикьёевна','1997-02-05',1,'в.Сугд,ш.Чкаловск,к.Говорова 14 х 1 ','+(992)',10,1,'2016-03-05 00:14:47','2016-03-05 00:14:47',1),(77,'st64443','27855ff728dd4a5f6d537bca8a861d2c','/dist/img/user6-128x128.jpg','Ахмедов','Мухаммадчон','Валиевич','1997-07-28',0,'ш.Хуҷанд,31-22-28 ','+(992) 929683344',1,1,'2016-03-05 00:16:31','2016-03-05 00:16:31',1),(78,'st71363','54ddd6eb4022dd1e086f06b04ebf0090','/dist/img/user6-128x128.jpg','Авазов','Чумахон','Шоидмардонович','1997-09-07',0,'ш. Қайроққум,ш. Сирдарё кӯчаи Эргашев 1 ','+(992) 927279136',1,1,'2016-03-05 00:19:06','2016-03-05 00:19:06',1),(79,'st57925','5e7da6814a18e950856c8fa536be76ec','/dist/img/user6-128x128.jpg','Кодирова','Азиза','Абдукодировна','1997-09-28',1,'20-29-57 ','+(992)',1,1,'2016-03-05 00:21:12','2016-03-05 00:21:12',1),(80,'st25923','ba5d6f779ab56abb69f8ad7fa15a7090','/dist/img/user6-128x128.jpg','Маюсупов','Шахбоз','Баходурович','1997-10-22',0,'в.Суғд, д.Ашт ','+(992) 928338398',1,1,'2016-03-05 00:22:18','2016-03-05 00:22:18',1),(81,'st93404','e76c7bda018fe0bf437d554e23836927','/dist/img/user6-128x128.jpg','Осимов','Баховаддинхон','Искандарович','1997-11-06',0,'19-2-144 ','+(992) 928530056',1,1,'2016-03-05 00:24:29','2016-03-05 00:24:29',1),(82,'st88102','301733aee1284a419c0e5b8eef987fdd','/dist/img/user6-128x128.jpg','Исломов','Кароматулло','Файзуллоевич','1997-07-02',0,'н.Ғончи,д.Далёни боло ','+(992) 900450229',1,1,'2016-03-05 00:25:47','2016-03-05 00:25:47',1),(83,'st15563','c43cf452ccbcfaef9165595f7241b13f','/dist/img/user6-128x128.jpg','Рашидов','Далер','Хасанович','1997-08-12',0,'ш. Исфара, д.Нефтобод, к. Гулистон ','+(992) 989082732',1,1,'2016-03-05 00:26:56','2016-03-05 00:26:56',1),(84,'st56587','e1170c3824c82f47195c909a9ca150ce','/dist/img/user6-128x128.jpg','Исроилов','Носир','Абдукодирович','1997-04-17',0,'в.Суғд, ш.Исфара,к. Лутфи Саид ','+(992) 927083144',1,1,'2016-03-05 00:28:04','2016-03-05 00:28:04',1),(85,'st36524','df20605b06b462906dff6065bb379520','/dist/img/user6-128x128.jpg','Абдуллоев','Азамат','Маъруфович','1998-01-01',0,'ш.Панҷакент,д.Чорбоғ ','+(992) 927439114',1,1,'2016-03-05 00:29:59','2016-03-05 00:29:59',1),(86,'st83272','8fae0b89c6946ba49bbb20bba98b1b98','/dist/img/user6-128x128.jpg','Ахматов','Исломчон','Акмалович','1997-03-22',0,'ш.Панҷакент,д.Чорбоғ ','+(992) 926111963',1,1,'2016-03-05 00:30:56','2016-03-05 00:30:56',1),(87,'st78730','8e344ba215859470dd751e056fdd5ed2','/dist/img/user6-128x128.jpg','Хайдаров','Акмалчон','Акрамбоевич','1997-10-08',0,'ш.Истаравшан ','+(992) 927643571',1,1,'2016-03-05 00:32:06','2016-03-05 00:32:06',1),(88,'st12546','0e1544532b3207b6f71b79eff4e89152','/dist/img/user6-128x128.jpg','Охунов','Назирхон','Набихонович','1997-05-25',0,'ш.Хуҷанд,80 солагии Душанбе,м.8,ҳ.90 ','+(992) 927640000',1,1,'2016-03-05 00:32:58','2016-03-05 00:32:58',1),(89,'st11937','2a95430b42d3631f85bab98ea8b3f8f6','/dist/img/user6-128x128.jpg','Хочиматов','Умедчон','Гайратчонович','1998-08-05',0,'н.Б.Ғафуров,ҷ.Зарзамин,к.Шерматов 33 ','+(992) 927843394',9,1,'2016-03-05 00:34:01','2016-03-05 00:34:01',1),(90,'st45836','fd1513bd0039df1506b0d1686cc024b6','/dist/img/user6-128x128.jpg','Шоназаров','Шахзод','Носирович','1997-06-25',0,'н.Ашт,д. Ашт, к. Наврӯз ','+(992) 929470187',1,1,'2016-03-05 00:35:09','2016-03-05 00:35:09',1),(91,'st83719','34bab77ee32fceefe996681c67565a82','/dist/img/user6-128x128.jpg','Бобоев','Чамшед','Мамадалиевич','1997-03-01',0,'ш.Истаравшан,д.Чорбоғ ','+(992) 926199333',1,1,'2016-03-05 00:40:40','2016-03-05 00:40:40',1),(92,'st39068','af66243c7cb38ad6b4173a65a19637aa','/dist/img/user6-128x128.jpg','Камолиддинов','Дилшод','Зикриддинович','1997-04-27',0,'н.Ашт,д.Понғоз,м.Бурак ','+(992) 927335942',1,1,'2016-03-05 00:41:53','2016-03-05 00:41:53',1),(93,'st91338','46c5044efddf8c55e05cfbfade280dde','/dist/img/user6-128x128.jpg','Имомов','Мухаммад','Гайратович','1997-12-12',0,'н.Ашт,ҷ.Ашт ','+(992) 927435392',1,1,'2016-03-05 00:43:03','2016-03-05 00:43:03',1),(94,'st97698','1c2bbb31eb9d17256e8ce5f6d387cedb','/dist/img/user6-128x128.jpg','Мадумаров','Шохин','Шодиматович','1997-01-10',0,'в.Суғд,н.Ашт,д.Понғоз ','+(992) 927627117',1,1,'2016-03-05 00:45:53','2016-03-05 00:45:53',1),(95,'st24371','7209dc7d88e51e7efff2a8f074a93d7c','/dist/img/user6-128x128.jpg','Оятуллои','Иброхим','.','1997-11-23',0,'ш.Қайроққум,к.И.Сомонӣ,х.12,х.10 ','+(992) 92832711',1,1,'2016-03-05 00:49:05','2016-03-05 00:49:05',1),(96,'st78481','994574dffbcbd8adf826e8365a6aa065','/dist/img/user6-128x128.jpg','Расулов','Мухаммадчон','Хусейнович','1998-07-06',0,'в.Суғд,н.Спитамен,д.Янгобод ','+(992) 927688028',1,1,'2016-03-05 00:50:14','2016-03-05 00:50:14',1),(97,'st28404','783e7b2e596c337eeb6604a2db863a50','/dist/img/user6-128x128.jpg','Шофозилов ','Мухаммадмунир','Мухаммадрашодович','1997-10-26',0,'в.Суғд,н.Ғончӣ,д.Обшорон ','+(992) 926102557',1,1,'2016-03-05 00:51:23','2016-03-05 00:51:23',1),(98,'st87722','527a57817c98c69ce1ec5ca0584b7b79','/dist/img/user6-128x128.jpg','Юнусов','Акбарчон','Шухратчонович','1997-07-10',0,'в.Суғд,ш.Хуҷанд,ҷ.Ҳ.Усмон  ','+(992) 938174000',1,1,'2016-03-05 00:52:24','2016-03-05 00:52:24',1),(99,'st40370','b292615ea0f40c9db5ac07968d1519a4','/dist/img/user6-128x128.jpg','Бойпочоев','Наимчон','Насимчонович','1997-05-04',0,'н.Б.Ғафуров ','+(992) 929979929',9,1,'2016-03-05 00:54:21','2016-03-05 00:54:21',1),(100,'st33635','abf182b81c611d9ded00d80818117fc8','/dist/img/user6-128x128.jpg','Каримов','Сардор','.','1996-07-01',0,'в.Сугд,н.Ашт,д.Кули-хоча ','+(992) 927576737',1,1,'2016-03-05 00:55:27','2016-03-05 00:55:27',1),(101,'st77426','6ad03a415ebe92b77bc49f71b62b1095','/dist/img/user6-128x128.jpg','Раупов','Шероз','Шарифчонович','1996-12-26',0,'в.Сугд,н.Б.Гафуров,ч.Гозиён,к.Дусти 54 ','+(992) 939120205',9,1,'2016-03-05 01:31:09','2016-03-05 01:31:09',1),(102,'st65301','123bfe233789c21c5e5754cd1821da66','/dist/img/user6-128x128.jpg','Бегматов','Парвиз','.','1997-11-14',0,'в.Сугд,ш Кайроккум,ш-ки Адрасмон,к. Хувойдуллоев 7 ','+(992) 929236116',1,1,'2016-03-05 01:32:14','2016-03-05 01:32:14',1),(103,'st49341','5f040d6787689aaf0d3a3aec6feae6f9','/dist/img/user6-128x128.jpg','Турсунов','Камолчон','Комилчонович','1997-08-01',0,'н.Б.Ғафуров,ҷ. Ёва,к. Ёва 87 ','+(992) 927706481',9,1,'2016-03-05 01:33:33','2016-03-05 01:33:33',1),(104,'st26109','d9d0c78a8a46932d77afde8d93a08aea','/dist/img/user6-128x128.jpg','Бобоев','Маманазар','.','1997-09-12',0,'в.Сугд,н.Спитамен,к.А.Нуров 32 ','+(992) 929209674',1,1,'2016-03-05 01:48:25','2016-03-05 01:48:25',1),(105,'st84045','6d02589929b300d862fa614ce8e2e2ac','/dist/img/user6-128x128.jpg','Мамадкулов','Дониёр','.','1997-10-29',0,'в.Сугд,н.Ашт,д.Саро,ч.Понгоз ','+(992) 927433065',1,1,'2016-03-05 01:49:27','2016-03-05 01:49:27',1),(106,'st73569','83079bd2197c5f75d3d2253a1bd465c1','/dist/img/user6-128x128.jpg','Салимов','Хасаншариф','Файзуллоевич','1997-02-01',0,'в.Сугд,н.Б.Гафуров,ч.Ёва,к.Саодат 64 ','+(992) 928883352',1,1,'2016-03-05 01:50:30','2016-03-05 01:50:30',1),(107,'st40761','ea5a4bc898fa6d95bdf80da236bdfb0f','/dist/img/user6-128x128.jpg','Лутфуллоев','Фируз','Файзуллоевич','1997-06-04',0,'34-98-87 ','+(992) 929727772',1,1,'2016-03-05 01:53:03','2016-03-05 01:53:03',1),(108,'st22589','1592ce5b295c896375da13861dd0be54','/dist/img/user6-128x128.jpg','Шосаидов','Саидумар','Саидбурхонович','1997-08-01',0,'Ш.Чкаловск,кучаи Кутузова 4 ','+(992) 927641093',10,1,'2016-03-05 01:54:12','2016-03-05 01:54:12',1),(109,'st99377','336a2c331bd99e0856cb416a9853d522','/dist/img/user6-128x128.jpg','Курбонов','Амирчон','.','1997-08-12',0,'н.Б.Ғафуров,ҷ.Ёва,к.Баҳор 40 ','+(992) 926166654',9,1,'2016-03-05 01:55:13','2016-03-05 01:55:13',1),(110,'st46540','c88a96eac968d885339aed76dad9296b','/dist/img/user6-128x128.jpg','Чамилов','Абдуваххоб','Абдумаликович','1997-02-14',0,'н.Б.Ғафуров,ҷ.Д.Ғозиён ','+(992)',9,1,'2016-03-05 01:56:13','2016-03-05 01:56:13',1),(111,'st70213','9b901278c842327cb0f8d44d3b0a21f5','/dist/img/user6-128x128.jpg','Бобочонова','Азиза','Амирчоновна','1994-07-01',1,'ш.Хуҷанд,12-11-134 ','+(992) 929252523',1,1,'2016-03-05 01:57:07','2016-03-05 01:57:07',1),(112,'st31729','201716833c9206a5017bfe3933691081','/dist/img/user6-128x128.jpg','Ахмедов','Шахром','Зокирович','1997-09-08',0,'н.Б.Ғафуров ','+(992) 927592054',9,1,'2016-03-05 01:58:01','2016-03-05 01:58:01',1),(113,'st38286','bb49f002845924e3cfa11aab4e266138','/dist/img/user6-128x128.jpg','Рахматов','Чумабой','Сафарович','1998-05-09',0,'н.Айнӣ,д.300 сун ','+(992) 937226936',1,1,'2016-03-05 01:59:18','2016-03-05 01:59:18',1),(114,'st54090','91af6844d8d1f06ef18ce029d70308ad','/dist/img/user6-128x128.jpg','Турсунов','Амирчон','Олимчонович','1997-03-20',0,'н.Б.Ғафуров,ҷ.Ёва,к.Шарқ 55 ','+(992) 929571504',9,1,'2016-03-05 02:00:21','2016-03-05 02:00:21',1),(115,'st93348','46d7373c33376b5e2251eb153ef52f0f','/dist/img/user6-128x128.jpg','Кодиров','Фарходчон','Равшанчонович','1995-06-20',0,'в.Суғд,н.Б.Ғафуров,ҷ.Унҷӣ,к.Ахмедов 109 ','+(992) 927591881',9,1,'2016-03-05 02:03:02','2016-03-05 02:03:02',1),(116,'st80910','cf6d3bf79a7cfc406dc01d59e4b8f705','/dist/img/user6-128x128.jpg','Каримов','Мубинчон','Муъминчонович','1997-02-13',0,'в.Суғд,н.Б.Ғафуров,к.Мир 30 ','+(992)',9,1,'2016-03-05 02:05:22','2016-03-05 02:05:22',1),(117,'st26928','53da327f30a51f59c35c80acc9856297','/dist/img/user6-128x128.jpg','Хобилов','Пайрав','Хакимович','1993-09-22',0,'в.Суғд,ш.Конибодом,д.Қарачиқум ','+(992) 928296565',1,1,'2016-03-05 02:06:31','2016-03-05 02:06:31',1),(118,'st70618','cd442b095ee7d9d72a9f882cf01b4412','/dist/img/user6-128x128.jpg','Усмонов','Дилшодчон','Абдумачидович','1997-03-10',0,'в.Суғд,н.Б.Ғафуров,к.С.Айнӣ 141 ','+(992) 918231346',9,1,'2016-03-05 02:07:43','2016-03-05 02:07:43',1),(119,'st83015','cc208a28224eb61d675638b5327a009e','/dist/img/user6-128x128.jpg','Солиев','Мухаммадчон','Муйдинчонович','1997-09-21',0,'в.Суғд,н.Б.Ғафуров,д.Пулчуқур ','+(992) 928362394',9,1,'2016-03-05 02:08:46','2016-03-05 02:08:46',1),(120,'st97158','29fac9f9ec0046ebd0547c352ce6d759','/dist/img/user6-128x128.jpg','Азимов','Чахонгир','Ортикбоевич','2016-03-05',0,'н.Ғончӣ,д.Нуробод ','+(992) 929251130',1,1,'2016-03-05 02:09:59','2016-03-05 02:09:59',1),(121,'st48377','432aa465367aa6098ae4d2cd8f7772d4','/dist/img/user6-128x128.jpg','Каландаров','Бегали','Шодмончонович','1997-01-01',0,'в.Суғд,ш.Панҷакент,д.Хунгарон ','+(992) 928475858',1,1,'2016-03-05 02:10:56','2016-03-05 02:10:56',1),(122,'st80948','440bf50d00846a2309c73affaf275abe','/dist/img/user6-128x128.jpg','Усмонов','Дилшодчон','Абдумачидович','1997-03-10',0,'в.Суғд,н.Б.Ғафуров,к.С.Айнӣ 141 ','+(992) 918231346',9,1,'2016-03-05 02:12:57','2016-03-05 02:12:57',1),(123,'st89045','af80397580cab6cedc1502668df74d49','/dist/img/user6-128x128.jpg','Солиев','Мухаммадчон','Муйдинчонович','2016-03-05',0,'в.Суғд,н.Б.Ғафуров,д.Пулчуқур ','+(992) 928362394',9,1,'2016-03-05 02:14:36','2016-03-05 02:14:36',1),(124,'st61024','25d2544b027df18b8158e41113f7270a','/dist/img/user6-128x128.jpg','Маллаев','Амиркул','.','1998-01-07',0,'н.Айнӣ,д.Зиндакон ','+(992) 927348300',1,1,'2016-03-05 02:19:03','2016-03-05 02:19:03',1),(125,'st52942','36f3429ef4127e9dc85d1b7573246b2b','/dist/img/user6-128x128.jpg','Мафтуна ','Рахмони','.','1997-09-07',1,'кӯчаи А. Расулов 45','+(992) 927101943',1,1,'2016-03-29 15:30:03','2016-03-29 15:30:03',1),(126,'st96704','496d3c8cf7c80c53621e6c7e960a847e','/dist/img/user6-128x128.jpg','Назмиддинов ','Довар','Ибодуллоевич','1997-12-01',0,'ҷ. Понғоз ','+(992) 928312109',3,1,'2016-03-29 15:30:52','2016-03-29 15:30:52',1),(127,'st22153','14262e4dc8e17b397d1df187c0d4181c','/dist/img/user6-128x128.jpg','Аманбоев ','Нозимчон','Шавкатчонович','1997-04-24',0,'кӯч Ҳамза','+(992) 934700029',4,1,'2016-03-29 15:32:21','2016-03-29 15:32:21',1),(128,'st62245','9f546fe87675bd28b7e798dfaf1dd730','/dist/img/user6-128x128.jpg','Махкамов','Халимхуча','Хасанович','1998-04-18',0,'кӯч Мир 1/25 а, хонаи 51','+(992)',1,1,'2016-03-29 15:33:40','2016-03-29 15:33:40',1),(129,'st26476','e59bd594ec812257083239ac93383238','/dist/img/user6-128x128.jpg','Раҳмонов','Абдувохид','Абдувосидович','1997-08-17',0,'кӯч Рӯдаки 7.','+(992) 904446300',9,1,'2016-03-29 15:34:45','2016-03-29 15:34:45',1),(130,'st97528','e12c4001d16da0fe5ad46eeb30fef233','/dist/img/user6-128x128.jpg','Ҳакимов','Абдуманон','Абдуробидович','1997-11-17',0,'деҳаи Нуробод','+(992)',5,1,'2016-03-29 15:35:45','2016-03-29 15:35:45',1),(131,'st95091','d553abc1ab8e3c45987df294194eba72','/dist/img/user6-128x128.jpg','Ғафурова','Малика','Фарҳодҷоновна','1997-05-12',1,'кӯч Р. Набиев 131 а','+(992) 929006075',1,1,'2016-03-29 15:37:58','2016-03-29 15:37:58',1),(132,'st53137','ac8a41523448e719d3d17adac6494829','/dist/img/user6-128x128.jpg','Боймурод','Шаҳромӣ','.','1997-09-17',0,'8 мкр дом 67 кв. 57','+(992) 933100909',1,1,'2016-03-29 15:39:20','2016-03-29 15:39:20',1),(133,'st51866','71f875aaa114f48c74ee8f6439a492ea','/dist/img/user6-128x128.jpg','Акпаров','Акмалхӯҷа','Баҳромҷонович','1998-04-16',0,'8 мкр дом 50 кв. 18','+(992) 927058144',1,1,'2016-03-29 15:40:03','2016-03-29 15:40:03',1),(134,'st80598','5366746962001923cdeb52b7fef34287','/dist/img/user6-128x128.jpg','Сидиқов','Шарифҷон','Сидиқҷонович','1997-01-24',0,'ҷ. Ҳайдар Усмон, кӯч М. Қосимов 44','+(992) 928144462',9,1,'2016-03-29 15:41:02','2016-03-29 15:41:02',1),(135,'st32854','6c4775d952160ffba182ff3d60344937','/dist/img/user6-128x128.jpg','Ҷумаев','Нодирҷон','Қодирович','1997-11-12',0,'ҷ. Ёва кӯч. А. Саркор 2','+(992) 928417863',9,1,'2016-03-29 15:41:47','2016-03-29 15:41:47',1),(136,'st69869','019a665ba03d53ea4424289bc22b49e2','/dist/img/user6-128x128.jpg','Фаттуллоев','Муҳаммад','Юсуфхоҷаевич','1997-07-15',0,'8 мкр. дом 90 кв. 55','+(992) 927774804',1,1,'2016-03-29 15:43:26','2016-03-29 15:43:26',1),(137,'st13798','f4e15ec82f48fa3688f59b4bbdcaea3c','/dist/img/user6-128x128.jpg','Ашӯров','Амир','Нуруллоҷонович','1997-04-16',0,'кӯч Ҳалимбабаев','+(992)',1,1,'2016-03-29 15:44:19','2016-03-29 15:44:19',1),(138,'st76851','a8f5ea2e05edb3eaccc517410b43f4d2','/dist/img/user6-128x128.jpg','Шокиров','Мунирҷон','Муқимҷонович','1997-06-02',0,'ҷ. Хистеварз, кӯч Ҳамза','+(992) 927733008',9,1,'2016-03-29 15:45:23','2016-03-29 15:45:23',1),(139,'st57628','4d96a2e995cced2bb49673c64f69112c','/dist/img/user6-128x128.jpg','Абдураҳимов','Шоҳзод','Иӯлдошбоевич','1997-08-06',0,'ҷ. Сарбанд','+(992)',4,1,'2016-03-29 15:46:09','2016-03-29 15:46:09',1),(140,'st30879','637bd3679b72114d10e96096d4bd45b9','/dist/img/user6-128x128.jpg','Тошматов','Парвиз','Рустамҷонович','1997-06-16',0,'12 мкр. 11 дом 132 квр.','+(992) 928225353',1,1,'2016-03-29 15:46:57','2016-03-29 15:46:57',1),(141,'st21961','b05907bf606b472c27e82211e5bdd8c6','/dist/img/user6-128x128.jpg','Ҷӯраев','Хусрав','Фаррухович','1997-06-18',0,'12 мкр. дом 11. квр 131.','+(992) 927444404',1,1,'2016-03-29 15:47:52','2016-03-29 15:47:52',1),(142,'st72226','17aab56b215fc342d41f1f0521903583','/dist/img/user6-128x128.jpg','Шодихӯҷаев','Абдуҷалил','Абдуҷабборович','1997-12-20',0,'деҳ. Кулангир, кӯч Бародарӣ 9','+(992) 928977425',9,1,'2016-03-29 15:48:56','2016-03-29 15:48:56',1),(143,'st76601','87901c8fb67000ba3b4d01122585124c','/dist/img/user6-128x128.jpg','Икромова','Сабринахон','Абдувалиевна','1997-11-02',1,'куч. Бахтиёр Ёқубов 1/5','+(992) 927150025',1,1,'2016-03-29 15:49:59','2016-03-29 15:49:59',1),(144,'st79603','cc581980f3467bd7073d10037d8153a7','/dist/img/user6-128x128.jpg','Раҳматов','Фариддун','Фарҳодович','1997-12-24',0,'13 мкр, дом 9, 17 квр','+(992) 927701320',1,1,'2016-03-29 15:51:02','2016-03-29 15:51:02',1),(145,'st51042','de4d985d30337cbead4480e3d4c725d0','/dist/img/user6-128x128.jpg','Каримов','Далерҷон','Абдураҳимович','1997-10-10',0,'ш. Хуҷанд','+(992) 927780033',1,1,'2016-03-29 15:54:11','2016-03-29 15:54:11',1),(146,'st97659','0fbca1ec6f8255b34dc9ac7230052a9d','/dist/img/user6-128x128.jpg','Бахтиёри','Сӯҳроб','.','1997-03-29',0,'кӯч. Ленин хонаи 7 ҳуҷраи 14','+(992) 927043363',1,1,'2016-03-29 15:55:06','2016-03-29 15:55:06',1),(147,'st41961','e8407529c299eae8a7039e4794c46bca','/dist/img/user6-128x128.jpg','Абдуғаниев','Сулаймонхӯҷа','Иброҳимхӯҷаевич','1997-02-19',0,'ҷ. Ҳайдар Усмон, кӯч А. Шуқӯҳи 61','+(992) 927855656',1,1,'2016-03-29 15:56:27','2016-03-29 15:56:27',1),(148,'st44480','f67706734bc711469fc15bcacf5ec075','/dist/img/user6-128x128.jpg','Дадобоев','Ғуфронҷон','Эҳсонҷонович','1997-04-26',0,'д. Шоҳон кӯч Исмоили Сомонӣ','+(992) 928439913',5,1,'2016-03-29 15:57:46','2016-03-29 15:57:46',1),(149,'st30728','c9c9998448549b12010031f8f7154cd3','/dist/img/user6-128x128.jpg','Қӯшоқов','Фаррух','Аҳадҷонович','1996-02-17',0,'ш. Конибодом, д Ниёзбек ','+(992) 928063503',1,1,'2016-03-29 15:59:21','2016-03-29 15:59:21',1),(150,'st49089','6a512c5d417789a33cb7965b0c0d6388','/dist/img/user6-128x128.jpg','Носирҷон','Ӯғли','Нодиржон','1996-02-06',0,'ш. Исфара ҷ. Навгилем','+(992) 921050505',1,1,'2016-03-29 16:00:07','2016-03-29 16:00:07',1),(151,'st40896','2a80df93e415dc4dbfacb44cdeec5112','/dist/img/user6-128x128.jpg','Олимов','Юсуфҷон','Рустамович','1996-02-01',0,'ҷ. Ёва кӯч. Т. Бойматов','+(992) 929278199',1,1,'2016-03-29 16:01:23','2016-03-29 16:01:23',1),(152,'st68947','ff913177376fa9e6bce2680624b6d0c3','/dist/img/user6-128x128.jpg','Холматов','Қодирҷон','Бахтиёрович','1997-10-02',0,'ш. Конибодом, д. Ниёзбек, кӯч Муқими','+(992) 928888451',1,1,'2016-03-29 16:02:28','2016-03-29 16:02:28',1),(153,'st25707','1d347ace11eb63b5d08151e0f80a73f8','/dist/img/user6-128x128.jpg','Солиев','Наврӯз','Ёқубович','1997-03-21',0,'д. Кулихоҷа','+(992) 929916781',3,1,'2016-03-29 16:03:18','2016-03-29 16:03:18',1),(154,'st47452','a538c909968f4dd6804793825e5c22e4','/dist/img/user6-128x128.jpg','Мирхӯҷаев','Абдуллохӯҷа','Дадохӯҷаевич','1997-10-20',0,'12 мкр, дом 37, квр 18','+(992) 927767644',1,1,'2016-03-29 16:04:40','2016-03-29 16:04:40',1),(155,'st25592','048f3491afa29b0d7b9db2909728b5b5','/dist/img/user6-128x128.jpg','Очилов','Алишер','Маъруфҷонович','1997-08-23',0,'ҷ. Хистеварз, кӯч Челон 58','+(992) 926220078',1,1,'2016-03-29 16:05:20','2016-03-29 16:05:20',1),(156,'st71441','21187f309d16b047b2f9fbecb7be7ca7','/dist/img/user6-128x128.jpg','Бобошехов','Акмалшех','Бобошехович','1998-01-05',0,'ҷ. Унҷи кӯч. М. Осими 22/23','+(992) 918525095',9,1,'2016-03-29 16:06:24','2016-03-29 16:06:24',1),(157,'st55667','e895e6846e4aa81bce190396d823acd5','/dist/img/user6-128x128.jpg','Раҷабов','Ҷамшед','Ботурович','1996-07-11',0,'ҷ. Ёва кӯч. А. К. 13','+(992) 929688803',9,1,'2016-03-29 16:07:26','2016-03-29 16:07:26',1),(158,'st78686','9f5301e6066a7979d09e1d857959a3cb','/dist/img/user6-128x128.jpg','Одилов','Назирҷон','Набиҷонович','1996-07-30',0,'кӯч. Навбаҳор хонаи 35, ҳуҷ. 65','+(992) 928350049',1,1,'2016-03-29 16:08:14','2016-03-29 16:08:14',1),(159,'st83237','455a92a749bd991271debf6995fb0f8d','/dist/img/user6-128x128.jpg','Раҳмонов','Абдумалик','Кобилчонович','1997-11-07',0,'Б.Ғафуров,ч.Унчи,к.Ӯрунхочаев 136','+(992) 918892474',9,1,'2016-03-29 16:11:40','2016-03-29 16:11:40',1),(160,'st15399','e5136a2ec711ec8bbd32792781b97123','/dist/img/user6-128x128.jpg','Азимов','Забихуллох','Исроилчонович','1997-04-23',0,'ш. Исфара,д.Баланд,к.Турсунзода 12','+(992) 927442247',1,1,'2016-03-29 16:14:20','2016-03-29 16:14:20',1),(161,'st95295','462d8bf2397fa3eea9aebaf022f96fcf','/dist/img/user6-128x128.jpg','Кузибоева ','Гулчахон','Шавкатовна','1997-10-08',0,'ш. Конибодом, д Мирзоалиева','+(992) 928026333',1,1,'2016-03-29 16:16:25','2016-03-29 16:16:25',1),(162,'st83729','c1f4c3931d7a08f87b21c980f426e4f3','/dist/img/user6-128x128.jpg','Умаров','Дилшодчон','Обидчонович','1997-08-30',0,'ш.Хучанд, к.Хучанд','+(992) 927709-3804',1,1,'2016-03-29 16:17:58','2016-03-29 16:17:58',1),(163,'st76019','e89aad6252aefbb47b67d677d5c79069','/dist/img/user6-128x128.jpg','Баходурхӯчаев','Умарчон','Пӯлотчонович','2016-03-29',0,'ш. Хуҷанд,к.Зариф ва Солех Рачабовхо','+(992) 927709119',1,1,'2016-03-29 16:21:50','2016-03-29 16:21:50',1),(164,'st89260','3313d7edfb165e4a47d57e5bcb2a8c44','/dist/img/user6-128x128.jpg','Негматов','Аминчон','Бобочонович','1997-05-08',0,'н.Б.Гафуров, ч.Унчи,к. 1 май','+(992) 918210621',1,1,'2016-03-29 16:23:57','2016-03-29 16:23:57',1),(165,'st11123','6702171155dad8f9dd41b09835946e84','/dist/img/user6-128x128.jpg','Носиров','Фирдавс','Алишерович','1997-05-19',0,'кӯч. М. Танбӯри 60','+(992) 929300751',1,1,'2016-04-01 09:29:07','2016-04-01 09:29:07',1),(166,'st97445','c4a2ea42bc2f53a8f25eb7b191dd3378','/dist/img/user6-128x128.jpg','Раҳматов','Иқболҷон','Зокирович','1996-05-20',0,'ҷ. Овчи Қалча, кӯч Дӯсти 30','+(992) 988758247',9,1,'2016-04-01 09:30:02','2016-04-01 09:30:02',1),(167,'st60798','2d821f19764b152a0ca33d594bed4b3a','/dist/img/user6-128x128.jpg','Ваҳобов','Шукрулло','Ихтиёрович','1997-11-05',0,'деҳаи Хоҷаҳо','+(992) 907482554',5,1,'2016-04-01 09:31:38','2016-04-01 09:31:38',1),(168,'st36048','4f2f5c3098f4db2e6c37623ca6131d58','/dist/img/user6-128x128.jpg','Шукуров','Рустам','Абдусабурович','1996-01-15',0,'ш. Исфара, кӯч Шарипов Д. 50','+(992) 928740417',1,1,'2016-04-01 09:32:57','2016-04-01 09:32:57',1),(169,'st31327','81d92f978ec5bee0f09a5d8df8b5f549','/dist/img/user6-128x128.jpg','Юсупов','Назирчон','Нозимчонович','1997-06-08',0,'кӯч С. Шариф 7','+(992)',9,1,'2016-04-01 09:33:33','2016-04-01 09:33:33',1),(170,'st10291','4247314605b72402cfd6248a5f477116','/dist/img/user6-128x128.jpg','Усмонов','Абдураҳим','Валиевич','1997-01-28',0,'ҷ. Хистеварз, кӯч М. Шокиров 13','+(992) 929172548',9,1,'2016-04-01 09:34:21','2016-04-01 09:34:21',1),(171,'st39078','fa88f179ccdb4a634be22b3479738dd5','/dist/img/user6-128x128.jpg','Абдуллоев','Муҳидинҷон','Муҳаммадҷонович','1997-09-05',0,'ҷ. Д. Холматов, кӯч Дача Водник','+(992) 928641406',9,1,'2016-04-01 09:35:18','2016-04-01 09:35:18',1),(172,'st14196','327dc1c9f1b7cc3047c5ef9056bc9c9c','/dist/img/user6-128x128.jpg','Мамадҷонов','Дилшодҷон','Давронҷонович','1998-02-12',0,'кӯч. Мирсаид Умаров 71','+(992) 927718285',1,1,'2016-04-01 09:36:07','2016-04-01 09:36:07',1),(173,'st62605','f2ff757f44bc21eb0bacf399b88a23e7','/dist/img/user6-128x128.jpg','Қодиров','Ғайратҷон','Файзуллоевич','1995-04-15',0,'ш. Исфара, кӯч Алишер Навои 62','+(992)',1,1,'2016-04-01 09:36:54','2016-04-01 09:36:54',1),(174,'st50226','f90965f63457399b17cabf372578cbc5','/dist/img/user6-128x128.jpg','Мамуров','Отабек','Каримбердиевич','1997-04-21',0,'д. Пунакандоз','+(992) 918977499',2,1,'2016-04-01 09:41:25','2016-04-01 09:41:25',1),(175,'st10151','658a11381478cb4870a2d59b4b85c51d','/dist/img/user6-128x128.jpg','Саидматов','Махсумҷон','Махсудҷонович','1998-04-21',0,'.','+(992) 927286018',8,1,'2016-04-01 09:43:13','2016-04-01 09:43:13',1),(176,'st16778','43c25ae5925d83b12740a26a6a6eeeee','/dist/img/user6-128x128.jpg','Дадобоев','Шӯҳрат','Икромҷонович','1996-12-29',0,'Д. Чорбоғ','+(992) 927198824',8,1,'2016-04-01 09:45:16','2016-04-01 09:45:16',1),(177,'st57145','06d119c34f04d237a969937f2cc1d2b4','/dist/img/user6-128x128.jpg','Раҳимов','Абудаҷалил','Абдурасулович','1996-10-17',0,'кӯч. Ёқӯбов 1/4 хонаи 9','+(992) 927061716',9,1,'2016-04-01 09:46:34','2016-04-01 09:46:34',1),(178,'st45689','ee39c534152531732520dfbab936f205','/dist/img/user6-128x128.jpg','Зоитов','Музаффарҷон','Зокирҷонович','1996-10-15',0,'8 мкр, доми 34, квр 32','+(992) 928440403',1,1,'2016-04-01 09:48:05','2016-04-01 09:48:05',1),(179,'st83704','57227e353ae18cdcb0f7b1548fa7def4','/dist/img/user6-128x128.jpg','Зарипов ','Фаррухҷон','Давронҷонович','1996-09-12',0,'8 мкр, дом 55, квр 54','+(992)',1,1,'2016-04-01 09:49:56','2016-04-01 09:49:56',1),(180,'st29731','58a6fcd66b30694bd3cd37c09639c660','/dist/img/user6-128x128.jpg','Раҳмонбердиев','Иномҷон','Абдулатипович','1996-10-24',0,'ҷ. Ёва, кӯч Н. Муродов 67','+(992)',9,1,'2016-04-01 09:51:43','2016-04-01 09:51:43',1),(181,'st12134','a1164cd7200c9dd42a5f9a87ca7cf9a4','/dist/img/user6-128x128.jpg','Алиматов','Алимамад','Анварҷонович','1996-08-22',0,'А. Абдунабиев 2','+(992) 928430204',9,1,'2016-04-01 10:02:14','2016-04-01 10:02:14',1),(182,'st50116','7108081daa4331c331187fa37afbbee9','/dist/img/user6-128x128.jpg','Юсуфчонов','Меъроч','Бахтиёрович','1996-08-22',0,'ш.Хучанд, к.Чукурак 1/24','+(992) 927242244',1,1,'2016-04-01 12:59:29','2016-04-01 12:59:29',1),(183,'st54425','276761ad254abe33465a224aa0187788','/dist/img/user6-128x128.jpg','Расулов','Абдумалик','Абдулакимович','1994-12-12',0,'ш.Чкаловск, Новый квартал 12/17','+(992) 927494948',10,1,'2016-04-01 13:04:17','2016-04-01 13:04:17',1),(184,'st83992','0c6f58aa9a2002021cefe61f6e4fe6d4','/dist/img/user6-128x128.jpg','Курбонов','Темур','Холмуродович','1996-05-04',0,'ш.Панчакент, к.Чорбог','+(992) 927469876',1,1,'2016-04-01 13:11:52','2016-04-01 13:11:52',1),(185,'st59759','9f6b9cae26c690176c6700581e0eaa56','/dist/img/user6-128x128.jpg','Мачитов','Рухиллохуча','Саидхучаевич','1996-06-11',0,'ш. Исфара ҷ. Навгилем 129','+(992)',1,1,'2016-04-01 13:18:17','2016-04-01 13:18:17',1),(186,'st71946','f15cad6901a07647d3473f7232e71eda','/dist/img/user6-128x128.jpg','Одинаев','Шарифхон','Абдучабборович','1996-02-17',0,'ш.Хучанд,3-11-8','+(992) 927543088',1,1,'2016-04-01 13:20:14','2016-04-01 13:20:14',1),(187,'st32002','200b6d6594fae55bcc8d49ea4c63a0c6','/dist/img/user6-128x128.jpg','Юнусов','Ғолибчон','Анварович','1993-05-28',0,'н.Б.Гафуров, ч.Ёва,Пахтакор 8а/45','+(992) 928846767',9,1,'2016-04-01 13:23:05','2016-04-01 13:23:05',1),(188,'st75500','5cedc410910a8875f0c87a3011a64c2f','/dist/img/user6-128x128.jpg','Эшматов','Азизчон','Файзуллоевич','1997-01-11',0,'н.Б.Гафуров, ч.Хистеварз, к. А.Каюмов 4','+(992) 928885362',9,1,'2016-04-01 13:25:47','2016-04-01 13:25:47',1),(189,'st17209','7fe696413a6a3de7444f4e162c7d95b6','/dist/img/user6-128x128.jpg','Ҳодиев','Камолиддин','Мирсабурович','1992-03-20',0,'н.Б.Гафуров, ч.Овчи-Қалъача,к. Искандаров 32','+(992)',9,1,'2016-04-01 13:34:25','2016-04-01 13:34:25',1),(190,'st74616','102b3ba719421fa24bb7efcf8cfad6ce','/dist/img/user6-128x128.jpg','Кобилов','Ҷаҳонгир','Ӯктамович','1995-01-27',0,'ш.Чкаловск, к.Палос ,к.Новая 20','+(992) 929877979',10,1,'2016-04-01 13:42:06','2016-04-01 13:42:06',1),(191,'st81122','313cc2dcaa6d548834e74ce99b18f056','/dist/img/user6-128x128.jpg','Султонов','Комил','Исмоилович','1995-10-10',0,'ш.Хучанд, к.Истиклол 56','+(992) 927125610',1,1,'2016-04-01 13:43:42','2016-04-01 13:43:42',1),(192,'st35191','a444236e88deee1497262c1d65cff830','/dist/img/user6-128x128.jpg','Очилов','Парвиз','Баходурович','1996-10-14',0,'ш.Истаравшан,д.Вогот','+(992) 928578457',1,1,'2016-04-01 13:47:17','2016-04-01 13:47:17',1),(193,'tr12345','2930828bd925a6b5123611bb1a1cbdce','/images/user.jpg','Усмонова','М','.','2016-04-29',1,'маълум не','+(992)',1,2,'2016-04-08 08:17:16','2016-04-08 08:17:16',1),(194,'tr12346','d19f6a9ac8ed940b78b5830e98df8b2e','/images/user.jpg','Шокирова','З.','.','2016-04-21',1,'маълум не','+(992)',1,2,'2016-04-08 08:28:45','2016-04-08 08:30:07',1),(195,'tr12347','57102f37afbd95ed082c04765f9f4006','/images/user.jpg','Ашӯров','С.','.','2016-04-12',0,'маълум не','+(992)',1,2,'2016-04-08 08:29:36','2016-04-08 08:29:36',1),(196,'tr12348','5016cb6c700f5292379d908386eac18d','/images/user.jpg','Туробова','Н.','А.','2016-04-05',1,'маълум не','+(992)',1,2,'2016-04-08 08:30:52','2016-04-08 08:30:52',1),(197,'tr12349','f64fb0613c6b306409c15769e705237d','/images/user.jpg','Охунзода','О.','А.','2016-03-28',0,'маълум не','+(992)',1,2,'2016-04-08 08:31:30','2016-04-08 08:31:30',1),(198,'tr12340','15a1c0fce1b6e68cb79cd23ddbc34929','/images/user.jpg','Шарипова','М.','А.','2016-04-25',1,'маълум не','+(992)',1,2,'2016-04-08 08:35:17','2016-04-08 08:35:17',1),(199,'tr12351','dc03c8d4d21fd3ac60f6b4a1538ae9f0','/images/user.jpg','Юнусов','А','.','2016-04-05',1,'маълум не','+(992)',1,2,'2016-04-08 08:36:34','2016-04-08 08:36:34',1),(200,'tr12352','c6c1a209538127be6d011a54179dfe2c','/images/user.jpg','Юсупов','П.','.','2016-04-12',0,'маълум не','+(992)',1,2,'2016-04-08 08:37:34','2016-04-08 08:37:34',1);

/*Table structure for table `user_access` */

DROP TABLE IF EXISTS `user_access`;

CREATE TABLE `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `user_access` */

insert  into `user_access`(`id`,`access`,`status`) values (1,'Донишҷӯ',1),(2,'Муаллим(а)',1),(3,'Назорат',1),(4,'Қайди донишҷӯён',1),(5,'Директор',1),(6,'Зам.дирек',1),(7,'Сармутахассис',1),(8,'Admin',1);

/*Table structure for table `user_menu` */

DROP TABLE IF EXISTS `user_menu`;

CREATE TABLE `user_menu` (
  `idMenu` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `href` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `orderId` int(11) NOT NULL,
  `accessId` int(11) NOT NULL,
  `has_chevron` enum('yes','no') NOT NULL,
  `datecreate` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMenu`),
  KEY `FK_user_menud` (`accessId`),
  CONSTRAINT `FK_user_menud` FOREIGN KEY (`accessId`) REFERENCES `user_access` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

/*Data for the table `user_menu` */

insert  into `user_menu`(`idMenu`,`parentId`,`name`,`href`,`title`,`icon`,`orderId`,`accessId`,`has_chevron`,`datecreate`,`lastupdate`,`status`) values (1,0,'Пешрафт','#','test 1','fa fa-files-o text-green',1,1,'yes','0000-00-00 00:00:00','2016-02-16 13:40:40',1),(2,1,'Умумӣ','site/dashed','test 2','fa fa-files-o text-green',3,2,'no','2015-09-15 12:29:26','2015-09-15 12:29:26',1),(3,1,'Нимослаи ҳозира','site/current','test 3','fa fa-files-o text-red',1,1,'no','2015-09-15 12:29:27','2015-09-15 12:29:27',1),(4,0,'Нимослаи гузашта','site/last','test 4','fa fa-circle-o text-aqua',2,1,'no','2015-09-15 12:29:27','2015-09-15 12:29:27',0),(5,0,'Давомот','#','test 5','fa fa-random text-green',5,1,'yes','2015-09-15 12:29:28','2015-09-15 12:29:28',1),(6,5,'Нимослаи ҳозира','site/discurr','test 6','fa fa-random text-red',1,1,'no','2015-09-15 12:29:28','2015-09-15 12:29:28',1),(7,5,'Умуми','site/dislast','test 7','fa fa-random text-aqua',2,2,'no','2015-09-15 12:29:29','2015-09-15 12:29:29',1),(8,0,'Маводи имтиҳонот','site/test8','Маводи имтиҳонот','fa fa-book text-green',8,1,'no','2015-09-15 12:29:29','2015-09-15 12:29:29',1),(12,0,'Дарсҳо','teacher/study','Дарсҳо','fa fa-file-text-o text-green',1,2,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(13,0,'Ғоибҳо','nozir/ruihat','Ғоибҳо','fa fa-calendar-o text-red',2,3,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(14,0,'Гурӯҳҳо','nozir/students','Донишҷӯён','fa fa-user text-yellow',1,3,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(15,13,'Нимсолаи ҳозира','nozir/ruihat','Нимсолаи ҳозира','fa fa-calendar-check-o text-aqua',1,3,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',0),(16,13,'Нимсолаҳои гузашта','nozir/ruihatAll','Нимсолаҳои гузашта','fa fa-calendar-minus-o text-red',2,3,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',0),(17,0,'Ғоибҳо','teacher/darsho','Ғоибҳо','fa fa-calendar-o text-red',3,2,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(18,0,'Руйхати дарсҳо','#','Руйхати дарсҳо','fa fa-calendar-minus-o text-green',1,7,'yes','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(19,0,'Руйхати муаллимон','shed/muallimon','Руйхати муаллимон','fa fa-book text-green',2,5,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(20,0,'Руйхати фаннҳо','shed/fann','Руйхати фаннҳо','fa fa-book text-green',3,7,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(21,18,'Ҳозира','shed/jadval','Ҳозира','fa fa-unlock text-green',1,7,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(22,18,'Гузашта','shed/jadvaliguzawta','Гузашта','fa fa-unlock-alt text-red',2,7,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(23,0,'Фаннҳо','shed/fannho','Фаннҳо','fa fa-book text-green',3,7,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(24,0,'Ҷадвали дарси','shed/jadval','Ҷадвали дарси','fa fa-book text-red',2,5,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(25,0,'Ҷадвали ғоибҳо','nozir/ruihat','Ҷадвали ғоибҳо','fa fa-book text-red',3,5,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(26,0,'Қарздориҳо','nozir/karzho','Қарздориҳо','fa fa-money text-green',4,3,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(27,0,'Транскрипт','site/transcript','Транскрипт','fa fa-files-o text-green',5,1,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(28,0,'Қайди довталаб','kaid/dovtalab','Қайди довталаб','fa fa-user-plus text-yellow',1,4,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(29,0,'Руйхати довталабон','kaid/ruihat','Руйхати довталабон','fa fa-users text-blue',2,4,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(30,0,'Ташкили гурӯҳ','kaid/group','Ташкили гурӯҳ','fa fa-user text-green',3,4,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(31,0,'Руйхати гурӯҳ','kaid/listgroup','Руйхати гурӯҳ','fa fa-list-alt text-red',4,4,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(32,0,'Руйхати кафедрахо','shed/cafedra','Руйхати кафедрахо','fa fa-cc text-green',4,7,'no','0000-00-00 00:00:00','2016-02-16 13:21:53',1),(33,0,'Руйхати секторҳо','shed/sector','Руйхати секторҳо','fa fa-cc text-yellow',5,7,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(34,0,'Кор бо қарздорон','teacher/karz','Кор бо қарздорон','fa fa-cc text-green',3,2,'no','0000-00-00 00:00:00','0000-00-00 00:00:00',1),(35,0,'Менюи истифодабаранда','admin/panel','Меню для пользователей','fa fa-cogs text-red',4,8,'no','0000-00-00 00:00:00','2016-04-08 08:27:16',1),(36,0,'Истифодабарандагон','admin/students','Студенты','fa fa-user text-green',1,8,'no','2016-02-17 09:28:13','2016-02-17 10:02:50',1),(37,0,'Руйхати Дарсҳо','admin/shedual','Руйхати Дарсҳо','fa fa-book text-green',3,8,'no','2016-02-22 16:16:05','2016-02-29 07:05:37',1),(41,0,'Ихтисосҳо','kaid/ihtisos','Ихтисосҳо','fa fa-gear text-red',5,4,'no','2016-02-25 14:31:43','2016-04-14 15:47:43',1),(42,0,'Қайди истифодабаранда','admin/ucreate','Қайди истифодабаранда','fa fa-user-plus text-green',2,8,'no','2016-02-29 02:39:47','2016-04-08 08:27:03',1)