<?php

return array(
    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'bizRule' => null,
        'data' => null
    ),
    '1' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Student',
        'bizRule' => null,
        'data' => null
    ),
    '2' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Teacher',
        'children' => array(
            '1',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '3' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Nozir',
        'children' => array(
            '2',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '4' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Kaid',
        'children' => array(
            '3',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '5' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Director',
        'children' => array(
            '4',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '6' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Zam.Director',
        'children' => array(
            '5',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '7' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Sarmu',
        'children' => array(
            '6',          // позволим модератору всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '8' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            '7',         // позволим админу всё, что позволено модератору
        ),
        'bizRule' => null,
        'data' => null
    ),
    '9' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Buhgalteriya',
        'children' => array(
            '7',         // позволим админу всё, что позволено модератору
        ),
        'bizRule' => null,
        'data' => null
    ),
    '10' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Buhgalteriya',
        'children' => array(
            '7',         // позволим админу всё, что позволено модератору
        ),
        'bizRule' => null,
        'data' => null
    ),
    '11' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Buhgalteriya',
        'children' => array(
            '7',         // позволим админу всё, что позволено модератору
        ),
        'bizRule' => null,
        'data' => null
    ),
);
?>